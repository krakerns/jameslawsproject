<?php
/**
 * Base Controller
 * 
 * @package   Components
 * @author    Pradesh <pradesh@datacraft.co.za>
 * @copyright 2014 florida.com
 */

/**
 * Controller is the customized base controller class
 *
 * Base controller class for all application controller classes.
 * Application controllers must extend this class :
 * class SomeClass extends Controller
 * 
 * @package Components
 * @version 1.0
 */
class Controller extends CController
{

    public function init() {
        // filter out garbage requests
        $uri = Yii::app()->request->requestUri;
        if (strpos($uri, 'favicon') || strpos($uri, 'robots'))
            Yii::app()->end();

    }

    /**
     * @var string the default layout for the controller view.
     * @access public
     * 
     *      Defaults to '//layouts/column1'.
     *      See 'protected|themes/views/layouts/column1.php'
     */
    public $layout = '//layouts/page';
    
    /**
     *
     * @var array context menu items.
     * @access public
     * 
     * This property will be assigned to {@link CMenu::items}.
     * 
     */
    public $menu = array();

    /**
     *
     * @var array the breadcrumbs of the current page.
     * @access public
     * 
     * The value of this property will be assigned to {@link CBreadcrumbs::links}.
     * 
     */
    public $breadcrumbs = array();

    public function filters()
    {
        // All backend classes must be subjected to access control rules
        return array(
            'accessControl'
        );
    }

    /**
     * Override CController access rules and provide base rules for derived class.
     * All derived classes will automatically inherit the access rules provided.
     *
     * @param <none> <none>
     *
     * @return array list of accessrules to apply
     * @access public
     */
    public function accessRules()
    {
        return array(
            // Allow all users the login method, so that they are not locked out.
            array(
                'allow',
                'users' => array(
                    '*'
                ),
                'actions' => array(
                    'login'
                )
            ),
            // All actions requires authentication.
            array(
                'allow',
                'users' => array(
                    '@'
                )
            ),
            // No one else gets access to any actions
            array(
                'deny',
                'users' => array(
                    '*'
                )
            )
        );
    }
}