<?php
/**
 * Created by PhpStorm.
 * User: JV
 * Date: 7/17/14
 * Time: 9:07 PM
 */

function leading_zeros($value, $places){
// Function written by Marcus L. Griswold (vujsa)
// Can be found at http://www.handyphp.com
// Do not remove this header!
    $leading= "";
    if(is_numeric($value)){
        for($x = 1; $x <= $places; $x++){
            $ceiling = pow(10, $x);
            if($value < $ceiling){
                $zeros = $places - $x;
                for($y = 1; $y <= $zeros; $y++){
                    $leading .= "0";
                }
                $x = $places + 1;
            }
        }
        $output = $leading . $value;
    }
    else{
        $output = $value;
    }
    return $output;
}

function formatDollar($total){
    return "$ ".number_format(sprintf('%0.2f', preg_replace("/[^0-9.]/", "", $total)),2);
}

?>