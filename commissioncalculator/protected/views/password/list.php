<?php
$data_url = Yii::app()->createUrl('users/listjson');
$edit_url = Yii::app()->createUrl('password/edit');
$delete_url = Yii::app()->createUrl('password/delete');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers"

        });

        $("ul.nav li").removeClass('active');
        $('#lipassword').addClass('active');

    });

    function deleteItem(passwordid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "passwordid": passwordid },
                dataType: 'json',
                url: '<?php echo CController::createURL("password/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("password/list"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Passwords &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('password/create'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="30%">Title</th>
                <th width="30%">Description</th>
                <th width="10%">&nbsp;</th>
                <th width="10%">&nbsp;</th>
            </tr>
            </thead>

            <?php
            if(count($listpassword>0)){
                echo '<tbody>';
                foreach($listpassword as $password){
                    echo '<tr>';
                    echo '<td><a href="http://'.$password->url.'" target="_blank">'.$password->title.'</a></td>';
                    echo '<td>'.$password->description.'</td>';
                    echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/pid/'.$password->id.'">Edit</a></td>';
                    echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$password->id.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
</section>