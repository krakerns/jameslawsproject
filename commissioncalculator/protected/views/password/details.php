<script type="text/javascript">
    $(document).ready(function(){
        $("ul.nav li").removeClass('active');
        $('#lipassword').addClass('active');
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <div class="form">

            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'passwordmngr-sad-form',
                'htmlOptions'=>array('class'=>'form-horizontal'),
                // Please note: When you enable ajax validation, make sure the corresponding
                // controller action is handling ajax validation correctly.
                // See class documentation of CActiveForm for details on this,
                // you need to use the performAjaxValidation()-method described there.
                'enableAjaxValidation'=>false,
            )); ?>


            <?php echo $form->errorSummary($model); ?>
            <section class="panel">
                <header class="panel-heading font-bold"><h4>User form</h4></header>
                <div class="panel-body">


                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'title',array('class'=>"col-sm-2 control-label")); ?>
                            <div class="col-sm-4">
                                <?php echo $form->textField($model,'title',array('class'=>"form-control")); ?>
                                <?php echo $form->error($model,'title'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'url',array('class'=>"col-sm-2 control-label")); ?>
                            <div class="col-sm-4">
                                <?php echo $form->textField($model,'url',array('class'=>"form-control")); ?>
                                <?php echo $form->error($model,'url'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'description',array('class'=>"col-sm-2 control-label")); ?>
                            <div class="col-sm-4">
                                <?php echo $form->textField($model,'description',array('class'=>"form-control")); ?>
                                <?php echo $form->error($model,'description'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'username',array('class'=>"col-sm-2 control-label")); ?>
                            <div class="col-sm-4">
                                <?php echo $form->textField($model,'username',array('class'=>"form-control")); ?>
                                <?php echo $form->error($model,'username'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'password',array('class'=>"col-sm-2 control-label")); ?>
                            <div class="col-sm-4">
                                <?php echo $form->textField($model,'password',array('class'=>"form-control")); ?>
                                <?php echo $form->error($model,'password'); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <section class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-white" type="submit">Cancel</button>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>

                </div>
            </section>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
    </div>
</div>