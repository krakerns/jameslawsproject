<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>James Law | log-in</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugin.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/app.css" type="text/css" />
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/respond.min.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/html5.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/fix.js" cache="false"></script>


<!-- Bootstrap -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.js"></script>
    <!-- app -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.plugin.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.data.js"></script>
    <![endif]-->
</head>
<body>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">
    <?php echo $content; ?>
</section>
<!-- footer -->
<footer id="footer">
    <div class="text-center padder clearfix">
        <p>

        </p>
    </div>
</footer>
<!-- / footer -->

</body>
</html>