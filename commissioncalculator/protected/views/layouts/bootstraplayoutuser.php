<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Commission Calculator</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font.css" type="text/css" cache="false" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/select2/select2.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/fuelux/fuelux.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/datepicker/datepicker.css" type="text/css" />

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/datatables/datatables.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/slider/slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/plugin.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/app.css" type="text/css" />
    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/respond.min.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/html5.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ie/fix.js" cache="false"></script>
    <![endif]-->

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.js"></script>
    <!-- app -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.plugin.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/app.data.js"></script>
    <!-- fuelux -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/fuelux/fuelux.js"></script>
    <!-- datepicker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/datepicker/bootstrap-datepicker.js"></script>
    <!-- slider -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/slider/bootstrap-slider.js"></script>
    <!-- file input -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/file-input/bootstrap.file-input.js"></script>
    <!-- combodate -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/moment.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/combodate/combodate.js" cache="false"></script>
    <!-- parsley -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/parsley/parsley.min.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/parsley/parsley.extend.js" cache="false"></script>
    <!-- select2 -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/select2/select2.min.js" cache="false"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/ajaxfileupload.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.seluser').select2({'width':'100%'});
        });
    </script>

    <style>
        #fn_layer8 {
            display: none !important;
        }
    </style>

</head>
<body>
<section class="hbox stretch">
<!-- .aside -->
<aside class="bg-dark aside-sm" id="nav">
    <section class="vbox">
        <header class="dker nav-bar">
            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
                <i class="fa fa-bars"></i>
            </a>
            <a href="#" class="nav-brand" data-toggle="fullscreen">&nbsp;</a>
            <a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-user">
                <i class="fa fa-comment-o"></i>
            </a>
        </header>
        <section>
            <div class="lter nav-user hidden-xs pos-rlt">
                <div class="nav-avatar pos-rlt">
                    <a href="#" class="thumb-sm avatar animated rollIn" data-toggle="dropdown">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar.jpg" alt="" class="">
                        <span class="caret caret-white"></span>
                    </a>
                    <ul class="dropdown-menu m-t-sm animated fadeInLeft">
                        <span class="arrow top"></span>

                        <li>
                            <a href="docs.html">Help</a>
                        </li>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>">Logout</a>
                        </li>
                    </ul>
                    <div class="visible-xs m-t m-b">
                        <a href="#" class="h3">John.Smith</a>
                        <p><i class="fa fa-map-marker"></i> London, UK</p>
                    </div>
                </div>
                <div class="nav-msg">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <b class="badge badge-white count-n">2</b>
                    </a>
                    <section class="dropdown-menu m-l-sm pull-left animated fadeInRight">
                        <div class="arrow left"></div>
                        <section class="panel bg-white">
                            <header class="panel-heading">
                                <strong>You have <span class="count-n">2</span> notifications</strong>
                            </header>
                            <div class="list-group">
                                <a href="#" class="media list-group-item">
                      <span class="pull-left thumb-sm">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/avatar.jpg" alt="John said" class="img-circle">
                      </span>
                      <span class="media-body block m-b-none">
                        Use awesome animate.css<br>
                        <small class="text-muted">28 Aug 13</small>
                      </span>
                                </a>
                                <a href="#" class="media list-group-item">
                      <span class="media-body block m-b-none">
                        1.0 initial released<br>
                        <small class="text-muted">27 Aug 13</small>
                      </span>
                                </a>
                            </div>
                            <footer class="panel-footer text-sm">
                                <a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
                                <a href="#">See all the notifications</a>
                            </footer>
                        </section>
                    </section>
                </div>
            </div>
            <nav class="nav-primary hidden-xs">
                <ul class="nav">

                    <li style="display:none;" id="liuser">
                        <a href="<?php echo Yii::app()->createUrl('/users/edit/userid/' . Yii::app()->user->id); ?>">
                            <i class="fa fa-user"></i>
                            <span>Profile</span>
                        </a>
                    </li>

                    <li id="litransaction"  class="dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tasks"></i>
                            <span>Transaction Reports</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/transaction/transactionlist'); ?>">Transaction Reports List</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl('/transaction/addtransactionreport'); ?>">New Transaction Report</a>
                            </li>
                            <li class="divider"></li>
                            <li style="display:none;">
                                <a href="<?php echo Yii::app()->createUrl('/transaction/spreadsheetlist'); ?>">Commission Calculation List</a>
                            </li>
                        </ul>
                    </li>

                    <li id="liorders">
                        <a href="<?php echo Yii::app()->createUrl('/order/list'); ?>">
                            <i class="fa fa-eye"></i>
                            <span>Sign-Orders</span>
                        </a>
                    </li>

                    <li id="lipassword">
                        <a href="<?php echo Yii::app()->createUrl('/password/list'); ?>">
                            <i class="fa fa-eye"></i>
                            <span>Password Manager</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </section>
        <footer class="footer bg-gradient hidden-xs">
            <a href="modal.lockme.html" data-toggle="ajaxModal" class="btn btn-sm btn-link m-r-n-xs pull-right">
                <i class="fa fa-power-off"></i>
            </a>
            <a href="#nav" data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm">
                <i class="fa fa-bars"></i>
            </a>
        </footer>
    </section>
</aside>
<!-- /.aside -->
<!-- .vbox -->
<section id="content">
    <section class="vbox">
        <header class="header bg-dark lter">
            <p></p>
        </header>
        <section class="scrollable wrapper">

            <?php echo $content; ?>

        </section>
    </section>
    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen" data-target="#nav"></a>
</section>
<!-- /.vbox -->
</section>

</body>
</html>