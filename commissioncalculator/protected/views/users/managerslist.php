<?php
$data_url = Yii::app()->createUrl('users/listjson');
$edit_url = Yii::app()->createUrl('users/editmanager');
$delete_url = Yii::app()->createUrl('users/deletemanager');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",

        });

        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

    });

    function deleteItem(managerid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "managerid": managerid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/deletemanager"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/managerslist"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Managers List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('users/createmanager'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="30%">Manager</th>
                <th width="30%">Managed User</th>
                <th width="20%">Share</th>
                <th width="10%">&nbsp;</th>
                <th width="10%">&nbsp;</th>
            </tr>
            </thead>

            <?php
            if(count($listManagers>0)){
                echo '<tbody>';
                foreach($listManagers as $manager){
                    echo '<tr>';
                    echo '<td>'.$manager->user->firstname. ' ' . $manager->user->lastname.'</td>';
                    echo '<td>'.$manager->usertomanage0->firstname. ' ' . $manager->usertomanage0->lastname.'</td>';
                    echo '<td>'.$manager->shareonuser.'</td>';
                    echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/managerid/'.$manager->managerid.'">Edit</a></td>';
                    echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$manager->managerid.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
</section>