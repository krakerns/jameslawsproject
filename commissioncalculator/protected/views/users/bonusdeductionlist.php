<?php
$data_url = Yii::app()->createUrl('users/listjson');
$edit_url = Yii::app()->createUrl('users/viewbonusdeduction');
$delete_url = Yii::app()->createUrl('users/deletebonusdeduction');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",

        });

        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

    });

    function deleteItem(bonusdeductionid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "bonusdeductionid": bonusdeductionid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/deletebonusdeduction"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/bonusdeductionlist"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Bonus And Deduction List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('users/createbonusdeduction'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="15%">User</th>
                <th width="15%">Amount</th>
                <th width="35%">Description</th>
                <th width="15%">GST</th>
                <th width="10%">&nbsp;</th>
                <th width="10%">&nbsp;</th>
            </tr>
            </thead>

            <?php
            if(count($listBonusdeduction>0)){
                echo '<tbody>';
                foreach($listBonusdeduction as $bonusdeduction){
                    echo '<tr>';
                    echo '<td>'.$bonusdeduction->user->firstname. ' ' . $bonusdeduction->user->lastname.'</td>';
                    echo '<td>'.$bonusdeduction->amount.'</td>';
                    echo '<td>'.$bonusdeduction->description.'</td>';
                    echo '<td>'.$bonusdeduction->gst.'</td>';
                    echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/bonusdeductionid/'.$bonusdeduction->bonusdeductionid.'">View Details</a></td>';
                    echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$bonusdeduction->bonusdeductionid.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
</section>