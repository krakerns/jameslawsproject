<script type="text/javascript">
    $(document).ready(function(){
        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');
    });
</script>

<div class="row">


    <div class="col-sm-12">



        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'users-sas-form',
            'htmlOptions'=>array(
                'class'=>'form-horizontal'
            ),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
        )); ?>


        <?php echo $form->errorSummary($model); ?>
      <section class="panel">
        <header class="panel-heading font-bold"><h4>User form</h4></header>
        <div class="panel-body">


                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-5">
                        <?php echo $form->textField($model,'firstname',array('class'=>"form-control",'placeholder'=>'First Name')); ?>
                    </div>

                    <div class="col-sm-5">
                        <?php echo $form->textField($model,'lastname',array('class'=>"form-control",'placeholder'=>'Last Name')); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">REAA number</label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'reeanumber',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">IRD Number</label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'irdnumber',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Branch Name </label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'companyname',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Branch Email </label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'companyemail',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Personal Email </label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'personalemail',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Mobile Phone Number </label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'mobilephonenumber',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">User Share</label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'usershare',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">With Holding Tax</label>
                    <div class="col-sm-10">
                        <?php echo $form->textField($model,'withholdingtax',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>


        </div>
      </section>

      <section class="panel" style="display:none;">
          <header class="panel-heading font-bold">
              <h4>Managers&nbsp;&nbsp;<a id="add" name="add" href="#dvprofshow" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a><a id="edit" name="edit" href="#dvworkshow" data-toggle="modal" class="btn btn-success btn-sm" style="display:none;"></a></h4>
          </header>

          <div class="table-responsive">
              <table class="table table-striped m-b-none" id="tblProfUploads">
                  <thead>
                  <tr>
                      <th width="20">
                          <input type="checkbox">
                      </th>
                      <th>Manager Name</th>
                      <th>Share</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                      <td><input type="checkbox" value="2" name="post[]"></td>
                      <td>Michelle Kennedy</td>
                      <td>20%</td>
                  </tr>

                  <tr>
                      <td><input type="checkbox" value="2" name="post[]"></td>
                      <td>Paul Ko</td>
                      <td>20%</td>
                  </tr>
                  </tbody>
              </table>
          </div>

          <footer class="panel-footer">
              <div class="row">
                  <div class="col-sm-4 hidden-xs">
                      <select class="input-sm form-control input-s-sm inline">
                          <option value="1">Delete selected</option>
                      </select>
                      <button class="btn btn-sm btn-white">Apply</button>
                  </div>
              </div>
          </footer>

      </section>

    <section class="panel">
        <div class="panel-body">
            <div class="form-group">
                <div class="col-sm-4">
                    <button class="btn btn-white" type="submit">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>
                </div>
            </div>

        </div>
    </section>


    </div>

 <?php $this->endWidget(); ?>

  </div>