<script type="text/javascript">
    $(document).ready(function(){
        $('#seluser').select2({'width':'100%'});
        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');
    });

</script>

<div class="row">
    <div class="col-sm-12">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'useraccounts-sda-form',
            'htmlOptions'=>array('class'=>'form-horizontal'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
        )); ?>

        <section class="panel">
            <header class="panel-heading font-bold"><h4>Edit User Account</h4></header>
            <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">User</label>
                        <div class="col-sm-5">
                            <?php echo $form->dropDownList($model,'userid', CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('class'=>"",'id'=>'seluser')); ?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Account Type</label>
                        <div class="col-sm-5">
                            <?php echo $form->dropDownList($model,
                                'accounttype',
                                $model->listType(),
                                array('prompt'=>'Select Type','class'=>"form-control"));?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">User Name</label>
                        <div class="col-sm-5">
                            <?php echo $form->textField($model,'username',array('class'=>"form-control")); ?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-5">
                            <?php echo $form->passwordField($model,'password',array('class'=>"form-control")); ?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

            </div>
        </section>



        <section class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>

            </div>
        </section>

    <?php $this->endWidget(); ?>


    </div>

</div>