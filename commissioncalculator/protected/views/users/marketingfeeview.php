<?php
$salesperson = "";
foreach($model->marketingFeeSalespeople as $person){
    $salesperson .= $person->user->fullname . ',';
}
?>

<script type="text/javascript">
    $(document).ready(function(){


        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

        $('#txtDate').datepicker();
    });

    function deleteItem(paymentid){
        var result = window.confirm("Do you really want to Delete the payment?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "paymentid": paymentid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/Deletepayment"); ?>',
                success: function (data) {

                    if (data.result == 'success') {

                        window.locations = '<?php echo CController::createURL("users/Editmarketingfee/marketingid/" . $model->id); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }

    function showadd(){

        $('#txtDescription').val('');
        $('#txtSupplier').val('');
        $('#txtAmount').val('0.00');
        $('#txtDate').val('<?php echo date('d-m-Y'); ?>');
        $('#hdisedit').val('0');
        $('#hdpaymentid').val('0');
        $('#btnPopedit').click();
    }

    function showpopup(id){
        $.ajax({
            type: 'POST',
            url: '<?php echo CController::createURL('users/Getpaymentdetails'); ?>',
            data: {paymentid : id},
            success: function(data){
                var data=$.parseJSON(data);

                $('#txtDescription').val(data.description);
                $('#txtSupplier').val(data.supplier);
                $('#txtAmount').val(data.amount);
                $('#txtDate').val(data.datepaid);
                $('#hdisedit').val('1');
                $('#hdpaymentid').val(data.id);

                $('#btnPopedit').click();
            }
        });
    }
</script>

<style type="text/css">
    .datepicker {
        z-index:99999 !important;
    }
</style>

<div class="row">
    <div class="col-sm-12">



        <section class="panel">
            <header class="panel-heading font-bold"><h4>Marketing Fee</h4></header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left !important;">Property : <?php echo $model->property; ?></label>


                    <label class="col-sm-4 control-label"  style="text-align: left !important;">Invoice : <?php echo $model->invoice; ?></label>

                    <label class="col-sm-4 control-label"  style="text-align: left !important;">Sales Persons : <?php echo $salesperson; ?></label>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left !important;">Fund Received : <?php echo date('d-m-Y',strtotime($model->fund_received)); ?></label>


                    <label class="col-sm-4 control-label"  style="text-align: left !important;">Description : <?php echo $model->description; ?></label>

                    <label class="col-sm-4 control-label"  style="text-align: left !important;">Amount : $ <?php echo number_format($model->initial_amount,2); ?></label>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

            </div>
            <button type="button" id="btnPopedit" style="display:none;" class="btn btn-primary"  data-toggle="modal" data-target="#myModal">Add Payment</button>
            <header class="panel-heading font-bold"><h4>Payment Details &nbsp;&nbsp;<button type="button" id="btnPop" class="btn btn-primary" onclick="showadd();">Add Payment</button></h4></header>
            <div class="panel-body">

                <table id="tbl_bonus" class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th width="20%">Supplier</th>
                        <th width="20%">Particular</th>
                        <th width="20%">Date</th>
                        <th width="20%" style="text-align: right">Amount Incl. GST</th>
                        <th width="10%">&nbsp;</th>
                        <th width="10%">&nbsp;</th>
                    </tr>
                    </thead>

                    <?php
                    $total = 0;
                    $edit_url='';
                    if(count($model->marketingFeePayments>0)){
                        echo '<tbody>';
                        foreach($model->marketingFeePayments as $fee){
                            echo '<tr>';
                            echo '<td>'.$fee->supplier.'</td>';
                            echo '<td>'.$fee->description.'</td>';
                            echo '<td>'.date('d/m/Y',strtotime($fee->date_paid)).'</td>';
                            echo '<td style="text-align:right;">'.$fee->amount.'</td>';
                            echo '<td><a class="editrow btn btn-info btn-xs" href="javascript:void(0);" onclick="showpopup('.$fee->id.');">Edit</a></td>';
                            echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$fee->id.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                            echo '</tr>';

                            $total+= $fee->amount;
                        }
                        echo '</tbody>';
                    }
                    ?>

                    <tr class="">
                        <td colspan="2" align="right"><span class="h1">Total Payments :</span></td>
                        <td  align="right" style="vertical-align: bottom;"><span class="font-bold"> - <?php echo formatDollar($total); ?></span></td>


                    </tr>


                </table>

            </div>
        </section>
        <form method="post" action="<?php echo CController::createURL("users/Savemarketingsales"); ?>" class="form-horizontal">

            <?php $amount = ($model->initial_amount - $total) / count($model->marketingFeeSalespeople); ?>
            <input type="hidden" id="hdmarketingid" name="hdmarketingid" value="<?php echo $model->id; ?>"/>
        <section class="panel">
            <header class="panel-heading font-bold"><h4>Deduct From</h4></header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Balance </label>
                    <div class="col-sm-3">

                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" id="txtBalance" name="txtBalance" readonly="readonly" value="<?php echo number_format($amount,2); ?>" class="form-control" placeholder="Date">
                        </div>

                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>


                    <?php

                    $counter = 1;
                    foreach($model->marketingFeeSalespeople as $person){

                        $gst = $amount / 1.15;

                        if($person->issaved== '1'){
                            $amount = $person->amount;
                            $gst = $person->gst;
                        }

                        ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $person->user->fullname; ?> </label>
                            <div class="col-sm-3">
                                <input type="hidden" id="hdsalesid<?php echo $counter; ?>" name="hdsalesid<?php echo $counter; ?>" value="<?php echo $person->id; ?>" />
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" id="txtSalesAmt<?php echo $counter; ?>" name="txtSalesAmt<?php echo $counter; ?>"  value="<?php echo number_format($amount,2); ?>" class="form-control" placeholder="Date">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <input type="text" id="txtGST<?php echo $counter; ?>" name="txtGST<?php echo $counter; ?>"  value="<?php echo number_format($gst,2); ?>" class="form-control" placeholder="Date">
                                </div>
                            </div>

                            <div class="col-sm-1">
                                Excl GST
                            </div>
                        </div>
                        <div class="line line-dashed line-lg pull-in"></div>
                        <?php
                        $counter++;
                    }

                    ?>
                    <input type="hidden" id="hdsalescount" name="hdsalescount" value="<?php echo $counter -1; ?>" />

            </div>

            <div class="form-group">
                <label class="col-sm-7 control-label">&nbsp; </label>
                <div class="col-sm-3">

                    <button class="btn btn-white" type="submit">Cancel</button>
                    <button class="btn btn-primary" type="submit">Save changes</button>

                </div>
            </div>
        </section>
        </form>


        <section class="panel" style="display:none;">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>

            </div>
        </section>



    </div>

</div>

<div id="myModal" class="modal" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="fmrValidate" name="frmValidate" action="<?php echo CController::createURL("users/Savemarketingfeepayment"); ?>" method="post" data-validate="parsley">
                <div class="modal-body scrollable">


                        <input type="hidden" id="hdmarketingfeeid" name="hdmarketingfeeid" value="<?php echo $model->id; ?>" />
                        <input type="hidden" id="hdisedit" name="hdisedit" value="0" />
                        <input type="hidden" id="hdpaymentid" name="hdpaymentid" value="0" />
                        <section class="panel">
                            <header class="panel-heading">
                                <span class="h4">Add Payment</span>
                            </header>
                            <div class="panel-body">

                                <div class="form-group pull-in clearfix">
                                    <div class="col-sm-12">
                                        <label>Date</label>
                                        <input type="text" id="txtDate" name="txtDate" value="<?php echo date('d-m-Y'); ?>" class="form-control" placeholder="Date">
                                    </div>
                                </div>

                                <div class="form-group pull-in clearfix">
                                    <div class="col-sm-12">
                                        <label>Supplier</label>
                                        <input type="text" id="txtSupplier" name="txtSupplier"  class="form-control">
                                    </div>
                                </div>

                                <div class="form-group pull-in clearfix">
                                    <div class="col-sm-12">
                                        <label>Particular</label>
                                        <input type="text" id="txtDescription" name="txtDescription"  class="form-control">
                                    </div>
                                </div>

                                <div class="form-group pull-in clearfix">
                                    <div class="col-sm-12">
                                        <label>Amount</label>

                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" id="txtAmount" name="txtAmount" value="0"  class="form-control">
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit Payment</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>