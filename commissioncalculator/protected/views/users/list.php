<?php
    $data_url = Yii::app()->createUrl('users/listjson');
    $edit_url = Yii::app()->createUrl('users/edit');
    $delete_url = Yii::app()->createUrl('users/delete');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",

        });

    });

    function deleteItem(userid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "userid": userid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/index"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Users List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('users/create'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="15%">REAA number</th>
                <th width="15%">IRD Number</th>
                <th width="20%"> Company Name</th>
                <th width="15%">&nbsp;</th>
                <th width="15%">&nbsp;</th>
            </tr>
            </thead>

            <?php
                if(count($listUsers>0)){
                    echo '<tbody>';
                    foreach($listUsers as $user){
                        echo '<tr>';
                        echo '<td>'.$user->firstname. ' ' . $user->lastname.'</td>';
                        echo '<td>'.$user->reeanumber.'</td>';
                        echo '<td>'.$user->irdnumber.'</td>';
                        echo '<td>'.$user->companyname.'</td>';
                        echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/userid/'.$user->userid.'">Edit</a></td>';
                        echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$user->userid.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                        echo '</tr>';
                    }
                    echo '</tbody>';
                }
            ?>


        </table>
    </div>
</section>