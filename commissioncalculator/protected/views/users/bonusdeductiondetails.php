<script type="text/javascript">
    $(document).ready(function(){

       <?php
        if(isset($userid)){
           ?>
            $('.seluser').val(<?php echo $userid; ?>);
        <?php
        }
       ?>
        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');
    });
</script>

<div class="row">
    <div class="col-sm-12">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'bonusdeductions-asd-form',
            'htmlOptions'=>array('class'=>'form-horizontal'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
        )); ?>

        <section class="panel">
            <header class="panel-heading font-bold"><h4>Edit Bonus and Deductions</h4></header>
            <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">User</label>
                        <div class="col-sm-5">
                            <?php echo CHtml::dropDownList('Bonusdeductions[userid]',$userid, CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('prompt'=>'Select Salesperson','class'=>"seluser")); ?>

                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-5">
                            <input type="text" id="txtDate" data-date-format="dd-mm-yyyy" class=" datepicker-input form-control"  name="txtDate">
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Deduction excl GST</label>
                        <div class="col-sm-5">
                            <?php echo $form->textField($model,'amount',array('class'=>"form-control")); ?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-5">
                            <?php echo $form->textField($model,'description',array('class'=>"form-control")); ?>
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Inclusive GST</label>
                    <div class="col-sm-5">
                        <?php echo $form->textField($model,'gst',array('class'=>"form-control")); ?>
                    </div>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

            </div>
        </section>



        <section class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>

            </div>
        </section>

    <?php $this->endWidget(); ?>

    </div>

</div>