<?php
$data_url = Yii::app()->createUrl('users/listjson');
$edit_url = Yii::app()->createUrl('users/editmarketingfee');
$delete_url = Yii::app()->createUrl('users/deletemarketingfee');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers"

        });

        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

    });

    function deleteItem(marketingid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "marketingid": marketingid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/deletemarketingfee"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/marketingfeelist"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Marketing Fee List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('users/createmarketingfee'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="20%">Sales Person</th>
                <th width="20%">Date</th>
                <th width="20%">Property</th>
                <th width="20%">Balance</th>
                <th width="10%">&nbsp;</th>
                <th width="10%">&nbsp;</th>
            </tr>
            </thead>

            <?php
            if(count($listMarketing>0)){
                echo '<tbody>';
                foreach($listMarketing as $marketing){

                    $salesperson = "";
                    foreach($marketing->marketingFeeSalespeople as $person){
                        $salesperson .= $person->user->fullname . ',';
                    }
                    $totalpayments = 0;
                    foreach($marketing->marketingFeePayments as $payment){
                        $totalpayments += $payment->amount . ',';
                    }
                    $balance = $marketing->initial_amount - $totalpayments;
                    echo '<tr>';
                    echo '<td>'.$salesperson.'</td>';
                    echo '<td>'.date('d-m-Y',strtotime($marketing->fund_received)).'</td>';
                    echo '<td>'.$marketing->property.'</td>';
                    echo '<td> $ '.number_format($balance,2).'</td>';
                    echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/marketingid/'.$marketing->id.'">Edit</a></td>';
                    echo '<td><a href="javascript:void(0);" onclick="deleteItem('.$marketing->id.');" class="deleterow btn btn-danger btn-xs">Delete</a></td>';
                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
</section>