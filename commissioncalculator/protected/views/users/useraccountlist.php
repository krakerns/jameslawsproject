<?php
    $edit_url = Yii::app()->createUrl('users/edituseraccounts');
?>


<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",

        });

        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

    });

    function deleteItem(userid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "userid": userid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/index"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }


</script>


<section class="panel">
    <header class="panel-heading">
        <h4>Users List &nbsp;&nbsp;<a id="add" style="display:none;" name="add" href="<?php echo Yii::app()->createUrl('users/create'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="20%">Name</th>
                <th width="15%">Username</th>
                <th width="15%">Account Type</th>
                <th width="15%">&nbsp;</th>
            </tr>
            </thead>

            <?php
            if(count($listUseraccounts>0)){
                echo '<tbody>';
                foreach($listUseraccounts as $accounts){
                    echo '<tr>';
                    echo '<td>'.$accounts->user->firstname. ' ' . $accounts->user->lastname.'</td>';
                    echo '<td>'.$accounts->username.'</td>';
                    echo '<td>'.$accounts->accounttype.'</td>';
                    echo '<td><a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/useraccountsid/'.$accounts->useraccountsid.'">Edit</a></td>';

                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
</section>