<script type="text/javascript">
    $(document).ready(function(){

        $('#tbl_bonus').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers"

        });

        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

    });
 </script>

<div class="row">
    <div class="col-sm-12">

        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'bonusdeductions-asd-form',
            'htmlOptions'=>array('class'=>'form-horizontal'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
        )); ?>

        <section class="panel">
            <header class="panel-heading font-bold"><h4>Bonus and Deductions for <?php echo $model->user->fullname;?> &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('users/createbonusdeduction',array('uid'=>$model->userid)); ?>" class="btn btn-success btn-sm">Add Bonus/Deduction</a></h4></header>
            <div class="panel-body">

                <div class="form-group">
                    <label class="col-sm-4 control-label" style="text-align: left !important;">Total Bonus Amount : <?php echo $model->amount; ?></label>


                    <label class="col-sm-4 control-label"  style="text-align: left !important;">Description : <?php echo $model->description; ?></label>

                    <label class="col-sm-4 control-label"  style="text-align: left !important;">GST : <?php echo $model->gst; ?></label>
                </div>
                <div class="line line-dashed line-lg pull-in"></div>

            </div>

            <header class="panel-heading font-bold"><h4>Details</h4></header>
            <div class="panel-body">

                <table id="tbl_bonus" class="table table-striped m-b-none">
                    <thead>
                    <tr>
                        <th width="15%">Date</th>
                        <th width="15%">Amount</th>
                        <th width="35%">Description</th>
                        <th width="15%">GST</th>
                    </tr>
                    </thead>

                    <?php


                    if(count($model->bonusdeductionslines>0)){
                        echo '<tbody>';
                        foreach($model->bonusdeductionslines as $bonusdeduction){
                            echo '<tr>';
                            echo '<td>'.date('d/m/Y',strtotime($bonusdeduction->createdtime)).'</td>';
                            echo '<td>'.$bonusdeduction->amount.'</td>';
                            echo '<td>'.$bonusdeduction->description.'</td>';
                            echo '<td>'.$bonusdeduction->gst.'</td>';
                            echo '</tr>';
                        }
                        echo '</tbody>';
                    }
                    ?>


                </table>

            </div>
        </section>



        <section class="panel" style="display:none;">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>

            </div>
        </section>

        <?php $this->endWidget(); ?>

    </div>

</div>

<script type="text/javascript">

</script>