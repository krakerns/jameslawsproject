<script type="text/javascript">
    $(document).ready(function(){
        $("ul.nav li").removeClass('active');
        $('#liuser').addClass('active');

        $('#btnaddanother').on('click',function(){
            marketing.fn.addPerson();

        });
    });

    var marketing = {};
    marketing.fn = {
        personCount:1,
        addPerson:function(){

            var spantype = $('<span class="relationship" id="dvlistercolleagueopt'+marketing.fn.personCount+'"></span>');

            var $relationship = $('.relationship' );
            var $clone = $("#selPerson").clone();


            $clone[0].id = 'selPerson'+marketing.fn.personCount;
            $clone[0].name = 'selPerson'+marketing.fn.personCount;

            $clone.show();
            $('#dvPerson').append($clone);
            $clone.select2({ "width" : "100%" });

            $('#hdPersonNumber').val(marketing.fn.personCount);
            marketing.fn.personCount++;
        }
    };

</script>

<div class="row">
    <div class="col-sm-12">



        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'marketing-fee-sad-form',
            'htmlOptions'=>array('class'=>'form-horizontal'),
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // See class documentation of CActiveForm for details on this,
            // you need to use the performAjaxValidation()-method described there.
            'enableAjaxValidation'=>false,
        )); ?>

        <?php echo $form->dropDownList($model,'user_id', CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('id'=>'selPerson','class'=>'m-t-sm','prompt'=>'Select Person','style'=>'display:none;')); ?>
        <input type="hidden" id="hdPersonNumber" name="hdPersonNumber" value="0" />
        <section class="panel">
            <header class="panel-heading font-bold"><h4>Add Marketing Fee</h4></header>
            <div class="panel-body">

                <div class="row">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'property',array('class'=>"col-sm-2 control-label")); ?>
                        <div class="col-sm-4">
                            <?php echo $form->textArea($model,'property',array('class'=>"form-control")); ?>
                            <?php echo $form->error($model,'property'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sales Person</label>
                        <div class="col-sm-4">

                            <div id="dvPerson" >

                            </div>
                            <a id="btnaddanother" class="pull-right m-t-sm m-r-sm" href="javascript:void(0);">+Add Another</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Amount</label>
                        <div class="col-sm-4">

                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <?php echo $form->textField($model,'initial_amount',array('class'=>"form-control")); ?>
                            <?php echo $form->error($model,'initial_amount'); ?>
                        </div>


                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'invoice',array('class'=>"col-sm-2 control-label")); ?>
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'invoice',array('class'=>"form-control")); ?>
                            <?php echo $form->error($model,'invoice'); ?>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'fund_received',array('class'=>"col-sm-2 control-label")); ?>
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'fund_received',array('class'=>"form-control datepicker-input",'data-date-format'=>"dd-mm-yyyy")); ?>
                            <?php echo $form->error($model,'fund_received'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <?php echo $form->labelEx($model,'description',array('class'=>"col-sm-2 control-label")); ?>
                        <div class="col-sm-4">
                            <?php echo $form->textField($model,'description',array('class'=>"form-control")); ?>
                            <?php echo $form->error($model,'description'); ?>
                        </div>
                    </div>
                </div>


            </div>
        </section>



        <section class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <button class="btn btn-white" type="submit">Cancel</button>
                        <button class="btn btn-primary" type="submit">Save changes</button>
                    </div>
                </div>

            </div>
        </section>

        <?php $this->endWidget(); ?>

    </div>

</div>

<?php
echo '<script>marketing.fn.addPerson();</script>';
?>