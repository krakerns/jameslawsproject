<?php
$data_url = Yii::app()->createUrl('order/listjson');
$edit_url = Yii::app()->createUrl('order/edit');
$delete_url = Yii::app()->createUrl('order/delete');
?>

<script type="text/javascript">
    $(document).ready(function(){

        $('.table').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [ 0,1,2,3,4,5,6,7,8 ] // <-- gets last column and turns off sorting
                }
            ]
        });

        $("ul.nav li").removeClass('active');
        $('#liorders').addClass('active');

        $('#btnStatus').on('click',function(){
            var result = window.confirm("Do you really want to change the status?");
            if (result == false) {
                e.preventDefault();
                return false;
            }
            else {

                $.ajax({
                    type: 'POST',
                    data: { "id": $('#hdcurrentid').val(),"status": $('#selStatus').val() },
                    dataType: 'json',
                    url: '<?php echo CController::createURL("order/Changestatus"); ?>',
                    success: function (data) {

                        if (data.result == 'success') {

                            window.location = '<?php echo CController::createURL("order/list"); ?>';
                        }
                        else {
                            alert('Failed to Update item - ' + data.message);
                        }

                    },
                    error: function () {
                        alert('Failed to Update item - unexpected error');
                        // todo: add proper error message
                    }
                });
            }
        });
    });

    function deleteItem(id){
        var result = window.confirm("Do you really want to Delete the order?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "id": id },
                dataType: 'json',
                url: '<?php echo CController::createURL("order/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {

                        window.location = '<?php echo CController::createURL("order/list"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }

    function approveItem(id){
        var result = window.confirm("Do you really want to Approve?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "id": id },
                dataType: 'json',
                url: '<?php echo CController::createURL("order/approve"); ?>',
                success: function (data) {

                    if (data.result == 'success') {

                        window.location = '<?php echo CController::createURL("order/list"); ?>';
                    }
                    else {
                        alert('Failed to Approve item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Approve item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }

    function showUpdate(id,status){

        $('#selStatus').val(status);
        $('#hdcurrentid').val(id);
        $('#btnpopup').click();
    }
</script>

<section class="panel">
    <header class="panel-heading">
        <h4>Sign-Order List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo CController::createURL("order/Create"); ?>" class="btn btn-success btn-sm"> New Order</a></h4>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-striped m-b-none" id="tblProfUploads">
                <thead>
                <tr>

                    <th style="width:7%;">ID</th>
                    <th style="width:7%;">Size</th>
                    <th style="width:20%;">Address</th>
                    <th style="width:12%;">Suburb</th>
                    <th style="width:7%;">Type</th>
                    <th style="width:10%;">Installation Date</th>
                    <th style="width:10%;">Status</th>
                    <th style="width:10%;">Approved</th>
                    <th style="width:17%;">Operation</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                        $signs = array();
                        if(Yii::app()->user->role == 'user'){
                            $signs = Signorder::model()->findAll('createdby_id = ' . Yii::app()->user->id);
                        }else{
                            $signs = Signorder::model()->findAll();
                        }

                        $sign = new Signorder();
                        foreach($signs as $sign){
                            echo '<tr>';
                            echo '<td>'.$sign->id.'</td>';
                            echo '<td>'.$sign->size.'</td>';
                            echo '<td>'.$sign->street_address.'</td>';
                            echo '<td>'.$sign->suburb.'</td>';
                            echo '<td>'.$sign->type.'</td>';
                            echo '<td>'.date('d-m-Y',strtotime($sign->installation_date)).'</td>';
                            echo '<td><a class="btn btn-default btn-xs" onclick="showUpdate('.$sign->id.',\''.$sign->status.'\');">'.$sign->status.'</a></td>';

                            if(empty($sign->approvedby_id)){
                                if(Yii::app()->user->role == 'superuser'){
                                    echo '<td><a class="btn btn-default btn-xs" onclick="approveItem('.$sign->id.');" href="javascript:void(0);">Approve</a></td>';
                                }else{
                                    echo '<td>Pending</td>';
                                }
                            }else{
                                echo '<td>Approved</td>';
                            }
                            echo '<td><a class="btn btn-default btn-xs" href="'.$edit_url.'/id/'.$sign->id.'">View</a> | <a class="btn btn-default btn-xs" href="'.$edit_url.'/id/'.$sign->id.'">Edit</a> | <a class="btn btn-default btn-xs" onclick="deleteItem('.$sign->id.');" href="javascript:void(0);">Delete</a></td>';
                            echo '</tr>';
                        }

                    ?>

                </tbody>
            </table>
        </div>
    </div>

</section>
<a href="#dvConfirm" data-toggle="modal" style="display: none;" id="btnpopup"></a>
<div id="dvConfirm" class="modal fade" style="display:none;">
    <br />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <section class="panel">
                    <header class="panel-heading">
                        <h4>Update Status</h4>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status : </label>
                                <div class="col-sm-8">
                                    <?php echo CHtml::dropDownList('selStatus',
                                        '',
                                        $sign->listStatus(),
                                        array('','class'=>"form-control",'id'=>'selStatus')
                                    );?>
                                    <input type="hidden" id="hdcurrentid" value="0" />
                                </div>
                            </div>
                            <br /><br /><br />
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <button class="btn btn-primary" id="btnStatus" type="button">Update Status</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
    </div>
</div>