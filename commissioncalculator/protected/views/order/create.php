<?php
/* @var $this SignorderController */
/* @var $model Signorder */
/* @var $form CActiveForm */
?>

<style>

</style>

<script type="text/javascript">
    $(document).ready(function(){
        $("ul.nav li").removeClass('active');
        $('#liorders').addClass('active');

        /*
        $("#txtPhoto").focusout(function(){
           $('#imgPhoto').attr('src',$(this).val());
           $('#imgPhoto').show();
        });
        */
        $('#picToUpload').on('change',function(){
            order.fn.imageAjaxUpload();
        });

        $('#btnaddanother').on('click',function(){
            order.fn.addPerson();
        });

        $('#imgPhoto').on('click',function(){
            $('#btnPop').click();
        });


    });

    var order = {};
    order.fn ={
      personCount:1,
      addPerson:function(){
          var spantype = $('<span class="relationship" id="dvlistercolleagueopt'+order.fn.personCount+'"></span>');

          var $relationship = $('.relationship' );
          var $clone = $("#txtClone").clone();


          $clone[0].id = 'txtPerson'+order.fn.personCount;
          $clone[0].name = 'txtPerson'+order.fn.personCount;

          $clone.show();
          $('#dvPerson').append($clone);

          $('#hdPersonNumber').val(order.fn.personCount);
          order.fn.personCount++;
      },
      loadPerson:function(personName){
          var spantype = $('<span class="relationship" id="dvlistercolleagueopt'+order.fn.personCount+'"></span>');

          var $relationship = $('.relationship' );
          var $clone = $("#txtClone").clone();


          $clone[0].id = 'txtPerson'+order.fn.personCount;
          $clone[0].name = 'txtPerson'+order.fn.personCount;
          $clone[0].value = personName;
          $clone.show();
          $('#dvPerson').append($clone);

          $('#hdPersonNumber').val(order.fn.personCount);
          order.fn.personCount++;
      },
      imageAjaxUpload:function(){
          var fld = $('#picToUpload').val();
          if(!/(\.bmp|\.gif|\.jpg|\.jpeg|\.png)$/i.test(fld)) {
              alert("Invalid image file type.");
              $('#picToUpload').val('');
          }else{
              $.ajaxFileUpload({
                  url:'<?php echo CController::createUrl('order/Imageupload'); ?>',
                  secureuri:false,
                  fileElementId:'picToUpload',
                  dataType: 'json',
                  success: function (data, status)
                  {
                      $('#hdPhoto').val(data.msg);
                      $('#imgPhoto').attr('src','<?php echo Yii::app()->request->baseUrl ?>/uploads/signorderuploads/'+data.msg);
                      $('#modalPhoto').attr('src','<?php echo Yii::app()->request->baseUrl ?>/uploads/signorderuploads/'+data.msg);
                      $('#imgPhoto').show();

                  },
                  error: function (data, status, e)
                  {
                      alert(e);
                  }
              });
          }
          return false;
      }
    };
</script>

<div class="form">
    <button type="button" id="btnPop" class="btn btn-primary" style="display:none;" data-toggle="modal" data-target="#myModal">Popup image</button>
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'signorder-dsa-form',
    'htmlOptions'=>array('class'=>'form-horizontal'),
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation'=>false,
)); ?>
    <input type="text" id="txtClone" class="form-control m-t-sm" style="display: none;" />
    <input type="hidden" id="hdPersonNumber" name="hdPersonNumber" value="0" />

    <div class="row">
        <div class="col-sm-6">
            <section class="panel">
                <header class="panel-heading font-bold"><h4>Sign-Order Form</h4></header>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'size',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->dropDownList($model,
                                    'size',
                                    $model->listSizes(),
                                    array('','class'=>"form-control")
                                );?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'street_address',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textArea($model,'street_address',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'suburb',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textField($model,'suburb',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'type',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->dropDownList($model,
                                    'type',
                                    $model->listType(),
                                    array('','class'=>"form-control")
                                );?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'line1',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textField($model,'line1',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'line2',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textField($model,'line2',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'line3',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textField($model,'line3',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'line4',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <?php echo $form->textField($model,'line4',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>

        <div class="col-sm-6">
            <section class="panel">
                <header class="panel-heading font-bold"><h4>&nbsp;</h4></header>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Licensee Salesperson: </label>
                            <div class="col-sm-8">
                                <div id="dvPerson" >

                                </div>
                                <a id="btnaddanother" class="pull-right m-t-sm m-r-sm" href="javascript:void(0);">+Add Another</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'installation_date',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">
                                <input type="text" value="<?php echo date('d-m-Y',strtotime($model->installation_date)); ?>"  data-date-format="dd-mm-yyyy" class=" datepicker-input form-control"  name="Signorder[installation_date]" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <?php echo $form->labelEx($model,'installation_instruction',array('class'=>"col-sm-3 control-label")); ?>
                            <div class="col-sm-8">

                                <div class="alert alert-info">
                                    In words NO drawings Must be specify exactly in relation to house, fences, ROW, trees, driveways, roads etc (State if O/H Wooden Strip required)
                                </div>
                                <?php echo $form->textArea($model,'installation_instruction',array('class'=>"form-control")); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Upload Photo: </label>
                            <div class="col-sm-8">
                                <?php echo $form->hiddenField($model,'photo',array('class'=>"form-control",'id'=>"hdPhoto")); ?>
                                <input id="picToUpload" class="btn" type="file" name="picToUpload">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-10">
                                <img id="imgPhoto" class="img-full" style="<?php echo (!empty($model->photo))?'':'display:none;' ?>" src="<?php echo Yii::app()->request->baseUrl.'/uploads/signorderuploads/'.  $model->photo; ?>">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>




<section class="panel">
    <div class="panel-body">
        <div class="form-group">
            <div class="col-sm-4">
                <button class="btn btn-white" type="submit">Cancel</button>
                <button class="btn btn-primary" type="submit">Save changes</button>
            </div>
        </div>

    </div>
</section>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$count = count($model->signorderpersons);
if($count<1){
    echo '<script>order.fn.addPerson();</script>';
}else{
    foreach($model->signorderpersons as $person){
        echo '<script>order.fn.loadPerson("'.$person->person_name.'");</script>';
    }
}
?>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" style="height:70%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body scrollable">
                <img id="modalPhoto" src="<?php echo Yii::app()->request->baseUrl.'/uploads/signorderuploads/'.  $model->photo; ?>" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>