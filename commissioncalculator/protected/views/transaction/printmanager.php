<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
$trdetails = Transactiondetails::model()->find('transactionid=' . $transaction->transactionid);
$trsalesperson = Transactionsalesperson::model()->find('transactionid='.$transaction->transactionid);
*/
$user = Users::model()->findByPk($userid);

$m = 0;
$plusgst = 0;
$gtotal = 0;

$m = $trmanager->managershare * .15;
$plusgst = round($m,2);

$m = $trmanager->managershare + $plusgst;
$gtotal = round($m,2);

?>
<html>
    <head>
        <title>Invoice</title>

        <style type="text/css">

          /*body{margin:2cm 1.5cm; }*/
          body, input, textarea { font-family:Myriad; font-size:8pt;line-height:10pt;border:1px solid #CCC;margin:80px 160px 480px 160px;height:3250px; }
          table {border-spacing:0; border-collapse: collapse;border-bottom:1px solid #CCC}
          table table{border-width:0px}

					.pad10{padding:20px 40px;}
					.pad5{padding:8px 40px;}

					.myriad10Reg{font-size:10pt;font-weight:normal}
					.myriad10Bold{font-size:10pt;font-weight:bold}
					.myriad11Bold{font-size:11pt;font-weight:bold}
					.myriad8Reg{font-size:8pt;font-weight:normal; line-height:110%}
					.myriad8Bold{font-size:8pt;font-weight:bold}

					.vabottom{vertical-align:bottom}


         	.center{text-align:center}
         	.left{text-align:left}
         	.right{text-align: right}



					td{ border-width: 1px;border-style: solid;border-color:#CCC}

					.black_bg{background-color:#000000;color:#FFF;}

					td.nobottom{border-width: 0px;}

					.alignbottom{vertical-align: bottom}

					 .brk {page-break-after:always}

					 a{color:#000;text-decoration:none}

					 #items tr td{border-bottom:1px solid #CCC}


        </style>



    </head>
    <body>

        <table width="100%" style="border:0px;margin:-1px;z-index:5;">

            <tr>
                <td colspan="8" class="left" style="border-left: 0px;border-top:0px;border-right:0px"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">INVOICE</div></td>
            </tr>

            <tr>
                <td colspan="2" width="20%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">PTR NUMBER</div></td>
                <td colspan="2"  width="30%"  class="left"><div class="pad10 myriad8Reg"><?php echo $transaction->ptr; ?></div></td>
                <td colspan="2" width="20%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">DATE</div></td>
                <td colspan="2" width="30%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg"><?php echo date('d-m-Y',strtotime($transaction->paymenttosalesperson)); ?></div></td>
            </tr>

            <tr>
                <td colspan="2" width="20%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">SALES PERSON</div></td>
                <td colspan="2"  width="30%"  class="left"><div class="pad10 myriad8Reg"><?php echo $user->fullname; ?></div></td>
                <td colspan="2" width="20%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">PROPERTY</div></td>
                <td colspan="2" width="30%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg"><?php echo $transaction->property; ?></div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Total Commission Received (inc GST)</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Total Commission Received (exc GST)</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Listing Part share for individual%</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">0.00 %</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Selling Part share for individual%</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">0.00 %</div></td>
            </tr>

            <tr>
                <td colspan="8" class="left" style="border-left: 0px;border-top:0px;border-right:0px"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">REFERRAL</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Listing part referral %</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">0.00 %</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Listing referral Payable to</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">0.00</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Selling part referral %</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">0.00 %</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Selling referral Payable to</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg"></div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Less "off-the-top deductions" other than referral (exc GST)</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold" style="display:none;">Bonus or Deductions (-)</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg" style="display:none;">$ 0.00</div></td>
            </tr>


        </table>

        <br />
        <br />

        <table width="100%" style="border:0px;margin:-1px;z-index:5;">
            <tr>
                <td colspan="8" class="left" style="border-left: 0px;border-top:0px;border-right:0px"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">Details</div></td>
            </tr>

            <tr>
                <td colspan="2" width="30%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Net commission to share (exc GST)</div></td>
                <td colspan="6"  width="70%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

            <tr>
                <td colspan="4" width="50%" style="border-left: 0px;" class="center"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">Listing Split</div></td>
                <td colspan="4" width="50%" style="border-right: 0px;" class="center"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">Selling Split</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Listing Split Amount</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Selling Split Amount</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Less Listing Referral amount in $ exc GST</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Less Selling Referral amount in $ exc GST</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Listing Gross Brought in </div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Selling Gross Brought in </div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Lister NET brought in Fees</div></td>
                <td colspan="2"  width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>

                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Seller NET brought in Fees</div></td>
                <td colspan="2"  width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>

        </table>

        <br /><br />

        <table width="100%" style="border:0px;margin:-1px;z-index:5;">
            <tr>
                <td colspan="8" class="left" style="border-left: 0px;border-top:0px;border-right:0px"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">Individual Share</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Lister fee share amount $ ex GST</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Individual share %</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg"><?php echo $user->usershare; ?> %</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Seller fee share amount $ ex GST</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Withholding Tax%</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">0.00 %</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">TOTAL fees brought in</div></td>
                <td colspan="2"  width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Individual Share amount $</div></td>
                <td colspan="2" width="25%" style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
            </tr>
        </table>
        <br /><br />

        <table width="100%" style="border:0px;margin:-1px;z-index:5;">
            <tr>
                <td colspan="8" class="left" style="border-left: 0px;border-top:0px;border-right:0px"><div style="margin-left: 30px;color:#807F83;font-weight:bold;font-size:12pt;padding:18px 0px 0px 0px;height:110px">For SalesPerson</div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Amount to Invoice Company</div></td>
                <td colspan="2"  width="25%" class="left"><div class="pad10 myriad8Reg">$ <?php echo $trmanager->managershare; ?></div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Bonus/Deductions</div></td>
                <td colspan="2" width="25%"  style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ 0.00 </div></td>
            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Net Share before GST</div></td>
                <td colspan="2" width="25%" class="left"><div class="pad10 myriad8Reg">$  <?php echo $trmanager->managershare; ?></div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Plus GST</div></td>
                <td colspan="2"  width="25%"  style="border-right: 0px;" class="left"><div class="pad10 myriad8Reg">$ <?php echo $plusgst; ?></div></td>

            </tr>

            <tr>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Less Withholding Tax </div></td>
                <td colspan="2" width="25%"  class="left"><div class="pad10 myriad8Reg">$ 0.00</div></td>
                <td colspan="2" width="25%" style="border-left: 0px;" class="right"><div class="pad10 myriad8Bold">Total Payable Amount</div></td>
                <td colspan="2"  width="25%" style="border-right: 0px;"  class="left"><div class="pad10 myriad8Reg">$ <?php echo $gtotal; ?></div></td>

            </tr>

        </table>
    </body>
</html>

