<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/spreadsheet.js',CClientScript::POS_END);

//$tr = new Transactionreport();

$listershare = '0.00';
$sellershare = '0.00';

$trlister = Trlistingpersons::model()->find('transactionreportid=' . $tr->transactionreportid . ' AND userid=' . $userid);
$trseller = Trsellingpersons::model()->find('transactionreportid=' . $tr->transactionreportid . ' AND userid=' . $userid);
$trsalesperson = Transactionsalesperson::model()->find('transactionid='.$transaction->transactionid);

if(!empty($trlister)){
    $listershare = $trlister->share;
}

if(!empty($trseller)){
    $sellershare = $trseller->share;
}
$user = Users::model()->findByPk($userid);

$arrmanager = array();
$managers = Managers::model()->findAll('usertomanage=' . $userid);
foreach($managers as $manager) {
    $tmp = array();
    $tmp = array("manager"	=>	$manager->user->fullname,
        "share"	=>	$manager->shareonuser);
    array_push($arrmanager, $tmp);
}
$manager_json = json_encode($arrmanager);


$invoicedate = empty($trmanager->invoicedate)?'':date('d-m-Y',strtotime($trmanager->invoicedate));
$paymentrecieveddate = empty($trmanager->paymentrecieveddate)?'':date('d-m-Y',strtotime($trmanager->paymentrecieveddate));
$paymenttosalesperson = empty($trmanager->paymenttosalesperson)?'':date('d-m-Y',strtotime($trmanager->paymenttosalesperson));

?>

<style>
    .mactive{
        background-color:#eeeeee;
    }
</style>

<script type="text/javascript">
    function calculateManager(){
        var m=0;
        m=parseFloat($('#txtSalesAmtToInvoice').val()) * .15;
        $('#txtSalesPlusGST').val(roundToTwo(m).toFixed(2));

        m=parseFloat($('#txtSalesAmtToInvoice').val()) + parseFloat($('#txtSalesPlusGST').val()) ;
        $('#txtSalesGrandTotal').val(roundToTwo(m).toFixed(2));

        m= parseFloat($('#txtCheckIndividualComp').val()) * 1.15;
        $('#txtCommTotalAmtGST').val(roundToTwo(m).toFixed(2));

        m= parseFloat($('#txtCommTotalAmtGST').val()) * (3/23);
        $('#txtCommTotalGSTComponent').val(roundToTwo(m).toFixed(2));

        m= ((parseFloat($('#txtIndividualSharePercentage').val())/100) + (1-(parseFloat($('#txtIndividualSharePercentage').val())/100)) * (parseFloat($('#managerPercentage').val())/100)) * 100;
        $('#txtCommTotalPayoutShare').val(roundToTwo(m));

        m= 100 - parseFloat($('#txtCommTotalPayoutShare').val());
        $('#txtCommCompShare').val(roundToTwo(m));

        $('#txtCommTotalPayableToPerson').val($('#txtSalesGrandTotal').val());
        $('#txtCommTotalPAYETransfer').val($('#txtSalesWithHoldingTax').val());

        m= parseFloat($('#txtCommTotalPayableToPerson').val()) + parseFloat($('#txtCommTotalPAYETransfer').val());
        $('#txtCommPayablePAYE').val(roundToTwo(m).toFixed(2));

        m= parseFloat($('#txtCommTotalGSTComponent').val()) - parseFloat($('#txtSalesPlusGST').val());
        $('#txtCommGSTTransfer').val(roundToTwo(m).toFixed(2));


        $('#panelSalesPerson').show();
        $('#panelCommission').show();
        $('#panelDetails').show();

        $("ul.nav li").removeClass('active');
        $('#litransaction').addClass('active');

    }

    function roundToTwo(num) {
        //alert(num);
        return +(Math.round(num + "e+2")  + "e-2");
        //return num;
    }
</script>


<aside class="aside bg-white b-r" id="subNav">
    <div class="wrapper b-b font-bold">Sales Persons</div>
    <ul class="nav">

        <?php
            $a=array();

            foreach($tr->trlistingpersons as $person){
                $name='';
                if($person->type=='colleague'){
                    $name=$person->user->fullname;
                    echo '<li id="lispreadsheet'.$person->userid.'" class="b-b" ><a href="/index.php/transaction/spreadsheet/transactionreportid/'.$person->transactionreportid.'/userid/'.$person->userid.'">'.$person->user->fullname.'</a></li>';
                }else{
                    $name=$person->otherperson;
                    echo '<li id="lispreadsheet'.$person->userid.'" class="b-b" ><a href="/index.php/transaction/spreadsheet/transactionreportid/'.$person->transactionreportid.'/userid/'.$person->userid.'">'.$person->otherperson.'</a></li>';
                }
                array_push($a,$name);
            }

            foreach($tr->trsellingpersons as $person1){
                $name='';
                if($person1->type=='colleague'){
                    $name=$person1->user->fullname;
                }else{
                    $name=$person1->otherperson;
                }

                if (!in_array($name, $a)) {
                    echo '<li id="lispreadsheet'.$person1->useridS.'" class="b-b" ><a href="/index.php/transaction/spreadsheet/transactionreportid/'.$person1->transactionreportid.'/userid/'.$person1->userid.'">'.$name.'</a></li>';

                }
            }

        ?>
    </ul>

    <?php

        if(count($tr->transactionmanagers)>0){
            ?>

            <div class="wrapper b-b font-bold">Managers</div>
            <ul class="nav">
                <?php
                foreach ($tr->transactionmanagers as $usermanager) {

                    echo '<li id="lispreadsheetm'.$usermanager->manager->userid.'" class="b-b" ><a href="/index.php/transaction/managercalulation/managerid/'.$usermanager->managerid.'">'.$usermanager->manager->user->fullname.'</a></li>';
                }
                ?>
            </ul>
        <?php
        }
    ?>

</aside>

<aside style="height:100%;">
    <section class="vbox">
        <div class="row">
            <div class="col-sm-12">
                <form method="post" id="frmTransaction" action="<?php echo CController::createURL("transaction/Updatemanagercalculation"); ?>" data-validate="parsley">
                    <input type="hidden" id="hdtransactionreportid" name="hdtransactionreportid" value="<?php echo $tr->transactionreportid; ?>" />
                    <input type="hidden" id="hdtransactionid" name="hdtransactionid" value="<?php echo $transaction->transactionid; ?>" />
                    <input type="hidden" id="hdmanagerid" name="hdmanagerid" value="<?php echo $trmanager->transactionmanagersid; ?>" />
                    <input type="hidden" id="hdisclosed" name="hdisclosed" value="0" />
                    <section class="panel">
                        <header class="panel-heading">
                        </header>
                        <div class="panel-body">
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Date</label>
                                    <input type="text" id="txtDate" name="txtDate" data-date-format="mm-dd-yyyy" value="<?php echo date('d-m-Y',strtotime($transaction->paymenttosalesperson)); ?>" class=" datepicker-input form-control" placeholder="Date">
                                </div>
                                <div class="col-sm-4">
                                    <label>Sales Person</label><br />
                                    <?php echo CHtml::dropDownList('selPerson1',$userid, CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('prompt'=>'Select Salesperson','class'=>"form-control",'disabled'=>'disabled')); ?>
                                </div>

                                <div class="col-sm-4">
                                    <label>Public Trust Reference</label>
                                    <input type="text" id="txtPTR" value="<?php echo $transaction->ptr; ?>" name="txtPTR" placeholder="Public Trust Reference" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Property</label>
                                <textarea placeholder="Property" id="txtProperty" name="txtProperty"  rows="3" class="form-control parsley-validated"><?php echo $transaction->property; ?></textarea>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Total Commission Received (inc GST)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" id="txtCTRMainInc" name="txtCTRMainInc" placeholder="0.00" class="form-control dollartext" value="0.00">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Total Commission Received  (exc GST)</label>
                                    <div class="input-group ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" id="txtCTRMainExc" name="txtCTRMainExc" placeholder="0.00" class="form-control dollartext" value="0.00">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-3">
                                    <label>Company Invoice</label>
                                    <input type="text" id="txtInvoice"  name="txtInvoice" placeholder="Company Invoice" value="<?php echo $trmanager->companyinvoice; ?>" class="c-save form-control">
                                </div>
                                <div class="col-sm-3">
                                    <label>Invoice Date</label>
                                    <input type="text" id="txtInvoiceDate" data-date-format="dd-mm-yyyy" class="c-save datepicker-input form-control" value="<?php echo $invoicedate; ?>"  name="txtInvoiceDate" placeholder="Invoice Date">
                                </div>
                                <div class="col-sm-3">
                                    <label>Payment Received</label>
                                    <input type="text" id="txtPaymentReceived" data-date-format="dd-mm-yyyy" class="c-save datepicker-input form-control" value="<?php echo $paymentrecieveddate; ?>" name="txtPaymentReceived" placeholder="Payment Received">
                                </div>
                                <div class="col-sm-3">
                                    <label>Payment Made To Salesperson</label>
                                    <input type="text" id="txtPaymentSalesperson" data-date-format="dd-mm-yyyy" class="c-save datepicker-input form-control" value="<?php echo $paymenttosalesperson; ?>"  name="txtPaymentSalesperson" placeholder="Payment Made">
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Listing part share for individual%</label>
                                    <div class="input-group ">
                                        <input type="text" id="txtListerPercentage" name="txtListerPercentage" placeholder="0" value="0.00" class="form-control percentagetext">
                                        <span class="input-group-addon">%</span>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <label>Selling part share for individual%</label>
                                    <div class="input-group ">
                                        <input type="text" id="txtSellerPercentage" name="txtSellerPercentage" placeholder="0" value="0.00" class="form-control percentagetext">
                                        <span class="input-group-addon">%</span>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <header class="panel-heading">
                                    <span class="h4">Referral</span>
                                </header> <br />
                                <div class="col-sm-3">
                                    <label>Listing part referral %</label>
                                    <div class="input-group">
                                        <input type="text" id="txtListingPartReferral" value="0.00"  name="txtListingPartReferral" placeholder="0"  class="form-control percentagetext">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Listing referral Payable to</label><br />
                                    <input type="text" id="txtListingPayable" name="txtListingPayable" placeholder="Listing referral Payable to"  value="0.00" class="form-control">
                                </div>
                                <div class="col-sm-3">
                                    <label>Selling part referral %</label>
                                    <div class="input-group">
                                        <input type="text" id="txtSellingPartReferral" name="txtSellingPartReferral" placeholder="0" value="0.00" class="form-control percentagetext">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <label>Selling referral Payable to</label><br />
                                    <input type="text" id="txtSellingPayable" name="txtSellingPayable" placeholder="Selling referral Payable to" value="0.00" class="form-control">
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Less "off-the-top deductions" other than referral (exc GST)</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" id="txtLessOtherReferral" name="txtLessOtherReferral" placeholder="0" value="<?php echo $transaction->lessdeductionsexcGST; ?>" class="form-control dollartext">
                                    </div>
                                </div>

                                <div class="col-sm-6" style="display:none;">
                                    <label>Bonus or Deductions (-)</label>
                                    <div class="input-group">
                                        <input type="text" id="txtBonusDeduction" name="txtBonusDeduction" value="<?php echo $transaction->bonusdeductions; ?>"  placeholder="0" class="form-control dollartext">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                    <span id="spanBonusDeduction1"><?php echo $bonusdesc; ?></span>
                                </div>
                            </div>

                            <div class="line line-dashed line-lg pull-in"></div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-12 text-right">
                                    <button class="btn btn-success btn-s-xs" style="display:none;" id="buttonCalculate" type="button">Re-Calculate</button>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="panel" id="panelDetails" style="display: none;">
                        <header class="panel-heading">
                            <span class="h4">Details</span>
                        </header>
                        <div class="panel-body">
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Net commission to share (exc GST)</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtNetCommToShare" name="txtNetCommToShare" placeholder="0" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <header class="panel-heading">
                                <span class="h4">Lister Split</span>
                            </header> <br />
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Listing Split Amount</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtListerSplitAmt" name="txtListerSplitAmt" placeholder="0" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Less Listing Referral amount in $ exc GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00"  readonly="readonly" id="txtLessListingRefAmt" name="txtLessListingRefAmt" placeholder="0" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Listing Gross Brought in </label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtListingGrossBrought" name="txtListingGrossBrought" placeholder="0" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <header class="panel-heading">
                                <span class="h4">Seller Split</span>
                            </header> <br />
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Selling Split Amount</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSellingSplitAmt" name="txtSellingSplitAmt" placeholder="0" class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>Less Selling Referral amount in $ exc GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtLessSellingRefAmt" name="txtLessSellingRefAmt" placeholder="0" class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>Selling Gross Brought in </label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSellingGrossBrought" name="txtSellingGrossBrought" placeholder="0" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Lister NET brought in Fees</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtListerNetBoughtFees" name="txtListerNetBoughtFees" placeholder="0" class="form-control">
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                    <label>Seller NET brought in Fees</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSellerNetBoughtFees" name="txtSellerNetBoughtFees" placeholder="0" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <header class="panel-heading">
                                <span class="h4">Individual Share</span>
                            </header> <br />

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Lister fee share amount $ ex GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtListerFeeShareAmt" name="txtListerFeeShareAmt" placeholder="0" class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>Seller fee share amount $ ex GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" placeholder="0" id="txtSellerFeeShareAmt" name="txtSellerFeeShareAmt" class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>TOTAL fees brought in</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" placeholder="0" id="txtIndTotalFeesBought" name="txtIndTotalFeesBought" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Individual share %</label>
                                    <div class="input-group  ">
                                        <input type="text"  readonly="readonly" placeholder="0" id="txtIndividualSharePercentage" name="txtIndividualSharePercentage" value="<?php echo $user->usershare; ?>" class="form-control">
                                        <span class="input-group-addon">%</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>Withholding Tax%</label>
                                    <div class="input-group  ">
                                        <input type="text" readonly="readonly" placeholder="0" id="txtIndWithHoldingTax" name="txtIndWithHoldingTax" value="<?php echo $user->withholdingtax; ?>" class="form-control">
                                        <span class="input-group-addon">%</span>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <label>Individual Share amount $</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" placeholder="0" id="txtIndividualShareAmount" name="txtIndividualShareAmount" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Company share $</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtCompanySharePercentage" name="txtCompanySharePercentage" placeholder="0"  class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <label>Check Individual + Company</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtCheckIndividualComp" name="txtCheckIndividualComp" placeholder="0" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>


                    <section class="panel" id="panelSalesPerson" style="display: none;">
                        <header class="panel-heading">
                            <span class="h4">For SalesPerson</span>
                        </header>
                        <div class="panel-body">
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Amount to Invoice Company</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" id="txtSalesAmtToInvoice" name="txtSalesAmtToInvoice" value="<?php echo $trmanager->managershare; ?>" placeholder="0" class="form-control">
                                    </div>

                                    <label class="m-t">Net Share before GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="<?php echo $trmanager->managershare; ?>" readonly="readonly" id="txtSalesNetShare" name="txtSalesNetShare" placeholder="0" class="form-control">
                                    </div>

                                    <label class="m-t">Less Withholding Tax </label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSalesWithHoldingTax" name="txtSalesWithHoldingTax" placeholder="0" class="form-control">
                                    </div>


                                </div>

                                <div class="col-sm-6">
                                    <label>Bonus/Deductions (<span id="spanBonusDeduction"><?php echo $bonusdesc; ?></span>)</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSalesBonusDeduction" name="txtSalesBonusDeduction" value="<?php echo $bonusamt; ?>"  placeholder="0" class="form-control">
                                    </div>

                                    <label class="m-t">Plus GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSalesPlusGST" name="txtSalesPlusGST" placeholder="0" class="form-control">
                                    </div>

                                    <label class="m-t">Total Payable Amount</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" value="0.00" readonly="readonly" id="txtSalesGrandTotal" name="txtSalesGrandTotal" placeholder="0" class="form-control">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </section>

                    <section class="panel" id="panelManagers" style="display: none;">
                        <header class="panel-heading">
                            <span class="h4">Managers</span>
                        </header>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <input type="hidden" id="managerPercentage" value="0">
                                <table class="table table-striped  -none" id="tblProfUploads">
                                    <thead>
                                    <tr>
                                        <th>Manager Name</th>
                                        <th>Share</th>
                                        <th>Company Share amount</th>
                                        <th>Manager's share</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tBodyDetails">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <section class="panel" id="panelCommission" style="display: none;">
                        <header class="panel-heading">
                            <span class="h4">Company GST and Commission Reserve</span>
                        </header>
                        <div class="panel-body">
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Total amount inc GST</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" id="txtCommTotalAmtGST" name="txtCommTotalAmtGST" placeholder="0" class="form-control">
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <label>Total GST component</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" id="txtCommTotalGSTComponent" name="txtCommTotalGSTComponent" placeholder="0" class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Total payout share %</label>
                                    <div class="input-group  ">
                                        <input type="text" readonly="readonly" id="txtCommTotalPayoutShare" name="txtCommTotalPayoutShare" placeholder="0" class="form-control">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <label>Company Share %</label>
                                    <div class="input-group  ">
                                        <input type="text" readonly="readonly" id="txtCommCompShare" name="txtCommCompShare"  placeholder="0" class="form-control">
                                        <span class="input-group-addon">%</span>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Total Payable to Salesperson</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" id="txtCommTotalPayableToPerson" name="txtCommTotalPayableToPerson" placeholder="0" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>Total Withholding Tax</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" id="txtCommTotalPAYETransfer" name="txtCommTotalPAYETransfer" placeholder="0" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">

                                <div class="col-sm-6">
                                    <label>Payable + Withholding Tax</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" placeholder="0" id="txtCommPayablePAYE" name="txtCommPayablePAYE" class="form-control">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label>GST Reserve transfer</label>
                                    <div class="input-group  ">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" readonly="readonly" placeholder="0" id="txtCommGSTTransfer" name="txtCommGSTTransfer" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="panel">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">

                                    <span id="secbtn">
                                    <button class="btn btn-white" id="btnCancel" type="button">Cancel</button>
                                    <button class="btn btn-primary" id="btnUpdate" type="submit">Update Calculation</button> &nbsp;
                                    <button class="btn btn-danger" id="btnClose" type="button">Close Transaction</button> &nbsp;
                                    </span>

                                    <a class="btn btn-success pull-right" href="<?php echo CController::createURL("transaction/PrintManagerCalculation",array('managerid'=>$trmanager->transactionmanagersid)); ?>">Download Invoice</a>
                                </div>
                            </div>

                        </div>
                    </section>


                </form>
            </div>
        </div>

    </section>
</aside>

<script type="text/javascript">
   $('#lispreadsheetm<?php echo $userid; ?>').addClass('mactive');
   calculateManager();
   $('input').prop('readonly',true);
   $('select').prop('disabled',true);
   $('textarea').prop('disabled',true);
   $('.c-save').prop('readonly',false);
   <?php
            if($trmanager->isclosed== '1'){
        ?>
            $('.c-save').prop('readonly',true);

            $('#secbtn').hide();
   <?php
       }
   ?>
</script>