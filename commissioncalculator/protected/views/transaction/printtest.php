<?php
$vendor = Trvendorlandlordassignor::model()->find('transactionreportid=' . $model->transactionreportid);
$purchaser = Trpurchasertenantasignee::model()->find('transactionreportid=' . $model->transactionreportid);
$notes =Trnotes::model()->find('transactionreportid='.$model->transactionreportid);
$confirm = Trconfirmation::model()->find('transactionreportid='.$model->transactionreportid);

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">

    <style type="text/css">
        body, input, textarea {
            font-family:Myriad;
            font-size:8pt;
            line-height:10pt;
            border:3px solid #000;
            margin:80px 160px 480px 160px;height:3250px;
        }

        table {border-spacing:0; border-collapse: collapse;border-bottom:1px solid #000000;}

        .pad10{padding:20px 40px;}
        .pad5{padding:8px 40px;}

        .myriad10Reg{font-size:10pt;font-weight:normal}
        .myriad10Bold{font-size:10pt;font-weight:bold}
        .myriad11Bold{font-size:11pt;font-weight:bold}
        .myriad8Reg{font-size:8pt;font-weight:normal; line-height:110%}
        .myriad8Bold{font-size:8pt;font-weight:bold}
        .myriad12Bold{font-size:12pt;font-weight:bold}
        .myriad14Bold{font-size:14pt;font-weight:bold}

        td{border-width: 1px;border-style: solid;border-color:#000000}

        .black_bg{background-color:#000000;color:#FFF;}

        td.nobottom{border-width: 0px;}

        .alignbottom{vertical-align: bottom}

        .brk {page-break-after:always}

        a{color:#000;text-decoration:none}

        #items tr td{border-bottom:1px solid #CCC}
    </style>
</head>
<body>

<table width="100%" style="border:0px;margin:-1px;z-index:5;">
    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
            <p class="myriad14Bold">TRANSACTION REPORT</p>
            <p class="myriad10Bold">IMPORTANT: To be completed for every executed transaction with <br />
                copy of agreement within 24 hours of completing a transaction. <br />
                Email: <span style="color:#0000FF;">accounts@jameslaw.co.nz</span>
            </p>
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Date</div><br />
            <?php echo date('d-m-Y',strtotime($model->createdtime)); ?>
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Transaction Type</div><br />
            <?php echo $model->getTransactionTypeName(); ?>
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Sale / Lease Price</div><br />
            <?php echo $model->saleleaseprice; ?>
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">PTR</div><br />
            <?php echo $model->ptdrnumber; ?>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="4" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Address of Property/Business :</div><br />
            <?php echo $model->address; ?>
        </td>

    </tr>

    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Date of Agreement</div><br />
            <?php echo date('d-m-Y',strtotime($model->agreementdate)); ?>
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Unconditional Date</div><br />
            <?php echo date('d-m-Y',strtotime($model->unconditionaldate)); ?>
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Possession Date</div><br />
            <?php echo date('d-m-Y',strtotime($model->posessiondate)); ?>
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">Settlement Date</div><br />
            <?php echo date('d-m-Y',strtotime($model->settlementdate)); ?>
        </td>

    </tr>

    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">COMMISSION
                </p>
            </div>
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Total Commission Received (exc GST)</div><br />
            <?php echo $model->commissionamtexcGST; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Total Commission Received (inc GST)</div><br />
            <?php echo $model->commissiondepositamtincGST; ?>
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Deposit (inc GST)</div><br />
            <?php echo $model->commissionamtincGST; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Deposit (exc GST)</div><br />
            <?php echo $model->commissiondepositamtexcGST; ?>
        </td>
    </tr>

    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">Lister/Seller Details
                </p>
            </div>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad10Bold">LISTER DETAILS</div><br />
            <div class="myriad8Bold">Lister Share</div><br />
            <?php echo $model->listershare; ?>%

            <div class="myriad8Bold">Lister / Share of the lister</div><br />
            <?php
                if(count($model->trlistingpersons)>0){
                    foreach($model->trlistingpersons as $person){
                        if($person->type == 'colleague'){
                            echo $person->user->fullname . ' / ' . $person->share . ' <br />';
                        }else{
                            echo $person->otherperson . ' / ' . $person->share;
                        }

                    }
                }
            ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad10Bold">SELLER DETAILS</div>
            <div class="myriad8Bold">Seller Share / Type</div><br />
            <?php echo $model->sellershare; ?> %

            <div class="myriad8Bold">Seller / Share of the Seller</div><br />
            <?php
            if(count($model->trlistingpersons)>0){
                foreach($model->trsellingpersons as $person1){
                    if($person->type == 'colleague'){
                        echo $person1->user->fullname . ' / ' . $person1->share . ' <br />';
                    }else{
                        echo $person1->otherperson . ' / ' . $person1->share;
                    }

                }
            }
            ?>
        </td>
    </tr>

    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">REFERRAL AND CONJUNCTIONAL SPLIT
                </p>
            </div>
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Referred by POWERTEAM?</div><br />
            <?php echo ($model->ispowerteam=='1')?"Yes":"No"; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Referred by POWERTEAM?</div><br />
            <?php echo ($model->ispowerteamseller=='1')?"Yes":"No"; ?>
        </td>
    </tr>


    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Listing Referral %</div><br />
            <?php echo $model->listingreferralperc; ?> %
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Selling Referral %</div><br />
            <?php echo $model->sellingreferralperc; ?> %
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Listing Split Amount</div><br />
            $ <?php echo $model->listingsplitamt; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Selling Split Amount</div><br />
            $ <?php echo $model->sellingsplitamt; ?>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Less Listing Referral Amount Exc GST</div><br />
            $ <?php echo $model->listingreferralamt; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Selling Listing Referral Amount Exc GST</div><br />
            $ <?php echo $model->sellingreferralamt; ?>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Listing Gross Brought In</div><br />
            $ <?php echo $model->listinggrossbroughtin; ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Selling Gross Brought In</div><br />
            $ <?php echo $model->sellinggrossbroughtin; ?>
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Listing Payable To</div><br />
            Payable to powerteam@jameslaw.co.nz
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Selling Payable To</div><br />
            Payable to powerteam@jameslaw.co.nz
        </td>
    </tr>
    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Conjunctional Split (if any)</div><br />
            <?php echo $model->conjuctionalothersplit; ?> &nbsp;
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Conjunctional Company, Salesperson & Postal Address:</div><br />
            <?php echo $model->conjuctionalothercompany; ?> &nbsp;
        </td>
    </tr>

    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">VENDOR | LANDLORD | ASSIGNOR - DETAILS
                </p>
            </div>
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Type</div><br />
            <?php
                switch($vendor->type){
                    case "1":
                        echo 'Vendor';
                        break;
                    case "2":
                        echo 'Landlord';
                        break;
                    case "3":
                        echo 'Commercial Lease';
                        break;
                    case "4":
                        echo 'Assignor';
                        break;
                }
            ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Legal/Entity Name</div><br />
            <?php echo $vendor->legalentity; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Contact Name</div><br />
            <?php echo $vendor->contactname; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Contact No.</div><br />
            <?php echo $vendor->contactnumber; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Fax</div><br />
            <?php echo $vendor->faxnumber; ?> &nbsp;
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">Email</div><br />
            <?php echo $vendor->email; ?> &nbsp;
        </td>

    </tr>

    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Street</div><br />
            <?php echo $vendor->streetaddress; ?>&nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Suburb</div><br />
            <?php echo $vendor->suburb; ?>&nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">City</div><br />
            <?php echo $vendor->city; ?>&nbsp;
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">Country</div><br />
            New Zealand &nbsp;
        </td>

    </tr>

    <tr valign="top">
        <td class="pad5" colspan="4" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Vendors Lawyer</div><br />
            <?php echo $vendor->solicitorsfirm; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Lawyers First Name</div><br />
            <?php echo $vendor->individualactingfirstname; ?> &nbsp;
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Last Name</div><br />
            <?php echo $vendor->individualactinglastname; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="1" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Lawyers Email</div><br />
            <?php echo $vendor->individualactingemail; ?> &nbsp;
        </td>
        <td class="pad5" colspan="1" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Phone</div><br />
            <?php echo $vendor->individualactingphonenumber; ?> &nbsp;
        </td>

        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Fax (Include Area Code)</div><br />
            <?php echo $vendor->individualactingfaxnumber; ?> &nbsp;
        </td>
    </tr>

    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">PURCHASER | TENANT | ASSIGNEE - DETAILS
                </p>
            </div>
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Type</div><br />
            <?php
                switch($vendor->type){
                    case "1":
                        echo 'Purchaser';
                        break;
                    case "2":
                        echo 'Tenant';
                        break;
                    case "3":
                        echo 'Assignee';
                        break;

                }
            ?>
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Legal/Entity Name</div><br />
            <?php echo $purchaser->legalentity; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Contact Name</div><br />
            <?php echo $purchaser->contactname; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Contact No.</div><br />
            <?php echo $purchaser->contactnumber; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Fax</div><br />
            <?php echo $purchaser->faxnumber; ?> &nbsp;
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">Email</div><br />
            <?php echo $purchaser->email; ?> &nbsp;
        </td>

    </tr>

    <tr valign="top">
        <td class="pad5" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Street</div><br />
            <?php echo $purchaser->streetaddress; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Suburb</div><br />
            <?php echo $purchaser->suburb; ?> &nbsp;
        </td>
        <td class="pad5" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">City</div><br />
            <?php echo $purchaser->city; ?> &nbsp;
        </td>
        <td class="pad5" align="left">
            <div class="myriad8Bold">Country</div><br />
            New Zealand &nbsp;
        </td>

    </tr>

    <tr valign="top">
        <td class="pad5" colspan="4" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Vendors Lawyer</div><br />
            <?php echo $purchaser->solicitorsfirm; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="2" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Lawyers First Name</div><br />
            <?php echo $purchaser->individualactingfirstname; ?> &nbsp;
        </td>
        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Last Name</div><br />
            <?php echo $purchaser->individualactinglastname; ?> &nbsp;
        </td>
    </tr>

    <tr valign="top">
        <td class="pad5" colspan="1" style="border-right: 1px;" align="left">
            <div class="myriad8Bold">Lawyers Email</div><br />
            <?php echo $purchaser->individualactingemail; ?> &nbsp;
        </td>
        <td class="pad5" colspan="1" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Phone</div><br />
            <?php echo $purchaser->individualactingphonenumber; ?> &nbsp;
        </td>

        <td class="pad5" colspan="2" align="left" style="border-right: 1px;">
            <div class="myriad8Bold">Lawyers Fax (Include Area Code)</div><br />
            <?php echo $purchaser->individualactingfaxnumber; ?> &nbsp;
        </td>
    </tr>



    <tr bgcolor="#D9D9D9" valign="top">
        <td colspan="4" class="text-align:center;">
            <div align="center">
                <p class="myriad10Bold">TRANSACTION NOTES</p>
                <p class="myriad8Bold">Record here the sequence of events, appointments, inspections and representations you made throughout the transaction. </p>
            </div>
        </td>
    </tr>

    <tr valign="top">
        <td  colspan="2" style="padding:20px;border-right: 1px;" align="left">
            <table width="98%" style="border:0px;margin:-1px;z-index:5;">
                <tr>
                    <td>
                        <p>Was the property inspected on site inside and out by Purchaser/Tenant ? <?php echo ($notes->ispropertyinspected==1)?"Yes":"No"; ?> <br />
                        <span style="text-decoration: underline"><?php echo $notes->ispropertyinspectedtext; ?> &nbsp;</span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Dates property shown to Purchaser /Tenant and/or others (include names with times):  <br />
                        <span style="text-decoration: underline"></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Statements made / discussions / questions and your answers,about the boundaries, titles (including Cross Lease / Body Corp)  <br />
                        <span style="text-decoration: underline"><?php echo $notes->statementboundaries; ?></span></p>
                    </td>
                </tr>


                <tr>
                    <td>
                        <p>Were any problems / potential problems / problem areas discussed? Specify:  ? <?php echo ($notes->ispotentialproblems==1)?"Yes":"No"; ?> <br />
                        <span style="text-decoration: underline"><?php echo $notes->ispotentialproblemtext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Were any specific items discussed where you gave advice? Specify: <br />
                        <span style="text-decoration: underline"></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>SALE ONLY: Vendor Warranty explained to: Purchaser: ? <?php echo ($notes->ispurchaserwarranty==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->ispurchaserwarrantytext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>SALE ONLY: Vendor Warranty explained to: Vendor? <?php echo ($notes->isvendorwarranty==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isvendorwarrantytext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>GST discussion ? <?php echo ($notes->isGSTdiscussion==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isGSTdiscussiontext; ?></span></p>
                    </td>
                </tr>


            </table>
        </td>
        <td colspan="2" align="left" style="padding:20px;border-right: 1px;">
            <table width="98%" style="border:0px;margin:-1px;z-index:5;">
                <tr>
                    <td>
                        <p>If the Purchaser has bought the property subject to selling his/her own property did you guarantee / lead them to believe that we / you could obtain a certain price and thus possibly entice them to purchase?  <?php echo ($notes->ispurchaserboughtproperty==1)?"Yes":"No"; ?>  <br />
                            <span style="text-decoration: underline"></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Have you viewed and appraised the Purchaser's property? <?php echo ($notes->isviewedandappraised==1)?"checked=checked":""; ?> <br />
                            <span style="text-decoration: underline"></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Did you at any time have a difference of opinion with the Vendor/Landlord over any matter whatsoever? If yes describe ? <?php echo ($notes->isdifferenceopinion==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isdifferenceopiniontext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Commission discussed ? <?php echo ($notes->iscommissiondiscussed==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->iscommissiondiscusstext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Did the Vendor exclude any chattels from the sale? <?php echo ($notes->isvendorincludechattel==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isvendorincludechatteltext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Did you refer the Purchaser / Vendor to their solicitor / accountant for clarification of any matters? If yes describe? <?php echo ($notes->isreferaccountant==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isreferaccountanttext; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Describe what was discussed regarding any development potential, or re-sale value
                            Vendor  <br />
                            <span style="text-decoration: underline"><?php echo $notes->vendordescriberesalevalue; ?></span></p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p>Purchaser  <br />
                            <span style="text-decoration: underline"><?php echo $notes->purchaserdiscussedresalevalue; ?></span></p>
                    </td>
                </tr>


            </table>
        </td>
    </tr>

</table>
<div class="brk"></div>
<table width="100%" style="border:0px;margin:-1px;z-index:5;">
    <tr valign="top">
        <td  colspan="2" style="padding:20px;border-right: 1px;" align="left">
            <table width="98%" style="border:0px;margin:-1px;z-index:5;">
                <tr>
                    <td>
                        <p> Was any advice sought? <?php echo ($notes->isadvicesought==1)?"Yes":"No"; ?> <br />
                            <span style="text-decoration: underline"><?php echo $notes->isadvicesoughttext; ?></span></p>
                    </td>
                </tr>
            </table>
        </td>
        <td  colspan="2" style="padding:20px;border-right: 1px;" align="left">
            <table width="98%" style="border:0px;margin:-1px;z-index:5;">
                <tr>
                    <td>
                        <p>Note any other comments / relevant points  <br />
                            <span style="text-decoration: underline"><?php echo $notes->othercomments; ?></span></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body> </html>
