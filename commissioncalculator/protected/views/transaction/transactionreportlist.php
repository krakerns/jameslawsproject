<?php
$spreadsheet_url = Yii::app()->createUrl('transaction/spreadsheet');
$edit_url = Yii::app()->createUrl('transaction/edittransaction');
$delete_url = Yii::app()->createUrl('users/delete');

//echo Yii::app()->user->role;die();
$userid=Yii::app()->user->id;

if(Yii::app()->user->role=='accounts' || Yii::app()->user->role == 'superuser'){
    $listTR = Transactionreport::model()->findAll('markdelete<>1 AND status <> 2  ORDER BY transactionreportid DESC');
}else{
    $listTR  = Transactionreport::model()->findAll('(listeruserpersonid=' .$userid . ' OR selleruserpersonid='.$userid . ' OR createdbyid='.$userid.' ) AND  status <> 2   AND markdelete<>1  ORDER BY transactionreportid DESC');
}

$arrcurrent = array();
$arrclosed = array();

foreach($listTR as $tr){

    $iscalculatelist = 0;
    $iscalculatesell = 0;

    if(count($tr->trlistingpersons)>0){
        foreach($tr->trlistingpersons as $listPerson){
            $trans = Transaction::model()->find('userid=' . $listPerson->userid . ' AND transactionreportid=' . $tr->transactionreportid);

            if(!empty($trans)){
                if($trans->isclosed != '1') {
                    $iscalculatelist = 1;
                    break;
                }
            }else{
                $iscalculatelist = 1;
            }

        }
    }else{
        $iscalculatelist = 0;
    }

    if(count($tr->trsellingpersons)>0){
        foreach($tr->trsellingpersons as $sellPerson){
            $trans = Transaction::model()->find('userid=' . $sellPerson->userid . ' AND transactionreportid=' . $tr->transactionreportid);
            if(!empty($trans)){
                if($trans->isclosed != '1') {
                    $iscalculatesell = 1;
                    break;
                }
            }else{
                $iscalculatesell = 1;
            }
        }
    }else{
        //echo 'a';
        $iscalculatesell = 0;
    }


    if($iscalculatelist == 0 && $iscalculatesell == 0){
        array_push($arrclosed, $tr);
    }else{
        array_push($arrcurrent, $tr);
    }
   // break;
}
//echo $iscalculatelist . ','. $iscalculatesell;
?>


<script type="text/javascript">

    var tbl_current;
    var tbl_closed;
    var tbl_fallen;

    $(document).ready(function(){

        tbl_current=$('#tbl_current').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 1, "desc" ]]
        });

        tbl_closed=$('#tbl_closed').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 1, "desc" ]]
        });

        tbl_fallen=$('#tbl_fallen').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 1, "desc" ]]
        });

        $('#btnActionCurrent').on('click',function(){
            if($('#selectAction').val()=="1"){
                confirmTransaction();
            }else {
                deleteTransactioncurrent();
            }
        });

        $('#btnActionClosed').on('click',function(){
            if($('#selectAction').val()=="1"){
                confirmTransaction();
            }else {
                deleteTransactionclosed();
            }
        });

        $('#btnActionFallen').on('click',function(){
            if($('#selectAction').val()=="1"){
                confirmTransaction();
            }else {
                deleteTransactionfallen();
            }
        });

        $("ul.nav li").removeClass('active');
        $('#litransaction').addClass('active');
        $('#lidefault').addClass('active');

    });

    function showStatus(id){

        jQuery.ajax({
            type: "POST",
            url: '<?php echo CController::createURL("transaction/GetCalculations"); ?>',
            dataType: "json",
            data:'trid='+id,
            success: function(data){
                var tr='';
                $("#tBodyDetails").html('');

                $.each(data, function(key,value) {
                    tr+='<tr><td> '+value.fullname+' <td><td> '+value.status+' <td></tr>';
                })
                $("#tBodyDetails").append(tr);

            }
        });

        $('#btnShow').click();
    }

    function confirmTransaction(){
        var answer = confirm("Do you want to confirm selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingissues:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/confirmadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteTransactioncurrent(){
        var answer = confirm("Do you want to delete selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingcurrent:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/deleteadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteTransactionclosed(){
        var answer = confirm("Do you want to delete selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingclosed:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/deleteadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteTransactionfallen(){
        var answer = confirm("Do you want to delete selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingfallen:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/deleteadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteItem(userid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "userid": userid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/index"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }
</script>

<section class="panel">
    <header class="panel-heading">
        <h4>Transaction Reports List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('transaction/addtransactionreport'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>

    <header class="panel-heading bg-light">
        <ul class="nav nav-tabs">
            <li id="lidefault" class="active"><a href="#home" data-toggle="tab">Current</a></li>
            <li><a href="#profile" data-toggle="tab">Closed</a></li>
            <li><a href="#messages" data-toggle="tab">Fallen</a></li>
        </ul>
    </header>
    <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active" id="home">
                <div class="table-responsive">
                    <table id="tbl_current" class="table table-striped m-b-none">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th width="10%">TR ID</th>
                            <th width="15%">Listing Persons</th>
                            <th width="15%">Selling Persons</th>
                            <th width="27%">Property</th>
                            <th width="10%">Date</th>
                            <th width="10%">Status</th>
                            <th width="5%">&nbsp;</th>
                            <th width="5%">&nbsp;</th>
                        </tr>
                        </thead>
                        <?php

                        /*
                        if(Yii::app()->user->role=='accounts' || Yii::app()->user->role == 'superuser'){
                            $listTR = Transactionreport::model()->findAll('markdelete<>1 AND status = 0');
                        }else{
                            $listTR  = Transactionreport::model()->findAll('(listeruserpersonid=' .$userid . ' OR selleruserpersonid='.$userid . ' OR createdbyid='.$userid.' ) AND  status = 0 AND markdelete<>1');
                        }
                        */



                        if(count($arrcurrent>0)){
                            echo '<tbody>';
                            foreach($arrcurrent as $tr){

                                $status = ($tr->status=='1')?'Confirmed':'Pending';
                                echo '<tr>';
                                echo '<td><input class="lingcurrent" type="checkbox" value="'.$tr->transactionreportid.'" style="width:50px !important;" name="post[]"></td>';

                                $style = 'style="color:blue !important;"';

                                if($tr->status=='1'){
                                    $style = 'style="color:green !important;"';
                                }else if ($tr->status=='2'){
                                    $style = 'style="color:orange !important;"';
                                }

                                echo '<td '.$style.' >TR-'.leading_zeros($tr->transactionreportid,8).'</td>';
                                $name ='';
                                if(count($tr->trlistingpersons)>0){
                                    foreach($tr->trlistingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }
                                echo '<td>'.$name.'</td>';
                                $name ='';
                                if(count($tr->trsellingpersons)>0){
                                    $name ='';
                                    foreach($tr->trsellingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }

                                echo '<td>'.$name.'</td>';
                                echo '<td>'.$tr->address.'</td>';
                                echo '<td>'. date('d-m-Y',strtotime($tr->createdtime)) .'</td>';

                                $color = 'btn-danger';

                                echo '<td><a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="showStatus('.$tr->transactionreportid.');">View Status</a></td>';

                                if((Yii::app()->user->role == 'accounts' || Yii::app()->user->role == 'superuser')){
                                    echo '<td><a class="editrow btn btn-success btn-xs" href="'.$spreadsheet_url.'/transactionreportid/'.$tr->transactionreportid.'">Calculate</a> </td>';
                                }else{
                                    echo '<td></td>';
                                }

                                echo '<td align="center"> <a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/transactionreportid/'.$tr->transactionreportid.'">View</a> &nbsp;&nbsp;</td>';
                                echo '</tr>';
                            }
                            echo '</tbody>';
                        }
                        ?>


                    </table>

                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-4 hidden-xs">
                                <select id="selectAction" class="input-sm form-control input-s inline">
                                    <option value="2">Delete selected</option>
                                </select>
                                <button id="btnActionCurrent" class="btn btn-sm btn-white">Apply</button>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            <div class="tab-pane" id="profile">

                <div class="table-responsive">
                    <table id="tbl_closed" class="table table-striped m-b-none">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th width="10%">TR ID</th>
                            <th width="15%">Listing Persons</th>
                            <th width="15%">Selling Persons</th>
                            <th width="27%">Property</th>
                            <th width="10%">Date</th>
                            <th width="10%">Status</th>
                            <th width="5%">&nbsp;</th>
                            <th width="5%">&nbsp;</th>
                        </tr>
                        </thead>
                        <?php

                        /*
                        if(Yii::app()->user->role=='accounts' || Yii::app()->user->role == 'superuser'){
                            $listTR = Transactionreport::model()->findAll('markdelete<>1 AND status = 1');
                        }else{
                            $listTR  = Transactionreport::model()->findAll('(listeruserpersonid=' .$userid . ' OR selleruserpersonid='.$userid . ' OR createdbyid='.$userid.' ) AND status = 1 AND markdelete<>1');
                        }
                        */


                        if(count($arrclosed>0)){
                            echo '<tbody>';
                            foreach($arrclosed as $tr){

                                $status = ($tr->status=='1')?'Confirmed':'Pending';
                                echo '<tr>';
                                echo '<td><input class="lingclosed" type="checkbox" value="'.$tr->transactionreportid.'" style="width:50px !important;" name="post[]"></td>';

                                $style = 'style="color:blue !important;"';

                                if($tr->status=='1'){
                                    $style = 'style="color:green !important;"';
                                }else if ($tr->status=='2'){
                                    $style = 'style="color:orange !important;"';
                                }

                                echo '<td '.$style.' >TR-'.leading_zeros($tr->transactionreportid,8).'</td>';
                                $name ='';
                                if(count($tr->trlistingpersons)>0){
                                    foreach($tr->trlistingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }
                                echo '<td>'.$name.'</td>';
                                $name ='';
                                if(count($tr->trsellingpersons)>0){
                                    $name ='';
                                    foreach($tr->trsellingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }

                                echo '<td>'.$name.'</td>';
                                echo '<td>'.$tr->address.'</td>';
                                echo '<td>'. date('d-m-Y',strtotime($tr->createdtime)) .'</td>';

                                $color = 'btn-danger';

                                echo '<td><a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="showStatus('.$tr->transactionreportid.');">View Status</a></td>';

                                if((Yii::app()->user->role == 'accounts' || Yii::app()->user->role == 'superuser')){
                                    echo '<td><a class="editrow btn btn-success btn-xs" href="'.$spreadsheet_url.'/transactionreportid/'.$tr->transactionreportid.'">Calculate</a> </td>';
                                }else{
                                    echo '<td></td>';
                                }

                                echo '<td align="center"> <a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/transactionreportid/'.$tr->transactionreportid.'">View</a> &nbsp;&nbsp;</td>';
                                echo '</tr>';
                            }
                            echo '</tbody>';
                        }
                        ?>


                    </table>

                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-4 hidden-xs">
                                <select id="selectAction" class="input-sm form-control input-s inline">
                                    <option value="2">Delete selected</option>
                                </select>
                                <button id="btnActionClosed" class="btn btn-sm btn-white">Apply</button>
                            </div>
                        </div>
                    </footer>
                </div>


            </div>
            <div class="tab-pane" id="messages">

                <div class="table-responsive">
                    <table id="tbl_fallen" class="table table-striped m-b-none">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th width="10%">TR ID</th>
                            <th width="15%">Listing Persons</th>
                            <th width="15%">Selling Persons</th>
                            <th width="27%">Property</th>
                            <th width="10%">Date</th>
                            <th width="10%">Status</th>
                            <th width="5%">&nbsp;</th>
                            <th width="5%">&nbsp;</th>
                        </tr>
                        </thead>
                        <?php

                        if(Yii::app()->user->role=='accounts' || Yii::app()->user->role == 'superuser'){
                            $listTR = Transactionreport::model()->findAll('markdelete<>1 AND status = 2');
                        }else{
                            $listTR  = Transactionreport::model()->findAll('(listeruserpersonid=' .$userid . ' OR selleruserpersonid='.$userid . ' OR createdbyid='.$userid.' ) AND status = 2 AND markdelete<>1');
                        }


                        if(count($listTR>0)){
                            echo '<tbody>';
                            foreach($listTR as $tr){

                                $status = ($tr->status=='1')?'Confirmed':'Pending';
                                echo '<tr>';
                                echo '<td><input class="lingfallen" type="checkbox" value="'.$tr->transactionreportid.'" style="width:50px !important;" name="post[]"></td>';

                                $style = 'style="color:blue !important;"';

                                if($tr->status=='1'){
                                    $style = 'style="color:green !important;"';
                                }else if ($tr->status=='2'){
                                    $style = 'style="color:orange !important;"';
                                }

                                echo '<td '.$style.' >TR-'.leading_zeros($tr->transactionreportid,8).'</td>';
                                $name ='';
                                if(count($tr->trlistingpersons)>0){
                                    foreach($tr->trlistingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }
                                echo '<td>'.$name.'</td>';
                                $name ='';
                                if(count($tr->trsellingpersons)>0){
                                    $name ='';
                                    foreach($tr->trsellingpersons as $person){
                                        if($person->type=='colleague'){
                                            $name .= $person->user->fullname .',';
                                        }else{
                                            $name.= $person->otherperson;
                                        }
                                    }
                                }

                                echo '<td>'.$name.'</td>';
                                echo '<td>'.$tr->address.'</td>';
                                echo '<td>'. date('d-m-Y',strtotime($tr->createdtime)) .'</td>';

                                $color = 'btn-danger';

                                echo '<td><a class="btn btn-danger btn-xs" href="javascript:void(0);" onclick="showStatus('.$tr->transactionreportid.');">View Status</a></td>';

                                if((Yii::app()->user->role == 'accounts' || Yii::app()->user->role == 'superuser')){
                                    echo '<td><a class="editrow btn btn-success btn-xs" href="'.$spreadsheet_url.'/transactionreportid/'.$tr->transactionreportid.'">Calculate</a> </td>';
                                }else{
                                    echo '<td></td>';
                                }

                                echo '<td align="center"> <a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/transactionreportid/'.$tr->transactionreportid.'">View</a> &nbsp;&nbsp;</td>';
                                echo '</tr>';
                            }
                            echo '</tbody>';
                        }
                        ?>


                    </table>

                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-4 hidden-xs">
                                <select id="selectAction" class="input-sm form-control input-s inline">
                                    <option value="2">Delete selected</option>
                                </select>
                                <button id="btnActionFallen" class="btn btn-sm btn-white">Apply</button>
                            </div>
                        </div>
                    </footer>
                </div>

            </div>
        </div>
    </div>


</section>
<a id="btnShow" data-backdrop="static"  name="btnShow" href="#dvStatus" data-toggle="modal" class="btn btn-success btn-sm" style="display:none;">s</a>

<div id="dvStatus" class="modal fade" data-role="" style="display:none;">
    <br />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="tbl_status" class="table table-striped m-b-none">
                        <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="tBodyDetails">

                        </tbody>
                    </table>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>