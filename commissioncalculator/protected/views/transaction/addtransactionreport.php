<?php

    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/js/transactionreport.js',CClientScript::POS_END);

?>

<div class="alert alert-danger">
    <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>
    <i class="fa fa-ban-circle"></i><strong>IMPORTANT:</strong> To be completed for every executed transaction with
    copy of agreement within 24 hours of completing a transaction.<br />
    Email: <a href="#">accounts@jameslaw.co.nz</a>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="fmrValidate" enctype="multipart/form-data" name="frmValidate" action="<?php echo CController::createURL("transaction/ConfirmTransaction"); ?>" method="post" data-validate="parsley">
        <a id="showConfirm" data-backdrop="static"  name="showConfirm" href="#dvConfirm" data-toggle="modal" class="btn btn-success btn-sm" style="display:none;">s</a>
            <input type="hidden" id="hdlistercolleague" name="hdlistercolleague" value="colleague" />
            <input type="hidden" id="hdsellercolleague" name="hdsellercolleague" value="colleague" />
            <input type="hidden" id="hdpowerteam" name="hdpowerteam" value="1" />
            <input type="hidden" id="hdpowerteamseller" name="hdpowerteamseller" value="1" />
            <input type="hidden" id="hduserid" name="hduserid" value="<?php echo Yii::app()->user->id; ?>" />

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">TRANSACTION REPORT</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Date</label>
                            <input type="text" id="txtDate" name="txtDate" data-date-format="dd-mm-yyyy" value="<?php echo date('d/m/Y'); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>
                        <div class="col-sm-3">
                            <label>Transaction Type</label><br />
                            <select class="form-control" data-required="true" id="selTranstype" name="selTranstype">
                                <option value="">Select Type</option>
                                <option value="1">Residential Sale</option>
                                <option value="2">Commercial Sale</option>
                                <option value="3">Commercial Lease</option>
                                <option value="4">Commercial Assignment of Lease</option>
                                <option value="5">Business Sale</option>
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <label>Sale / Lease Price</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesPrice" data-type="number" data-required="true" name="txtSalesPrice" class="form-control" placeholder="0.00">
                            </div>

                        </div>

                        <div class="col-sm-3">
                            <label>PTR</label>
                            <input type="text" id="txtPtr" name="txtPtr" data-required="true" class="form-control" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Address of Property/Business :</label>
                        <input type="text" id="txtAddressProperty" name="txtAddressProperty" data-required="true" class="form-control" placeholder="Address Of Property">
                    </div>

                    <div class="form-group pull-in clearfix">

                        <div class="col-sm-3">
                            <label>Date of Agreement</label>
                            <input type="text" id="txtDateAgreement" data-required="true" name="txtDateAgreement" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Unconditional Date</label>
                            <input type="text" id="txtDateUnconditional" name="txtDateUnconditional" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Possession Date</label>
                            <input type="text" id="txtDatePossession" name="txtDatePossession" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Settlement Date</label>
                            <input type="text" id="txtDateSettlement" name="txtDateSettlement" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">COMMISSION</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Total Commission Received (exc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" id="txtCommissionExcGST" name="txtCommissionExcGST" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Deposit (exc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" id="txtDepositExcGST" name="txtDepositExcGST" class="form-control" placeholder="0.00">
                            </div>


                        </div>

                        <div class="col-sm-6">
                            <label>Total Commission Received (inc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" id="txtCommissionIncGST" name="txtCommissionIncGST" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Deposit (inc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" id="txtDepositIncGST" name="txtDepositIncGST" class="form-control" placeholder="0.00">
                            </div>


                        </div>

                    </div>

                </div>
             </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">Lister/Seller Details</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">

                        <div class="col-sm-6">
                            <label>Lister Type</label>  &nbsp;&nbsp;
                            <input type="radio" checked="" value="option1" id="optionsColleague" name="optionsListertype">
                            Colleague
                            &nbsp;&nbsp;
                            <input type="radio" value="option2" id="optionsOther" name="optionsListertype">
                            Other Company

                            <span id="dvlistercolleagueopt">
                                <?php echo CHtml::dropDownList('selListerPerson',Yii::app()->user->id, CHtml::listData(Users::model()->findAll(), 'userid', 'fullname'), array('prompt'=>'Select Lister','class'=>"form-control")); ?>
                            </span>

                            <span id="dvlisterotheropt" style="display: none;">
                                <input type="text" id="txtListerOtherComp" name="txtListerOtherComp"  class="form-control" placeholder="Enter Company Name">
                                <input type="text"  id="txtListerOtherSalesPerson" name="txtListerOtherSalesPerson"  class="form-control m-t-sm" placeholder="Enter Salesperson Name">
                            </span>

                            <label class="m-t">Lister Share</label>
                            <div class="input-group  ">
                                <input type="text" id="txtListerShare"  name="txtListerShare" value="50" data-type="number"  class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>



                            <label class="m-t">Referred by POWERTEAM?</label>  &nbsp;&nbsp;

                            <input type="radio"  value="yes" id="optionsPowerteamyes" name="optionsPowerteam">
                            Yes
                            &nbsp;&nbsp;
                            <input type="radio" checked="checked" value="no" id="optionsPowerteamno" name="optionsPowerteam">
                            No

                        </div>

                        <div class="col-sm-6">
                            <label>Seller Type</label>  &nbsp;&nbsp;
                            <input type="radio" checked="" value="option1" id="optionsColleague" name="optionSellertype">
                            Colleague
                            &nbsp;&nbsp;
                            <input type="radio" value="option2" id="optionsOther" name="optionSellertype">
                            Other Company

                            <span id="dvsellercolleagueopt">
                                <?php echo CHtml::dropDownList('selSellerPerson',Yii::app()->user->id, CHtml::listData(Users::model()->findAll(), 'userid', 'fullname'), array('prompt'=>'Select Seller','class'=>"form-control")); ?>
                            </span>

                            <span id="dvsellerotheropt" style="display: none;">
                                <input type="text" id="txtSellerOtherComp" name="txtSellerOtherComp" class="form-control" placeholder="Enter Company Name">
                                <input type="text"  id="txtSellerOtherSalesPerson" name="txtSellerOtherSalesPerson"  class="form-control m-t-sm" placeholder="Enter Salesperson Name">
                            </span>
                            <label class="m-t">Seller Share</label>
                            <div class="input-group  ">
                                <input type="text"  id="txtSellerShare"  value="50"  data-type="number" name="txtSellerShare" class="form-control" placeholder="0.00" >
                                <span class="input-group-addon">%</span>
                            </div>

                            <label class="m-t">Referred by POWERTEAM?</label>  &nbsp;&nbsp;

                            <input type="radio"  value="yes" id="opselleryes" name="p1">
                            Yes
                            &nbsp;&nbsp;
                            <input type="radio" checked="checked" value="no" id="opsellerno" name="p1">
                            No

                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">REFERRAL AND CONJUNCTIONAL SPLIT</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Listing Referral %</label>
                            <div class="input-group  ">
                                <input type="text" value="0.00" data-type="number" id="txtListingReferralPerc" name="txtListingReferralPerc" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Listing Split Amount</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtListingSplitAmt" name="txtListingSplitAmt" class="form-control" placeholder="0.00">
                            </div>

                            <label class="m-t">Less Listing Referral Amount Exc GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtListingReferralAmt" name="txtListingReferralAmt" class="form-control" placeholder="0.00">
                            </div>

                            <label class="m-t">Listing Gross Brought In</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtListingGrossBroughtIn" name="txtListingGrossBroughtIn" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Listing Payable To</label>
                            Payable to powerteam@jameslaw.co.nz
                        </div>

                        <div class="col-sm-6">
                            <label >Selling Referral %</label>
                            <div class="input-group  ">
                                <input type="text" value="0.00" data-type="number" id="txtSellingReferralPerc" name="txtSellingReferralPerc" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Selling Split Amount</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtSellingSplitAmt" name="txtSellingSplitAmt" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Selling Listing Referral Amount Exc GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtSellingReferralAmt" name="txtSellingReferralAmt" class="form-control" placeholder="0.00">
                            </div>

                            <label class="m-t">Selling Gross Brought In</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="0.00" data-type="number" readonly="readonly" id="txtSellingGrossBroughtIn" name="txtSellingGrossBroughtIn" class="form-control" placeholder="0.00">
                            </div>

                            <label class="m-t">Selling Payable To</label>
                            Payable to powerteam@jameslaw.co.nz
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label >Conjunctional Split (if any)</label>
                            <input type="text" id="txtConjunctionalSplit" name="txtConjunctionalSplit" class="form-control" placeholder="0.00">

                        </div>

                        <div class="col-sm-6">
                            <label >Conjunctional Company, Salesperson & Postal Address:</label>
                            <input type="text" id="txtConjunctionalComp" name="txtConjunctionalComp" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">VENDOR | LANDLORD | ASSIGNOR - DETAILS</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Type</label><br />
                            <select class="form-control" data-required="true" id="selVendorType" name="selVendorType">
                                <option value="">Select Type</option>
                                <option value="1">Vendor</option>
                                <option value="2">Lanlord</option>
                                <option value="3">Assignor</option>
                            </select>
                        </div>

                        <div class="col-sm-8">
                            <label>Legal/Entity Name</label>
                            <input type="text" id="txtVendorLegalName" name="txtVendorLegalName" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Contact Name</label><br />
                            <input type="text" id="txtVendorContactName" name="txtVendorContactName" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Contact No.</label>
                            <input type="text" id="txtVendorContactNo" name="txtVendorContactNo" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>Fax</label>
                            <input type="text" id="txtVendorFax" name="txtVendorFax" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Email</label>
                            <input type="text" id="txtVendorEmail" name="txtVendorEmail" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Street</label><br />
                            <input type="text" id="txtVendorStreet" name="txtVendorStreet" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Suburb</label>
                            <input type="text" id="txtVendorSuburb" name="txtVendorSuburb" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>City</label>
                            <input type="text" id="txtVendorCity" name="txtVendorCity" value="Auckland" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Country</label>
                            <select class="form-control" id="selVendorCountry" name="selVendorCountry">
                                <option value="-1">Select Country</option>
                                <option value="1">New Zealand</option>
                                <option value="1">Australia</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Vendors Lawyer</label><br />
                            <input type="text" id="txtVendorLawfirm" name="txtVendorLawfirm" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Lawyers First Name</label><br />
                            <input type="text" id="txtVendorLawyerFname" name="txtVendorLawyerFname" class="form-control" >
                        </div>

                        <div class="col-sm-6">
                            <label>Lawyers Last Name</label>
                            <input type="text" id="txtVendorLawyerLname" name="txtVendorLawyerLname" class="form-control">
                        </div>

                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Lawyers Email</label><br />
                            <input type="text" id="txtVendorLawyerEmail" name="txtVendorLawyerEmail" class="form-control" >
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Phone</label>
                            <input type="text" id="txtVendorLawyerPhone" name="txtVendorLawyerPhone" class="form-control">
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Fax (Include Area Code)</label>
                            <input type="text" id="txtVendorLawyerFax" name="txtVendorLawyerFax" class="form-control">
                        </div>

                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">PURCHASER | TENANT | ASSIGNEE - DETAILS</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Type</label><br />
                            <select class="form-control" data-required="true" id="selPurchaserType" name="selPurchaserType">
                                <option value="">Select Type</option>
                                <option value="1">Purchaser</option>
                                <option value="2">Tenant</option>
                                <option value="3">Assignee</option>
                            </select>
                        </div>

                        <div class="col-sm-8">
                            <label>Legal/Entity Name</label>
                            <input type="text" id="txtPurchaserLegalName" name="txtPurchaserLegalName" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Contact Name</label><br />
                            <input type="text" id="txtPurchaserContactName" name="txtPurchaserContactName" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Contact No.</label>
                            <input type="text" id="txtPurchaserContactNo" name="txtPurchaserContactNo" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>Fax</label>
                            <input type="text" id="txtPurchaserFax" name="txtPurchaserFax" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Email</label>
                            <input type="text" id="txtPurchaserEmail" name="txtPurchaserEmail" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Street</label><br />
                            <input type="text" id="txtPurchaserStreet" name="txtPurchaserStreet" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Suburb</label>
                            <input type="text" id="txtPurchaserSuburb" name="txtPurchaserSuburb" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>City</label>
                            <input type="text" id="txtPurchaserCity" name="txtPurchaserCity" value="Auckland" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Country</label>
                            <select class="form-control" id="selPurchaserCountry" name="selPurchaserCountry">
                                <option value="-1">Select Country</option>
                                <option value="1">New Zealand</option>
                                <option value="1">Australia</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Purchasers Lawyer</label><br />
                            <input type="text" id="txtPurchaserLawfirm" name="txtPurchaserLawfirm" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Lawyers First Name</label><br />
                            <input type="text" id="txtPurchaserLawyerFname" name="txtPurchaserLawyerFname" class="form-control" >
                        </div>

                        <div class="col-sm-6">
                            <label>Lawyers Last Name</label>
                            <input type="text" id="txtPurchaserLawyerLname" name="txtPurchaserLawyerLname" class="form-control">
                        </div>

                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Lawyers Email</label><br />
                            <input type="text" id="txtPurchaserLawyerEmail" name="txtPurchaserLawyerEmail" class="form-control" >
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Phone</label>
                            <input type="text" id="txtPurchaserLawyerPhone" name="txtPurchaserLawyerPhone" class="form-control">
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Fax (Include Area Code)</label>
                            <input type="text" id="txtPurchaserLawyerFax" name="txtPurchaserLawyerFax" class="form-control">
                        </div>

                    </div>
                </div>
            </section>


            <h4 class="m-t">TRANSACTION NOTES</h4>
            <div class="alert alert-info">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>
                <i class="fa fa-info-sign"></i>Record here the sequence of events, appointments, inspections and representations you made throughout the
                transaction.
            </div>




            <div class="row">

                <div class="col-sm-6">
                    <section class="panel">
                        <div class="panel-body">

                            <input type='hidden' id="ispropertyinspected" name="ispropertyinspected" value="1"/>
                            <input type='hidden' id="ispotentialproblems" name="ispotentialproblems" value="0"/>
                            <input type='hidden' id="ispurchasewarranty" name="ispurchasewarranty" value="0"/>
                            <input type='hidden' id="isvendorwarranty" name="isvendorwarranty" value="0"/>
                            <input type='hidden' id="isgstdiscussion" name="isgstdiscussion" value="0"/>
                            <input type='hidden' id="isadvicesought" name="isadvicesought" value="0"/>
                            <input type='hidden' id="ispurchaserbroughtproperty" name="ispurchaserbroughtproperty" value="0"/>
                            <input type='hidden' id="isviewedandappraised" name="isviewedandappraised" value="0"/>
                            <input type='hidden' id="isdifferenceopinon" name="isdifferenceopinon" value="0"/>
                            <input type='hidden' id="iscommissiondiscussed" name="iscommissiondiscussed" value="0"/>
                            <input type='hidden' id="isvendorincludechattel" name="isvendorincludechattel" value="0"/>
                            <input type='hidden' id="isreferaccountant" name="isreferaccountant" value="0"/>


                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-12">

                                    <span>Was the property inspected on site inside and out by Purchaser/Tenant</span>  <br />
                                    <input type="radio" checked="" value="yes" name="optionIspropertyinspected">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" value="no" name="optionIspropertyinspected">
                                    No

                                    <span id="dvIspropertyinspected" style="display:none;">
                                        <input type="text" id="txtIspropertyinspected" name="txtIspropertyinspected" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Dates property shown to Purchaser /Tenant and/or others (include names with times):</span>  <br />

                                    <input type="text" id="txtDateShown" name="txtDateShown" class="form-control m-t-sm">


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Statements made / discussions / questions and your answers,about the boundaries, titles (including Cross Lease / Body Corp)</span>  <br />

                                    <textarea  id="txtStatemenBoundaries" name="txtStatemenBoundaries"  rows="2" class="form-control parsley-validated"></textarea>


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Statements made/discussions and your answers about the construction, piles, electrical, cladding, roofing, insulation,heating, sewage/water reticulation, drainage, fencing</span>  <br />

                                    <textarea  id="txtStatementConstruction" name="txtStatementConstruction"  rows="2" class="form-control parsley-validated"></textarea>


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Were any problems / potential problems / problem areas discussed? Specify:</span>  <br />

                                    <input type="radio"  value="yes" name="optionIspotentialproblems">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no" name="optionIspotentialproblems">
                                    No

                                    <span id="dvIspotentialproblems" style="display:none;">
                                        <input type="text" id="txtIspotentialproblems" name="txtIspotentialproblems" class="form-control m-t-sm">
                                    </span>


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Were any specific items discussed where you gave advice? Specify:</span>  <br />

                                    <input type="text" id="txtSpecificItems" name="txtSpecificItems" class="form-control m-t-sm">


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>SALE ONLY: Vendor Warranty explained to: Purchaser:</span>  <br />
                                    <input type="radio"  value="yes" name="optionIsPurchaserWarranty">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsPurchaserWarranty">
                                    No

                                    <span id="dvIsPurchaserWarranty" style="display: none;">
                                        <input type="text" id="txtIsPurchaserWarranty" name="txtIsPurchaserWarranty" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>SALE ONLY: Vendor Warranty explained to: Vendor</span>  <br />
                                    <input type="radio"  value="yes" name="optionIsVendorWarranty">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no" name="optionIsVendorWarranty">
                                    No

                                    <span id="dvIsVendorWarranty" style="display: none;">
                                        <input type="text" id="txtIsVendorWarranty" name="txtIsVendorWarranty" class="form-control m-t-sm">
                                    </span>
                                </div>



                                <div class="col-sm-12 m-t">

                                    <span>GST discussion</span>  <br />
                                    <input type="radio"  value="yes"  name="optionIsGSTDiscussion">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no" name="optionIsGSTDiscussion">
                                    No

                                    <span id="dvIsGSTDiscussion" style="display:none;">
                                        <input type="text"  id="txtIsGSTDiscussion" name="txtIsGSTDiscussion" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Was any advice sought?</span>  <br />
                                    <input type="radio"  value="yes"  name="optionIsAdviceSought">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsAdviceSought">
                                    No

                                    <span id="dvIsAdviceSought" style="display: none;">
                                        <input type="text" id="txtoptionIsAdviceSought" name="txtoptionIsAdviceSought" class="form-control m-t-sm">
                                    </span>
                                </div>





                            </div>

                        </div>
                    </section>
                </div>

                <div class="col-sm-6">
                    <section class="panel">
                        <div class="panel-body">
                            <div class="form-group pull-in clearfix">

                                <div class="col-sm-12 m-t">

                                    <span>If the Purchaser has bought the property subject to selling his/her own property did you guarantee / lead them to believe
that we / you could obtain a certain price and thus possibly
entice them to purchase?</span>  <br />
                                    <input type="radio"  value="yes" name="optionIsPurchaserBroughtProperty">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsPurchaserBroughtProperty">
                                    No

                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Have you viewed and appraised the Purchaser’s property?</span>  <br />
                                    <input type="radio"  value="yes" name="optionIsViewedAndAppraised">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no" name="optionoptionIsViewedAndAppraised">
                                    No
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Did you at any time have a difference of opinion with the Vendor/Landlord over any matter whatsoever? If yes describe</span>  <br />
                                    <input type="radio"  value="yes"  name="optionIsDifferenceInOpinion">
                                    Yes
                                    &nbsp;&nbsp;
                                        <input type="radio" checked="" value="no"  name="optionIsDifferenceInOpinion">
                                    No

                                    <span id="dvIsDifferenceInOpinion" style="display: none;">
                                        <input type="text" id="txtIsDifferenceInOpinion" name="txtIsDifferenceInOpinion" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Commission discussed</span>  <br />
                                    <input type="radio"  value="yes"  name="optionIsCommissionDiscussed">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsCommissionDiscussed">
                                    No

                                    <span id="dvIsCommissionDiscussed" style="display: none;">
                                        <input type="text" id="txtIsCommissionDiscussed" name="txtIsCommissionDiscussed" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Did the Vendor exclude any chattels from the sale?</span>  <br />
                                    <input type="radio"  value="yes"  name="optionIsVendorIncludeChattel">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsVendorIncludeChattel">
                                    No

                                    <span id="dvIsVendorIncludeChattel" style="display: none;">
                                        <input type="text" id="txtIsVendorIncludeChattel" name="txtIsVendorIncludeChattel" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Did you refer the Purchaser / Vendor to their solicitor / accountant for clarification of any matters? If yes describe?</span>  <br />
                                    <input type="radio"  value="yes" name="optionIsReferAccountant">
                                    Yes
                                    &nbsp;&nbsp;
                                    <input type="radio" checked="" value="no"  name="optionIsReferAccountant">
                                    No

                                    <span id="dvIsReferAccountant" style="display: none;">
                                        <input type="text" id="txtIsReferAccountant" name="txtIsReferAccountant" class="form-control m-t-sm">
                                    </span>
                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Describe what was discussed regarding any development potential, or re-sale value</span>  <br />

                                    <span>Vendor</span>
                                    <input type="text" id="txtVendorDescribeSaleValue" name="txtVendorDescribeSaleValue" class="form-control m-t-sm">

                                    <span>Purchaser</span>
                                    <input type="text" id="txtPurchaserDescribeSaleValue" name="txtPurchaserDescribeSaleValue" class="form-control m-t-sm">


                                </div>

                                <div class="col-sm-12 m-t">

                                    <span>Note any other comments / relevant points</span>  <br />

                                    <textarea  id="txtOtherComments" name="txtOtherComments"  rows="2" class="form-control"></textarea>


                                </div>

                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <section class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-white" id="btnCancel" type="button">Cancel</button>
                            <button class="btn btn-primary" id="btnConfirm" type="button">Confirm</button>
                        </div>
                    </div>

                </div>
            </section>

            <div id="dvConfirm" class="modal fade" style="display:none;">
                <br />
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group in-line">
                                    <h4>
                                        I Hereby certify that these statements are a true record of the main,
                                        dealings that took place regarding the above property and that these notes
                                        have been made within 48 hours of the completion of the contract.
                                    </h4>

                                </div>

                            </div>

                            <div class="row">

                                <div class="container-fluid bg-info" style="padding: 10px;">
                                    <div class="row-fluid">

                                        <div class="col-lg-6">
                                            <label style="display: block;">Transaction Type : <span id="sumtrtype" ></span></label>
                                            <label style="display: block;">Address/Property : <span id="sumtradress" ></span></label>
                                            <label style="display: block;">Listing Person : <span id="sumtrlistperson" ></span></label>
                                            <label style="display: block;">Lister Share : <span id="sumtrlistshare" ></span></label>
                                            <label style="display: block;">PTR : <span id="sumtrptr" ></span></label>

                                        </div>
                                        <div class="col-lg-6" >

                                            <label style="display: block;">Sale Price : <span id="sumtrsaleprice"></span></label>
                                            <label style="display: block;">Selling Person : <span id="sumtrsalesperson"></span></label>
                                            <label style="display: block;">Seller Share : <span id="sumtrsellershare"></span></label>
                                            <label style="display: block;">Agreement Date : <span id="sumtraggrdate"></span></label>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="form-group in-line">
                                    <label class="col-lg-4 control-label m-t-sm">Type "AGREE" to confirm</label>
                                    <div class="col-lg-8">
                                        <input type="text" id="txtConfirmAgree"  class="form-control" name="txtConfirmAgree"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="form-group in-line">
                                    <label class="col-lg-4 control-label m-t-sm">Upload Agreement</label>
                                    <div class="col-lg-8">
                                        <input type="file" class="form-control" id="uploadAgreement"  class="form-control" name="uploadAgreement"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="form-group in-line">
                                    <label class="col-lg-4 control-label m-t-sm">Upload Transaction Report (written)</label>
                                    <div class="col-lg-8">
                                        <input type="file" class="form-control" id="uploadTransactionReport"  class="form-control" name="uploadTransactionReport"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="form-group in-line">
                                    <label class="col-lg-4 control-label m-t-sm">Upload Other Documents</label>
                                    <div class="col-lg-8">
                                        <input type="file" class="form-control" id="uploadOther1"  class="form-control" name="uploadOther1"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-t">
                                <div class="form-group in-line">
                                    <label class="col-lg-4 control-label m-t-sm">Upload Other Documents</label>
                                    <div class="col-lg-8">
                                        <input type="file" class="form-control" id="uploadOther2"  class="form-control" name="uploadOther2"/>
                                    </div>
                                </div>
                            </div>

                            <div class="line line-dashed line-lg pull-in"></div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label m-t-sm" style="text-align:right;">&nbsp;</label>
                                    <button type="button" id="btnFinalConfirm"  class="btn btn-s-md btn-success">
                                        Confirm
                                    </button> &nbsp;

                                    <button id="closemodal" name="closemodal"  class="btn btn-white" type="button" data-dismiss="modal" style="padding:6px 42px;">Close</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="dvConfirm2" class="modal fade" style="display:none;">
    <br />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                test
            </div>
        </div>
    </div>
</div>
