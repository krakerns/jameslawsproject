<?php

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/transactionreport.js',CClientScript::POS_END);
//$model = new Transactionreport();
$vendor = Trvendorlandlordassignor::model()->find('transactionreportid=' . $model->transactionreportid);
$purchaser = Trpurchasertenantasignee::model()->find('transactionreportid=' . $model->transactionreportid);
$notes =Trnotes::model()->find('transactionreportid='.$model->transactionreportid);
$confirm = Trconfirmation::model()->find('transactionreportid='.$model->transactionreportid);
//$notes = new Trnotes();
?>

<div class="alert alert-danger">
    <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>
    <i class="fa fa-ban-circle"></i><strong>IMPORTANT:</strong> To be completed for every executed transaction with
    copy of agreement within 24 hours of completing a transaction.<br />
    Email: <a href="#">accounts@jameslaw.co.nz</a>
</div>

<div class="row">
    <div class="col-sm-12">
        <form id="fmrValidate" enctype="multipart/form-data" name="frmValidate" action="<?php echo CController::createURL("transaction/Updateconfirm"); ?>" method="post" data-validate="parsley">
            <a id="showConfirm" data-backdrop="static"  name="showConfirm" href="#dvConfirm" data-toggle="modal" class="btn btn-success btn-sm" style="display:none;">s</a>
            <input type="hidden" id="hdlistercolleague" name="hdlistercolleague" value="<?php echo $model->listertype; ?>" />
            <input type="hidden" id="hdsellercolleague" name="hdsellercolleague" value="<?php echo $model->sellertype; ?>" />
            <input type="hidden" id="hdpowerteam" name="hdpowerteam" value="<?php echo $model->ispowerteam; ?>" />
            <input type="hidden" id="hdpowerteamseller" name="hdpowerteamseller" value="<?php echo $model->ispowerteamseller; ?>" />
            <input type="hidden" id="hduserid" name="hduserid" value="<?php echo Yii::app()->user->id; ?>" />
            <input type="hidden" id="hdtransactionreportid" name="hdtransactionreportid" value="<?php echo $model->transactionreportid; ?>" />

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">TRANSACTION REPORT</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Date</label>
                            <input type="text" id="txtDate" name="txtDate" data-date-format="dd-mm-yyyy" value="<?php echo date('d/m/Y',strtotime($model->createdtime)); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>
                        <div class="col-sm-3">
                            <label>Transaction Type</label><br />
                            <?php echo CHtml::dropDownList('selTranstype',$model->transactiontype, $model->listTransactiontype(), array('prompt'=>'Select Type','class'=>"form-control")); ?>

                        </div>

                        <div class="col-sm-3">
                            <label>Sale / Lease Price</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" value="<?php echo $model->saleleaseprice; ?>" id="txtSalesPrice" data-type="number" data-required="true" name="txtSalesPrice" class="form-control" placeholder="0.00">
                            </div>

                        </div>

                        <div class="col-sm-3">
                            <label>PTR</label>
                            <input type="text" value="<?php echo $model->ptdrnumber; ?>" id="txtPtr" name="txtPtr" data-required="true" class="form-control" placeholder="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Address of Property/Business :</label>
                        <input type="text" value="<?php echo $model->address; ?>" id="txtAddressProperty" name="txtAddressProperty" data-required="true" class="form-control" placeholder="Address Of Property">
                    </div>

                    <div class="form-group pull-in clearfix">

                        <div class="col-sm-3">
                            <label>Date of Agreement</label>
                            <input value="<?php echo date('d/m/Y',strtotime($model->agreementdate)); ?>" type="text" id="txtDateAgreement" data-required="true" name="txtDateAgreement" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Unconditional Date</label>
                            <input value="<?php echo date('d/m/Y',strtotime($model->unconditionaldate)); ?>" type="text" id="txtDateUnconditional" name="txtDateUnconditional" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Possession Date</label>
                            <input value="<?php echo date('d/m/Y',strtotime($model->posessiondate)); ?>" type="text" id="txtDatePossession" name="txtDatePossession" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>

                        <div class="col-sm-3">
                            <label>Settlement Date</label>
                            <input value="<?php echo date('d/m/Y',strtotime($model->settlementdate)); ?>" type="text" id="txtDateSettlement" name="txtDateSettlement" data-date-format="dd-mm-yyyy" class="form-control datepicker-input">
                        </div>
                    </div>

                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">COMMISSION</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Total Commission Received (exc GST)</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->commissionamtexcGST; ?>" id="txtCommissionExcGST" name="txtCommissionExcGST" class="form-control" placeholder="0.00">
                            </div>

                            <label>Deposit (inc GST)</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->commissiondepositamtincGST; ?>" id="txtDepositIncGST" name="txtDepositIncGST" class="form-control" placeholder="0.00">
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <label>Total Commission Received (inc GST)</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->commissionamtincGST; ?>" id="txtCommissionIncGST" name="txtCommissionIncGST" class="form-control" placeholder="0.00">
                            </div>


                            <label>Deposit (exc GST)</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->commissiondepositamtexcGST; ?>" id="txtDepositExcGST" name="txtDepositExcGST" class="form-control" placeholder="0.00">
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">Lister/Seller Details</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">

                        <div class="col-sm-6">
                            <label>Lister Type</label>  &nbsp;&nbsp;
                            <input type="radio"  <?php echo ($model->listertype=='colleague')?"checked=checked":""; ?>  value="option1" id="optionsColleague" name="optionsListertype">
                            Colleague
                            &nbsp;&nbsp;
                            <input type="radio"  <?php echo ($model->listertype=='other')?"checked=checked":""; ?>  value="option2" id="optionsOther" name="optionsListertype">
                            Other Company

                                        <span id="dvlistercolleagueopt">
                                            <?php echo CHtml::dropDownList('selListerPerson',$model->listeruserpersonid, CHtml::listData(Users::model()->findAll(), 'userid', 'fullname'), array('prompt'=>'Select Lister','class'=>"form-control")); ?>
                                        </span>

                                        <span id="dvlisterotheropt" style="display: none;">
                                            <input type="text" id="txtListerOtherComp" value="<?php echo $model->listerotherperson; ?>" name="txtListerOtherComp" class="form-control" placeholder="Enter Company Name">
                                            <input type="text" id="txtListerOtherSalesperson" value="<?php echo $model->listerotherpersonname; ?>" name="txtListerOtherSalesperson" class="form-control m-t-sm" placeholder="Enter Salesperson Name">
                                        </span>

                            <label class="m-t">Lister Share</label>
                            <div class="input-group">
                                <input type="text" data-type="number" value="<?php echo $model->listershare; ?>" id="txtListerShare" name="txtListerShare" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Referred by POWERTEAM?</label>  &nbsp;&nbsp;

                            <input type="radio" <?php echo ($model->ispowerteam=='1')?"checked=checked":""; ?>   value="yes" id="optionsPowerteamyes" name="optionsPowerteam">
                            Yes
                            &nbsp;&nbsp;
                            <input type="radio" <?php echo ($model->ispowerteam=='0')?"checked=checked":""; ?>  value="no" id="optionsPowerteamno" name="optionsPowerteam">
                            No
                        </div>

                        <div class="col-sm-6">
                            <label>Seller Type</label>  &nbsp;&nbsp;
                            <input type="radio" <?php echo ($model->sellertype=='colleague')?"checked=checked":""; ?>  value="option1" id="optionsColleague" name="optionSellertype">
                            Colleague
                            &nbsp;&nbsp;
                            <input type="radio" <?php echo ($model->sellertype=='other')?"checked=checked":""; ?>  value="option2" id="optionsOther" name="optionSellertype">
                            Other Company

                                        <span id="dvsellercolleagueopt">
                                            <?php echo CHtml::dropDownList('selSellerPerson',$model->selleruserpersonid, CHtml::listData(Users::model()->findAll(), 'userid', 'fullname'), array('prompt'=>'Select Seller','class'=>"form-control")); ?>
                                        </span>

                                        <span id="dvsellerotheropt" style="display: none;">
                                            <input value="<?php echo $model->sellerotherperson; ?>" type="text" id="txtSellerOtherComp" name="txtSellerOtherComp" class="form-control" placeholder="Enter Company Name">
                                            <input type="text" id="txtSellerSalesperson" value="<?php echo $model->sellerotherpersonname; ?>" name="txtSellerOtherSalesperson" class="form-control m-t-sm" placeholder="Enter Salesperson Name">
                                        </span>
                            <label class="m-t">Seller Share</label>
                            <div class="input-group">
                                <input type="text" data-type="number" value="<?php echo $model->sellershare; ?>" id="txtSellerShare" name="txtSellerShare" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Referred by POWERTEAM?</label>  &nbsp;&nbsp;

                            <input type="radio" <?php echo ($model->ispowerteamseller=='1')?"checked=checked":""; ?>   value="yes" id="opselleryes" name="p1">
                            Yes
                            &nbsp;&nbsp;
                            <input type="radio" <?php echo ($model->ispowerteamseller=='0')?"checked=checked":""; ?>  value="no" id="opsellerno" name="p1">
                        </div>
                    </div>
                </div>
            </section>


            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">REFERRAL AND CONJUNCTIONAL SPLIT</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Listing Referral %</label>
                            <div class="input-group">
                                <input type="text" data-type="number" value="<?php echo $model->listingreferralperc; ?>" id="txtListingReferralPerc" name="txtListingReferralPerc" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Listing Split Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->listingsplitamt; ?>" id="txtListingSplitAmt" name="txtListingSplitAmt" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Less Listing Referral Amount Exc GST</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" data-type="number" value="<?php echo $model->listingreferralamt; ?>" id="txtListingReferralAmt" name="txtListingReferralAmt" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Listing Gross Brought In</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->listinggrossbroughtin; ?>" id="txtListingGrossBroughtIn" name="txtListingGrossBroughtIn" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Listing Payable To</label>
                            Payable to powerteam@jameslaw.co.nz
                        </div>

                        <div class="col-sm-6">
                            <label >Selling Referral %</label>
                            <div class="input-group">
                                <input type="text" data-type="number" value="<?php echo $model->sellingreferralperc; ?>" id="txtSellingReferralPerc" name="txtSellingReferralPerc" class="form-control" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>


                            <label class="m-t">Selling Split Amount</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->sellingsplitamt; ?>" id="txtSellingSplitAmt" name="txtSellingSplitAmt" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Selling Listing Referral Amount Exc GST</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->sellingreferralamt; ?>"  id="txtSellingReferralAmt" name="txtSellingReferralAmt" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Selling Gross Brought In</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" data-type="number" value="<?php echo $model->sellinggrossbroughtin; ?>" id="txtSellingGrossBroughtIn" name="txtSellingGrossBroughtIn" class="form-control" placeholder="0.00">
                            </div>


                            <label class="m-t">Selling Payable To</label>
                            Payable to powerteam@jameslaw.co.nz
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label >Conjunctional Split (if any)</label>
                            <input type="text" value="<?php echo $model->conjuctionalothersplit; ?>" id="txtConjunctionalSplit" name="txtConjunctionalSplit" class="form-control" placeholder="0.00">

                        </div>

                        <div class="col-sm-6">
                            <label >Conjunctional Company, Salesperson & Postal Address:</label>
                            <input type="text" value="<?php echo $model->conjuctionalothercompany; ?>" id="txtConjunctionalComp" name="txtConjunctionalComp" class="form-control" placeholder="">
                        </div>
                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">VENDOR | LANDLORD | ASSIGNOR - DETAILS</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Type</label><br />
                            <?php echo CHtml::dropDownList('selVendorType',$vendor->type, $vendor->listType(), array('prompt'=>'Select Type','class'=>"form-control")); ?>
                        </div>

                        <div class="col-sm-8">
                            <label>Legal/Entity Name</label>
                            <input type="text" value="<?php echo $vendor->legalentity; ?>" id="txtVendorLegalName" name="txtVendorLegalName" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Contact Name</label><br />
                            <input type="text" value="<?php echo $vendor->contactname; ?>" id="txtVendorContactName" name="txtVendorContactName" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Contact No.</label>
                            <input type="text" value="<?php echo $vendor->contactnumber; ?>" id="txtVendorContactNo" name="txtVendorContactNo" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>Fax</label>
                            <input type="text" value="<?php echo $vendor->faxnumber; ?>" id="txtVendorFax" name="txtVendorFax" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Email</label>
                            <input type="text" value="<?php echo $vendor->email; ?>" id="txtVendorEmail" name="txtVendorEmail" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Street</label><br />
                            <input type="text" value="<?php echo $vendor->streetaddress; ?>" id="txtVendorStreet" name="txtVendorStreet" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Suburb</label>
                            <input type="text" value="<?php echo $vendor->suburb; ?>" id="txtVendorSuburb" name="txtVendorSuburb" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>City</label>
                            <input type="text" value="<?php echo $vendor->city; ?>" id="txtVendorCity" name="txtVendorCity" value="Auckland" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Country</label>
                            <select class="form-control" id="selVendorCountry" name="selVendorCountry">
                                <option value="-1">Select Country</option>
                                <option value="1">New Zealand</option>
                                <option value="1">Australia</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Vendors Lawyer</label><br />
                            <input type="text" value="<?php echo $vendor->solicitorsfirm; ?>" id="txtVendorLawfirm" name="txtVendorLawfirm" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Lawyers First Name</label><br />
                            <input type="text" value="<?php echo $vendor->individualactingfirstname; ?>" id="txtVendorLawyerFname" name="txtVendorLawyerFname" class="form-control" >
                        </div>

                        <div class="col-sm-6">
                            <label>Lawyers Last Name</label>
                            <input type="text" value="<?php echo $vendor->individualactinglastname; ?>" id="txtVendorLawyerLname" name="txtVendorLawyerLname" class="form-control">
                        </div>

                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Lawyers Email</label><br />
                            <input type="text" value="<?php echo $vendor->individualactingemail; ?>" id="txtVendorLawyerEmail" name="txtVendorLawyerEmail" class="form-control" >
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Phone</label>
                            <input type="text" value="<?php echo $vendor->individualactingphonenumber; ?>" id="txtVendorLawyerPhone" name="txtVendorLawyerPhone" class="form-control">
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Fax (Include Area Code)</label>
                            <input type="text" value="<?php echo $vendor->individualactingfaxnumber; ?>" id="txtVendorLawyerFax" name="txtVendorLawyerFax" class="form-control">
                        </div>

                    </div>
                </div>
            </section>

            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">PURCHASER | TENANT | ASSIGNEE - DETAILS</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Type</label><br />
                            <?php echo CHtml::dropDownList('selPurchaserType',$purchaser->type, $purchaser->listType(), array('prompt'=>'Select Type','class'=>"form-control")); ?>

                        </div>

                        <div class="col-sm-8">
                            <label>Legal/Entity Name</label>
                            <input type="text" value="<?php echo $purchaser->legalentity; ?>" id="txtPurchaserLegalName" name="txtPurchaserLegalName" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Contact Name</label><br />
                            <input type="text" value="<?php echo $purchaser->contactname; ?>" id="txtPurchaserContactName" name="txtPurchaserContactName" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Contact No.</label>
                            <input type="text" value="<?php echo $purchaser->contactnumber; ?>" id="txtPurchaserContactNo" name="txtPurchaserContactNo" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>Fax</label>
                            <input type="text" value="<?php echo $purchaser->faxnumber; ?>" id="txtPurchaserFax" name="txtPurchaserFax" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Email</label>
                            <input type="text" value="<?php echo $purchaser->email; ?>" id="txtPurchaserEmail" name="txtPurchaserEmail" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-3">
                            <label>Street</label><br />
                            <input type="text" value="<?php echo $purchaser->streetaddress; ?>" id="txtPurchaserStreet" name="txtPurchaserStreet" class="form-control" >
                        </div>

                        <div class="col-sm-3">
                            <label>Suburb</label>
                            <input type="text" value="<?php echo $purchaser->suburb; ?>" id="txtPurchaserSuburb" name="txtPurchaserSuburb" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>City</label>
                            <input type="text" value="<?php echo $purchaser->city; ?>" id="txtPurchaserCity" name="txtPurchaserCity" value="Auckland" class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            <label>Country</label>
                            <select class="form-control" id="selPurchaserCountry" name="selPurchaserCountry">
                                <option value="-1">Select Country</option>
                                <option value="1">New Zealand</option>
                                <option value="1">Australia</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12">
                            <label>Purchasers Lawyer</label><br />
                            <input type="text" value="<?php echo $purchaser->solicitorsfirm; ?>" id="txtPurchaserLawfirm" name="txtPurchaserLawfirm" class="form-control" >
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Lawyers First Name</label><br />
                            <input type="text" value="<?php echo $purchaser->individualactingfirstname; ?>" id="txtPurchaserLawyerFname" name="txtPurchaserLawyerFname" class="form-control" >
                        </div>

                        <div class="col-sm-6">
                            <label>Lawyers Last Name</label>
                            <input type="text" value="<?php echo $purchaser->individualactinglastname; ?>" id="txtPurchaserLawyerLname" name="txtPurchaserLawyerLname" class="form-control">
                        </div>

                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Lawyers Email</label><br />
                            <input type="text" value="<?php echo $purchaser->individualactingemail; ?>" id="txtPurchaserLawyerEmail" name="txtPurchaserLawyerEmail" class="form-control" >
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Phone</label>
                            <input type="text" value="<?php echo $purchaser->individualactingphonenumber; ?>" id="txtPurchaserLawyerPhone" name="txtPurchaserLawyerPhone" class="form-control">
                        </div>

                        <div class="col-sm-4">
                            <label>Lawyers Fax (Include Area Code)</label>
                            <input type="text" value="<?php echo $purchaser->individualactingfaxnumber; ?>" id="txtPurchaserLawyerFax" name="txtPurchaserLawyerFax" class="form-control">
                        </div>

                    </div>
                </div>
            </section>


            <h4 class="m-t">TRANSACTION NOTES</h4>
            <div class="alert alert-info">
                <button data-dismiss="alert" class="close" type="button"><i class="fa fa-times"></i></button>
                <i class="fa fa-info-sign"></i>Record here the sequence of events, appointments, inspections and representations you made throughout the
                transaction.
            </div>




            <div class="row">

            <div class="col-sm-6">
                <section class="panel">
                    <div class="panel-body">

                        <input type='hidden' id="ispropertyinspected" name="ispropertyinspected" value="<?php echo $notes->ispropertyinspected; ?>"/>
                        <input type='hidden' id="ispotentialproblems" name="ispotentialproblems" value="<?php echo $notes->ispotentialproblems; ?>"/>
                        <input type='hidden' id="ispurchasewarranty" name="ispurchasewarranty" value="<?php echo $notes->ispurchaserwarranty; ?>"/>
                        <input type='hidden' id="isvendorwarranty" name="isvendorwarranty" value="<?php echo $notes->isvendorwarranty; ?>"/>
                        <input type='hidden' id="isgstdiscussion" name="isgstdiscussion" value="<?php echo $notes->isGSTdiscussion; ?>"/>
                        <input type='hidden' id="isadvicesought" name="isadvicesought" value="<?php echo $notes->isadvicesought; ?>"/>
                        <input type='hidden' id="ispurchaserbroughtproperty" name="ispurchaserbroughtproperty" value="<?php echo $notes->ispurchaserboughtproperty; ?>"/>
                        <input type='hidden' id="isviewedandappraised" name="isviewedandappraised" value="<?php echo $notes->isviewedandappraised; ?>"/>
                        <input type='hidden' id="isdifferenceopinon" name="isdifferenceopinon" value="<?php echo $notes->isdifferenceopinion; ?>"/>
                        <input type='hidden' id="iscommissiondiscussed" name="iscommissiondiscussed" value="<?php echo $notes->iscommissiondiscussed; ?>"/>
                        <input type='hidden' id="isvendorincludechattel" name="isvendorincludechattel" value="<?php echo $notes->isvendorincludechattel; ?>"/>
                        <input type='hidden' id="isreferaccountant" name="isreferaccountant" value="<?php echo $notes->isreferaccountant; ?>"/>


                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-12">

                                <span>Was the property inspected on site inside and out by Purchaser/Tenant</span>  <br />
                                <input type="radio" <?php echo ($notes->ispropertyinspected==1)?"checked=checked":""; ?> value="yes" name="optionIspropertyinspected">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->ispropertyinspected==0)?"checked=checked":""; ?> value="no" name="optionIspropertyinspected">
                                No

                                                <span id="dvIspropertyinspected" style="<?php echo ($notes->ispropertyinspected==0)?"":"display:none;"; ?>">
                                                    <input type="text" value="<?php echo $notes->ispropertyinspectedtext; ?>" id="txtIspropertyinspected" name="txtIspropertyinspected" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Dates property shown to Purchaser /Tenant and/or others (include names with times):</span>  <br />

                                <input type="text" value="" id="txtDateShown" name="txtDateShown" class="form-control m-t-sm">


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Statements made / discussions / questions and your answers,about the boundaries, titles (including Cross Lease / Body Corp)</span>  <br />

                                <textarea  id="txtStatemenBoundaries" name="txtStatemenBoundaries"  rows="2" class="form-control parsley-validated"><?php echo $notes->statementboundaries; ?></textarea>


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Statements made/discussions and your answers about the construction, piles, electrical, cladding, roofing, insulation,heating, sewage/water reticulation, drainage, fencing</span>  <br />

                                <textarea  id="txtStatementConstruction" name="txtStatementConstruction"  rows="2" class="form-control parsley-validated"><?php echo $notes->statementconstruction; ?></textarea>


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Were any problems / potential problems / problem areas discussed? Specify:</span>  <br />

                                <input type="radio" <?php echo ($notes->ispotentialproblems==1)?"checked=checked":""; ?> value="yes" name="optionIspotentialproblems">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->ispotentialproblems==0)?"checked=checked":""; ?> value="no" name="optionIspotentialproblems">
                                No

                                                <span id="dvIspotentialproblems" style="<?php echo ($notes->ispotentialproblems==0)?"dispay:none;":""; ?>">
                                                    <input type="text" value="<?php echo $notes->ispotentialproblemtext; ?>" id="txtIspotentialproblems" name="txtIspotentialproblems" class="form-control m-t-sm">
                                                </span>


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Were any specific items discussed where you gave advice? Specify:</span>  <br />

                                <input type="text" value="" id="txtSpecificItems" name="txtSpecificItems" class="form-control m-t-sm">


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>SALE ONLY: Vendor Warranty explained to: Purchaser:</span>  <br />
                                <input type="radio" <?php echo ($notes->ispurchaserwarranty==1)?"checked=checked":""; ?>  value="yes" name="optionIsPurchaserWarranty">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->ispurchaserwarranty==0)?"checked=checked":""; ?> value="no"  name="optionIsPurchaserWarranty">
                                No

                                                <span id="dvIsPurchaserWarranty" style="<?php echo ($notes->ispurchaserwarranty==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->ispurchaserwarrantytext; ?>" type="text" id="txtIsPurchaserWarranty" name="txtIsPurchaserWarranty" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>SALE ONLY: Vendor Warranty explained to: Vendor</span>  <br />
                                <input type="radio" <?php echo ($notes->isvendorwarranty==1)?"checked=checked":""; ?>  value="yes" name="optionIsVendorWarranty">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isvendorwarranty==0)?"checked=checked":""; ?> value="no" name="optionIsVendorWarranty">
                                No

                                                <span id="dvIsVendorWarranty" style="<?php echo ($notes->isvendorwarranty==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isvendorwarrantytext; ?>" type="text" id="txtIsVendorWarranty" name="txtIsVendorWarranty" class="form-control m-t-sm">
                                                </span>
                            </div>



                            <div class="col-sm-12 m-t">

                                <span>GST discussion</span>  <br />
                                <input type="radio" <?php echo ($notes->isGSTdiscussion==1)?"checked=checked":""; ?>  value="yes"  name="optionIsGSTDiscussion">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isGSTdiscussion==0)?"checked=checked":""; ?> value="no" name="optionIsGSTDiscussion">
                                No

                                                <span id="dvIsGSTDiscussion" style="<?php echo ($notes->isGSTdiscussion==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isGSTdiscussiontext; ?>" type="text"  id="txtIsGSTDiscussion" name="txtIsGSTDiscussion" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Was any advice sought?</span>  <br />
                                <input type="radio"  value="yes" <?php echo ($notes->isadvicesought==1)?"checked=checked":""; ?> name="optionIsAdviceSought">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isadvicesought==0)?"checked=checked":""; ?> value="no"  name="optionIsAdviceSought">
                                No

                                                <span id="dvIsAdviceSought" style="<?php echo ($notes->isadvicesought==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isadvicesoughttext; ?>" type="text" id="txtoptionIsAdviceSought" name="txtoptionIsAdviceSought" class="form-control m-t-sm">
                                                </span>
                            </div>





                        </div>

                    </div>
                </section>
            </div>

            <div class="col-sm-6">
                <section class="panel">
                    <div class="panel-body">
                        <div class="form-group pull-in clearfix">

                            <div class="col-sm-12 m-t">

                                                <span>If the Purchaser has bought the property subject to selling his/her own property did you guarantee / lead them to believe
            that we / you could obtain a certain price and thus possibly
            entice them to purchase?</span>  <br />
                                <input type="radio" <?php echo ($notes->ispurchaserboughtproperty==1)?"checked=checked":""; ?>  value="yes" name="optionIsPurchaserBroughtProperty">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->ispurchaserboughtproperty==0)?"checked=checked":""; ?> value="no"  name="optionIsPurchaserBroughtProperty">
                                No

                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Have you viewed and appraised the Purchaser’s property?</span>  <br />
                                <input type="radio"  <?php echo ($notes->isviewedandappraised==1)?"checked=checked":""; ?> value="yes" name="optionIsViewedAndAppraised">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isviewedandappraised==0)?"checked=checked":""; ?> value="no" name="optionIsViewedAndAppraised">
                                No
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Did you at any time have a difference of opinion with the Vendor/Landlord over any matter whatsoever? If yes describe</span>  <br />
                                <input type="radio"  value="yes"  <?php echo ($notes->isdifferenceopinion==1)?"checked=checked":""; ?> name="optionIsDifferenceInOpinion">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isdifferenceopinion==0)?"checked=checked":""; ?> value="no"  name="optionIsDifferenceInOpinion">
                                No

                                                <span id="dvIsDifferenceInOpinion" style="<?php echo ($notes->isdifferenceopinion==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isdifferenceopiniontext; ?>" type="text" id="txtIsDifferenceInOpinion" name="txtIsDifferenceInOpinion" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Commission discussed</span>  <br />
                                <input type="radio"  value="yes" <?php echo ($notes->iscommissiondiscussed==1)?"checked=checked":""; ?>  name="optionIsCommissionDiscussed">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->iscommissiondiscussed==0)?"checked=checked":""; ?> value="no"  name="optionIsCommissionDiscussed">
                                No

                                                <span id="dvIsCommissionDiscussed" style="<?php echo ($notes->iscommissiondiscussed==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->iscommissiondiscusstext; ?>" type="text" id="txtIsCommissionDiscussed" name="txtIsCommissionDiscussed" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Did the Vendor exclude any chattels from the sale?</span>  <br />
                                <input type="radio"  value="yes"  <?php echo ($notes->isvendorincludechattel==1)?"checked=checked":""; ?> name="optionIsVendorIncludeChattel">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isvendorincludechattel==0)?"checked=checked":""; ?> value="no"  name="optionIsVendorIncludeChattel">
                                No

                                                <span id="dvIsVendorIncludeChattel" style="<?php echo ($notes->isvendorincludechattel==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isvendorincludechatteltext; ?>" type="text" id="txtIsVendorIncludeChattel" name="txtIsVendorIncludeChattel" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Did you refer the Purchaser / Vendor to their solicitor / accountant for clarification of any matters? If yes describe?</span>  <br />
                                <input type="radio" <?php echo ($notes->isreferaccountant==1)?"checked=checked":""; ?> value="yes" name="optionIsReferAccountant">
                                Yes
                                &nbsp;&nbsp;
                                <input type="radio" <?php echo ($notes->isreferaccountant==0)?"checked=checked":""; ?> value="no"  name="optionIsReferAccountant">
                                No

                                                <span id="dvIsReferAccountant" style="<?php echo ($notes->isreferaccountant==0)?"display:none;":""; ?>">
                                                    <input value="<?php echo $notes->isreferaccountanttext; ?>" type="text" id="txtIsReferAccountant" name="txtIsReferAccountant" class="form-control m-t-sm">
                                                </span>
                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Describe what was discussed regarding any development potential, or re-sale value</span>  <br />

                                <span>Vendor</span>
                                <input type="text" value="<?php echo $notes->vendordescriberesalevalue; ?>" id="txtVendorDescribeSaleValue" name="txtVendorDescribeSaleValue" class="form-control m-t-sm">

                                <span>Purchaser</span>
                                <input type="text" value="<?php echo $notes->purchaserdiscussedresalevalue; ?>" id="txtPurchaserDescribeSaleValue" name="txtPurchaserDescribeSaleValue" class="form-control m-t-sm">


                            </div>

                            <div class="col-sm-12 m-t">

                                <span>Note any other comments / relevant points</span>  <br />

                                <textarea  id="txtOtherComments" name="txtOtherComments"  rows="2" class="form-control"><?php echo $notes->othercomments; ?></textarea>


                            </div>

                        </div>
                    </div>
                </section>
            </div>
            </div>

            <h4 class="m-t">Confirmation</h4>

            <?php
            $listingsalesperson = '';
            $listingsignature='';
            $listingdate = '';
            $sellingsalesperson = '';
            $sellingsignature='';
            $sellingdate = '';

            if(!empty($confirm->listingpersonsigniture)){
                $listingsalesperson = $model->listeruserperson->fullname;
                $listingsignature=$confirm->listingpersonsigniture;
                $listingdate = date('d/m/Y',strtotime($confirm->listingpersonagreeddate));
            }

            if(!empty($confirm->sellingpersonsigniture)){
                $sellingsalesperson = $model->selleruserperson->fullname;
                $sellingsignature=$confirm->sellingpersonsigniture;
                $sellingdate = date('d/m/Y',strtotime($confirm->sellingpersonagreeddate));
            }
            ?>

            <div class="row">
                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading">
                            <span class="h5">Listing Salesperson</span>
                        </header>

                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Signature</label><br />
                                    <input type="text" value="<?php echo $listingsignature; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Name</label><br />
                                    <input type="text" value="<?php echo $listingsalesperson; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Date</label><br />
                                    <input type="text" value="<?php echo $listingdate; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading">
                            <span class="h5">Selling Salesperson</span>
                        </header>
                        <div class="panel-body">
                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Signature</label><br />
                                    <input type="text" value="<?php echo $sellingsignature; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Name</label><br />
                                    <input type="text" value="<?php echo $sellingsalesperson; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Date</label><br />
                                    <input type="text" value="<?php echo $sellingdate; ?>" readonly="readonly" class="form-control" >
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading">
                            <span class="h5">Report signed by Manager</span>
                        </header>

                        <div class="panel-body">
                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Signature</label><br />
                                    <input type="text" value="" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Name</label><br />
                                    <input type="text" value="" readonly="readonly" class="form-control" >
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Date</label><br />
                                    <input type="text" value="" readonly="readonly" class="form-control" >
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="col-sm-6">
                    <section class="panel">
                        <header class="panel-heading">
                            <span class="h5">Documents</span>
                        </header>

                        <div class="panel-body">
                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Agreement</label><br />

                                    <?php
                                    if(!empty($confirm->fileagreement)){
                                        echo '<a style="color:blue;" href="'.CController::createURL("transaction/Downloadlink").'/file/'.$confirm->fileagreement.'">'.$confirm->fileagreementlocalname.'</a>';
                                    }
                                    else{
                                        echo '<input type="file"  id="uploadAgreement"   name="uploadAgreement"/>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Transaction Report (written)</label><br />
                                    <?php
                                    if(!empty($confirm->fileTransaction)){
                                        echo '<a style="color:blue;" href="'.CController::createURL("transaction/Downloadlink").'/file/'.$confirm->fileTransaction.'">'.$confirm->filetransactionlocalname.'</a>';
                                    }
                                    else{
                                        echo '<input type="file"  id="uploadTransactionReport"   name="uploadTransactionReport"/>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Other Documents</label><br />
                                    <?php
                                    if(!empty($confirm->fileotherdoc1)){
                                        echo '<a style="color:blue;" href="'.CController::createURL("transaction/Downloadlink").'/file/'.$confirm->fileotherdoc1.'">'.$confirm->fileotherdoc1localname.'</a>';
                                    }
                                    else{
                                        echo '<input type="file" id="uploadOther1"   name="uploadOther1"/>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group m-t">
                                <div class="col-sm-12">
                                    <label>Other Documents</label><br />
                                    <?php
                                    if(!empty($confirm->fileotherdoc2)){
                                        echo '<a style="color:blue;" href="'.CController::createURL("transaction/Downloadlink").'/file/'.$confirm->fileotherdoc1.'">'.$confirm->fileotherdoc2localname.'</a>';
                                    }
                                    else{
                                        echo '<input type="file" id="uploadOther2"  name="uploadOther2"/>';
                                    }
                                    ?>
                                </div>
                            </div>

                        </div>
                    </section>
                </div>
            </div>



            <section class="panel">
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <button class="btn btn-white" id="btnCancel" type="button">Cancel</button>
                            <button class="btn btn-primary" id="btnUpdate" type="button">Update & Confirm</button>
                        </div>
                    </div>

                </div>
            </section>

        </form>
    </div>
</div>
