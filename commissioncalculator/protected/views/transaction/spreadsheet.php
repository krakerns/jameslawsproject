<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/js/spreadsheet.js',CClientScript::POS_END);

    //$tr = new Transactionreport();
?>

<style>



</style>

<div class="row">
    <div class="col-sm-12">
        <form data-validate="parsley">
            <section class="panel">
                <header class="panel-heading">
                    <span class="h4">Transasction Report</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Date</label>
                            <input type="text" id="txtDate" name="txtDate" data-date-format="mm-dd-yyyy" value="<?php echo date('d/m/Y',strtotime($tr->createdtime)); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>
                        <div class="col-sm-4">
                            <label>Sales Person</label><br />

                            <?php echo CHtml::dropDownList('selPerson',$tr->createdbyid, CHtml::listData(Users::model()->findAll(), 'userid', 'fullname'), array('prompt'=>'Select Salesperson','class'=>"form-control")); ?>


                        </div>

                        <div class="col-sm-4">
                            <label>Public Trust Reference</label>
                            <input type="text" id="txtPTR" value="<?php echo $tr->ptdrnumber; ?>" name="txtPTR" placeholder="Public Trust Reference" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Property</label>
                        <textarea placeholder="Property" id="txtProperty" name="txtProperty"  rows="3" class="form-control parsley-validated"><?php echo $tr->address; ?></textarea>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Total Commission Received (inc GST)</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCTRMainInc" name="txtCTRMainInc" placeholder="0.00" class="form-control dollartext" value="<?php echo $tr->commissionamtincGST; ?>">
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <label>Total Commission Received (exc GST)</label>
                            <div class="input-group ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCTRMainExc" name="txtCTRMainExc" placeholder="0.00" class="form-control dollartext" value="<?php echo $tr->commissionamtexcGST; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Listing Part share for individual%</label>
                            <div class="input-group ">
                                <input type="text" id="txtListerPercentage" name="txtListerPercentage" placeholder="0" value="<?php echo $tr->listershare; ?>" class="form-control percentagetext">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <label>Selling Part share for individual%</label>
                            <div class="input-group ">
                                <input type="text" id="txtSellerPercentage" name="txtSellerPercentage" placeholder="0" value="<?php echo $tr->sellershare; ?>" class="form-control percentagetext">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <header class="panel-heading">
                            <span class="h4">Referral</span>
                        </header> <br />
                        <div class="col-sm-3">
                            <label>Listing part referral %</label>
                            <div class="input-group">
                                <input type="text" id="txtListingPartReferral" value="<?php echo $tr->listingreferralperc; ?>"  name="txtListingPartReferral" placeholder="0"  class="form-control percentagetext">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label>Listing referral Payable to</label><br />
                            <input type="text" id="txtListingPayable" name="txtListingPayable" placeholder="Listing referral Payable to"  value="<?php echo ($tr->ispowerteam == '1')?'powerteam@jameslaw.co.nz':''; ?>" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <label>Selling part referral %</label>
                            <div class="input-group">
                                <input type="text" id="txtSellingPartReferral" name="txtSellingPartReferral" placeholder="0" value="<?php echo $tr->sellingreferralperc; ?>" class="form-control percentagetext">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <label>Selling referral Payable to</label><br />
                            <input type="text" id="txtSellingPayable" name="txtSellingPayable" placeholder="Selling referral Payable to" value="<?php echo ($tr->ispowerteamseller == '1')?'powerteam@jameslaw.co.nz':''; ?>" class="form-control">
                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Less "off-the-top deductions" other than referral (exc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtLessOtherReferral" name="txtLessOtherReferral" placeholder="0" value="0.00" class="form-control dollartext">
                            </div>

                        </div>

                        <div class="col-sm-6">
                            <label>Bonus or Deductions (-)</label>
                            <div class="input-group">
                                <input type="text" id="txtBonusDeduction" name="txtBonusDeduction" value="<?php echo $bonusamt; ?>"  placeholder="0" class="form-control dollartext">
                                <span class="input-group-addon">%</span>
                            </div>
                            <span id="spanBonusDeduction1"><?php echo $bonusdesc; ?></span>
                        </div>
                    </div>

                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-success btn-s-xs" id="buttonCalculate" type="button">Calculate</button>
                        </div>
                    </div>

                </div>

            </section>

            <section class="panel" id="panelDetails" style="display: none;">
                <header class="panel-heading">
                    <span class="h4">Details</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Net commission to share (exc GST)</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtNetCommToShare" name="txtNetCommToShare" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>
                    <header class="panel-heading">
                        <span class="h4">Lister Split</span>
                    </header> <br />
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Listing Split Amount</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtListerSplitAmt" name="txtListerSplitAmt" placeholder="0" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>Less Listing Referral amount in $ exc GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtLessListingRefAmt" name="txtLessListingRefAmt" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Listing Gross Brought in </label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtListingGrossBrought" name="txtListingGrossBrought" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>

                    <header class="panel-heading">
                        <span class="h4">Seller Split</span>
                    </header> <br />
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Selling Split Amount</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSellingSplitAmt" name="txtSellingSplitAmt" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Less Selling Referral amount in $ exc GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtLessSellingRefAmt" name="txtLessSellingRefAmt" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Selling Gross Brought in </label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSellingGrossBrought" name="txtSellingGrossBrought" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-5">
                            <label>Lister NET brought in Fees</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtListerNetBoughtFees" name="txtListerNetBoughtFees" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>

                    <header class="panel-heading">
                        <span class="h4">Individual Share</span>
                    </header> <br />

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Lister fee share amount $ ex GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" id="txtListerFeeShareAmt" name="txtListerFeeShareAmt" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Seller fee share amount $ ex GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" placeholder="0" id="txtSellerFeeShareAmt" name="txtSellerFeeShareAmt" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>TOTAL fees brought in</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" placeholder="0" id="txtIndTotalFeesBought" name="txtIndTotalFeesBought" class="form-control">
                            </div>

                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Individual share %</label>
                            <div class="input-group  ">
                                <input type="text" readonly="readonly" placeholder="0" id="txtIndividualSharePercentage" name="txtIndividualSharePercentage" value="<?php echo $tr->createdby->usershare; ?>" class="form-control">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Withholding Tax%</label>
                            <div class="input-group  ">
                                <input type="text" readonly="readonly" placeholder="0" id="txtIndWithHoldingTax" name="txtIndWithHoldingTax" value="<?php echo $tr->createdby->withholdingtax; ?>" class="form-control">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Individual Share amount $</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" placeholder="0" id="txtIndividualShareAmount" name="txtIndividualShareAmount" class="form-control">
                            </div>

                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Company share $</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" id="txtCompanySharePercentage" name="txtCompanySharePercentage" placeholder="0"  class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <label>Check Individual + Company</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" readonly="readonly" id="txtCheckIndividualComp" name="txtCheckIndividualComp" placeholder="0" class="form-control">
                            </div>

                        </div>

                    </div>
                </div>
            </section>


            <section class="panel" id="panelSalesPerson" style="display: none;">
                <header class="panel-heading">
                    <span class="h4">For SalesPerson</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Bonus/Deductions</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesBonusDeduction" name="txtSalesBonusDeduction" value="<?php echo $bonusamt; ?>"  placeholder="0" class="form-control">
                            </div>
                            <span id="spanBonusDeduction"><?php echo $bonusdesc; ?></span>
                        </div>
                        <div class="col-sm-4">
                            <label>Net share before Tax</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesNetShare" name="txtSalesNetShare" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Witholding Tax/PAYE</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesWithHoldingTax" name="txtSalesWithHoldingTax" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Amount to Invoice Company</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesAmtToInvoice" name="txtSalesAmtToInvoice" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Plus GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesPlusGST" name="txtSalesPlusGST" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Grand Total</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtSalesGrandTotal" name="txtSalesGrandTotal" placeholder="0" class="form-control">
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="panel" id="panelManagers" style="display: none;">
                <header class="panel-heading">
                    <span class="h4">Managers</span>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <input type="hidden" id="managerPercentage" value="0">
                        <table class="table table-striped  -none" id="tblProfUploads">
                            <thead>
                            <tr>
                                <th>Manager Name</th>
                                <th>Share</th>
                                <th>Company Share amount</th>
                                <th>Manager's share</th>
                            </tr>
                            </thead>
                            <tbody id="tBodyDetails">

                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

            <section class="panel" id="panelCommission" style="display: none;">
                <header class="panel-heading">
                    <span class="h4">Company GST and Commission Reserve</span>
                </header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Total amount inc GST</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCommTotalAmtGST" name="txtCommTotalAmtGST" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Total GST component</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCommTotalGSTComponent" name="txtCommTotalGSTComponent" placeholder="0" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Total payout share %</label>
                            <div class="input-group  ">
                                <input type="text" id="txtCommTotalPayoutShare" name="txtCommTotalPayoutShare" placeholder="0" class="form-control">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-4">
                            <label>Company Share %</label>
                            <div class="input-group  ">
                                <input type="text" id="txtCommCompShare" name="txtCommCompShare"  placeholder="0" class="form-control">
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <label>Total Payable to Salesperson</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCommTotalPayableToPerson" name="txtCommTotalPayableToPerson" placeholder="0" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label>Total PAYE Transfer</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" id="txtCommTotalPAYETransfer" name="txtCommTotalPAYETransfer" placeholder="0" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>PAYABLE + PAYE</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" placeholder="0" id="txtCommPayablePAYE" name="txtCommPayablePAYE" class="form-control">
                            </div>

                        </div>
                        <div class="col-sm-6">
                            <label>GST Transfer to another account</label>
                            <div class="input-group  ">
                                <span class="input-group-addon">$</span>
                                <input type="text" placeholder="0" id="txtCommGSTTransfer" name="txtCommGSTTransfer" class="form-control">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </form>
    </div>
</div>