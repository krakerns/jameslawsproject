<script type="text/javascript">
    $(document).ready(function(){
        $('.relationship1').val('<?php echo $arr['type']; ?>');
        $('#txtDateFrom').val('<?php echo $arr['from']; ?>');
        $('#txtDateTo').val('<?php echo $arr['to']; ?>');

        $('.relationship1').select2();

        $("ul.nav li").removeClass('active');
        $('#lireports').addClass('active');

        $('#buttonCalculate').on('click',function(){
            $('#hdiscsv').val('0');
            $('#frmReport').submit();
        });

        $('#buttonCSV').on('click',function(){
            $('#hdiscsv').val('1');
            $('#frmReport').submit();
        });
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <form method="post" id="frmReport" action="<?php echo CController::createURL("report/ListMonthlyWTax"); ?>" data-validate="parsley">
            <input type="hidden" id="hdiscsv" name="report[hdiscsv]" value="0"/>
            <section class="panel">
                <header class="panel-heading font-bold"><h4>Transaction - Net Amount Paid To Salesperson</h4></header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Salesperson</label><br />
                            <?php echo CHtml::dropDownList('report[selSellerPerson]',"", CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('prompt'=>'All','class'=>'relationship1','style'=>'width:100%;')); ?>
                        </div>

                        <div class="col-sm-3">
                            <label>Date From</label><br />
                            <input type="text" id="txtDateFrom" name="report[txtDateFrom]" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>

                        <div class="col-sm-3">
                            <label>Date To</label><br />
                            <input type="text" id="txtDateTo" name="report[txtDateTo]" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>
                    </div>

                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12 text-left">
                            <button class="btn btn-success btn-s-xs" id="buttonCalculate" type="submit">Generate Report</button>
                        </div>
                    </div>
                </div>
            </section>

            <?php if(isset($dataReader)) { ?>
                <section class="panel">
                    <header class="panel-heading font-bold"><h4>Results <button class="btn btn-info btn-s-xs pull-right" id="buttonCSV" type="button">Download CSV</button></h4></header>
                    <div class="panel-body">
                        <table id="tbl_users" class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th style="width:10%;text-align: center;">Payment Made To Salesperson</th>
                                <th style="width:10%;text-align: center;">Salesperson</th>
                                <th style="width:10%;text-align: right;">Amount To Invoice Company</th>
                                <th style="width:10%;text-align: right;">Less Withholding Tax</th>
                                <th style="width:10%;text-align: right;">Bonus/Deductions</th>
                                <th style="width:10%;text-align: right;">Plus GST</th>
                                <th style="width:10%;text-align: right;">Total Payable Amount</th>
                                <th style="width:10%;text-align: center;">PTR</th>
                                <th style="width:10%;text-align: center;">Property</th>
                            </tr>
                            </thead>

                            <?php
                            $total = 0;
                            $totalamt=0;
                            $totaltax=0;
                            $totalbonus=0;
                            $totalgst=0;

                            foreach($dataReader as $row){
                                echo '<tr>';

                                if(!empty($row['paymenttosalesperson'])){
                                    echo '<td>'.date('d-m-Y',strtotime($row['paymenttosalesperson'])).'</td>';
                                }else{
                                    echo '<td></td>';
                                }
                                $salesperson =  Users::model()->findByPk($row['userid']);
                                echo '<td>'.$salesperson->fullname.'</td>';
                                $trsales = Transactionsalesperson::model()->find('transactionid=' . $row['transactionid']);


                                echo '<td align="right">'.formatDollar($trsales->amtinvoicecompany).'</td>';
                                echo '<td align="right">'.formatDollar($trsales->withholdingtax).'</td>';
                                echo '<td align="right">'.formatDollar($trsales->bonusdeduction).'</td>';
                                echo '<td align="right">'.formatDollar($trsales->plusgst).'</td>';
                                echo '<td align="right">'.formatDollar($trsales->grandtotal).'</td>';
                                echo '<td>'.$row['ptr'].'</td>';
                                echo '<td>'.$row['property'].'</td>';


                                echo '</tr>';

                                $totalamt += $trsales->amtinvoicecompany;
                                $totaltax += $trsales->withholdingtax;
                                $totalbonus += $trsales->bonusdeduction;
                                $totalgst += $trsales->plusgst;
                                $total +=  $trsales->grandtotal;
                            }
                            ?>

                            <tr class="">
                                <td colspan="2" align="right"><span class="h1">Totals :</span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totalamt); ?></span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totaltax); ?></span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totalbonus); ?></span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totalgst); ?></span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($total); ?></span></td>
                                <td  align="right"></td>
                                <td  align="right"></td>

                            </tr>

                        </table>
                    </div>
                </section>

                <section class="panel" style="display: none;">
                    <div class="panel-body">
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-10 text-right">
                                <label style="font-size:20px !important;"> Total Net Amount Paid To Salesperson : </label>
                            </div>

                            <div class="col-sm-2 text-right">
                                <label style="font-size:20px !important;"><?php echo formatDollar($total); ?></label>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
        </form>

    </div>
</div>