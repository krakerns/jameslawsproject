<script type="text/javascript">
    $(document).ready(function(){

        $('#selTranstype').val('<?php echo $arr['type']; ?>');
        $('#txtDateFrom').val('<?php echo $arr['from']; ?>');
        $('#txtDateTo').val('<?php echo $arr['to']; ?>');

        $("ul.nav li").removeClass('active');
        $('#lireports').addClass('active');


        $('#buttonCalculate').on('click',function(){
            $('#hdiscsv').val('0');
            $('#frmReport').submit();
        });

        $('#buttonCSV').on('click',function(){
            $('#hdiscsv').val('1');
            $('#frmReport').submit();
        });
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <form method="post" id="frmReport" action="<?php echo CController::createURL("report/ListTransactionGross"); ?>" data-validate="parsley">
            <input type="hidden" id="hdiscsv" name="report[hdiscsv]" value="0"/>

            <section class="panel">
                <header class="panel-heading font-bold"><h4>Transaction - Gross Brought In</h4></header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Transaction Type</label><br />
                            <select class="form-control" id="selTranstype" name="report[selTranstype]">
                                <option value="">All</option>
                                <option value="1">Residential Sale</option>
                                <option value="2">Commercial Sale</option>
                                <option value="3">Commercial Lease</option>
                                <option value="4">Commercial Assignment of Lease</option>
                                <option value="5">Business Sale</option>
                            </select>
                        </div>

                        <div class="col-sm-3">
                            <label>Date From</label><br />
                            <input type="text" id="txtDateFrom" name="report[txtDateFrom]" data-date-format="dd-mm-yyyy"  class=" datepicker-input form-control" placeholder="Date">
                            <input type="text" id="txtDateFrom" name="report[txtDateFrom]" data-date-format="dd-mm-yyyy"  class=" datepicker-input form-control" placeholder="Date">
                        </div>

                        <div class="col-sm-3">
                            <label>Date To</label><br />
                            <input type="text" id="txtDateTo" name="report[txtDateTo]" data-date-format="dd-mm-yyyy"  class=" datepicker-input form-control" placeholder="Date">
                        </div>
                    </div>

                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12 text-left">
                            <button class="btn btn-success btn-s-xs" id="buttonCalculate" type="button">Generate Report</button>
                        </div>
                    </div>
                </div>
            </section>

            <?php if(isset($dataReader)) { ?>
            <section class="panel">
                <header class="panel-heading font-bold"><h4>Results <button class="btn btn-info btn-s-xs pull-right" id="buttonCSV" type="button">Download CSV</button></h4></header>
                <div class="panel-body">
                    <table id="tbl_users" class="table table-striped m-b-none">
                        <thead>
                        <tr>
                            <th style="width:10%;text-align: center;">Payment Received</th>
                            <th style="width:13%;text-align: center;">Salesperson</th>
                            <th style="width:11%;text-align: center;">Transaction Type</th>
                            <th style="width:10%;text-align: center;">Invoice</th>
                            <th style="width:13%;text-align: center;">PTR</th>
                            <th style="width:16%;text-align: center;">Address</th>
                            <th style="width:13%;text-align: center;">Exclusive GST</th>
                            <th style="width:12%;text-align: center;">Inclusive GST</th>
                        </tr>
                        </thead>

                        <?php
                            $total = 0;
                            $total1 = 0;
                            foreach($dataReader as $row){
                                echo '<tr>';

                                if(!empty($row['paymentrecieveddate'])){
                                    echo '<td>'.date('d-m-Y',strtotime($row['paymentrecieveddate'])).'</td>';
                                }else{
                                    echo '<td></td>';
                                }
                                $salesperson =  Users::model()->findByPk($row['userid']);
                                echo '<td>'.$salesperson->fullname.'</td>';

                                $transactiontype= '';
                                switch($row['transactiontype']){
                                    case '1':
                                        $transactiontype = 'Residential Sale';
                                        break;
                                    case '2':
                                        $transactiontype = 'Commercial Sale';
                                        break;
                                    case '3':
                                        $transactiontype = 'Commercial Lease';
                                        break;
                                    case '4':
                                        $transactiontype = 'Commercial Assignment Of Lease';
                                        break;
                                    case '5':
                                        $transactiontype = 'Business Sale';
                                        break;
                                }

                                echo '<td>'.$transactiontype.'</td>';
                                echo '<td>'.$row['companyinvoice'].'</td>';
                                echo '<td>'.$row['ptr'].'</td>';
                                echo '<td>'.$row['property'].'</td>';

                                //$trdetails = Transactiondetails::model()->find('transactionid=' . $row['transactionid']);
                                echo '<td style="text-align:right;">'. formatDollar($row['totalcommissionrecievedexcGST']).'</td>';
                                echo '<td style="text-align:right;">'. formatDollar($row['totalcommissionrecievedincGST']).'</td>';
                                echo '</tr>';

                                $total +=  $row['totalcommissionrecievedexcGST'];
                                $total1 +=  $row['totalcommissionrecievedincGST'];
                            }
                        ?>

                        <tr class="">
                            <td colspan="6" align="right"><span class="h1">Totals :</span></td>
                            <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($total); ?></span></td>
                            <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($total1); ?></span></td>


                        </tr>

                    </table>
                </div>
            </section>

             <section class="panel" style="display: none;">
                 <div class="panel-body">
                     <div class="form-group pull-in clearfix">
                         <div class="col-sm-10 text-right">
                             <label style="font-size:20px !important;"> Total Gross Amount Brought In : </label>
                         </div>

                         <div class="col-sm-2 text-right">
                             <label style="font-size:20px !important;"><?php echo formatDollar($total); ?></label>
                         </div>
                     </div>
                 </div>
             </section>
            <?php } ?>
        </form>

    </div>
</div>

