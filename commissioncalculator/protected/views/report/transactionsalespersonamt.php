<script type="text/javascript">
    $(document).ready(function(){
        $('.relationship1').val('<?php echo $arr['type']; ?>');
        $('#txtDateFrom').val('<?php echo $arr['from']; ?>');
        $('#txtDateTo').val('<?php echo $arr['to']; ?>');

        $('.relationship1').select2();

        $("ul.nav li").removeClass('active');
        $('#lireports').addClass('active');

        $('#buttonCalculate').on('click',function(){
            $('#hdiscsv').val('0');
            $('#frmReport').submit();
        });

        $('#buttonCSV').on('click',function(){
            $('#hdiscsv').val('1');
            $('#frmReport').submit();
        });
    });
</script>

<div class="row">
    <div class="col-sm-12">
        <form method="post" id="frmReport" action="<?php echo CController::createURL("report/ListTransactionSalespersonAmt"); ?>" data-validate="parsley">
            <input type="hidden" id="hdiscsv" name="report[hdiscsv]" value="0"/>

            <section class="panel">
                <header class="panel-heading font-bold"><h4>Transaction - Net Amount Paid To Salesperson</h4></header>
                <div class="panel-body">
                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-6">
                            <label>Salesperson</label><br />
                            <?php echo CHtml::dropDownList('report[selSellerPerson]',"", CHtml::listData(Users::model()->findAll('isactive=1'), 'userid', 'fullname'), array('prompt'=>'All','class'=>'relationship1','style'=>'width:100%;')); ?>
                        </div>

                        <div class="col-sm-3">
                            <label>Date From</label><br />
                            <input type="text" id="txtDateFrom" name="report[txtDateFrom]" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>

                        <div class="col-sm-3">
                            <label>Date To</label><br />
                            <input type="text" id="txtDateTo" name="report[txtDateTo]" data-date-format="dd-mm-yyyy" value="<?php echo date('d-m-Y'); ?>" class=" datepicker-input form-control" placeholder="Date">
                        </div>
                    </div>

                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group pull-in clearfix">
                        <div class="col-sm-12 text-left">
                            <button class="btn btn-success btn-s-xs" id="buttonCalculate" type="submit">Generate Report</button>
                        </div>
                    </div>
                </div>
            </section>

            <?php if(isset($dataReader)) { ?>
                <section class="panel">
                    <header class="panel-heading font-bold"><h4>Results <button class="btn btn-info btn-s-xs pull-right" id="buttonCSV" type="button">Download CSV</button></h4></header>
                    <div class="panel-body">
                        <table id="tbl_users" class="table table-striped m-b-none">
                            <thead>
                            <tr>
                                <th style="width:10%;text-align: center;">Payment Made To Salesperson</th>
                                <th style="width:5%;text-align: center;">Sales Person</th>
                                <th style="width:10%;text-align: center;">Transaction Type</th>
                                <th style="width:5%;text-align: center;">Invoice</th>
                                <th style="width:10%;text-align: center;">PTR</th>
                                <th style="width:10%;text-align: center;">Address</th>
                                <th style="width:10%;text-align: center;">Amount To Invoice</th>
                                <th style="width:5%;text-align: center;">Bonus / Deductions</th>
                                <th style="width:10%;text-align: center;">Plus GST</th>
                                <th style="width:10%;text-align: center;">Less With holding Tax</th>
                                <th style="width:15%;text-align: center;">Net Amount Paid</th>
                            </tr>
                            </thead>

                            <?php
                            $total = 0;
                            $totalamtinvoice =0;
                            $totaltax =0;
                            foreach($dataReader as $row){
                                echo '<tr style="font-size: 11px !important;">';

                                if(!empty($row['paymenttosalesperson'])){
                                    echo '<td>'.date('d-m-Y',strtotime($row['paymenttosalesperson'])).'</td>';
                                }else{
                                    echo '<td></td>';
                                }
                                $salesperson =  Users::model()->findByPk($row['userid']);
                                echo '<td>'.$salesperson->fullname.'</td>';

                                $transactiontype= '';
                                switch($row['transactiontype']){
                                    case '1':
                                        $transactiontype = 'Residential Sale';
                                        break;
                                    case '2':
                                        $transactiontype = 'Commercial Sale';
                                        break;
                                    case '3':
                                        $transactiontype = 'Commercial Lease';
                                        break;
                                    case '4':
                                        $transactiontype = 'Commercial Assignment Of Lease';
                                        break;
                                    case '5':
                                        $transactiontype = 'Business Sale';
                                        break;
                                }

                                echo '<td>'.$transactiontype.'</td>';
                                echo '<td>'.$row['companyinvoice'].'</td>';
                                echo '<td>'.$row['ptr'].'</td>';
                                echo '<td>'.$row['property'].'</td>';

                                $trsales = Transactionsalesperson::model()->find('transactionid=' . $row['transactionid']);
                                echo '<td style="text-align:right;">'. formatDollar($trsales->amtinvoicecompany).'</td>';
                                echo '<td style="text-align:right;">'. formatDollar($trsales->bonusdeduction).'</td>';
                                echo '<td style="text-align:right;">'. formatDollar($trsales->plusgst).'</td>';
                                echo '<td style="text-align:right;">'. formatDollar($trsales->withholdingtax).'</td>';

                                echo '<td style="text-align:right;">'. formatDollar($trsales->grandtotal).'</td>';
                                echo '</tr>';

                                $total +=  $trsales->grandtotal;
                                $totalamtinvoice +=  $trsales->amtinvoicecompany;
                                $totaltax +=  $trsales->withholdingtax;
                            }
                            ?>

                            <tr class="">
                                <td colspan="6" align="right"><span class="h1">Totals:</span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totalamtinvoice); ?></span></td>
                                <td colspan="3" align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($totaltax); ?></span></td>
                                <td  align="right" style="vertical-align: bottom;"><span class="font-bold"><?php echo formatDollar($total); ?></span></td>


                            </tr>

                        </table>
                    </div>
                </section>

                <section class="panel" style="display: none;">
                    <div class="panel-body">
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-10 text-right">
                                <label style="font-size:20px !important;"> Total Net Amount Paid To Salesperson : </label>
                            </div>

                            <div class="col-sm-2 text-right">
                                <label style="font-size:20px !important;"><?php echo formatDollar($total); ?></label>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } ?>
        </form>

    </div>
</div>