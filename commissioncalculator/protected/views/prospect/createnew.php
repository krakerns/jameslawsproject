<?php

$userddl = Users::model()->findAll('isactive=1');
$arruser = array();
foreach($userddl as $usr) {
    $tmp = array();
    $tmp = array("id"	=>	$usr->userid,
        "text"	=>	$usr->fullname);
    array_push($arruser, $tmp);
}
$user_json = CJSON::encode($arruser);

?>

<script type="text/javascript">
    var optionsList = [];

    $(document).ready(function(){
        $('#selCity').select2();

        $("#suburbtag").select2({
            data: function() {
                return { results: optionsList }; // Use the global variable to populate the list
            },
            multiple:true,
            tokenSeparators: [",", " "],
            placeholder: "Suburb"
        });

        $('#selCity').on("select2-selecting", function(e) {
            $.ajax({
                type: "POST",
                url: '<?php echo CController::createURL("prospect/Getsuburb"); ?>',
                dataType: "text",
                data:'cityid='+ e.val,
                success: function(data){

                    optionsList = [];
                    $('#suburbtag').select2('val', '');

                    $.each( $.trim(data).split(","), function(i, v) {
                        var vData = v.split("-");  // value and text was split by hyphen

                        // Finally update your optionsList with the data
                        optionsList.push({ id: vData[0], text: vData[1] });
                    });
                }
            });
        });

        $('#selProspecttype').on('change',function(){
            switch($(this).val()){
                case 'commerciallease':
                    $('#dvComLease').show();
                    $('#dvResSale').hide();
                    $('#dvComSale').hide();
                    $('#dvBusLease').hide();
                    break;

                case 'residentialsale':
                    $('#dvComLease').hide();
                    $('#dvResSale').show();
                    $('#dvComSale').hide();
                    $('#dvBusLease').hide();
                    break;

                case 'commercialsale':
                    $('#dvComLease').hide();
                    $('#dvResSale').hide();
                    $('#dvComSale').show();
                    $('#dvBusLease').hide();
                    break;

                case 'businesssale':
                    $('#dvComLease').hide();
                    $('#dvResSale').hide();
                    $('#dvComSale').hide();
                    $('#dvBusLease').show();
                    break;
            }
        });

        $("#usertag").select2({
                data:<?php echo $user_json; ?>,
                tokenSeparators: [",", " "],
                multiple:true,
                placeholder: "Tag User"
            }
        );

        $("ul.nav li").removeClass('active');
        $('#liprospect').addClass('active');
    });

    $(document).on('click', '[data-last]', function (e) {

        if($(this).html() == 'Finish')
            $('#fmrValidate').submit();
    });
</script>

<div class="row">
    <div class="col-sm-12">

    <section id="wizard" class="tab-pane active">
        <div class="panel">
            <div id="form-wizard" class="wizard clearfix">
                <ul class="steps">
                    <li class="active" data-target="#step1"><span class="badge badge-info">1</span>Client Information</li>
                    <li data-target="#step2"><span class="badge">2</span>Criteria</li>
                    <li data-target="#step3"><span class="badge">3</span>Access</li>
                </ul>
            </div>
            <div class="step-content">
            <form id="fmrValidate" enctype="multipart/form-data" name="frmValidate" action="<?php echo CController::createURL("prospect/SaveProspect"); ?>" method="post" data-validate="parsley">
                    <div id="step1" class="step-pane active">
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>First Name</label>
                                <input type="text" id="txtFirstName"   name="txtFirstName" class="form-control" placeholder="First Name">
                            </div>

                            <div class="col-sm-6">
                                <label>Last Name</label>
                                <input type="text" id="txtLastName"  name="txtLastName" class="form-control" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>Company</label>
                                <input type="text" id="txtCompany"   name="txtCompany" class="form-control" placeholder="Company">
                            </div>

                            <div class="col-sm-6">
                                <label>Email</label>
                                <input type="text" id="txtEmail" data-type="email" data-required="true" name="txtEmail" class="form-control" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-4">
                                <label>Phone Number</label>
                                <input type="text" id="txtPhone"   name="txtPhone" class="form-control" placeholder="Phone Number">
                            </div>

                            <div class="col-sm-4">
                                <label>Mobile Number</label>
                                <input type="text" id="txtMobile" name="txtMobile" class="form-control" placeholder="Mobile Number">
                            </div>

                            <div class="col-sm-4">
                                <label>Fax Number</label>
                                <input type="text" id="txtFax" name="txtFax" class="form-control" placeholder="Fax Number">
                            </div>
                        </div>
                    </div>

                    <div id="step2" class="step-pane">
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>Prospect Type</label>
                                <?php echo CHtml::dropDownList('selProspecttype',$model->prospect_type_enum, $model->listProspecttype(), array('prompt'=>'Select Type','data-required'=>'true','class'=>"form-control")); ?>
                            </div>
                        </div>

                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>City</label>
                                <?php echo CHtml::dropDownList('selCity',"", CHtml::listData(City::model()->findAll(), 'id', 'name'), array('prompt'=>'Select City','style'=>'width:100%;')); ?>
                            </div>

                            <div class="col-sm-6">
                                <label>Suburb</label>
                                <input type="hidden" id="suburbtag" name="suburbtag" style="width:98%;"/>
                            </div>
                        </div>

                        <div class="line line-dashed line-lg pull-in"></div>

                        <div id="dvComLease" <?php echo ($model->prospect_type_enum == 'commerciallease')?'':'style="display:none;"'; ?>>
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Property Type</label>
                                    <?php
                                    $comlease = new ProspectComlease();
                                    echo CHtml::dropDownList('selPropertytypecomlease','', $comlease->listPropertytype(), array('prompt'=>'Select Type','class'=>"form-control"));
                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <label>Floor Area</label><br />

                                    <select id="txtFloorFrom" name="txtFloorFromcomlease" style="width:50% !important;display: inline;" class="form-control" placeholder="From">
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="200">200</option>
                                        <option value="250">250</option>
                                        <option value="300">300</option>
                                        <option value="350">350</option>
                                        <option value="400">400</option>
                                        <option value="450">450</option>
                                        <option value="500">500</option>
                                        <option value="600">600</option>
                                        <option value="700">700</option>
                                        <option value="800">800</option>
                                        <option value="1000">1000</option>
                                        <option value="1500">1500</option>
                                        <option value="2000">2000+</option>
                                    </select>
                                    &nbsp;&nbsp; - &nbsp;&nbsp;

                                    <select id="txtFloorTo" name="txtFloorTocomlease" style="width:43% !important;display: inline;" class="form-control" placeholder="To">
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="200">200</option>
                                        <option value="250">250</option>
                                        <option value="300">300</option>
                                        <option value="350">350</option>
                                        <option value="400">400</option>
                                        <option value="450">450</option>
                                        <option value="500">500</option>
                                        <option value="600">600</option>
                                        <option value="700">700</option>
                                        <option value="800">800</option>
                                        <option value="1000">1000</option>
                                        <option value="1500">1500</option>
                                        <option value="2000">2000+</option>
                                    </select>

                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Business</label>
                                    <input type="text" id="txtBusiness" name="txtBusinesscomlease" class="form-control" placeholder="Business">
                                </div>
                                <div class="col-sm-6">
                                    <label>Movein Time</label>
                                    <select name="selMoveincomlease" id="selMoveincomlease" class="form-control" placeholder="To">
                                        <option value="ASAP">ASAP</option>
                                        <option value="1 Month">1 Month</option>
                                        <option value="3 Months">3 Months</option>
                                        <option value="6 Months">6 Months</option>
                                        <option value="1 Year">1 Year</option>
                                        <option value="-">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>No Of People</label>
                                    <select name="selNoOfPeoplecomlease" id="selNoOfPeople" class="form-control" placeholder="To">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="5">5</option>
                                        <option value="7">7</option>
                                        <option value="10">10</option>
                                        <option value="15">15+</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label>Required Carparks</label>
                                    <select name="selCarParkscomlease" id="selCarParks" class="form-control" placeholder="To">
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="5">5</option>
                                        <option value="7">7</option>
                                        <option value="10">10</option>
                                        <option value="15">15+</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label>Budget</label>
                                    <select name="selBudget" id="selBudgetcomlease" class="form-control" placeholder="To">
                                        <option value="5000">$5,000</option>
                                        <option value="10000">$10,000</option>
                                        <option value="15000">$15,000</option>
                                        <option value="20000">$25,000</option>
                                        <option value="30000">$30,000</option>
                                        <option value="40000">$40,000</option>
                                        <option value="50000">$50,000</option>
                                        <option value="60000">$60,000</option>
                                        <option value="70000">$70,000</option>
                                        <option value="80000">$80,000</option>
                                        <option value="90000">$90,000</option>
                                        <option value="100000">$100,000</option>
                                        <option value="110000">$110,000</option>
                                        <option value="120000">$120,000</option>
                                        <option value="130000">$130,000</option>
                                        <option value="140000">$140,000</option>
                                        <option value="150000">$150,000</option>
                                        <option value="175000">$175,000</option>
                                        <option value="200000">$200,000</option>
                                        <option value="225000">$225,000</option>
                                        <option value="250000">$250,000</option>
                                        <option value="275000">$275,000</option>
                                        <option value="300000">$300,000</option>
                                        <option value="350000">$350,000</option>
                                        <option value="400000">$400,000</option>
                                        <option value="450000">$450,000</option>
                                        <option value="500000">$500,000</option>
                                        <option value="600000">$600,000</option>
                                        <option value="700000">$700,000</option>
                                        <option value="800000">$800,000</option>
                                        <option value="900000">$900,000</option>
                                        <option value="1000000">$1M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="dvResSale" <?php echo ($model->prospect_type_enum == 'residentialsale')?'':'style="display:none;"'; ?>>
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Property Type</label>
                                    <?php
                                    $ressale = new ProspectRessale();
                                    echo CHtml::dropDownList('selPropertytyperessale','', $ressale->listPropertytype(), array('prompt'=>'Select Type','class'=>"form-control"));
                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <label>Purpose</label>
                                    <select name="selPurposeressale" id="selPurpose" class="form-control" placeholder="To">
                                        <option value="3">Investment</option>
                                        <option value="4">Own Use</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-4">
                                    <label>Bedroom</label>
                                    <select name="selBedroomressale" id="selBedroom" class="form-control" placeholder="To">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7+</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label>Bathroom</label>
                                    <select name="selBathroomressale" id="selBathroom" class="form-control" placeholder="To">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7+</option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label>Price</label>
                                    <select name="selPriceressale" id="selPrice" class="form-control" placeholder="To">
                                        <option value="100000">$100,000</option>
                                        <option value="200000">$200,000</option>
                                        <option value="400000">$400,000</option>
                                        <option value="500000">$500,000</option>
                                        <option value="1000000">$1M</option>
                                        <option value="1500000">$1.5M</option>
                                        <option value="2000000">$2M</option>
                                        <option value="2500000">$2.5M</option>
                                        <option value="3000000">$3M</option>
                                        <option value="3500000">$3.5M</option>
                                        <option value="4000000">$4M</option>
                                        <option value="4500000">$4.5M</option>
                                        <option value="5000000">$5M</option>
                                        <option value="6000000">$6M</option>
                                        <option value="7000000">$7M</option>
                                        <option value="8000000">$8M</option>
                                        <option value="9000000">$9M</option>
                                        <option value="10000000">$10M</option>
                                        <option value="15000000">$15M</option>
                                        <option value="20000000">$20M</option>
                                        <option value="25000000">$25M</option>
                                        <option value="30000000">$30M</option>
                                        <option value="35000000">$35M</option>
                                        <option value="40000000">$40M</option>
                                        <option value="45000000">$45M</option>
                                        <option value="50000000">$50M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="dvComSale" <?php echo ($model->prospect_type_enum == 'commercialsale')?'':'style="display:none;"'; ?>>
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Property Type</label>
                                    <?php
                                    $comsale = new ProspectComsale();
                                    echo CHtml::dropDownList('selPropertytypecomsale','', $comsale->listPropertytype(), array('prompt'=>'Select Type','class'=>"form-control"));
                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <label>Price</label>
                                    <select name="selPricecomsale" id="selPricecomsale" class="form-control" placeholder="To">
                                        <option value="100000">$100,000</option>
                                        <option value="200000">$200,000</option>
                                        <option value="400000">$400,000</option>
                                        <option value="500000">$500,000</option>
                                        <option value="1000000">$1M</option>
                                        <option value="1500000">$1.5M</option>
                                        <option value="2000000">$2M</option>
                                        <option value="2500000">$2.5M</option>
                                        <option value="3000000">$3M</option>
                                        <option value="3500000">$3.5M</option>
                                        <option value="4000000">$4M</option>
                                        <option value="4500000">$4.5M</option>
                                        <option value="5000000">$5M</option>
                                        <option value="6000000">$6M</option>
                                        <option value="7000000">$7M</option>
                                        <option value="8000000">$8M</option>
                                        <option value="9000000">$9M</option>
                                        <option value="10000000">$10M</option>
                                        <option value="15000000">$15M</option>
                                        <option value="20000000">$20M</option>
                                        <option value="25000000">$25M</option>
                                        <option value="30000000">$30M</option>
                                        <option value="35000000">$35M</option>
                                        <option value="40000000">$40M</option>
                                        <option value="45000000">$45M</option>
                                        <option value="50000000">$50M</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Purpose</label>
                                    <select name="selPuroposecomsale" id="selPricecomsale" class="form-control" placeholder="To">
                                        <option value="1">Investment</option>
                                        <option value="2">Own Use</option>
                                    </select>
                                </div>

                                <div class="col-sm-6">
                                    <label>Return Desired</label>
                                    <select name="selRetdescomsale" id="selRetdescomsale" class="form-control" placeholder="To">
                                        <option value="4">4%</option>
                                        <option value="5">5%</option>
                                        <option value="6">6%</option>
                                        <option value="7">7%</option>
                                        <option value="8">8%</option>
                                        <option value="9">9%</option>
                                        <option value="10">10%</option>
                                        <option value="11">11%</option>
                                        <option value="12">12%</option>
                                        <option value="13">13%</option>
                                        <option value="14">14%</option>
                                        <option value="15">15%</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="dvBusLease" <?php echo ($model->prospect_type_enum == 'businesssale')?'':'style="display:none;"'; ?>>
                            <div class="form-group pull-in clearfix">
                                <div class="col-sm-6">
                                    <label>Business Type</label>
                                    <input type="text" id="txtBusNamebuslease"   name="txtBusNamebuslease" class="form-control" placeholder="Company">
                                </div>

                                <div class="col-sm-6">
                                    <label>Price</label><br />
                                    <select id="selPriceFrombuslease" name="selPriceFrombuslease" style="width:50% !important;display: inline;" class="form-control" placeholder="From">
                                        <option value="30000">$30,000</option>
                                        <option value="40000">$40,000</option>
                                        <option value="50000">$50,000</option>
                                        <option value="60000">$60,000</option>
                                        <option value="70000">$70,000</option>
                                        <option value="80000">$80,000</option>
                                        <option value="90000">$90,000</option>
                                        <option value="100000">$100,000</option>
                                        <option value="150000">$150,000</option>
                                        <option value="200000">$200,000</option>
                                        <option value="250000">$250,000</option>
                                        <option value="300000">$300,000</option>
                                        <option value="350000">$350,000</option>
                                        <option value="400000">$400,000</option>
                                        <option value="450000">$450,000</option>
                                        <option value="500000">$500,000</option>
                                        <option value="550000">$550,000</option>
                                        <option value="600000">$600,000</option>
                                        <option value="650000">$650,000</option>
                                        <option value="700000">$700,000</option>
                                        <option value="750000">$750,000</option>
                                        <option value="800000">$800,000</option>
                                        <option value="850000">$850,000</option>
                                        <option value="900000">$900,000</option>
                                        <option value="950000">$950,000</option>
                                        <option value="1000000">$1M</option>
                                        <option value="1500000">$1.5M</option>
                                        <option value="2000000">$2M</option>
                                    </select>
                                    &nbsp;&nbsp; - &nbsp;&nbsp;

                                    <select id="selPriceTobuslease" name="selPriceTobuslease" style="width:43% !important;display: inline;" class="form-control" placeholder="To">
                                        <option value="30000">$30,000</option>
                                        <option value="40000">$40,000</option>
                                        <option value="50000">$50,000</option>
                                        <option value="60000">$60,000</option>
                                        <option value="70000">$70,000</option>
                                        <option value="80000">$80,000</option>
                                        <option value="90000">$90,000</option>
                                        <option value="100000">$100,000</option>
                                        <option value="150000">$150,000</option>
                                        <option value="200000">$200,000</option>
                                        <option value="250000">$250,000</option>
                                        <option value="300000">$300,000</option>
                                        <option value="350000">$350,000</option>
                                        <option value="400000">$400,000</option>
                                        <option value="450000">$450,000</option>
                                        <option value="500000">$500,000</option>
                                        <option value="550000">$550,000</option>
                                        <option value="600000">$600,000</option>
                                        <option value="650000">$650,000</option>
                                        <option value="700000">$700,000</option>
                                        <option value="750000">$750,000</option>
                                        <option value="800000">$800,000</option>
                                        <option value="850000">$850,000</option>
                                        <option value="900000">$900,000</option>
                                        <option value="950000">$950,000</option>
                                        <option value="1000000">$1M</option>
                                        <option value="1500000">$1.5M</option>
                                        <option value="2000000">$2M</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="line line-dashed line-lg pull-in"></div>

                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>Enquiry On</label>
                                <textarea  id="txtEnquiry" name="txtEnquiry"  rows="2" class="form-control"></textarea>
                            </div>

                            <div class="col-sm-6">
                                <label>Free Time</label>
                                <textarea  id="txtFreeTime" name="txtFreeTime"  rows="2" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>Note</label>
                                <textarea  id="txtNote" name="txtNote"  rows="2" class="form-control"></textarea>
                            </div>

                            <div class="col-sm-6">
                                <label>Private Note</label>
                                <textarea  id="txtPrivateNote" name="txtPrivateNote"  rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div id="step3" class="step-pane">
                        <div class="form-group pull-in clearfix">
                            <div class="col-sm-6">
                                <label>Allow access for</label>
                                <input type="hidden" id="usertag" name="usertag" style="width:98%;"/>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="actions m-t">
                    <button disabled="disabled" data-wizard="previous" data-target="#form-wizard" class="btn btn-white btn-sm btn-prev" type="button">Prev</button>
                    <button data-last="Finish" data-wizard="next" data-target="#form-wizard" class="btn btn-white btn-sm btn-next" type="button">Next</button>
                </div>
            </div>
        </div>
    </section>

    </div>
</div>