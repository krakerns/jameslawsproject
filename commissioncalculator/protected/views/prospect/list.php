<?php
/**
 * Created by PhpStorm.
 * User: JV
 * Date: 7/18/14
 * Time: 1:03 AM
 */

?>

<script type="text/javascript">
    $(document).ready(function(){

        $('.table').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "bAutoWidth": false,
            "aoColumnDefs": [
                {
                    "bSortable": false,
                    "aTargets": [ 0 ] // <-- gets last column and turns off sorting
                }
            ]
        });

       // $("ul.nav li").removeClass('active');
        $('#liprospect').addClass('active');

    });
</script>

<section class="panel">
    <header class="panel-heading">
        <h4>Prospects List &nbsp;&nbsp;</h4>
    </header>

    <header class="bg-light">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tab1">Commercial Lease</a></li>
            <li class=""><a data-toggle="tab" href="#tab2">Residential Sale</a></li>
            <li class=""><a data-toggle="tab" href="#tab3">Commercial Sale</a></li>
            <li class=""><a data-toggle="tab" href="#tab4">Business Sale</a></li>
        </ul>
    </header>



    <div class="panel-body">
        <div class="tab-content">
            <div id="tab1" class="tab-pane active">

                <a href="<?php echo CController::createURL("prospect/Create",array('type'=>'commerciallease')); ?>" class="btn btn-success btn-sm"> Create Prospect</a>

                <div class="table-responsive">
                    <table class="table table-striped m-b-none" id="tblProfUploads">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th>ID #</th>
                            <th>Property Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                            <?php
                                $prospects  = Prospect::model()->findAll('prospect_type_enum="commerciallease"');
                                foreach($prospects as $prospect){
                                    echo '<tr>';
                                    $comlease = ProspectComlease::model()->find('prospect_id=' . $prospect->id);
                                    echo '<td><input class="lingissues" type="checkbox" value="'.$prospect->id.'" style="width:50px !important;" name="post[]"></td>';
                                    echo '<td>'.leading_zeros($prospect->id,8).'</td>';
                                    echo '<td>'.$comlease->propertytypename.'</td>';
                                    echo '<td>'.date('d/m/Y',strtotime($prospect->created_time)).'</td>';
                                    echo '<td>'.$prospect->createby->fullname.'</td>';
                                    echo '<td><a class="editrow btn btn-info btn-xs" href="#">View Details</a></td>';

                                    echo '</tr>';
                                }
                            ?>

                        </tbody>
                    </table>
                </div>

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <select class="input-sm form-control input-s-sm inline">
                                <option value="1">Delete selected</option>
                            </select>
                            <button class="btn btn-sm btn-white">Apply</button>
                        </div>
                    </div>
                </footer>

            </div>
            <div id="tab2" class="tab-pane">

                <a href="<?php echo CController::createURL("prospect/Create",array('type'=>'residentialsale')); ?>" class="btn btn-success btn-sm"> Create Prospect</a>

                <div class="table-responsive">
                    <table class="table table-striped m-b-none" id="tblProfUploads">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th>ID #</th>
                            <th>Property Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $prospects  = Prospect::model()->findAll('prospect_type_enum="residentialsale"');
                        foreach($prospects as $prospect){
                            echo '<tr>';
                            $ressale = ProspectRessale::model()->find('prospect_id=' . $prospect->id);
                            echo '<td><input class="lingissues" type="checkbox" value="'.$prospect->id.'" style="width:50px !important;" name="post[]"></td>';
                            echo '<td>'.leading_zeros($prospect->id,8).'</td>';
                            echo '<td>'.$ressale->propertytypename.'</td>';
                            echo '<td>'.date('d/m/Y',strtotime($prospect->created_time)).'</td>';
                            echo '<td>'.$prospect->createdby->fullname.'</td>';
                            echo '<td><a class="editrow btn btn-info btn-xs" href="#">View Details</a></td>';

                            echo '</tr>';
                        }
                        ?>

                        </tbody>
                    </table>
                </div>

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <select class="input-sm form-control input-s-sm inline">
                                <option value="1">Delete selected</option>
                            </select>
                            <button class="btn btn-sm btn-white">Apply</button>
                        </div>
                    </div>
                </footer>
            </div>
            <div id="tab3" class="tab-pane">

                <a href="<?php echo CController::createURL("prospect/Create",array('type'=>'commercialsale')); ?>" class="btn btn-success btn-sm"> Create Prospect</a>

                <div class="table-responsive">
                    <table class="table table-striped m-b-none" id="tblProfUploads">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th>ID #</th>
                            <th>Property Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $prospects  = Prospect::model()->findAll('prospect_type_enum="commercialsale"');
                        foreach($prospects as $prospect){
                            echo '<tr>';
                            $comsale = ProspectComsale::model()->find('prospect_id=' . $prospect->id);
                            echo '<td><input class="lingissues" type="checkbox" value="'.$prospect->id.'" style="width:50px !important;" name="post[]"></td>';
                            echo '<td>'.leading_zeros($prospect->id,8).'</td>';
                            echo '<td>'.$comsale->propertytypename.'</td>';
                            echo '<td>'.date('d/m/Y',strtotime($prospect->created_time)).'</td>';
                            echo '<td>'.$prospect->createdby->fullname.'</td>';
                            echo '<td><a class="editrow btn btn-info btn-xs" href="#">View Details</a></td>';

                            echo '</tr>';
                        }
                        ?>

                        </tbody>
                    </table>
                </div>

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <select class="input-sm form-control input-s-sm inline">
                                <option value="1">Delete Selected</option>
                            </select>
                            <button class="btn btn-sm btn-white">Apply</button>
                        </div>
                    </div>
                </footer>
            </div>

            <div id="tab4" class="tab-pane">

                <a href="<?php echo CController::createURL("prospect/Create",array('type'=>'businesssale')); ?>" class="btn btn-success btn-sm"> Create Prospect</a>
                <div class="table-responsive">
                    <table class="table table-striped m-b-none" id="tblProfUploads">
                        <thead>
                        <tr>
                            <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                            <th>ID #</th>
                            <th>Property Type</th>
                            <th>Date Created</th>
                            <th>Created By</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $prospects  = Prospect::model()->findAll('prospect_type_enum="businesssale"');
                        foreach($prospects as $prospect){
                            echo '<tr>';
                            $bussale = ProspectBussale::model()->find('prospect_id=' . $prospect->id);
                            echo '<td><input class="lingissues" type="checkbox" value="'.$prospect->id.'" style="width:50px !important;" name="post[]"></td>';
                            echo '<td>'.leading_zeros($prospect->id,8).'</td>';
                            echo '<td>'.$bussale->business_type.'</td>';
                            echo '<td>'.date('d/m/Y',strtotime($prospect->created_time)).'</td>';
                            echo '<td>'.$prospect->createby->fullname.'</td>';
                            echo '<td><a class="editrow btn btn-info btn-xs" href="#">View Details</a></td>';

                            echo '</tr>';
                        }
                        ?>

                        </tbody>
                    </table>
                </div>

                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-4 hidden-xs">
                            <select class="input-sm form-control input-s-sm inline">
                                <option value="1">Delete selected</option>
                            </select>
                            <button class="btn btn-sm btn-white">Apply</button>
                        </div>
                    </div>
                </footer>
            </div>

        </div>
    </div>

</section>