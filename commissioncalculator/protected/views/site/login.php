

<a class="nav-brand" href="index.html">James Law</a>
<div class="row m-n">
    <div class="col-md-4 col-md-offset-4 m-t-lg">

        <?php
            if($islogin == '0'){
        ?>

        <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button"><i class="icon-remove"></i></button>
            <i class="icon-ban-circle"></i><strong>Oops!</strong> Please check that you've entered the correct username and password and try again.
        </div>

        <?php } ?>

        <section class="panel">
            <header class="panel-heading text-center">
                Sign in
            </header>
            <form action="<?php echo CController::createURL("site/login");  ?>" method="post" class="panel-body">
                <div class="form-group">
                    <label class="control-label">Username</label>
                    <input type="text" placeholder="test@example.com" id="username" name="username" class="form-control">
                </div>
                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input type="password" id="inputPassword" id="password" name="password" placeholder="Password" class="form-control">
                </div>

                <a href="#" class="pull-right m-t-xs"><small>Forgot password?</small></a>
                <input type="submit" class="btn btn-info" value="Sign in">
                <div class="line line-dashed"></div>


            </form>
        </section>
    </div>
</div>