<div class="row">
    <div class="col-sm-6">
        <section class="panel">
            <header class="panel-heading bg-dark lter no-borders">
                <div class="clearfix">
                    <div class="clear">
                        <div class="h3 m-t-xs m-b-xs">Monthly Totals</div>
                    </div>

                </div>
            </header>
            <div class="list-group no-radius alt">
                <a href="#" class="list-group-item">
                    <span class="badge bg-success"><?php echo $monthlyTr; ?></span>
                    <i class="fa fa-comment icon-muted"></i>
                    Transaction Reports
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-info"><?php echo $monthlyTrans; ?></span>
                    <i class="fa fa-envelope icon-muted"></i>
                    Transactions
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-light"><?php echo $monthlyProspect; ?></span>
                    <i class="fa fa-eye icon-muted"></i>
                    Prospects
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-light"><?php echo $monthlySign; ?></span>
                    <i class="fa fa-clock-o icon-muted"></i>
                    Sign Orders
                </a>
            </div>
        </section>

    </div>

    <div class="col-sm-6">
        <section class="panel">
            <header class="panel-heading bg-danger lter no-borders">
                <div class="clearfix">
                    <div class="clear">
                        <div class="h3 m-t-xs m-b-xs">Overall Totals</div>
                    </div>

                </div>
            </header>
            <div class="list-group no-radius alt">
                <a href="#" class="list-group-item">
                    <span class="badge bg-success"><?php echo $overAllTr; ?></span>
                    <i class="fa fa-comment icon-muted"></i>
                    Transaction Reports
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-info"><?php echo $overAllTrans; ?></span>
                    <i class="fa fa-envelope icon-muted"></i>
                    Transactions
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-light"><?php echo $overAllProspect; ?></span>
                    <i class="fa fa-eye icon-muted"></i>
                    Prospects
                </a>
                <a href="#" class="list-group-item">
                    <span class="badge bg-light"><?php echo $overAllSign; ?></span>
                    <i class="fa fa-clock-o icon-muted"></i>
                    Sign Orders
                </a>
            </div>
        </section>

    </div>

</div>