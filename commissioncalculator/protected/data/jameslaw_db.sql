﻿# SQL Manager 2007 for MySQL 4.2.0.2
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : jameslaw_db


SET FOREIGN_KEY_CHECKS=0;

USE `jameslaw_db`;

#
# Structure for the `tbl_users` table : 
#

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(36) DEFAULT NULL,
  `lastname` varchar(36) DEFAULT NULL,
  `reeanumber` varchar(36) DEFAULT NULL,
  `irdnumber` varchar(36) DEFAULT NULL,
  `companyname` varchar(100) DEFAULT NULL,
  `companyemail` varchar(100) DEFAULT NULL,
  `personalemail` varchar(100) DEFAULT NULL,
  `mobilephonenumber` varchar(50) DEFAULT NULL,
  `usershare` double(15,2) DEFAULT '0.00',
  `payepercentage` double(15,2) DEFAULT '0.00',
  `withholdingtax` double(15,2) DEFAULT '0.00',
  `createdtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modifiedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `isactive` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userid`),
  KEY `createdby` (`createdby`),
  KEY `modifiedby` (`modifiedby`),
  CONSTRAINT `tbl_users_fk` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `tbl_users_fk1` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_bonusdeductions` table : 
#

DROP TABLE IF EXISTS `tbl_bonusdeductions`;

CREATE TABLE `tbl_bonusdeductions` (
  `bonusdeductionid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `amount` double(15,2) DEFAULT '0.00',
  `description` mediumtext,
  `gst` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`bonusdeductionid`),
  KEY `userid` (`userid`),
  CONSTRAINT `tbl_bonusdeductions_fk` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_bonusdeductionsline` table : 
#

DROP TABLE IF EXISTS `tbl_bonusdeductionsline`;

CREATE TABLE `tbl_bonusdeductionsline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bonusdeductionsid` int(11) DEFAULT NULL,
  `amount` double(15,2) DEFAULT '0.00',
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(225) DEFAULT NULL,
  `refnumber` int(11) DEFAULT NULL,
  `gst` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `bonusdeductionsid` (`bonusdeductionsid`),
  CONSTRAINT `tbl_bonusdeductionsline_fk` FOREIGN KEY (`bonusdeductionsid`) REFERENCES `tbl_bonusdeductions` (`bonusdeductionid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_city` table : 
#

DROP TABLE IF EXISTS `tbl_city`;

CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `is_valid` tinyint(4) NOT NULL,
  `is_default` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_client` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_client`;

CREATE TABLE `tbl_prospect_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(36) DEFAULT NULL,
  `last_name` varchar(36) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=ujis;

#
# Structure for the `tbl_prospect` table : 
#

DROP TABLE IF EXISTS `tbl_prospect`;

CREATE TABLE `tbl_prospect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `prospect_type_enum` enum('commerciallease','residentialsale','commercialsale','businesssale') DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createby_id` int(11) DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `tbl_prospect_fk1` (`city_id`),
  KEY `createby_id` (`createby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  CONSTRAINT `tbl_prospect_fk` FOREIGN KEY (`client_id`) REFERENCES `tbl_prospect_client` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_fk1` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_fk2` FOREIGN KEY (`createby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_fk3` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_comlease_notes` table : 
#

DROP TABLE IF EXISTS `tbl_comlease_notes`;

CREATE TABLE `tbl_comlease_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `note` text,
  `isprivate` tinyint(1) DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `createdby_id` (`createdby_id`),
  CONSTRAINT `tbl_comlease_notes_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_comlease_notes_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_country` table : 
#

DROP TABLE IF EXISTS `tbl_country`;

CREATE TABLE `tbl_country` (
  `countryid` int(11) NOT NULL AUTO_INCREMENT,
  `countryname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`countryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_managers` table : 
#

DROP TABLE IF EXISTS `tbl_managers`;

CREATE TABLE `tbl_managers` (
  `managerid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `usertomanage` int(11) DEFAULT NULL,
  `shareonuser` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`managerid`),
  KEY `userid` (`userid`),
  KEY `usertomanage` (`usertomanage`),
  CONSTRAINT `tbl_managers_fk` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_managers_ibfk_1` FOREIGN KEY (`usertomanage`) REFERENCES `tbl_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_marketing_fee` table : 
#

DROP TABLE IF EXISTS `tbl_marketing_fee`;

CREATE TABLE `tbl_marketing_fee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(100) DEFAULT NULL,
  `property` mediumtext,
  `user_id` int(11) DEFAULT NULL,
  `fund_received` datetime DEFAULT NULL,
  `description` mediumtext,
  `initial_amount` double(15,2) DEFAULT NULL,
  `current_amount` int(11) DEFAULT NULL,
  `datetime_created` datetime DEFAULT NULL,
  `createdby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `createdby_id` (`createdby_id`),
  CONSTRAINT `tbl_marketing_fee_fk` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_marketing_fee_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

#
# Structure for the `tbl_marketing_fee_payments` table : 
#

DROP TABLE IF EXISTS `tbl_marketing_fee_payments`;

CREATE TABLE `tbl_marketing_fee_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketing_fee_id` int(11) DEFAULT NULL,
  `amount` double(15,2) DEFAULT NULL,
  `description` mediumtext,
  `date_paid` datetime DEFAULT NULL,
  `supplier` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marketing_fee_id` (`marketing_fee_id`),
  CONSTRAINT `tbl_marketing_fee_payments_fk` FOREIGN KEY (`marketing_fee_id`) REFERENCES `tbl_marketing_fee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Structure for the `tbl_marketing_fee_salesperson` table : 
#

DROP TABLE IF EXISTS `tbl_marketing_fee_salesperson`;

CREATE TABLE `tbl_marketing_fee_salesperson` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marketing_fee_id` int(11) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `amount` double(15,2) DEFAULT '0.00',
  `gst` double(15,2) DEFAULT '0.00',
  `issaved` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `marketing_fee_id` (`marketing_fee_id`),
  KEY `userid` (`userid`),
  CONSTRAINT `table_marketing_fee_salesperson_fk` FOREIGN KEY (`marketing_fee_id`) REFERENCES `tbl_marketing_fee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `table_marketing_fee_salesperson_fk1` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_passwordmngr` table : 
#

DROP TABLE IF EXISTS `tbl_passwordmngr`;

CREATE TABLE `tbl_passwordmngr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(36) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(225) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `url` varchar(225) DEFAULT NULL,
  `username` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tbl_passwordmngr_fk` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_bussale` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_bussale`;

CREATE TABLE `tbl_prospect_bussale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `business_type` varchar(225) DEFAULT NULL,
  `price_from` int(11) DEFAULT NULL,
  `price_to` int(11) DEFAULT NULL,
  `enquiry_on` text,
  `free_time_to` datetime DEFAULT NULL,
  `free_time_from` datetime DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `createdby_id` (`createdby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  CONSTRAINT `tbl_prospect_bussale_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_bussale_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_bussale_fk2` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_comlease` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_comlease`;

CREATE TABLE `tbl_prospect_comlease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `property_type` enum('retail','office','industrial') DEFAULT 'retail',
  `floor_from` int(11) DEFAULT NULL,
  `floor_to` int(11) DEFAULT NULL,
  `business` varchar(100) DEFAULT NULL,
  `move_in` varchar(100) DEFAULT NULL,
  `move_in_other` varchar(100) DEFAULT NULL,
  `no_of_people` int(11) DEFAULT NULL,
  `required_car_parks` int(11) DEFAULT NULL,
  `budget` double(15,2) DEFAULT NULL,
  `include_outgoings` tinyint(1) DEFAULT NULL,
  `enquiry_on` text,
  `free_time_to` datetime DEFAULT NULL,
  `free_time_from` datetime DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `createdby_id` (`createdby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  CONSTRAINT `tbl_prospect_comlease_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_comlease_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_comlease_fk2` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_comsale` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_comsale`;

CREATE TABLE `tbl_prospect_comsale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `property_type` enum('retail','office','industrial') DEFAULT 'retail',
  `purpose` enum('investment','ownuse') DEFAULT 'investment',
  `price` int(11) DEFAULT NULL,
  `returned_desire` int(11) DEFAULT NULL,
  `enquiry_on` text,
  `free_time_to` datetime DEFAULT NULL,
  `free_time_from` datetime DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `createdby_id` (`createdby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  CONSTRAINT `tbl_prospect_comsale_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_comsale_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_comsale_fk2` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_ressale` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_ressale`;

CREATE TABLE `tbl_prospect_ressale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `property_type` enum('house','apartment','townhouse','unit') DEFAULT 'house',
  `purpose` enum('investment','ownuse') DEFAULT 'investment',
  `bedroom` int(11) DEFAULT NULL,
  `bathroom` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `enquiry_on` text,
  `free_time_to` datetime DEFAULT NULL,
  `free_time_from` datetime DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  `modified_time` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `createdby_id` (`createdby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  CONSTRAINT `tbl_prospect_ressale_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_ressale_fk1` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_ressale_fk2` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_suburb` table : 
#

DROP TABLE IF EXISTS `tbl_suburb`;

CREATE TABLE `tbl_suburb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `is_valid` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `tbl_suburb_fk` FOREIGN KEY (`city_id`) REFERENCES `tbl_city` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_suburb` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_suburb`;

CREATE TABLE `tbl_prospect_suburb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `suburb_id` int(11) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `suburb_id` (`suburb_id`),
  KEY `createdby_id` (`createdby_id`),
  CONSTRAINT `tbl_prospect_suburb_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_suburb_fk1` FOREIGN KEY (`suburb_id`) REFERENCES `tbl_suburb` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_suburb_fk2` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_prospect_user` table : 
#

DROP TABLE IF EXISTS `tbl_prospect_user`;

CREATE TABLE `tbl_prospect_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prospect_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prospect_id` (`prospect_id`),
  KEY `user_id` (`user_id`),
  KEY `createdby_id` (`createdby_id`),
  CONSTRAINT `tbl_prospect_user_fk` FOREIGN KEY (`prospect_id`) REFERENCES `tbl_prospect` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_prospect_user_fk1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_prospect_user_fk2` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_signorder` table : 
#

DROP TABLE IF EXISTS `tbl_signorder`;

CREATE TABLE `tbl_signorder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size` enum('small','medium','large') NOT NULL DEFAULT 'small',
  `on_sign` smallint(1) NOT NULL,
  `street_address` mediumtext NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `status` enum('ordered','installed','removed') NOT NULL,
  `line1` varchar(200) NOT NULL,
  `line2` varchar(200) NOT NULL,
  `line3` varchar(200) NOT NULL,
  `line4` varchar(200) NOT NULL,
  `installation_date` date NOT NULL,
  `installation_instruction` mediumtext NOT NULL,
  `photo` mediumtext NOT NULL,
  `sent_order` varchar(5) NOT NULL DEFAULT 'order',
  `sent_date` datetime NOT NULL,
  `createdby_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modifiedby_id` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `approvedby_id` int(11) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_valid` tinyint(4) NOT NULL DEFAULT '0',
  `salesperson_id` int(11) DEFAULT NULL,
  `type` enum('sale','lease','auction','tender') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `createdby_id` (`createdby_id`),
  KEY `modifiedby_id` (`modifiedby_id`),
  KEY `approvedby_id` (`approvedby_id`),
  KEY `salesperson_id` (`salesperson_id`),
  CONSTRAINT `tbl_signorder_fk` FOREIGN KEY (`createdby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_signorder_fk1` FOREIGN KEY (`modifiedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_signorder_fk2` FOREIGN KEY (`approvedby_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_signorder_fk3` FOREIGN KEY (`salesperson_id`) REFERENCES `tbl_users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_signorderpersons` table : 
#

DROP TABLE IF EXISTS `tbl_signorderpersons`;

CREATE TABLE `tbl_signorderpersons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `signorderid` int(11) DEFAULT NULL,
  `person_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `signorderid` (`signorderid`),
  CONSTRAINT `tbl_signorderpersons_fk` FOREIGN KEY (`signorderid`) REFERENCES `tbl_signorder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactionreport` table : 
#

DROP TABLE IF EXISTS `tbl_transactionreport`;

CREATE TABLE `tbl_transactionreport` (
  `transactionreportid` int(11) NOT NULL AUTO_INCREMENT,
  `transactiontype` int(11) DEFAULT NULL,
  `address` mediumtext,
  `saleleaseprice` double(15,3) DEFAULT NULL,
  `agreementdate` datetime DEFAULT NULL,
  `unconditionaldate` datetime DEFAULT NULL,
  `posessiondate` datetime DEFAULT NULL,
  `settlementdate` datetime DEFAULT NULL,
  `listertype` enum('colleague','other') DEFAULT 'colleague',
  `listeruserpersonid` int(11) DEFAULT NULL,
  `listerotherperson` varchar(100) DEFAULT NULL,
  `listershare` double(15,2) DEFAULT '0.50',
  `sellertype` enum('colleague','other') DEFAULT 'colleague',
  `selleruserpersonid` int(11) DEFAULT NULL,
  `sellerotherperson` varchar(100) DEFAULT NULL,
  `sellershare` double(15,2) DEFAULT '0.50',
  `ispowerteam` tinyint(1) DEFAULT '0',
  `listingsplitamt` double(15,2) DEFAULT '0.00',
  `listingreferralamt` double(15,2) DEFAULT '0.00',
  `listinggrossbroughtin` double(15,2) DEFAULT '0.00',
  `sellingsplitamt` double(15,2) DEFAULT '0.00',
  `sellingreferralamt` double(15,2) DEFAULT '0.00',
  `sellinggrossbroughtin` double(15,2) DEFAULT '0.00',
  `conjuctionaldefaultsplit` varchar(20) DEFAULT NULL,
  `conjuctionalothersplit` varchar(20) DEFAULT NULL,
  `conjuctionalothercompany` varchar(100) DEFAULT NULL,
  `commissionamtexcGST` double(15,2) DEFAULT '0.00',
  `commissionamtincGST` double(15,2) DEFAULT '0.00',
  `commissiondepositamtincGST` double(15,2) DEFAULT '0.00',
  `commissiondepositamtexcGST` double(15,2) DEFAULT '0.00',
  `ptdrnumber` varchar(36) DEFAULT NULL,
  `createdtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modifiedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdbyid` int(11) DEFAULT NULL,
  `modifiedbyid` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `listingreferralperc` double(15,2) DEFAULT NULL,
  `sellingreferralperc` double(15,2) DEFAULT NULL,
  `ispowerteamseller` tinyint(1) DEFAULT NULL,
  `listerotherpersonname` varchar(100) DEFAULT NULL,
  `sellerotherpersonname` varchar(100) DEFAULT NULL,
  `markdelete` int(11) DEFAULT '0',
  `isdraft` smallint(6) DEFAULT '0',
  PRIMARY KEY (`transactionreportid`),
  KEY `listeruserperson` (`listeruserpersonid`),
  KEY `selleruserpersonid` (`selleruserpersonid`),
  KEY `createdbyid` (`createdbyid`),
  KEY `modifiedbyid` (`modifiedbyid`),
  CONSTRAINT `tbl_transactionreport_fk` FOREIGN KEY (`listeruserpersonid`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `tbl_transactionreport_fk1` FOREIGN KEY (`selleruserpersonid`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `tbl_transactionreport_fk2` FOREIGN KEY (`createdbyid`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `tbl_transactionreport_fk3` FOREIGN KEY (`modifiedbyid`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transaction` table : 
#

DROP TABLE IF EXISTS `tbl_transaction`;

CREATE TABLE `tbl_transaction` (
  `transactionid` int(11) NOT NULL AUTO_INCREMENT,
  `transactiondate` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  `property` mediumtext,
  `propertylistingsplit` double(15,2) DEFAULT '0.00',
  `propertysellingsplit` double(15,2) DEFAULT '0.00',
  `ptr` varchar(36) DEFAULT NULL,
  `totalcommissionrecievedincGST` double(15,2) DEFAULT '0.00',
  `totalcommissionrecievedexcGST` double(15,2) DEFAULT '0.00',
  `listingpartshareindividual` double(15,2) DEFAULT '0.00',
  `sellingpartshareindividual` double(15,2) DEFAULT '0.00',
  `listingpartreferal` double(15,2) DEFAULT '0.00',
  `listingreferalpayableto` varchar(100) DEFAULT NULL,
  `sellingpartreferal` double(15,2) DEFAULT '0.00',
  `sellingreferalpayableto` varchar(100) DEFAULT NULL,
  `lessdeductionsexcGST` double(15,2) DEFAULT '0.00',
  `bonusdeductions` double(15,2) DEFAULT '0.00',
  `createdtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modifiedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `transactionreportid` int(11) DEFAULT NULL,
  `isclosed` tinyint(4) DEFAULT NULL,
  `companyinvoice` varchar(100) DEFAULT NULL,
  `invoicedate` datetime DEFAULT NULL,
  `paymentrecieveddate` datetime DEFAULT NULL,
  `paymenttosalesperson` datetime DEFAULT NULL,
  `bonusdeductiondesc` varchar(225) DEFAULT NULL,
  `gst` double(15,2) DEFAULT NULL,
  PRIMARY KEY (`transactionid`),
  KEY `userid` (`userid`),
  KEY `createdby` (`createdby`),
  KEY `modifiedby` (`modifiedby`),
  KEY `transactionreportid` (`transactionreportid`),
  CONSTRAINT `tbl_transaction_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_fk` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `transaction_fk1` FOREIGN KEY (`createdby`) REFERENCES `tbl_users` (`userid`),
  CONSTRAINT `transaction_fk2` FOREIGN KEY (`modifiedby`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactioncompanycommission` table : 
#

DROP TABLE IF EXISTS `tbl_transactioncompanycommission`;

CREATE TABLE `tbl_transactioncompanycommission` (
  `transactioncompanycommissionid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionid` int(11) DEFAULT NULL,
  `totalamtincGST` double(15,2) DEFAULT '0.00',
  `totalGSTcomponent` double(15,2) DEFAULT '0.00',
  `totalpayoutshare` double(15,2) DEFAULT '0.00',
  `companyshare` double(15,2) DEFAULT '0.00',
  `totalpaytosalesperson` double(15,2) DEFAULT '0.00',
  `totalpayetransfer` double(15,2) DEFAULT '0.00',
  `payablepaye` double(15,2) DEFAULT '0.00',
  `GSTtransfertootheraccnt` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`transactioncompanycommissionid`),
  KEY `transactionid` (`transactionid`),
  CONSTRAINT `tbl_transactioncompanycommission_fk` FOREIGN KEY (`transactionid`) REFERENCES `tbl_transaction` (`transactionid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactiondetails` table : 
#

DROP TABLE IF EXISTS `tbl_transactiondetails`;

CREATE TABLE `tbl_transactiondetails` (
  `transactiondetailsid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionid` int(11) DEFAULT NULL,
  `netcommisionshare` double(15,2) DEFAULT '0.00',
  `listingsplitamt` double(15,2) DEFAULT '0.00',
  `lesslistingreferalamt` double(15,2) DEFAULT '0.00',
  `listinggrossbroughtin` double(15,2) DEFAULT '0.00',
  `sellersplitamt` double(15,2) DEFAULT '0.00',
  `lesssellingreferealamt` double(15,2) DEFAULT '0.00',
  `sellinggrossbroughtin` double(15,2) DEFAULT '0.00',
  `listernetbroughtfees` double(15,2) DEFAULT '0.00',
  `listerfeeshareamt` double(15,2) DEFAULT '0.00',
  `sellerfeeshareamt` double(15,2) DEFAULT '0.00',
  `totalfeesbroughtin` double(15,2) DEFAULT '0.00',
  `individualshare` double(15,2) DEFAULT '0.00',
  `withholdingtax` double(15,2) DEFAULT '0.00',
  `individualshareamt` double(15,2) DEFAULT '0.00',
  `companyshare` double(15,2) DEFAULT '0.00',
  `checkindividualcompany` double(15,2) DEFAULT '0.00',
  `sellernetbroughtfees` double(15,2) DEFAULT NULL,
  PRIMARY KEY (`transactiondetailsid`),
  KEY `transactionid` (`transactionid`),
  CONSTRAINT `tbl_transactiondetails_fk` FOREIGN KEY (`transactionid`) REFERENCES `tbl_transaction` (`transactionid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactionmanagers` table : 
#

DROP TABLE IF EXISTS `tbl_transactionmanagers`;

CREATE TABLE `tbl_transactionmanagers` (
  `transactionmanagersid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionid` int(11) DEFAULT NULL,
  `managerid` int(11) DEFAULT NULL,
  `companyshareamt` double(15,2) DEFAULT '0.00',
  `managershare` double(15,2) DEFAULT '0.00',
  `transactionreportid` int(11) DEFAULT NULL,
  `companyinvoice` varchar(100) DEFAULT NULL,
  `invoicedate` datetime DEFAULT NULL,
  `paymentrecieveddate` datetime DEFAULT NULL,
  `paymenttosalesperson` datetime DEFAULT NULL,
  `isclosed` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`transactionmanagersid`),
  KEY `transactionid` (`transactionid`),
  KEY `managerid` (`managerid`),
  KEY `transactionreportid` (`transactionreportid`),
  CONSTRAINT `tbl_transactionmanagers_fk` FOREIGN KEY (`transactionid`) REFERENCES `tbl_transaction` (`transactionid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_transactionmanagers_fk1` FOREIGN KEY (`managerid`) REFERENCES `tbl_managers` (`managerid`),
  CONSTRAINT `tbl_transactionmanagers_fk2` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactionsalesperson` table : 
#

DROP TABLE IF EXISTS `tbl_transactionsalesperson`;

CREATE TABLE `tbl_transactionsalesperson` (
  `transactionsalespersonid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionid` int(11) DEFAULT NULL,
  `bonusdeduction` double(15,2) DEFAULT '0.00',
  `netsharebeforetax` double(15,2) DEFAULT '0.00',
  `withholdingtax` double(15,2) DEFAULT '0.00',
  `amtinvoicecompany` double(15,2) DEFAULT '0.00',
  `plusgst` double(15,2) DEFAULT '0.00',
  `grandtotal` double(15,2) DEFAULT '0.00',
  `bonusdeductiongst` double(15,2) DEFAULT NULL,
  PRIMARY KEY (`transactionsalespersonid`),
  KEY `transactionid` (`transactionid`),
  CONSTRAINT `tbl_transactionsalesperson_fk` FOREIGN KEY (`transactionid`) REFERENCES `tbl_transaction` (`transactionid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_transactiontype` table : 
#

DROP TABLE IF EXISTS `tbl_transactiontype`;

CREATE TABLE `tbl_transactiontype` (
  `transactiontypeid` int(11) NOT NULL AUTO_INCREMENT,
  `descriptioin` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`transactiontypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trconfirmation` table : 
#

DROP TABLE IF EXISTS `tbl_trconfirmation`;

CREATE TABLE `tbl_trconfirmation` (
  `trconfirmationid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `listingpersonagreed` tinyint(1) DEFAULT NULL,
  `listingpersonagreeddate` datetime DEFAULT NULL,
  `listingpersonsigniture` varchar(100) DEFAULT NULL,
  `lisingpersonipaddress` varchar(100) DEFAULT NULL,
  `sellingpersonagreed` tinyint(1) DEFAULT NULL,
  `sellingpersonagreeddate` datetime DEFAULT NULL,
  `sellingpersonsigniture` varchar(100) DEFAULT NULL,
  `sellingpersonipaddress` varchar(100) DEFAULT NULL,
  `manageragreed` tinyint(1) DEFAULT NULL,
  `manageragreeddate` datetime DEFAULT NULL,
  `managersigniture` varchar(100) DEFAULT NULL,
  `manageripaddress` varchar(100) DEFAULT NULL,
  `isagree` tinyint(1) DEFAULT NULL,
  `fileagreement` varchar(225) DEFAULT NULL,
  `fileagreementlocalname` varchar(225) DEFAULT NULL,
  `fileTransaction` varchar(225) DEFAULT NULL,
  `filetransactionlocalname` varchar(225) DEFAULT NULL,
  `fileotherdoc1` varchar(225) DEFAULT NULL,
  `fileotherdoc1localname` varchar(225) DEFAULT NULL,
  `fileotherdoc2` varchar(225) DEFAULT NULL,
  `fileotherdoc2localname` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`trconfirmationid`),
  KEY `transactionreportid` (`transactionreportid`),
  CONSTRAINT `tbl_trconfirmation_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

#
# Structure for the `tbl_trconfirmationdocuments` table : 
#

DROP TABLE IF EXISTS `tbl_trconfirmationdocuments`;

CREATE TABLE `tbl_trconfirmationdocuments` (
  `trconfirmationdocumentsid` int(11) NOT NULL AUTO_INCREMENT,
  `trconfirmationid` int(11) DEFAULT NULL,
  `filename` varchar(300) DEFAULT NULL,
  `localname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`trconfirmationdocumentsid`),
  KEY `trconfirmationid` (`trconfirmationid`),
  CONSTRAINT `tbl_trconfirmationdocuments_fk` FOREIGN KEY (`trconfirmationid`) REFERENCES `tbl_trconfirmation` (`trconfirmationid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trlistingpersons` table : 
#

DROP TABLE IF EXISTS `tbl_trlistingpersons`;

CREATE TABLE `tbl_trlistingpersons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `type` enum('colleague','other') DEFAULT 'colleague',
  `userid` int(11) DEFAULT NULL,
  `otherperson` varchar(200) DEFAULT NULL,
  `share` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `transactionreportid` (`transactionreportid`),
  KEY `userid` (`userid`),
  CONSTRAINT `tbl_listingpersons_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_listingpersons_fk1` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trlogs` table : 
#

DROP TABLE IF EXISTS `tbl_trlogs`;

CREATE TABLE `tbl_trlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `message` mediumtext CHARACTER SET latin1,
  `createdtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdbyid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactionreportid` (`transactionreportid`),
  KEY `createdbyid` (`createdbyid`),
  CONSTRAINT `tbl_trlogs_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_trlogs_fk1` FOREIGN KEY (`createdbyid`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trnotes` table : 
#

DROP TABLE IF EXISTS `tbl_trnotes`;

CREATE TABLE `tbl_trnotes` (
  `trnotesid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `notes` mediumtext,
  `ispropertyinspected` tinyint(1) DEFAULT '0',
  `ispropertyinspectedtext` varchar(100) DEFAULT NULL,
  `statementboundaries` mediumtext,
  `statementconstruction` mediumtext,
  `ispotentialproblems` tinyint(1) DEFAULT NULL,
  `ispotentialproblemtext` varchar(255) DEFAULT NULL,
  `ispurchaserwarranty` tinyint(1) DEFAULT NULL,
  `isvendorwarranty` tinyint(1) DEFAULT NULL,
  `isGSTdiscussion` tinyint(1) DEFAULT NULL,
  `isGSTdiscussiontext` varchar(225) DEFAULT NULL,
  `isadvicesought` tinyint(1) DEFAULT NULL,
  `isadvicesoughttext` varchar(255) DEFAULT NULL,
  `ispurchaserboughtproperty` tinyint(1) DEFAULT NULL,
  `isviewedandappraised` tinyint(1) DEFAULT NULL,
  `isdifferenceopinion` tinyint(1) DEFAULT NULL,
  `isdifferenceopiniontext` varchar(255) DEFAULT NULL,
  `iscommissiondiscussed` tinyint(1) DEFAULT NULL,
  `iscommissiondiscusstext` varchar(225) DEFAULT NULL,
  `isvendorincludechattel` tinyint(1) DEFAULT NULL,
  `isvendorincludechatteltext` varchar(225) DEFAULT NULL,
  `isreferaccountant` tinyint(1) DEFAULT NULL,
  `isreferaccountanttext` varchar(225) DEFAULT NULL,
  `vendordescriberesalevalue` varchar(255) DEFAULT NULL,
  `purchaserdiscussedresalevalue` varchar(255) DEFAULT NULL,
  `othercomments` mediumtext,
  `isagree` tinyint(1) DEFAULT NULL,
  `ispurchaserwarrantytext` varchar(225) DEFAULT NULL,
  `isvendorwarrantytext` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`trnotesid`),
  KEY `transactionreportid` (`transactionreportid`),
  CONSTRAINT `tbl_trnotes_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trpurchasertenantasignee` table : 
#

DROP TABLE IF EXISTS `tbl_trpurchasertenantasignee`;

CREATE TABLE `tbl_trpurchasertenantasignee` (
  `trpurchasertenantasigneeid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `legalentity` mediumtext,
  `contactname` varchar(100) DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `faxnumber` varchar(20) DEFAULT NULL,
  `streetaddress` varchar(255) DEFAULT NULL,
  `suburb` varchar(100) DEFAULT NULL,
  `city` varchar(36) DEFAULT 'Auckland',
  `countryid` int(11) DEFAULT NULL,
  `solicitorsfirm` varchar(100) DEFAULT NULL,
  `individualactingfirstname` varchar(36) DEFAULT NULL,
  `individualactinglastname` varchar(100) DEFAULT NULL,
  `individualactingemail` varchar(100) DEFAULT NULL,
  `individualactingphonenumber` varchar(20) DEFAULT NULL,
  `individualactingfaxnumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`trpurchasertenantasigneeid`),
  KEY `transactionreportid` (`transactionreportid`),
  KEY `countryid` (`countryid`),
  CONSTRAINT `tbl_trpurchasertenantasigneeid_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_trpurchasertenantasignee_fk` FOREIGN KEY (`countryid`) REFERENCES `tbl_country` (`countryid`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trsellingpersons` table : 
#

DROP TABLE IF EXISTS `tbl_trsellingpersons`;

CREATE TABLE `tbl_trsellingpersons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `type` enum('colleague','other') DEFAULT 'colleague',
  `userid` int(11) DEFAULT NULL,
  `otherperson` varchar(200) DEFAULT NULL,
  `share` double(15,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `transactionreportid` (`transactionreportid`),
  KEY `userid` (`userid`),
  CONSTRAINT `tbl_sellingpersons_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_sellingpersons_fk1` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_trvendorlandlordassignor` table : 
#

DROP TABLE IF EXISTS `tbl_trvendorlandlordassignor`;

CREATE TABLE `tbl_trvendorlandlordassignor` (
  `trvendorlandlorassignorid` int(11) NOT NULL AUTO_INCREMENT,
  `transactionreportid` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `legalentity` mediumtext,
  `contactname` varchar(100) DEFAULT NULL,
  `contactnumber` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `faxnumber` varchar(20) DEFAULT NULL,
  `streetaddress` varchar(255) DEFAULT NULL,
  `suburb` varchar(100) DEFAULT NULL,
  `city` varchar(36) DEFAULT 'Auckland',
  `countryid` int(11) DEFAULT NULL,
  `solicitorsfirm` varchar(100) DEFAULT NULL,
  `individualactingfirstname` varchar(36) DEFAULT NULL,
  `individualactinglastname` varchar(100) DEFAULT NULL,
  `individualactingemail` varchar(100) DEFAULT NULL,
  `individualactingphonenumber` varchar(20) DEFAULT NULL,
  `individualactingfaxnumber` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`trvendorlandlorassignorid`),
  KEY `transactionreportid` (`transactionreportid`),
  KEY `countryid` (`countryid`),
  CONSTRAINT `tbl_trvendorlandlordassignor_fk` FOREIGN KEY (`transactionreportid`) REFERENCES `tbl_transactionreport` (`transactionreportid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_trvendorlandlordassignor_fk1` FOREIGN KEY (`countryid`) REFERENCES `tbl_country` (`countryid`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

#
# Structure for the `tbl_useraccounts` table : 
#

DROP TABLE IF EXISTS `tbl_useraccounts`;

CREATE TABLE `tbl_useraccounts` (
  `useraccountsid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `accounttype` enum('superuser','accounts','user') DEFAULT 'user',
  `username` varchar(36) DEFAULT NULL,
  `password` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`useraccountsid`),
  KEY `userid` (`userid`),
  CONSTRAINT `tbl_useraccounts_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `tbl_users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

#
# Data for the `tbl_users` table  (LIMIT 0,500)
#

INSERT INTO `tbl_users` (`userid`, `firstname`, `lastname`, `reeanumber`, `irdnumber`, `companyname`, `companyemail`, `personalemail`, `mobilephonenumber`, `usershare`, `payepercentage`, `withholdingtax`, `createdtime`, `modifiedtime`, `createdby`, `modifiedby`, `isactive`) VALUES 
  (6,'Burt','Ong','10012773','','Property Management and Letting','burtong.jameslaw.co.nz','burtong.jameslaw.co.nz','027 650 2363',55,55,20,'2014-07-04 12:08:54','2014-07-16 06:01:29',NULL,NULL,1),
  (7,'James','Law','10007022','65-710-900','Property Management & Letting','james@jameslaw.co.nz','james@jameslaw.co.nz','021 668 078',60,60,20,'2014-07-04 12:15:10','2014-07-29 09:21:33',NULL,NULL,1),
  (8,'Letty','Ho','10009020','53-479-065','Business Sales & Property Management','lettyho@jameslaw.co.nz','lettyho@jameslaw.co.nz','021 368 802',55,55,20,'2014-07-04 12:20:59','2014-07-16 03:57:28',NULL,NULL,1),
  (9,'Luna','Lu','10011261','76-546-371','Commercial Lease & Residential Sales','luna@jameslaw.co.nz','luna@jameslaw.co.nz','021 049 6126',55,55,20,'2014-07-04 12:21:33','2014-07-16 03:59:55',NULL,NULL,1),
  (10,'Martin','Chin','10011276','','Apartment Sales','martin@jameslaw.co.nz','martin@jameslaw.co.nz','022 388 3456',60,60,0,'2014-07-04 12:22:11','2014-07-16 04:01:01',NULL,NULL,1),
  (11,'Michael','Chen','10029978','109-967-890','Commercial Lease & Business Sales','michaelc@jameslaw.co.nz','michaelc@jameslaw.co.nz','021 184 2238',55,55,0,'2014-07-04 12:22:51','2014-07-16 04:03:09',NULL,NULL,1),
  (12,'Michelle','Kennedy','10002813','44-999-218','Commercial Sale & Lease','michellek@jameslaw.co.nz','michellek@jameslaw.co.nz','021 886 089',60,65,20,'2014-07-04 12:23:31','2014-07-16 12:12:13',NULL,NULL,1),
  (13,'Paul','Ko','10029397','','Commercial & Industrial','paulko@jameslaw.co.nz','paulko@jameslaw.co.nz','021 333 779',80,0,0,'2014-07-04 12:24:32','2014-07-16 04:09:22',NULL,NULL,1),
  (14,'Satish','Masters','10038773','','Commercial & Industrial','satish@jameslaw.co.nz','satish@jameslaw.co.nz','021 502 337',55,55,0,'2014-07-04 12:25:35','2014-07-16 04:12:41',NULL,NULL,1),
  (15,'Vivien','Chang','10053590','67-005-049','Residential Sales','vivien@jameslaw.co.nz','vivien@jameslaw.co.nz','021 738884',55,55,20,'2014-07-04 12:26:34','2014-07-16 04:21:11',NULL,NULL,1),
  (16,'Francis','Lai','10016774','17-500-120','Business Brokerage, Merger & Acquisition, Property Development','francis@jameslaw.co.nz','francis@jameslaw.co.nz','021 309 708',80,80,NULL,'2014-07-04 12:27:36','2014-07-16 03:40:29',NULL,NULL,1),
  (17,'Jim','Young','10055715','56-366-113','Auckland Central','jim.young@jameslaw.co.nz','jim.young@jameslaw.co.nz','022 500 8090',50,50,20,'2014-07-04 12:28:48','2014-07-16 03:46:28',NULL,NULL,1),
  (18,'Graham','Truman','10014283','49-814-704','Special Projects & Developments','graham.truman@jameslaw.co.nz','graham.truman@jameslaw.co.nz','021 953 375',55,55,20,'2014-07-04 12:29:35','2014-07-16 03:43:42',NULL,NULL,1),
  (19,'Rosh','Daji','10014472','71-988-600','Residential SuperStar','rosh@jameslaw.co.nz','rosh@jameslaw.co.nz','027 5649655',80,80,20,'2014-07-04 12:31:24','2014-07-16 04:11:53',NULL,NULL,1),
  (20,'Arvind','Daji','','','Newmarket','arvind@skyproperty.co.nz','arvind@skyproperty.co.nz','027 2817596',0,0,0,'2014-07-04 12:32:08','2014-07-04 12:32:08',NULL,NULL,1),
  (21,'Dianne','Fraser','10005289','28-816-308','High end Residential Sales','dianne@jameslaw.co.nz','dianne@jameslaw.co.nz','027 224 7632',60,60,20,'2014-07-04 12:32:52','2014-07-16 03:19:45',NULL,NULL,1),
  (22,'William','Lee','10014511','61-178-562','COMMERCIAL SALE & LEASE, DEVELOPMENTS','william.lee@jameslaw.co.nz','william.lee@jameslaw.co.nz','021 894 910',55,55,20,'2014-07-04 12:33:33','2014-07-16 04:23:53',NULL,NULL,1),
  (23,'Iris','Yao','10009780','','Business & Residential Sales (with Francis Lai)','iris.yao@jameslaw.co.nz','iris.yao@jameslaw.co.nz','022 5296688',0,0,0,'2014-07-04 12:34:32','2014-07-16 03:44:28',NULL,NULL,1),
  (24,'Irene','Ng','20011580','087-272-044','Francis Team','irene.ng@jameslaw.co.nz','irene.ng@jameslaw.co.nz','021 473630',55,55,20,'2014-07-04 12:35:08','2014-07-16 03:33:37',NULL,NULL,1),
  (25,'Kim','Diack','20013213','24-503-437','Milford Office','kim@jameslaw.co.nz','kim@jameslaw.co.nz','021 755007',60,65,20,'2014-07-04 12:35:45','2014-07-19 21:41:22',NULL,NULL,1),
  (26,'Elizabeth','Ling','20013731','','Cook St','elizabeth@jameslaw.co.nz','elizabeth@jameslaw.co.nz','021 383 939',0,0,20,'2014-07-04 12:36:41','2014-07-16 04:16:26',NULL,NULL,1),
  (27,'Ivy','Bai','10013864','','Overseas Sales/Chinese Clients Group','ivy.bai@jameslaw.co.nz','ivy.bai@jameslaw.co.nz','021 860788',0,0,0,'2014-07-04 12:37:47','2014-07-16 03:45:03',NULL,NULL,1),
  (28,'Tina','Huang','20013839','85-555-685','Cook St','tina.huang@jameslaw.co.nz','tina.huang@jameslaw.co.nz','021 230 3286',50,50,20,'2014-07-04 12:38:21','2014-07-16 04:16:13',NULL,NULL,1),
  (29,'Reinka','Ratti','10008190','','Francis team','reinka@jameslaw.co.nz','reinka@jameslaw.co.nz','021 212 0315',0,0,0,'2014-07-04 12:39:12','2014-07-16 04:10:26',NULL,NULL,1),
  (30,'Katie','Luo','20013723','93-124-200','Paul''s team','katie.luo@jameslaw.co.nz','katie.luo@jameslaw.co.nz',' 021 772 771',50,55,20,'2014-07-04 12:40:07','2014-07-16 03:50:35',NULL,NULL,1),
  (31,'Tracey','Lin','','','Paul''s team','tracey.lin@jameslaw.co.nz','tracey.lin@jameslaw.co.nz','021 111 4887',0,0,0,'2014-07-04 12:40:51','2014-07-16 04:19:37',NULL,NULL,1),
  (32,'Lonny','Tian','','','','longlong@jameslaw.co.nz','longlong@jameslaw.co.nz','021 620 885',0,0,0,'2014-07-04 12:41:52','2014-07-16 03:58:42',NULL,NULL,1),
  (33,'John','Foreman','','','Milford Office','john.foreman@jameslaw.co.nz','john.foreman@jameslaw.co.nz','027 490 4449',0,0,0,'2014-07-04 12:42:38','2014-07-16 03:48:59',NULL,NULL,1),
  (34,'Laura','Lin','','','Paul''s team','laura.lin@jameslaw.co.nz','laura.lin@jameslaw.co.nz','021 1050 890',0,0,0,'2014-07-04 12:44:19','2014-07-04 12:44:19',NULL,NULL,1),
  (35,'Brite','Ling','','','Milford Office','brite.ling@jameslaw.co.nz','brite.ling@jameslaw.co.nz','021 119 6891',0,0,0,'2014-07-04 12:44:52','2014-07-16 02:24:53',NULL,NULL,0),
  (36,'Alison','Chen','','','Administration & Accounts','alison@jameslaw.co.nz','alison@jameslaw.co.nz','022 6726980',0,0,0,'2014-07-04 12:46:34','2014-08-12 00:57:14',NULL,NULL,1),
  (37,'Li','Han','','110-542-151','Marketing Co-ordinator','li@jameslaw.co.nz','li@jameslaw.co.nz','0223180488',0,0,0,'2014-07-04 12:47:22','2014-07-16 03:58:18',NULL,NULL,1),
  (38,'Edison','Tang','','100-244-713','Letty''s team','edison.tang@jameslaw.co.nz','edison.tang@jameslaw.co.nz','021-862629',0,0,20,'2014-07-04 12:48:11','2014-07-16 03:23:34',NULL,NULL,1),
  (39,'Larry','Grigson','','96-882-424','Letty''s team','l.grigson@jameslaw.co.nz','l.grigson@jameslaw.co.nz','027 290 7252',0,0,0,'2014-07-04 12:48:51','2014-07-16 03:55:15',NULL,NULL,1),
  (40,'Stephen','Lu','','76-674-817','Letty''s team','stephen.lu@jameslaw.co.nz','stephen.lu@jameslaw.co.nz','021-117 3321',0,0,0,'2014-07-04 12:49:32','2014-07-16 04:14:41',NULL,NULL,1),
  (41,'Anshari','Saro','','','Administration Team Leader','anshari@jameslaw.co.nz','anshari@jameslaw.co.nz','',0,0,0,'2014-07-04 12:50:45','2014-07-04 12:50:45',NULL,NULL,1),
  (42,'Ellen','Badoria','','','Appointment Team','ellen@jameslaw.co.nz','ellen@jameslaw.co.nz','',0,0,0,'2014-07-04 12:53:50','2014-07-04 12:53:50',NULL,NULL,1),
  (43,'Jonalyn','Saro','','','Research','jonalyn@jameslaw.co.nz','jonalyn@jameslaw.co.nz','',0,0,0,'2014-07-04 12:54:23','2014-07-04 12:54:23',NULL,NULL,1),
  (44,'JV','Junsay','1234','2133','','','jvjunsay@gmail.com','1221',0,0,20,'2014-07-08 15:31:51','2014-07-13 18:08:23',NULL,NULL,0),
  (45,'Fiona','Ma','','078-002-506','Letty''s team','fiona.ma@jameslaw.co.nz','fiona.ma@jameslaw.co.nz','022 1757226',0,0,20,'2014-07-16 04:31:08','2014-07-16 04:31:08',NULL,NULL,1),
  (46,'Allen','Sun','','','Paul''s team','allen.sun@jameslaw.co.nz','allen.sun@jameslaw.co.nz','0221618858',0,0,0,'2014-07-16 04:32:47','2014-07-31 20:16:41',NULL,NULL,1),
  (47,'sad','sad','sda','dsa','dsa','das','sad','das',25,0,25,'2014-07-19 23:04:41','2014-07-19 23:05:55',NULL,NULL,0),
  (48,'FFFF','FFF','223','231','2213','asdas@sadsa.com','asdas@sad.com','12121',0,0,0,'2014-10-14 17:47:45','2014-10-14 17:47:55',NULL,NULL,0);

COMMIT;

#
# Data for the `tbl_bonusdeductions` table  (LIMIT 0,500)
#

INSERT INTO `tbl_bonusdeductions` (`bonusdeductionid`, `userid`, `amount`, `description`, `gst`) VALUES 
  (14,6,400,'Test',10),
  (18,7,16,'Positive',NULL),
  (19,8,510,'',0),
  (20,45,252,'Test',NULL),
  (21,23,120,'Test',NULL),
  (22,9,0,'sad',1);

COMMIT;

#
# Data for the `tbl_bonusdeductionsline` table  (LIMIT 0,500)
#

INSERT INTO `tbl_bonusdeductionsline` (`id`, `bonusdeductionsid`, `amount`, `createdtime`, `description`, `refnumber`, `gst`) VALUES 
  (1,14,500,'2014-07-22','Good Job',NULL,NULL),
  (2,14,-200,'2014-07-22','Paid thru transaction',27,NULL),
  (9,18,-1,'2014-07-22','Phone Bill',35,NULL),
  (10,18,-1,'2014-07-26','',29,NULL),
  (11,18,10,'2014-07-28 11:07:00','Positive',NULL,NULL),
  (12,19,10,'2014-07-28','da',NULL,NULL),
  (13,14,50,'2014-07-28 18:55:37','Positive',NULL,NULL),
  (14,20,252,'2014-09-04','Test',NULL,NULL),
  (15,19,250,'0000-00-00','1',NULL,NULL),
  (16,19,250,'2014-05-12','test',NULL,NULL),
  (17,21,120,'2014-03-19','Test',NULL,NULL),
  (18,14,50,'2014-08-06','Test',NULL,10),
  (19,19,0,'0000-00-00','',NULL,0),
  (20,18,8,'2014-08-11','Positive',31,0),
  (21,14,400,'2014-08-12','Test',33,0),
  (22,22,200,'2014-08-15','Test',NULL,0),
  (23,22,200,'2014-08-14','Test',36,0),
  (24,22,300,'2014-08-20','sad',NULL,1),
  (25,22,-300,'2014-08-15','sad',37,0),
  (26,14,-400,'0000-00-00','Test',33,0);

COMMIT;

#
# Data for the `tbl_city` table  (LIMIT 0,500)
#

INSERT INTO `tbl_city` (`id`, `name`, `is_valid`, `is_default`) VALUES 
  (1,'Auckland City',1,1),
  (2,'Manukau City',1,0),
  (3,'North Shore City',1,0),
  (4,'Papakura',1,0),
  (5,'Waitakere City',1,0);

COMMIT;

#
# Data for the `tbl_prospect_client` table  (LIMIT 0,500)
#

INSERT INTO `tbl_prospect_client` (`id`, `first_name`, `last_name`, `email`, `company`, `phone`, `mobile`, `fax`) VALUES 
  (9,'sad','das','ads@Sda.com','dasa','adsasd','asd','d');

COMMIT;

#
# Data for the `tbl_prospect` table  (LIMIT 0,500)
#

INSERT INTO `tbl_prospect` (`id`, `client_id`, `city_id`, `prospect_type_enum`, `created_time`, `createby_id`, `modified_time`, `modifiedby_id`) VALUES 
  (9,9,1,'commerciallease','2014-08-10 21:39:53',7,NULL,NULL);

COMMIT;

#
# Data for the `tbl_managers` table  (LIMIT 0,500)
#

INSERT INTO `tbl_managers` (`managerid`, `userid`, `usertomanage`, `shareonuser`) VALUES 
  (2,13,15,20),
  (3,12,18,20),
  (4,12,22,20),
  (5,19,21,50),
  (6,13,17,20),
  (7,16,24,20),
  (8,13,30,50),
  (9,16,29,20),
  (10,12,33,20),
  (11,16,23,20);

COMMIT;

#
# Data for the `tbl_marketing_fee` table  (LIMIT 0,500)
#

INSERT INTO `tbl_marketing_fee` (`id`, `invoice`, `property`, `user_id`, `fund_received`, `description`, `initial_amount`, `current_amount`, `datetime_created`, `createdby_id`) VALUES 
  (4,'231','Test',NULL,'2014-09-09','Test',250,169,'2014-08-31',NULL),
  (5,'Test','Test2',NULL,'2014-09-02','test',200,124,'2014-09-02',NULL),
  (6,'dsa','sdada',NULL,'2014-09-16','test',245,122,'2014-09-02',NULL),
  (7,'1','adasdas',NULL,'2014-09-03','2',0,0,'2014-09-03',NULL),
  (8,'12234','Tesssss',NULL,'2014-09-10','test',0,-100,'2014-09-10',NULL),
  (9,'test','fdsfd',NULL,'2014-09-15','sda',100,35,'2014-09-14',NULL);

COMMIT;

#
# Data for the `tbl_marketing_fee_payments` table  (LIMIT 0,500)
#

INSERT INTO `tbl_marketing_fee_payments` (`id`, `marketing_fee_id`, `amount`, `description`, `date_paid`, `supplier`) VALUES 
  (4,6,123,'Test','2014-09-02','Test'),
  (6,4,100,'test','2014-09-03','100'),
  (7,4,1,'sda','2014-09-03','sxzczx'),
  (8,5,25,'tesss1','2014-09-07','test'),
  (9,8,100,'tee','2014-09-10','test'),
  (10,4,-20,'sadsdsa','2014-09-11','sdasds'),
  (11,9,20,'fdsfd','2014-09-14','sdfdf'),
  (12,9,45,'sad','2014-09-14','45');

COMMIT;

#
# Data for the `tbl_marketing_fee_salesperson` table  (LIMIT 0,500)
#

INSERT INTO `tbl_marketing_fee_salesperson` (`id`, `marketing_fee_id`, `userid`, `amount`, `gst`, `issaved`) VALUES 
  (1,4,6,84.5,56.33,1),
  (2,4,9,84.5,56.33,1),
  (3,5,7,0,0,NULL),
  (4,5,9,0,0,NULL),
  (5,6,7,0,0,NULL),
  (6,6,10,0,0,NULL),
  (7,8,7,0,0,NULL),
  (8,9,10,12,10,0);

COMMIT;

#
# Data for the `tbl_passwordmngr` table  (LIMIT 0,500)
#

INSERT INTO `tbl_passwordmngr` (`id`, `user_id`, `title`, `description`, `password`, `url`, `username`) VALUES 
  (5,46,'Gmail','for gmail','das2asda','mail.google.com',NULL),
  (6,7,'Gmail','google','password','mail.google.com','jvjunsay');

COMMIT;

#
# Data for the `tbl_prospect_comlease` table  (LIMIT 0,500)
#

INSERT INTO `tbl_prospect_comlease` (`id`, `prospect_id`, `property_type`, `floor_from`, `floor_to`, `business`, `move_in`, `move_in_other`, `no_of_people`, `required_car_parks`, `budget`, `include_outgoings`, `enquiry_on`, `free_time_to`, `free_time_from`, `created_time`, `createdby_id`, `modified_time`, `modifiedby_id`) VALUES 
  (1,9,'office',50,50,'sda','3 Months',NULL,3,2,5000,NULL,'dsa',NULL,NULL,'2014-08-10 21:39:54',7,NULL,NULL);

COMMIT;

#
# Data for the `tbl_suburb` table  (LIMIT 0,500)
#

INSERT INTO `tbl_suburb` (`id`, `city_id`, `name`, `is_valid`) VALUES 
  (1,2,'Airport Oaks',1),
  (2,3,'Albany',1),
  (3,4,'Ardmore',1),
  (4,1,'Avondale',1),
  (5,1,'Balmoral',1),
  (6,3,'Bayswater',1),
  (7,3,'Beach Haven',1),
  (8,2,'Beachlands',1),
  (9,3,'Belmont',1),
  (10,3,'Birkdale',1),
  (11,3,'Birkenhead',1),
  (12,1,'Blockhouse Bay',1),
  (13,2,'Botany Downs',1),
  (14,3,'Browns Bay',1),
  (15,2,'Burswood',1),
  (16,1,'City Centre',1),
  (17,2,'Clendon Park',1),
  (18,2,'Clevedon',1),
  (19,2,'Clover Park',1),
  (20,2,'Dannemora',1),
  (21,3,'Devonport',1),
  (22,4,'Drury',1),
  (23,2,'East Tamaki',1),
  (24,1,'Eden Terrace',1),
  (25,1,'Ellerslie',1),
  (26,1,'Epsom',1),
  (27,2,'Favona',1),
  (28,2,'Flat Bush',1),
  (29,3,'Forrest Hill',1),
  (30,1,'Freemans Bay',1),
  (31,5,'Glen Eden',1),
  (32,1,'Glen Innes',1),
  (33,5,'Glendene',1),
  (34,3,'Glenfield',1),
  (35,2,'Goodwood Heights',1),
  (36,1,'Grafton',1),
  (37,1,'Great Barrier Island',1),
  (38,3,'Greenhithe',1),
  (39,1,'Greenlane',1),
  (40,1,'Grey Lynn',1),
  (41,2,'Half Moon Bay',1),
  (42,3,'Hauraki',1),
  (43,5,'Henderson',1),
  (44,5,'Henderson Valley',1),
  (45,1,'Herne Bay',1),
  (46,2,'Highland Park',1),
  (47,3,'Hillcrest',1),
  (48,1,'Hillsborough',1),
  (49,5,'Hobsonville',1),
  (50,2,'Howick',1),
  (51,2,'Huntington Park',1),
  (52,5,'Karekare',1),
  (53,5,'Kelston',1),
  (54,1,'Kingsland',1),
  (55,1,'Kohimarama',1),
  (56,1,'Lynfield',1),
  (57,3,'Mairangi Bay',1),
  (58,2,'Mangere',1),
  (59,2,'Mangere Bridge',1),
  (60,2,'Manukau',1),
  (61,2,'Manurewa',1),
  (62,5,'Massey North',1),
  (63,5,'Massey West',1),
  (64,1,'Meadowbank',1),
  (65,3,'Milford',1),
  (66,1,'Mission Bay',1),
  (67,1,'Morningside',1),
  (68,1,'Mount Albert',1),
  (69,1,'Mount Eden',1),
  (70,1,'Mount Roskill',1),
  (71,1,'Mount Wellington',1),
  (72,5,'New Lynn',1),
  (73,1,'Newmarket',1),
  (74,1,'Newton',1),
  (75,3,'North Harbour',1),
  (76,3,'Northcote',1),
  (77,3,'Northcross',1),
  (78,1,'One Tree Hill',1),
  (79,1,'Onehunga',1),
  (80,1,'Orakei',1),
  (81,1,'Oranga',1),
  (82,5,'Oratia',1),
  (83,1,'Otahuhu',1),
  (84,2,'Otara',1),
  (85,2,'Pakuranga',1),
  (86,1,'Panmure',1),
  (87,4,'Papakura',1),
  (88,2,'Papatoetoe',1),
  (89,1,'Parnell',1),
  (90,1,'Penrose',1),
  (91,3,'Pinehill',1),
  (92,1,'Point Chevalier',1),
  (93,1,'Ponsonby',1),
  (94,2,'Puhinui',1),
  (95,5,'Ranui',1),
  (96,1,'Remuera',1),
  (97,3,'Rothesay Bay',1),
  (98,1,'Royal Oak',1),
  (99,1,'Saint Heliers',1),
  (100,1,'Saint Johns',1),
  (101,1,'Saint Marys Bay',1),
  (102,1,'Sandringham',1),
  (103,1,'St Lukes',1),
  (104,3,'Sunnynook',1),
  (105,5,'Swanson',1),
  (106,4,'Takanini',1),
  (107,3,'Takapuna',1),
  (108,1,'Tamaki',1),
  (109,5,'Te Atatu Peninsula',1),
  (110,5,'Te Atatu South',1),
  (111,1,'Three Kings',1),
  (112,5,'Titirangi',1),
  (113,3,'Torbay',1),
  (114,2,'Totara Heights',1),
  (115,3,'Unsworth Heights',1),
  (116,5,'Waitakere',1),
  (117,1,'Westmere',1),
  (118,5,'Whenuapai',1),
  (119,3,'Windsor Park',1),
  (120,2,'Wiri',1),
  (121,1,'Western Springs',1);

COMMIT;

#
# Data for the `tbl_prospect_suburb` table  (LIMIT 0,500)
#

INSERT INTO `tbl_prospect_suburb` (`id`, `prospect_id`, `suburb_id`, `created_time`, `createdby_id`) VALUES 
  (10,9,12,'2014-08-10 21:39:53',NULL);

COMMIT;

#
# Data for the `tbl_prospect_user` table  (LIMIT 0,500)
#

INSERT INTO `tbl_prospect_user` (`id`, `prospect_id`, `user_id`, `created_time`, `createdby_id`) VALUES 
  (9,9,12,'2014-08-10 21:39:53',NULL);

COMMIT;

#
# Data for the `tbl_signorder` table  (LIMIT 0,500)
#

INSERT INTO `tbl_signorder` (`id`, `size`, `on_sign`, `street_address`, `suburb`, `status`, `line1`, `line2`, `line3`, `line4`, `installation_date`, `installation_instruction`, `photo`, `sent_order`, `sent_date`, `createdby_id`, `create_date`, `modifiedby_id`, `modified_date`, `approvedby_id`, `approved_date`, `is_valid`, `salesperson_id`, `type`) VALUES 
  (2,'small',1,'sadsdf','fds','installed','fds','fds','dsf','fdsfds','2014-07-30','fdssdfds','http://i.imgur.com/F382KFk.jpg','order','0000-00-00',7,'2014-07-30',NULL,NULL,NULL,NULL,0,10,'sale'),
  (3,'small',1,'gf','das','ordered','das','das','as','da','2014-07-31','dsa','','order','0000-00-00',46,'2014-07-31',NULL,NULL,NULL,NULL,0,7,'sale'),
  (4,'medium',0,'Test','test','ordered','fadas','das','das','da','2014-08-01','sadas','dass','order','0000-00-00',7,'2014-08-01',NULL,NULL,NULL,NULL,0,NULL,'auction'),
  (5,'medium',0,'tewasd','dasasd','ordered','das','dsa','asd','dsa','2014-08-03','sdasdsa','1407042332.JPG','order','0000-00-00',7,'2014-08-03',7,'2014-08-03',NULL,NULL,0,NULL,'lease'),
  (6,'medium',0,'s','dsa','installed','dsa','sad','sad','sad','2014-08-11','sadasd','1407776274.JPG','order','0000-00-00',7,'2014-08-11',NULL,NULL,7,'2014-08-11',0,NULL,'auction');

COMMIT;

#
# Data for the `tbl_signorderpersons` table  (LIMIT 0,500)
#

INSERT INTO `tbl_signorderpersons` (`id`, `signorderid`, `person_name`) VALUES 
  (1,4,'test'),
  (2,4,'test2'),
  (3,5,'das'),
  (4,5,'ggasdsa'),
  (5,6,'dsa'),
  (6,6,'sda');

COMMIT;

#
# Data for the `tbl_transactionreport` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transactionreport` (`transactionreportid`, `transactiontype`, `address`, `saleleaseprice`, `agreementdate`, `unconditionaldate`, `posessiondate`, `settlementdate`, `listertype`, `listeruserpersonid`, `listerotherperson`, `listershare`, `sellertype`, `selleruserpersonid`, `sellerotherperson`, `sellershare`, `ispowerteam`, `listingsplitamt`, `listingreferralamt`, `listinggrossbroughtin`, `sellingsplitamt`, `sellingreferralamt`, `sellinggrossbroughtin`, `conjuctionaldefaultsplit`, `conjuctionalothersplit`, `conjuctionalothercompany`, `commissionamtexcGST`, `commissionamtincGST`, `commissiondepositamtincGST`, `commissiondepositamtexcGST`, `ptdrnumber`, `createdtime`, `modifiedtime`, `createdbyid`, `modifiedbyid`, `status`, `listingreferralperc`, `sellingreferralperc`, `ispowerteamseller`, `listerotherpersonname`, `sellerotherpersonname`, `markdelete`, `isdraft`) VALUES 
  (61,2,'Suite C, First Floor, Building 6, 331 Rosedale Road, Albany, Auckland',0,'2014-04-02','2014-04-02','2014-04-02','2014-04-02','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,1126.66,0,1126.66,1126.66,0,1126.66,NULL,'','',2253.33,2591.33,0,0,'200254','2014-04-02','2014-07-15 11:21:10',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (62,1,'Suite 4, Building D, 59 Apollo Drive, Albany',2012.5,'2014-04-11','2014-04-11','2014-04-11','2014-04-11','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,875,0,875,875,0,875,NULL,'','',1750,2012.5,0,0,'200256','2014-04-11','2014-07-15 11:53:33',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (63,1,'Shop 4, 919 Dominion Rd, Mt Roskill, Auckland',2586.67,'1969-12-31','1969-12-31','1969-12-31','1969-12-31','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,1293.34,0,1293.34,1293.34,0,1293.34,NULL,'','',2586.67,2974.67,0,0,'INV-0486','2014-04-14','2014-07-15 12:27:24',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (64,1,'108 Commercial Rd, SH16, Helensville',18400,'2014-04-14','2014-04-14','2014-04-14','2014-04-14','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,8000,1600,6400,8000,0,8000,NULL,'','',16000,18400,0,0,'200253','2014-04-14','2014-07-15 12:42:43',7,NULL,0,20,0,0,NULL,NULL,0,0),
  (65,2,'211-213 Sandringham Road, Sandringham, Auckland',7667.67,'2014-04-15','2014-04-15','2014-04-15','2014-04-15','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,3333.34,0,3333.34,3333.34,0,3333.34,NULL,'','',6666.67,7666.67,0,0,'200319','2014-04-15','2014-07-15 12:57:54',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (66,3,'338 Great South Road, Papatoetoe',4331.67,'2014-04-15','2014-04-15','2014-04-15','2014-04-15','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,1883.34,0,1883.34,1883.34,0,1883.34,NULL,'','',3766.67,4331.67,0,0,'200570','2014-04-15','2014-07-15 13:15:37',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (67,1,'Suite 5, Level 10, 300 Queen St, Auckland Central',5540,'1970-01-01','1970-01-01','1970-01-01','1970-01-01','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,2408.91,0,2408.91,2408.91,0,2408.91,NULL,'','',4817.83,5540.5,0,0,'INV-0478','2014-04-18','2014-07-15 13:40:00',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (68,3,'Level 3, 16-22 Anzac Ave, Tasman Bldg, Akl',3450,'2014-07-18','2014-07-18','2014-07-18','2014-07-18','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,1500,0,1500,1500,0,1500,NULL,'','',3000,3450,0,0,'INV-0500','2014-04-28','2014-07-15 17:25:57',7,NULL,2,0,0,0,NULL,NULL,0,0),
  (69,1,'a',1,'2014-07-17','2014-07-23','2014-07-15','2014-07-14','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,0.5,0,0.5,0.5,0,0.5,NULL,'','',1,1.15,0,0,'1','2014-07-17','2014-07-17 21:46:25',7,NULL,0,0,0,0,NULL,NULL,1,0),
  (70,4,'sad',25000,'1970-01-01','1970-01-01','1970-01-01','1970-01-01','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,600,120,480,600,120,480,NULL,'','',1200,1380,0,0,'e232','2014-07-17','2014-07-17 23:51:52',7,NULL,0,20,20,1,NULL,NULL,1,0),
  (71,1,'dsa',124,'2014-07-17','2014-07-18','2014-07-18','2014-07-18','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,5000,0,5000,5000,0,5000,NULL,'','',10000,11500,0,0,'213','2014-07-18','2014-07-18 22:11:22',7,NULL,NULL,0,0,0,NULL,NULL,1,0),
  (72,3,'Lot 2, 14 Eastwood Rise, Browns Bay, Akl',32740.5,'2014-05-02','2014-05-02','2014-05-02','2014-05-02','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,14235,1423.5,12811.5,14235,0,14235,NULL,'','',28470,32740.5,0,0,'INV-0413','2014-05-02','2014-07-19 21:28:00',7,NULL,1,10,0,0,NULL,NULL,0,0),
  (73,2,'sda',25000,'2014-07-22','2014-07-23','2014-07-23','2014-07-23','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,5000,0,5000,5000,0,5000,NULL,'','',10000,11500,0,0,'1234','2014-07-22','2014-07-22 11:55:07',7,NULL,NULL,0,0,0,NULL,NULL,1,0),
  (74,1,'Test',25000,'2014-07-22','2014-07-22','2014-07-22','2014-07-22','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,5000,0,5000,5000,0,5000,NULL,'','',10000,11500,0,0,'12345','2014-07-22','2014-07-22 22:00:23',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (75,2,'test',25000,'2014-07-31','2014-07-31','2014-07-31','2014-07-31','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,5000,0,5000,5000,0,5000,NULL,'','',10000,11500,0,0,'12342','2014-07-31','2014-07-31 20:19:55',46,NULL,0,0,0,0,NULL,NULL,0,0),
  (76,1,'a',25000,'2014-08-06','2014-08-06','2014-08-12','2014-08-13','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,5000,0,5000,5000,0,5000,NULL,'','',10000,11500,0,0,'2255','2014-08-05','2014-08-06 00:25:10',7,NULL,0,0,0,0,NULL,NULL,0,0),
  (77,1,'sad',123,'2014-08-11','2014-08-11','2014-08-11','2014-08-11','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,500,100,400,500,0,500,NULL,'','',1000,1150,0,0,'asdasd','2014-08-11','2014-08-11 15:17:00',7,NULL,NULL,20,0,0,NULL,NULL,1,0),
  (78,2,'dsa',21,'2014-10-13','2014-10-22','2014-10-14','2014-10-14','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,0,0.5,0,0.5,0.5,0,0.5,NULL,'','',1,1.15,2.3,2,'2','2014-10-12','2014-10-13 01:13:01',46,NULL,0,0,0,0,NULL,NULL,0,1),
  (79,1,'Test',25000,'2014-10-24','2014-10-24','2014-10-24','2014-10-24','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,5.5,1.1,4.4,5.5,1.1,4.4,NULL,'','',11,12.65,25.3,22,'2323232','2014-10-23','2014-10-24 01:41:03',7,NULL,0,20,20,1,NULL,NULL,0,0),
  (80,1,'213',200000,'2014-11-12','2014-11-12','2014-11-12','2014-11-12','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,200000,40000,160000,200000,40000,160000,NULL,'244','21312',400000,460000,2300,2000,'21312','2014-11-11','2014-11-11 11:41:26',7,NULL,0,20,20,1,NULL,NULL,0,0),
  (81,2,'dsaasdsa',25000,'2014-11-13','2014-11-13','2014-11-13','2014-11-13','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,25000,5000,20000,25000,5000,20000,NULL,'','',50000,57500,34500,30000,'2132312','2014-11-12','2014-11-12 22:48:38',44,NULL,0,20,20,1,NULL,NULL,0,0),
  (82,3,'Adasd',25000,'2014-11-13','2014-11-13','2014-11-13','2014-11-13','colleague',NULL,NULL,50,'colleague',NULL,NULL,50,1,1250,250,1000,1250,250,1000,NULL,'','',2500,2875,3450,3000,'324432','2014-11-12','2014-11-12 23:00:04',44,NULL,0,20,20,1,NULL,NULL,0,0);

COMMIT;

#
# Data for the `tbl_transaction` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transaction` (`transactionid`, `transactiondate`, `userid`, `property`, `propertylistingsplit`, `propertysellingsplit`, `ptr`, `totalcommissionrecievedincGST`, `totalcommissionrecievedexcGST`, `listingpartshareindividual`, `sellingpartshareindividual`, `listingpartreferal`, `listingreferalpayableto`, `sellingpartreferal`, `sellingreferalpayableto`, `lessdeductionsexcGST`, `bonusdeductions`, `createdtime`, `modifiedtime`, `createdby`, `modifiedby`, `transactionreportid`, `isclosed`, `companyinvoice`, `invoicedate`, `paymentrecieveddate`, `paymenttosalesperson`, `bonusdeductiondesc`, `gst`) VALUES 
  (1,'2014-04-02',12,'Suite C, First Floor, Building 6, 331 Rosedale Road, Albany, Auckland',0,0,'200254',2591.33,2253.33,25,50,0,'',0,'',0,0,'0000-00-00','2014-07-15 11:26:30',NULL,NULL,61,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (2,'2014-04-02',25,'Suite C, First Floor, Building 6, 331 Rosedale Road, Albany, Auckland',0,0,'200254',2591.33,2253.33,25,50,0,'',0,'',0,-80,'0000-00-00','2014-07-15 11:47:34',NULL,NULL,61,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (3,'2014-04-11',12,'Suite 4, Building D, 59 Apollo Drive, Albany',0,0,'200256',1750,1750,50,50,0,'',0,'',0,-259.98,'0000-00-00','2014-07-15 12:15:48',NULL,NULL,62,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (4,'2014-04-14',11,'Shop 4, 919 Dominion Rd, Mt Roskill, Auckland',0,0,'INV-0486',2974.67,2586.67,100,100,0,'',0,'',0,0,'0000-00-00','2014-07-15 12:30:34',NULL,NULL,63,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (5,'2014-04-14',25,'108 Commercial Rd, SH16, Helensville',0,0,'200253',18400,16000,50,50,20,'powerteam@jameslaw.co.nz',0,'',0,0,'0000-00-00','2014-07-15 12:45:07',NULL,NULL,64,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (6,'2014-04-14',12,'108 Commercial Rd, SH16, Helensville',0,0,'200253',18400,16000,50,50,20,'powerteam@jameslaw.co.nz',0,'',0,0,'0000-00-00','2014-07-15 12:45:29',NULL,NULL,64,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (7,'2014-04-15',8,'211-213 Sandringham Road, Sandringham, Auckland',0,0,'200319',7666.67,6666.67,100,100,0,'',0,'',0,0,'0000-00-00','2014-07-15 13:12:37',NULL,NULL,65,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (8,'2014-04-15',30,'338 Great South Road, Papatoetoe',0,0,'200570',4331.67,3766.67,100,100,0,'',0,'',0,0,'0000-00-00','2014-07-15 13:18:38',NULL,NULL,66,0,'','2014-01-01','2014-01-01','2014-01-01','',NULL),
  (9,'2014-04-18',17,'Suite 5, Level 10, 300 Queen St, Auckland Central',0,0,'INV-0478',5540.5,4817.83,33.33,50,0,'',0,'',0,0,'0000-00-00','2014-07-15 13:51:22',NULL,NULL,67,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (10,'2014-04-18',28,'Suite 5, Level 10, 300 Queen St, Auckland Central',0,0,'INV-0478',4817.83,4817.83,33.33,50,0,'',0,'',0,-44.5,'0000-00-00','2014-07-15 14:03:29',NULL,NULL,67,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (11,'2014-04-28',17,'Level 3, 16-22 Anzac Ave, Tasman Bldg, Akl',0,0,'INV-0500',3000,3000,50,50,0,'',0,'',0,0,'0000-00-00','2014-07-15 17:28:07',NULL,NULL,68,0,'','1970-01-01','1970-01-01','1970-01-01','',NULL),
  (12,'2014-04-28',28,'Level 3, 16-22 Anzac Ave, Tasman Bldg, Akl',0,0,'INV-0500',3450,3000,50,50,0,'',0,'',0,0,'0000-00-00','2014-07-15 17:28:18',NULL,NULL,68,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (17,'2014-07-17',12,'sad',0,0,'e232',1200,1200,50,0,20,'powerteam@jameslaw.co.nz',20,'powerteam@jameslaw.co.nz',0,0,'0000-00-00','2014-07-17 23:59:00',NULL,NULL,70,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (18,'2014-07-17',11,'sad',0,0,'e232',1380,1200,0,100,20,'powerteam@jameslaw.co.nz',20,'powerteam@jameslaw.co.nz',0,0,'0000-00-00','2014-07-18 00:00:12',NULL,NULL,70,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (23,'2014-07-18',17,'dsa',0,0,'213',11500,10000,33,0,0,'',0,'',0,0,'0000-00-00','2014-07-19 01:27:38',NULL,NULL,71,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (24,'2014-05-02',12,'Lot 2, 14 Eastwood Rise, Browns Bay, Akl',0,0,'INV-0413',32740.5,28470,50,50,10,'James Law',0,'',0,-129.56,'0000-00-00','2014-07-19 21:36:45',NULL,NULL,72,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (25,'2014-04-11',25,'Suite 4, Building D, 59 Apollo Drive, Albany',0,0,'200256',2012.5,1750,50,50,0,'',0,'',0,0,'0000-00-00','2014-07-19 21:42:02',NULL,NULL,62,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (27,'2014-07-22',6,'sda',0,0,'1234',11500,10000,100,0,0,'',0,'',0,-200,'0000-00-00','2014-07-22 12:02:22',NULL,NULL,73,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (28,'2014-07-22',12,'sda',0,0,'1234',11500,10000,0,100,0,'',0,'',0,0,'0000-00-00','2014-07-22 21:44:47',NULL,NULL,73,NULL,'','1970-01-01','1970-01-01','1970-01-01','No Bonus / Deduction',15),
  (29,'2014-07-22',7,'Test',0,0,'12345',11500,10000,100,0,0,'',0,'',0,-1,'0000-00-00','2014-07-26 21:26:31',NULL,NULL,74,1,'INV','2014-07-23','2014-07-26','2014-07-11','',15),
  (31,'2014-08-11',7,'sad',0,0,'asdasd',1150,1000,100,0,20,'powerteam@jameslaw.co.nz',0,'',0,8,'0000-00-00','2014-08-12 00:56:05',NULL,NULL,77,1,'','1970-01-01','1970-01-01','1970-01-01','Positive',15),
  (33,'2014-08-05',6,'a',0,0,'2255',11500,10000,100,0,0,'',0,'',0,400,'0000-00-00','2014-08-13 00:38:24',NULL,NULL,76,1,'','1970-01-01','1970-01-01','1970-01-01','Test',15),
  (35,'2014-07-31',10,'test',0,0,'12342',11500,10000,0,100,0,'',0,'',0,0,'0000-00-00','2014-08-14 11:43:07',NULL,NULL,75,1,'dsa','2014-08-14','2014-08-14','2014-08-14','No Bonus / Deduction',15),
  (37,'2014-07-31',9,'test',0,0,'12342',11500,10000,100,0,0,'',0,'',0,300,'0000-00-00','2014-08-15 01:30:12',NULL,NULL,75,1,'ddsa','2014-08-15','2014-08-15','2014-08-15','sad',15),
  (38,'2014-07-22',11,'Test',0,0,'12345',11500,10000,0,100,0,'',0,'',0,0,'0000-00-00','2014-09-28 20:23:30',NULL,NULL,74,1,'','1970-01-01','1970-01-01','1970-01-01','No Bonus / Deductions',15);

COMMIT;

#
# Data for the `tbl_transactioncompanycommission` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transactioncompanycommission` (`transactioncompanycommissionid`, `transactionid`, `totalamtincGST`, `totalGSTcomponent`, `totalpayoutshare`, `companyshare`, `totalpaytosalesperson`, `totalpayetransfer`, `payablepaye`, `GSTtransfertootheraccnt`) VALUES 
  (1,1,971.76,126.75,60,40,481.66,101.4,583.06,50.7),
  (2,2,971.76,126.75,60,40,389.66,101.4,491.06,62.7),
  (3,3,1006.25,131.25,60,40,199.77,105,304.77,91.5),
  (4,4,2974.68,388,55,45,1636.07,0,1636.07,174.6),
  (5,5,8280,1080,60,40,4104,864,4968,432),
  (6,6,8280,1080,60,40,4104,864,4968,432),
  (7,7,7666.68,1000,55,45,3483.34,733.33,4216.67,450),
  (8,8,4331.68,565,75,25,1789.17,376.67,2165.84,282.5),
  (9,9,2308.45,301.1,60,40,953.49,200.74,1154.23,150.55),
  (10,10,2308.44,301.1,50,50,902.32,200.73,1103.05,157.22),
  (11,11,1725,225,60,40,600,150,750,225),
  (12,12,1725,225,50,50,712.5,150,862.5,112.5),
  (17,17,276,36,60,40,136.8,28.8,165.6,14.4),
  (18,18,552,72,55,45,303.6,0,303.6,32.4),
  (23,23,1897.5,247.5,60,40,783.75,165,948.75,123.75),
  (24,24,15551.74,2028.49,60,40,7559.26,1622.79,9182.05,830.83),
  (25,25,1006.25,131.25,60,40,498.75,105,603.75,52.5),
  (27,27,5750,750,55,45,2382.5,550,2932.5,367.5),
  (28,28,5750,750,60,40,3450,600,4050,300),
  (29,29,5750,750,60,40,3449.85,600,4049.85,300.15),
  (31,31,460,60,60,40,237.2,48,285.2,22.8),
  (32,33,5750,750,55,45,2272.5,550,2822.5,277.5),
  (34,35,5750,750,60,40,3450,0,3450,300),
  (36,37,5750,750,55,45,2267.5,550,2817.5,382.5),
  (37,38,5750,750,55,45,3162.5,0,3162.5,337.5);

COMMIT;

#
# Data for the `tbl_transactiondetails` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transactiondetails` (`transactiondetailsid`, `transactionid`, `netcommisionshare`, `listingsplitamt`, `lesslistingreferalamt`, `listinggrossbroughtin`, `sellersplitamt`, `lesssellingreferealamt`, `sellinggrossbroughtin`, `listernetbroughtfees`, `listerfeeshareamt`, `sellerfeeshareamt`, `totalfeesbroughtin`, `individualshare`, `withholdingtax`, `individualshareamt`, `companyshare`, `checkindividualcompany`, `sellernetbroughtfees`) VALUES 
  (1,1,2253.33,1126.67,0,1126.67,1126.67,0,1126.67,1126.67,281.67,563.34,845.01,60,20,507.01,338,845.01,NULL),
  (2,2,2253.33,1126.67,0,1126.67,1126.67,0,1126.67,1126.67,281.67,563.34,845.01,60,20,507.01,338,845.01,NULL),
  (3,3,1750,875,0,875,875,0,875,875,437.5,437.5,875,60,20,525,350,875,NULL),
  (4,4,2586.67,1293.34,0,1293.34,1293.34,0,1293.34,1293.34,1293.34,1293.34,2586.68,55,0,1422.67,1164.01,2586.68,NULL),
  (5,5,16000,8000,1600,6400,8000,0,8000,8000,3200,4000,7200,60,20,4320,2880,7200,NULL),
  (6,6,16000,8000,1600,6400,8000,0,8000,8000,3200,4000,7200,60,20,4320,2880,7200,NULL),
  (7,7,6666.67,3333.34,0,3333.34,3333.34,0,3333.34,3333.34,3333.34,3333.34,6666.68,55,20,3666.67,3000.01,6666.68,NULL),
  (8,8,3766.67,1883.34,0,1883.34,1883.34,0,1883.34,1883.34,1883.34,1883.34,3766.68,50,20,1883.34,1883.34,3766.68,1883.34),
  (9,9,4817.83,2408.92,0,2408.92,2408.92,0,2408.92,2408.92,802.89,1204.46,2007.35,50,20,1003.68,1003.67,2007.35,NULL),
  (10,10,4817.83,2408.91,0,2408.91,2408.91,0,2408.91,2408.91,802.89,1204.45,2007.34,50,20,1003.67,1003.67,2007.34,NULL),
  (11,11,3000,1500,0,1500,1500,0,1500,1500,750,750,1500,50,20,750,750,1500,NULL),
  (12,12,3000,1500,0,1500,1500,0,1500,1500,750,750,1500,50,20,750,750,1500,NULL),
  (17,17,1200,600,120,480,600,120,480,600,240,0,240,60,20,144,96,240,NULL),
  (18,18,1200,600,120,480,600,120,480,600,0,480,480,55,0,264,216,480,NULL),
  (23,23,10000,5000,0,5000,5000,0,5000,5000,1650,0,1650,50,20,825,825,1650,NULL),
  (24,24,28470,14235,1423.5,12811.5,14235,0,14235,14235,6405.75,7117.5,13523.25,60,20,8113.95,5409.3,13523.25,NULL),
  (25,25,1750,875,0,875,875,0,875,875,437.5,437.5,875,60,20,525,350,875,NULL),
  (27,27,10000,5000,0,5000,5000,0,5000,5000,5000,0,5000,55,20,2750,2250,5000,NULL),
  (28,28,10000,5000,0,5000,5000,0,5000,5000,0,5000,5000,60,20,3000,2000,5000,NULL),
  (29,29,10000,5000,0,5000,5000,0,5000,5000,5000,0,5000,60,20,3000,2000,5000,NULL),
  (31,31,1000,500,100,400,500,0,500,500,400,0,400,60,20,240,160,400,NULL),
  (33,33,10000,5000,0,5000,5000,0,5000,5000,5000,0,5000,55,20,2750,2250,5000,5000),
  (35,35,10000,5000,0,5000,5000,0,5000,5000,0,5000,5000,60,0,3000,2000,5000,5000),
  (37,37,10000,5000,0,5000,5000,0,5000,5000,5000,0,5000,55,20,2750,2250,5000,5000),
  (38,38,10000,5000,0,5000,5000,0,5000,5000,0,5000,5000,55,0,2750,2250,5000,5000);

COMMIT;

#
# Data for the `tbl_transactionmanagers` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transactionmanagers` (`transactionmanagersid`, `transactionid`, `managerid`, `companyshareamt`, `managershare`, `transactionreportid`, `companyinvoice`, `invoicedate`, `paymentrecieveddate`, `paymenttosalesperson`, `isclosed`) VALUES 
  (4,23,6,825,165,71,NULL,NULL,NULL,NULL,0),
  (7,9,6,1003.67,200.73,67,'TESDA','2014-08-15','2014-08-04','2014-08-19',1),
  (8,11,6,750,150,68,NULL,NULL,NULL,NULL,0),
  (9,8,8,1883.34,941.67,66,'',NULL,NULL,NULL,0);

COMMIT;

#
# Data for the `tbl_transactionsalesperson` table  (LIMIT 0,500)
#

INSERT INTO `tbl_transactionsalesperson` (`transactionsalespersonid`, `transactionid`, `bonusdeduction`, `netsharebeforetax`, `withholdingtax`, `amtinvoicecompany`, `plusgst`, `grandtotal`, `bonusdeductiongst`) VALUES 
  (1,1,0,507.01,101.4,405.61,76.05,481.66,NULL),
  (2,2,-80,427.01,101.4,325.61,64.05,389.66,NULL),
  (3,3,-259.98,265.02,105,160.02,39.75,199.77,NULL),
  (4,4,0,1422.67,0,1422.67,213.4,1636.07,NULL),
  (5,5,0,4320,864,3456,648,4104,NULL),
  (6,6,0,4320,864,3456,648,4104,NULL),
  (7,7,0,3666.67,733.33,2933.34,550,3483.34,NULL),
  (8,8,0,1883.34,376.67,1883.34,282.5,1789.17,0),
  (9,9,0,1003.68,200.74,802.94,150.55,953.49,NULL),
  (10,10,-44.5,959.17,200.73,758.44,143.88,902.32,NULL),
  (11,11,0,750,150,750,0,600,0),
  (12,12,0,750,150,600,112.5,712.5,NULL),
  (17,17,0,144,28.8,115.2,21.6,136.8,NULL),
  (18,18,0,264,0,264,39.6,303.6,NULL),
  (23,23,0,825,165,660,123.75,783.75,NULL),
  (24,24,-129.56,7984.39,1622.79,6361.6,1197.66,7559.26,NULL),
  (25,25,0,525,105,420,78.75,498.75,NULL),
  (27,27,-200,2550,550,2000,382.5,2382.5,NULL),
  (28,28,0,3000,600,3000,450,3450,NULL),
  (29,29,-1,2999,600,3000,449.85,3449.85,NULL),
  (31,31,8,248,48,240,37.2,237.2,NULL),
  (33,33,400,2350,550,2750,472.5,2272.5,10),
  (35,35,0,3000,0,3000,450,3450,0),
  (37,37,300,2450,550,2750,367.5,2267.5,1),
  (38,38,0,2750,0,2750,412.5,3162.5,0);

COMMIT;

#
# Data for the `tbl_trconfirmation` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trconfirmation` (`trconfirmationid`, `transactionreportid`, `listingpersonagreed`, `listingpersonagreeddate`, `listingpersonsigniture`, `lisingpersonipaddress`, `sellingpersonagreed`, `sellingpersonagreeddate`, `sellingpersonsigniture`, `sellingpersonipaddress`, `manageragreed`, `manageragreeddate`, `managersigniture`, `manageripaddress`, `isagree`, `fileagreement`, `fileagreementlocalname`, `fileTransaction`, `filetransactionlocalname`, `fileotherdoc1`, `fileotherdoc1localname`, `fileotherdoc2`, `fileotherdoc2localname`) VALUES 
  (56,61,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (57,62,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (58,63,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (59,64,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (60,65,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (61,66,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (62,67,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (63,68,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (64,69,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (65,70,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (66,71,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (67,72,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (68,73,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (69,74,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (70,75,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (71,76,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (72,77,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'agr1407741421.pdf','Jan Vincent Junsay.pdf',NULL,NULL,NULL,NULL,NULL,NULL),
  (73,78,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (74,79,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (75,80,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (76,81,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
  (77,82,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

COMMIT;

#
# Data for the `tbl_trlistingpersons` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trlistingpersons` (`id`, `transactionreportid`, `type`, `userid`, `otherperson`, `share`) VALUES 
  (23,61,'colleague',12,NULL,25),
  (24,61,'colleague',25,NULL,25),
  (25,62,'colleague',12,NULL,50),
  (26,62,'colleague',25,NULL,50),
  (28,63,'colleague',11,NULL,100),
  (29,64,'colleague',25,NULL,50),
  (30,64,'colleague',12,NULL,50),
  (34,65,'colleague',8,NULL,100),
  (44,69,'colleague',8,NULL,50),
  (55,70,'colleague',12,NULL,50),
  (56,67,'colleague',17,NULL,33.33),
  (57,67,'colleague',28,NULL,33.33),
  (60,68,'colleague',17,NULL,50),
  (61,68,'colleague',28,NULL,50),
  (62,66,'colleague',30,NULL,100),
  (63,71,'colleague',17,NULL,33),
  (72,73,'colleague',6,NULL,100),
  (74,74,'colleague',7,NULL,100),
  (77,75,'colleague',9,NULL,100),
  (78,76,'colleague',6,NULL,100),
  (79,77,'colleague',7,NULL,100),
  (80,72,'colleague',12,NULL,50),
  (81,72,'colleague',25,NULL,50),
  (82,78,'colleague',6,NULL,1),
  (83,79,'colleague',11,NULL,100),
  (84,80,'colleague',8,NULL,100),
  (85,81,'colleague',7,NULL,100),
  (86,82,'colleague',12,NULL,100);

COMMIT;

#
# Data for the `tbl_trlogs` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trlogs` (`id`, `transactionreportid`, `message`, `createdtime`, `createdbyid`) VALUES 
  (1,70,'Transaction report was created.','2014-07-17 23:51:53',7),
  (2,70,'Transaction report was updated.','2014-07-17 23:53:15',44),
  (4,70,'Commission calculation for Michelle Kennedy was saved.','2014-07-17 23:59:01',44),
  (5,70,'Commission calculation for Michelle Kennedy was saved.','2014-07-17 23:59:26',44),
  (6,70,'Commission calculation for Michael Chen was saved.','2014-07-18 00:00:13',44),
  (7,70,'Transaction report was updated.','2014-07-18 00:00:31',44),
  (8,70,'Transaction report was confirmed.','2014-07-18 00:03:06',44),
  (9,70,'Transaction report was confirmed.','2014-07-18 00:45:46',44),
  (10,67,'Transaction report was updated.','2014-07-18 01:24:53',44),
  (11,68,'Transaction report was updated.','2014-07-18 01:25:08',44),
  (12,68,'Commission calculation for Jim Young was updated.','2014-07-18 12:26:02',7),
  (13,68,'Transaction report was updated.','2014-07-18 12:26:42',7),
  (14,66,'Transaction report was updated.','2014-07-18 14:41:13',7),
  (15,71,'Transaction report was created.','2014-07-18 22:11:22',7),
  (16,71,'Commission calculation for Jim Young was saved.','2014-07-18 22:21:59',7),
  (17,71,'Commission calculation for Jim Young was saved.','2014-07-18 22:24:06',7),
  (18,71,'Commission calculation for Jim Young was saved.','2014-07-19 00:15:16',7),
  (19,71,'Commission calculation for Jim Young was saved.','2014-07-19 01:27:38',7),
  (20,68,'Commission calculation for Jim Young was updated.','2014-07-19 01:31:58',7),
  (21,68,'Commission calculation for Jim Young was updated.','2014-07-19 01:34:55',7),
  (22,68,'Commission calculation for Tina Huang was updated.','2014-07-19 01:35:45',7),
  (23,61,'Commission calculation for Michelle Kennedy was updated.','2014-07-19 01:35:55',7),
  (24,61,'Commission calculation for Kim Diack was updated.','2014-07-19 01:36:01',7),
  (25,62,'Commission calculation for Michelle Kennedy was updated.','2014-07-19 01:36:11',7),
  (26,66,'Commission calculation for Katie Luo was updated.','2014-07-19 01:37:11',7),
  (27,67,'Commission calculation for Jim Young was updated.','2014-07-19 01:38:34',7),
  (28,68,'Commission calculation for Tina Huang was updated.','2014-07-19 18:07:38',7),
  (29,72,'Transaction report was created.','2014-07-19 21:28:00',7),
  (30,72,'Transaction report was updated.','2014-07-19 21:28:51',7),
  (31,72,'Transaction report was updated.','2014-07-19 21:32:11',7),
  (32,72,'Commission calculation for Michelle Kennedy was saved.','2014-07-19 21:36:45',7),
  (33,72,'Transaction report was updated.','2014-07-19 21:39:29',7),
  (34,61,'Commission calculation for Kim Diack was updated.','2014-07-19 21:41:52',7),
  (35,62,'Commission calculation for Kim Diack was saved.','2014-07-19 21:42:03',7),
  (36,64,'Commission calculation for Kim Diack was updated.','2014-07-19 21:42:15',7),
  (37,64,'Commission calculation for Michelle Kennedy was updated.','2014-07-19 21:42:20',7),
  (38,73,'Transaction report was created.','2014-07-22 11:55:08',7),
  (39,73,'Commission calculation for Burt Ong was saved.','2014-07-22 12:02:24',7),
  (40,73,'Commission calculation for Michelle Kennedy was saved.','2014-07-22 21:44:48',7),
  (41,74,'Transaction report was created.','2014-07-22 22:00:24',7),
  (42,74,'Commission calculation for James Law was saved.','2014-07-22 22:01:01',7),
  (43,74,'Commission calculation for James Law was saved.','2014-07-22 22:03:41',7),
  (44,74,'Transaction report was updated.','2014-07-22 22:10:28',7),
  (45,74,'Commission calculation for James Law was saved.','2014-07-22 22:14:12',7),
  (46,74,'Commission calculation for James Law was updated.','2014-07-22 22:14:21',7),
  (47,74,'Commission calculation for James Law was saved.','2014-07-22 22:16:49',7),
  (48,72,'Transaction report was confirmed.','2014-07-22 22:23:51',7),
  (49,74,'Commission calculation for James Law was saved.','2014-07-22 22:34:23',7),
  (50,74,'Commission calculation for James Law was saved.','2014-07-22 22:38:08',7),
  (51,74,'Commission calculation for James Law was saved.','2014-07-22 22:43:59',7),
  (52,74,'Commission calculation for James Law was saved.','2014-07-26 21:26:31',7),
  (53,74,'Commission calculation for James Law was updated.','2014-07-28 21:09:31',7),
  (54,74,'Commission calculation for James Law was updated.','2014-07-28 21:10:02',7),
  (55,74,'Commission calculation for James Law was updated.','2014-07-28 21:11:12',7),
  (56,74,'Commission calculation for James Law was updated.','2014-07-28 21:11:30',7),
  (57,74,'Commission calculation for James Law was updated.','2014-07-29 08:35:58',7),
  (58,74,'Commission calculation for James Law was updated.','2014-07-29 08:38:38',7),
  (59,74,'Commission calculation for James Law was updated.','2014-07-29 08:40:09',7),
  (60,74,'Commission calculation for James Law was updated.','2014-07-29 08:40:44',7),
  (61,74,'Commission calculation for James Law was updated.','2014-07-29 08:42:30',7),
  (62,74,'Commission calculation for James Law was updated.','2014-07-29 08:43:51',7),
  (63,75,'Transaction report was created.','2014-07-31 20:19:56',46),
  (64,68,'Commission calculation for Jim Young was updated.','2014-08-06 00:22:34',7),
  (65,75,'Commission calculation for Luna Lu was saved.','2014-08-06 00:24:10',7),
  (66,76,'Transaction report was created.','2014-08-06 00:25:11',7),
  (67,77,'Transaction report was created.','2014-08-11 15:17:02',7),
  (68,77,'Commission calculation for James Law was saved.','2014-08-12 00:56:06',7),
  (69,74,'Commission calculation for Michael Chen was saved.','2014-08-12 13:23:31',7),
  (70,77,'Commission calculation for James Law was updated.','2014-08-12 20:48:39',7),
  (71,76,'Commission calculation for Burt Ong was saved.','2014-08-13 00:38:25',7),
  (72,76,'Commission calculation for Burt Ong was updated.','2014-08-13 00:38:30',7),
  (73,75,'Commission calculation for Martin Chin was saved.','2014-08-14 11:40:06',7),
  (74,75,'Commission calculation for Martin Chin was saved.','2014-08-14 11:43:11',7),
  (75,66,'Commission calculation for Katie Luo was updated.','2014-08-15 00:46:03',7),
  (76,75,'Commission calculation for Luna Lu was saved.','2014-08-15 01:26:11',7),
  (77,75,'Commission calculation for Luna Lu was updated.','2014-08-15 01:26:34',7),
  (78,75,'Commission calculation for Luna Lu was updated.','2014-08-15 01:26:59',7),
  (79,75,'Commission calculation for Luna Lu was saved.','2014-08-15 01:30:12',7),
  (80,75,'Commission calculation for Luna Lu was updated.','2014-08-15 01:30:25',7),
  (81,72,'Transaction report was confirmed.','2014-09-17 20:25:22',7),
  (82,76,'Commission calculation for Burt Ong was updated.','2014-09-28 19:32:07',7),
  (83,74,'Commission calculation for Michael Chen was saved.','2014-09-28 20:23:30',7),
  (84,74,'Commission calculation for Michael Chen was updated.','2014-09-28 20:23:36',7),
  (85,75,'Commission calculation for Martin Chin was updated.','2014-09-28 20:28:11',7),
  (86,78,'Transaction report was created.','2014-10-13 01:13:02',46),
  (87,79,'Transaction report was created.','2014-10-24 01:41:12',7),
  (88,80,'Transaction report was created.','2014-11-11 11:41:35',7),
  (89,80,'Transaction report was updated.','2014-11-11 11:50:57',7),
  (90,81,'Transaction report was created.','2014-11-12 22:48:45',44),
  (91,82,'Transaction report was created.','2014-11-12 23:00:11',44);

COMMIT;

#
# Data for the `tbl_trnotes` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trnotes` (`trnotesid`, `transactionreportid`, `notes`, `ispropertyinspected`, `ispropertyinspectedtext`, `statementboundaries`, `statementconstruction`, `ispotentialproblems`, `ispotentialproblemtext`, `ispurchaserwarranty`, `isvendorwarranty`, `isGSTdiscussion`, `isGSTdiscussiontext`, `isadvicesought`, `isadvicesoughttext`, `ispurchaserboughtproperty`, `isviewedandappraised`, `isdifferenceopinion`, `isdifferenceopiniontext`, `iscommissiondiscussed`, `iscommissiondiscusstext`, `isvendorincludechattel`, `isvendorincludechatteltext`, `isreferaccountant`, `isreferaccountanttext`, `vendordescriberesalevalue`, `purchaserdiscussedresalevalue`, `othercomments`, `isagree`, `ispurchaserwarrantytext`, `isvendorwarrantytext`) VALUES 
  (49,61,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (50,62,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (51,63,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (52,64,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (53,65,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (54,66,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (55,67,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (56,68,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (57,69,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (58,70,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (59,71,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (60,72,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (61,73,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (62,74,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (63,75,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (64,76,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (65,77,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (66,78,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (67,79,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (68,80,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (69,81,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'',''),
  (70,82,NULL,1,'','','',0,'',0,0,0,'',0,'',0,0,0,'',0,'',0,'',0,'','','','',NULL,'','');

COMMIT;

#
# Data for the `tbl_trpurchasertenantasignee` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trpurchasertenantasignee` (`trpurchasertenantasigneeid`, `transactionreportid`, `type`, `legalentity`, `contactname`, `contactnumber`, `email`, `faxnumber`, `streetaddress`, `suburb`, `city`, `countryid`, `solicitorsfirm`, `individualactingfirstname`, `individualactinglastname`, `individualactingemail`, `individualactingphonenumber`, `individualactingfaxnumber`) VALUES 
  (55,61,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (56,62,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (57,63,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (58,64,3,'','','','','','','','Auckland',NULL,'','','','','',''),
  (59,65,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (60,66,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (61,67,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (62,68,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (63,69,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (64,70,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (65,71,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (66,72,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (67,73,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (68,74,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (69,75,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (70,76,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (71,77,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (72,78,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (73,79,2,'dassa','dsad','asad','asd','dsad','dsads','dsasa','Auckland',NULL,'dsa','dsa','','dsa','dsa','sad'),
  (74,80,3,'','','','','','','','Auckland',NULL,'','','','','',''),
  (75,81,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (76,82,2,'','','','','','','','Auckland',NULL,'','','','','','');

COMMIT;

#
# Data for the `tbl_trsellingpersons` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trsellingpersons` (`id`, `transactionreportid`, `type`, `userid`, `otherperson`, `share`) VALUES 
  (23,61,'colleague',12,NULL,50),
  (24,61,'colleague',25,NULL,50),
  (25,62,'colleague',12,NULL,50),
  (26,62,'colleague',25,NULL,50),
  (28,63,'colleague',11,NULL,100),
  (29,64,'colleague',25,NULL,50),
  (30,64,'colleague',12,NULL,50),
  (34,65,'colleague',8,NULL,100),
  (44,69,'colleague',9,NULL,30),
  (55,70,'colleague',11,NULL,100),
  (56,67,'colleague',17,NULL,50),
  (57,67,'colleague',28,NULL,50),
  (60,68,'colleague',17,NULL,50),
  (61,68,'colleague',28,NULL,50),
  (62,66,'colleague',30,NULL,100),
  (63,71,'colleague',28,NULL,50),
  (72,73,'colleague',12,NULL,100),
  (74,74,'colleague',11,NULL,100),
  (77,75,'colleague',10,NULL,100),
  (78,72,'colleague',12,NULL,50),
  (79,72,'colleague',25,NULL,50),
  (80,78,'colleague',7,NULL,1),
  (81,79,'colleague',12,NULL,100);

COMMIT;

#
# Data for the `tbl_trvendorlandlordassignor` table  (LIMIT 0,500)
#

INSERT INTO `tbl_trvendorlandlordassignor` (`trvendorlandlorassignorid`, `transactionreportid`, `type`, `legalentity`, `contactname`, `contactnumber`, `email`, `faxnumber`, `streetaddress`, `suburb`, `city`, `countryid`, `solicitorsfirm`, `individualactingfirstname`, `individualactinglastname`, `individualactingemail`, `individualactingphonenumber`, `individualactingfaxnumber`) VALUES 
  (65,61,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (66,62,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (67,63,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (68,64,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (69,65,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (70,66,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (71,67,3,'','','','','','','','Auckland',NULL,'','','','','',''),
  (72,68,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (73,69,3,'','','','','','','','Auckland',NULL,'','','','','',''),
  (74,70,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (75,71,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (76,72,3,'','','','','','','','Auckland',NULL,'','','','','',''),
  (77,73,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (78,74,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (79,75,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (80,76,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (81,77,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (82,78,2,'','','','','','','','Auckland',NULL,'','','','','',''),
  (83,79,1,'dasda','sadsa','dsasa','asdsa','ds','asds','sadsa','Auckland',NULL,'dsa','sad','asdsa','sad','sadsa','sda'),
  (84,80,1,'dsa','sad','dsa','ads','dsa','dsa','asd','Auckland',NULL,'asd','asd','dsa','sda','dsa','dsa'),
  (85,81,1,'','','','','','','','Auckland',NULL,'','','','','',''),
  (86,82,2,'','','','','','','','Auckland',NULL,'','','','','','');

COMMIT;

#
# Data for the `tbl_useraccounts` table  (LIMIT 0,500)
#

INSERT INTO `tbl_useraccounts` (`useraccountsid`, `userid`, `accounttype`, `username`, `password`) VALUES 
  (3,6,'user','tempusername6','newpassword'),
  (4,7,'superuser','james@jameslaw.co.nz','jameslaw'),
  (5,8,'user','lettyho@jameslaw.co.nz','jameslaw'),
  (6,9,'user','luna@jameslaw.co.nz','jameslaw'),
  (7,10,'user','martin@jameslaw.co.nz','jameslaw'),
  (8,11,'user','michaelc@jameslaw.co.nz','jameslaw'),
  (9,12,'user','michellek@jameslaw.co.nz','jameslaw'),
  (10,13,'user','paulko@jameslaw.co.nz','jameslaw'),
  (11,14,'user','satish@jameslaw.co.nz','jameslaw'),
  (12,15,'user','vivien@jameslaw.co.nz','jameslaw'),
  (13,16,'user','francis@jameslaw.co.nz','jameslaw'),
  (14,17,'user','jim.young@jameslaw.co.nz','jameslaw'),
  (15,18,'user','graham.truman@jameslaw.co.nz','jameslaw'),
  (16,19,'user','rosh@jameslaw.co.nz','jameslaw'),
  (17,20,'user','arvind@skyproperty.co.nz','jameslaw'),
  (18,21,'user','dianne@jameslaw.co.nz','123456'),
  (19,22,'user','william.lee@jameslaw.co.nz','jameslaw'),
  (20,23,'user','iris.yao@jameslaw.co.nz','jameslaw'),
  (21,24,'user','irene.ng@jameslaw.co.nz','jameslaw'),
  (22,25,'user','kim@jameslaw.co.nz','jameslaw'),
  (23,26,'user','elizabeth@jameslaw.co.nz','jameslaw'),
  (24,27,'user','ivy.bai@jameslaw.co.nz','jameslaw'),
  (25,28,'user','tina.huang@jameslaw.co.nz','jameslaw'),
  (26,29,'user','reinka@jameslaw.co.nz','jameslaw'),
  (27,30,'user','katie.luo@jameslaw.co.nz','jameslaw'),
  (28,31,'user','tracey.lin@jameslaw.co.nz','jameslaw'),
  (29,32,'user','longlong@jameslaw.co.nz','jameslaw'),
  (30,33,'user','john.foreman@jameslaw.co.nz','jameslaw'),
  (31,34,'user','laura.lin@jameslaw.co.nz','jameslaw'),
  (32,35,'user','brite.ling@jameslaw.co.nz','jameslaw'),
  (33,36,'accounts','alison@jameslaw.co.nz','jameslaw'),
  (34,37,'user','li@jameslaw.co.nz','jameslaw'),
  (35,38,'user','fiona.ma@jameslaw.co.nz','jameslaw'),
  (36,39,'user','l.grigson@jameslaw.co.nz','jameslaw'),
  (37,40,'user','stephen.lu@jameslaw.co.nz','jameslaw'),
  (38,41,'user','anshari@jameslaw.co.nz','jameslaw'),
  (39,42,'user','ellen@jameslaw.co.nz','jameslaw'),
  (40,43,'user','jonalyn@jameslaw.co.nz','jameslaw'),
  (41,44,'superuser','jvjunsay@gmail.com','jameslaw'),
  (42,45,'user','fiona.ma@jameslaw.co.nz','jameslaw'),
  (43,46,'user','allen.sun@jameslaw.co.nz','jameslaw'),
  (44,47,'user','sad','jameslaw'),
  (45,48,'user','asdas@sad.com','jameslaw');

COMMIT;

