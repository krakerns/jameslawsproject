<?php
/**
 * Created by PhpStorm.
 * User: JV
 * Date: 7/18/14
 * Time: 1:01 AM
 */

class ProspectController extends Controller
{
    public $layout='bootstraplayout';

    public function actionList()
    {
        $this->render('list');
    }

    public function actionIndex()
    {
        $this->render('list');
    }

    public function actionCreate(){
        $model = new Prospect();
        $model->prospect_type_enum = $_GET['type'];
        $this->render('createnew',array('model'=>$model));
    }

    public function actionGetsuburb(){

        $cityid = $_POST['cityid'];
        $criteria = new CDbCriteria;
        $criteria->condition = "city_id=:val1";
        $criteria->params = array(':val1' => $cityid);
        $criteria->order = 'name';

        $suburbs =  Suburb::model()->findAll($criteria);

        //echo '<option value="-1">Select Client</option>';

        foreach ($suburbs as $suburb){
            //echo '<option value="'.$person->id.'">'.$person->lastname . ' , ' . $person->firstname .'</option>';
            echo $suburb->id .'-'. $suburb->name . ',';
        }
    }

    public function actionSaveprospect(){
        $prospect = new Prospect();
        $prospectClient = new ProspectClient();

        $prospectClient->first_name = $_POST['txtFirstName'];
        $prospectClient->last_name = $_POST['txtLastName'];
        $prospectClient->company = $_POST['txtCompany'];
        $prospectClient->email = $_POST['txtEmail'];
        $prospectClient->phone = $_POST['txtPhone'];
        $prospectClient->mobile = $_POST['txtMobile'];
        $prospectClient->fax = $_POST['txtFax'];
        $prospectClient->save();

        $prospect->client_id = $prospectClient->id;
        $prospect->city_id = $_POST['selCity'];
        $prospect->prospect_type_enum = $_POST['selProspecttype'];
        $prospect->createby_id = Yii::app()->user->id;
        if($prospect->save()){

            $usertag = $_POST['usertag'];
            $listuser = explode(',', $usertag);
            $affected = ProspectUser::model()->deleteAll('prospect_id='.$prospect->id);

            $suburbtag = $_POST['suburbtag'];
            $listsuburb = explode(',', $suburbtag);
            $affected = ProspectSuburb::model()->deleteAll('prospect_id='.$prospect->id);

            foreach($listuser as $val) {
                if ($val != '') {
                    $prosUser = new ProspectUser();
                    $prosUser->prospect_id = $prospect->id;
                    $prosUser->user_id = $val;

                    $prosUser->save();
                }
            }

            foreach($listsuburb as $val2) {
                if ($val2 != '') {
                    $prosSub = new ProspectSuburb();
                    $prosSub->prospect_id = $prospect->id;
                    $prosSub->suburb_id = $val2;

                    $prosSub->save();
                }
            }

            switch($_POST['selProspecttype']){
                case "commerciallease":

                    $comlease = new ProspectComlease();
                    $comlease->prospect_id = $prospect->id;
                    $comlease->property_type = $_POST['selPropertytypecomlease'];
                    $comlease->floor_from = $_POST['txtFloorFromcomlease'];
                    $comlease->floor_to = $_POST['txtFloorTocomlease'];
                    $comlease->business = $_POST['txtBusinesscomlease'];
                    $comlease->move_in = $_POST['selMoveincomlease'];
                    $comlease->no_of_people = $_POST['selNoOfPeoplecomlease'];
                    $comlease->required_car_parks = $_POST['selCarParkscomlease'];
                    $comlease->budget = $_POST['selBudget'];
                    $comlease->enquiry_on = $_POST['txtEnquiry'];
                    $comlease->createdby_id = Yii::app()->user->id;

                    if(!$comlease->save()){
                        print_r($comlease->getErrors());die();
                    }

                    //$comlease->save();

                    break;
                case "residentialsale":

                    $ressale = new ProspectRessale();
                    $ressale->prospect_id = $prospect->id;
                    $ressale->property_type = $_POST['selPropertytyperessale'];
                    $ressale->purpose = $_POST['selPurposeressale'];
                    $ressale->bedroom = $_POST['selBedroomressale'];
                    $ressale->bathroom = $_POST['selBathroomressale'];
                    $ressale->price = $_POST['selPriceressale'];
                    $ressale->enquiry_on = $_POST['txtEnquiry'];
                    $ressale->createdby_id = Yii::app()->user->id;

                    $ressale->save();

                    break;
                case "commercialsale":

                    $commercialsale = new ProspectComsale();
                    $commercialsale->prospect_id = $prospect->id;
                    $commercialsale->property_type = $_POST['selPropertytypecomsale'];
                    $commercialsale->purpose = $_POST['selPuroposecomsale'];
                    $commercialsale->price = $_POST['selPricecomsale'];
                    $commercialsale->returned_desire = $_POST['selRetdescomsale'];
                    $commercialsale->enquiry_on = $_POST['txtEnquiry'];
                    $commercialsale->createdby_id = Yii::app()->user->id;
                    $commercialsale->save();

                    break;
                case "businesssale":

                    $businessale = new ProspectBussale();
                    $businessale->prospect_id = $prospect->id;
                    $businessale->business_type = $_POST['txtBusNamebuslease'];
                    $businessale->price_from = $_POST['selPriceFrombuslease'];
                    $businessale->price_to = $_POST['selPriceTobuslease'];
                    $businessale->enquiry_on = $_POST['txtEnquiry'];
                    $businessale->createdby_id = Yii::app()->user->id;
                    $businessale->save();

                    break;
            }

            //$notes = new ComleaseNotes();
            $this->redirect('/prospect/list');

        }
    }

}

?>