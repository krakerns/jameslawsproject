<?php

class SiteController extends Controller
{
	public $layout='bootstraplayout';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    /**
     * This is the action to handle external exceptions.
     */
    public function actionIndex()
    {

        if(Yii::app()->user->isGuest){
            $this->redirect('/site/login',array('islogin'=>'1'));
        }

        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
            $this->render('index');
        }else{
            $monthlyTr = 0;
            $monthlyTrans =0;
            $monthlyProspect = 0;
            $monthlySign = 0;

            $dateStart = date('Y/m/1');
            $dateTo = date('Y/m/d');

            $monthlyTr = count(Transactionreport::model()->findAll("markdelete<>1 AND createdtime BETWEEN  CAST('".$dateStart."' AS DATE) AND  CAST('".$dateTo."' AS DATE)"));
            $monthlyTrans = count(Transaction::model()->findAll("transactiondate BETWEEN  CAST('".$dateStart."' AS DATE) AND  CAST('".$dateTo."' AS DATE)"));
            $monthlyProspect = count(Prospect::model()->findAll("created_time BETWEEN  CAST('".$dateStart."' AS DATE) AND  CAST('".$dateTo."' AS DATE)"));
            $monthlySign = count(Signorder::model()->findAll("create_date BETWEEN  CAST('".$dateStart."' AS DATE) AND  CAST('".$dateTo."' AS DATE)"));


            $overAllTr = count(Transactionreport::model()->findAll("markdelete<>1 "));
            $overAllTrans = count(Transaction::model()->findAll());
            $overAllProspect = count(Prospect::model()->findAll());
            $overAllSign = count(Signorder::model()->findAll());

            $this->render('indexadmin',array('monthlyTr'=>$monthlyTr,'monthlyTrans'=>$monthlyTrans,'monthlyProspect'=>$monthlyProspect,'monthlySign'=>$monthlySign,
                'overAllTr'=>$overAllTr,'overAllTrans'=>$overAllTrans,'overAllProspect'=>$overAllProspect,'overAllSign'=>$overAllSign,
            ));
        }


    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $this->layout = 'blank';
        //print_r($_POST);
        $islogin =1;
        $model=new LoginForm();

        if(isset($_POST['username'])){
            //echo 'sad';die();
            $model->username = $_POST['username'];
            $model->password = $_POST['password'];
            $islogin = 1;

            if($model->validate() && $model->login()){
                //echo Yii::app()->user->returnUrl;die();
                $this->redirect(Yii::app()->user->returnUrl);
            }else{
                $islogin = 0;
            }
        }


        // display the login form
        $this->render('login',array('model'=>$model,'islogin'=>$islogin));


		//$this->render('login');
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}


}
