<?php

class ReportController extends Controller
{
    public $layout='bootstraplayout';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php.php?r=site/page&view=Filename
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionIndex()
    {
        if(Yii::app()->user->isGuest){
            $this->redirect('/site/login',array('islogin'=>'1'));
        }
        $this->render('index');
    }

    public function actionListTransactionGross(){

        if(isset($_POST['report'])){

            $date_from = $_POST['report']['txtDateFrom'];
            $date_from = strtotime($date_from);
            $date_from = date("Y/m/d", $date_from);

            $date_to = $_POST['report']['txtDateTo'];
            $date_to = strtotime($date_to);
            $date_to = date("Y/m/d", $date_to);

            $connection=Yii::app()->db;

            $trtype = 'tr.transactiontype <> 0 ';
            if($_POST['report']['selTranstype'] != ''){
                $trtype = 'tr.transactiontype =' . $_POST['report']['selTranstype'];
            }

            $sql ="select t.*,tr.transactiontype from tbl_transaction t ";
            $sql .= "INNER JOIN tbl_transactionreport tr ON (t.transactionreportid = tr.transactionreportid) ";
            $sql .= "WHERE " .$trtype." AND t.paymentrecieveddate BETWEEN  CAST('".$date_from."' AS DATE) AND  CAST('".$date_to."' AS DATE) AND tr.markdelete<>1";

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();


            $arr = array('type'=>$_POST['report']['selTranstype'],'from'=>$_POST['report']['txtDateFrom'],'to'=>$_POST['report']['txtDateTo']);

            if($_POST['report']['hdiscsv'] == '0'){

                $this->render('transactiongrossbroughtin',array('dataReader'=>$dataReader,'arr'=>$arr));

            }else{

                header('Content-Description: File Transfer');
                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=report.csv');

                $separator=",";

                foreach($dataReader as $row){
                    $salesperson =  Users::model()->findByPk($row['userid']);

                    $transactiontype= '';
                    switch($row['transactiontype']){
                        case '1':
                            $transactiontype = 'Residential Sale';
                            break;
                        case '2':
                            $transactiontype = 'Commercial Sale';
                            break;
                        case '3':
                            $transactiontype = 'Commercial Lease';
                            break;
                        case '4':
                            $transactiontype = 'Commercial Assignment Of Lease';
                            break;
                        case '5':
                            $transactiontype = 'Business Sale';
                            break;
                    }

                    $tmp = array(
                        '"'.date('d-m-Y',strtotime($row['paymentrecieveddate'])).'"',
                        '"'.$salesperson->fullname.'"',
                        '"'.$transactiontype.'"',
                        '"'.$row['companyinvoice'].'"',
                        '"'.$row['ptr'].'"',
                        '"'.addslashes($row['property']).'"',
                        '"'.$row['totalcommissionrecievedexcGST'].'"',
                        '"'.$row['totalcommissionrecievedincGST'].'"'
                    );

                    echo join($separator, $tmp)."\n";
                }

                exit();
                Yii::app()->end();
            }

        }else{
            $arr = array('type'=>'1','from'=>date('01-m-Y'),'to'=>date('d-m-Y'));
            $this->render('transactiongrossbroughtin',array('arr'=>$arr));
        }

    }

    public function actionListTransactionSalespersonAmt(){
        if(isset($_POST['report'])){

            $date_from = $_POST['report']['txtDateFrom'];
            $date_from = strtotime($date_from);
            $date_from = date("Y/m/d", $date_from);

            $date_to = $_POST['report']['txtDateTo'];
            $date_to = strtotime($date_to);
            $date_to = date("Y/m/d", $date_to);

            $connection=Yii::app()->db;

            $trperson = 't.userid <> 0 ';
            if($_POST['report']['selSellerPerson'] != ''){
                $trperson = 't.userid =' . $_POST['report']['selSellerPerson'];
            }

            $sql ="select t.*,tr.transactiontype from tbl_transaction t ";
            $sql .= "INNER JOIN tbl_transactionreport tr ON (t.transactionreportid = tr.transactionreportid) ";
            $sql .= "WHERE ".$trperson." AND t.paymenttosalesperson BETWEEN  CAST('".$date_from."' AS DATE) AND  CAST('".$date_to."' AS DATE) AND tr.markdelete<>1";

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();

            $arr = array('type'=>$_POST['report']['selSellerPerson'],'from'=>$_POST['report']['txtDateFrom'],'to'=>$_POST['report']['txtDateTo']);


            if($_POST['report']['hdiscsv'] == '0'){

                $this->render('transactionsalespersonamt',array('dataReader'=>$dataReader,'arr'=>$arr));

            }else{

                header('Content-Description: File Transfer');
                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=report.csv');

                $separator=",";

                foreach($dataReader as $row){
                    $salesperson =  Users::model()->findByPk($row['userid']);

                    $transactiontype= '';
                    switch($row['transactiontype']){
                        case '1':
                            $transactiontype = 'Residential Sale';
                            break;
                        case '2':
                            $transactiontype = 'Commercial Sale';
                            break;
                        case '3':
                            $transactiontype = 'Commercial Lease';
                            break;
                        case '4':
                            $transactiontype = 'Commercial Assignment Of Lease';
                            break;
                        case '5':
                            $transactiontype = 'Business Sale';
                            break;
                    }
                    $trsales = Transactionsalesperson::model()->find('transactionid=' . $row['transactionid']);
                    $tmp = array(
                        '"'.date('d-m-Y',strtotime($row['paymenttosalesperson'])).'"',
                        '"'.$salesperson->fullname.'"',
                        '"'.$transactiontype.'"',
                        '"'.$row['companyinvoice'].'"',
                        '"'.$row['ptr'].'"',
                        '"'.addslashes($row['property']).'"',
                        '"'.$trsales->amtinvoicecompany.'"',
                        '"'.$trsales->bonusdeduction.'"',
                        '"'.$trsales->plusgst.'"',
                        '"'.$trsales->withholdingtax.'"',
                        '"'.$trsales->grandtotal.'"'
                    );

                    echo join($separator, $tmp)."\n";
                }

                exit();
                Yii::app()->end();
            }


        }else{
            $arr = array('type'=>'1','from'=>date('01-m-Y'),'to'=>date('d-m-Y'));
            $this->render('transactionsalespersonamt',array('arr'=>$arr));
        }
    }

    public function actionListOverallGross(){
        if(isset($_POST['report'])){

            $date_from = $_POST['report']['txtDateFrom'];
            $date_from = strtotime($date_from);
            $date_from = date("Y/m/d", $date_from);

            $date_to = $_POST['report']['txtDateTo'];
            $date_to = strtotime($date_to);
            $date_to = date("Y/m/d", $date_to);

            $connection=Yii::app()->db;

            $sql ="select t.*,tr.transactiontype from tbl_transaction t ";
            $sql .= "INNER JOIN tbl_transactionreport tr ON (t.transactionreportid = tr.transactionreportid) ";

            $trtype = 'tr.transactiontype <> 0 ';
            if($_POST['report']['selTranstype'] != ''){
                $trtype = 'tr.transactiontype =' . $_POST['report']['selTranstype'];
            }

            $trperson = 't.userid <> 0 ';
            if($_POST['report']['selSellerPerson'] != ''){
                $trperson = 't.userid =' . $_POST['report']['selSellerPerson'];
            }

            $sql .= "WHERE " .$trtype." AND ".$trperson." AND t.paymentrecieveddate BETWEEN  CAST('".$date_from."' AS DATE) AND  CAST('".$date_to."' AS DATE) AND tr.markdelete<>1";

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();


            $arr = array('type'=>$_POST['report']['selTranstype'],'person'=>$_POST['report']['selSellerPerson'],'from'=>$_POST['report']['txtDateFrom'],'to'=>$_POST['report']['txtDateTo']);


            if($_POST['report']['hdiscsv'] == '0'){

                $this->render('overallgrossbroughtin',array('dataReader'=>$dataReader,'arr'=>$arr));

            }else{

                header('Content-Description: File Transfer');
                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=report.csv');

                $separator=",";

                foreach($dataReader as $row){
                    $salesperson =  Users::model()->findByPk($row['userid']);

                    $transactiontype= '';
                    switch($row['transactiontype']){
                        case '1':
                            $transactiontype = 'Residential Sale';
                            break;
                        case '2':
                            $transactiontype = 'Commercial Sale';
                            break;
                        case '3':
                            $transactiontype = 'Commercial Lease';
                            break;
                        case '4':
                            $transactiontype = 'Commercial Assignment Of Lease';
                            break;
                        case '5':
                            $transactiontype = 'Business Sale';
                            break;
                    }
                    $trdetails = Transactiondetails::model()->find('transactionid=' . $row['transactionid']);
                    $tmp = array(
                        '"'.date('d-m-Y',strtotime($row['paymentrecieveddate'])).'"',
                        '"'.$salesperson->fullname.'"',
                        '"'.$transactiontype.'"',
                        '"'.$row['companyinvoice'].'"',
                        '"'.$row['totalcommissionrecievedincGST'].'"',
                        '"'.$row['ptr'].'"',
                        '"'.addslashes($row['property']).'"',
                        '"'.$trdetails->totalfeesbroughtin.'"'
                    );

                    echo join($separator, $tmp)."\n";
                }

                exit();
                Yii::app()->end();
            }

        }else{
            $arr = array('type'=>'','person'=>'','from'=>date('01-m-Y'),'to'=>date('d-m-Y'));
            $this->render('overallgrossbroughtin',array('arr'=>$arr));
        }
    }

    public function actionListOverallAmt(){
        if(isset($_POST['report'])){

            $date_from = $_POST['report']['txtDateFrom'];
            $date_from = strtotime($date_from);
            $date_from = date("Y/m/d", $date_from);

            $date_to = $_POST['report']['txtDateTo'];
            $date_to = strtotime($date_to);
            $date_to = date("Y/m/d", $date_to);

            $connection=Yii::app()->db;

            $sql ="select t.*,tr.transactiontype from tbl_transaction t ";
            $sql .= "INNER JOIN tbl_transactionreport tr ON (t.transactionreportid = tr.transactionreportid) ";

            $trtype = 'tr.transactiontype <> 0 ';
            if($_POST['report']['selTranstype'] != ''){
                $trtype = 'tr.transactiontype =' . $_POST['report']['selTranstype'];
            }

            $trperson = 't.userid <> 0 ';
            if($_POST['report']['selSellerPerson'] != ''){
                $trperson = 't.userid =' . $_POST['report']['selSellerPerson'];
            }

            $sql .= "WHERE " .$trtype." AND ".$trperson." AND t.paymenttosalesperson BETWEEN CAST('".$date_from."' AS DATE) AND  CAST('".$date_to."' AS DATE) AND tr.markdelete<>1";

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();


            $arr = array('type'=>$_POST['report']['selTranstype'],'person'=>$_POST['report']['selSellerPerson'],'from'=>$_POST['report']['txtDateFrom'],'to'=>$_POST['report']['txtDateTo']);


            if($_POST['report']['hdiscsv'] == '0'){

                $this->render('overallamount',array('dataReader'=>$dataReader,'arr'=>$arr));

            }else{

                header('Content-Description: File Transfer');
                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=report.csv');

                $separator=",";

                foreach($dataReader as $row){
                    $salesperson =  Users::model()->findByPk($row['userid']);

                    $transactiontype= '';
                    switch($row['transactiontype']){
                        case '1':
                            $transactiontype = 'Residential Sale';
                            break;
                        case '2':
                            $transactiontype = 'Commercial Sale';
                            break;
                        case '3':
                            $transactiontype = 'Commercial Lease';
                            break;
                        case '4':
                            $transactiontype = 'Commercial Assignment Of Lease';
                            break;
                        case '5':
                            $transactiontype = 'Business Sale';
                            break;
                    }
                    $trsales = Transactionsalesperson::model()->find('transactionid=' . $row['transactionid']);
                    $tmp = array(
                        '"'.date('d-m-Y',strtotime($row['paymenttosalesperson'])).'"',
                        '"'.$salesperson->fullname.'"',
                        '"'.$transactiontype.'"',
                        '"'.$row['companyinvoice'].'"',
                        '"'.$row['ptr'].'"',
                        '"'.addslashes($row['property']).'"',
                        '"'.$trsales->grandtotal.'"'
                    );

                    echo join($separator, $tmp)."\n";
                }

                exit();
                Yii::app()->end();
            }

        }else{
            $arr = array('type'=>'','person'=>'','from'=>date('01-m-Y'),'to'=>date('d-m-Y'));
            $this->render('overallamount',array('arr'=>$arr));
        }
    }

    public function actionListMonthlyWTax(){
        if(isset($_POST['report'])){

            $date_from = $_POST['report']['txtDateFrom'];
            $date_from = strtotime($date_from);
            $date_from = date("Y/m/d", $date_from);

            $date_to = $_POST['report']['txtDateTo'];
            $date_to = strtotime($date_to);
            $date_to = date("Y/m/d", $date_to);

            $connection=Yii::app()->db;

            $sql ="select t.*,tr.transactiontype from tbl_transaction t ";
            $sql .= "INNER JOIN tbl_transactionreport tr ON (t.transactionreportid = tr.transactionreportid) ";

            $trperson = 't.userid <> 0 ';
            if($_POST['report']['selSellerPerson'] != ''){
                $trperson = 't.userid =' . $_POST['report']['selSellerPerson'];
            }

            $sql .= "WHERE ".$trperson." AND t.paymenttosalesperson BETWEEN  CAST('".$date_from."' AS DATE) AND  CAST('".$date_to."' AS DATE) AND tr.markdelete<>1";

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();

            $arr = array('type'=>$_POST['report']['selSellerPerson'],'from'=>$_POST['report']['txtDateFrom'],'to'=>$_POST['report']['txtDateTo']);


            if($_POST['report']['hdiscsv'] == '0'){

                $this->render('monthlywithholdingtax',array('dataReader'=>$dataReader,'arr'=>$arr));

            }else{

                header('Content-Description: File Transfer');
                header('Content-Type: application/csv');
                header('Content-disposition: attachment; filename=report.csv');

                $separator=",";

                foreach($dataReader as $row){
                    $salesperson =  Users::model()->findByPk($row['userid']);
                    $trsales = Transactionsalesperson::model()->find('transactionid=' . $row['transactionid']);
                    $tmp = array(
                        '"'.date('d-m-Y',strtotime($row['paymenttosalesperson'])).'"',
                        '"'.$salesperson->fullname.'"',
                        '"'.$trsales->amtinvoicecompany.'"',
                        '"'.$trsales->withholdingtax.'"',
                        '"'.$trsales->bonusdeduction.'"',
                        '"'.$trsales->plusgst.'"',
                        '"'.$trsales->grandtotal.'"',
                        '"'.$row['ptr'].'"',
                        '"'.addslashes($row['property']).'"'

                    );

                    echo join($separator, $tmp)."\n";
                }

                exit();
                Yii::app()->end();
            }

        }else{
            $arr = array('type'=>'1','from'=>date('01-m-Y'),'to'=>date('d-m-Y'));
            $this->render('monthlywithholdingtax',array('arr'=>$arr));
        }
    }


}
