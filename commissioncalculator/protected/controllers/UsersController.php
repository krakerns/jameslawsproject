<?php

class UsersController extends Controller
{

    public $layout='bootstraplayout';

    public function actionCreate()
    {
        $usersModel  = new Users();

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($advertisementModel);

        if(isset($_POST['Users']))
        {

            $usersModel->attributes             = $_POST['Users'];

            if($usersModel->save()){

                $userAccnt = new Useraccounts();
                $userAccnt->userid = $usersModel->userid;
                $userAccnt->username =  $usersModel->personalemail;
                $userAccnt->password = 'jameslaw';

                $userAccnt->save();

                $this->redirect(array('index'));
            }else{

                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('details',array(
            'model'=>$usersModel,
        ));


        //$this->render('details');
    }

    public function actionEdit($userid)
    {

        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $usersModel = Users::model()->findByPk((int) $userid);
        if($usersModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Users']))
        {

            $usersModel->attributes             = $_POST['Users'];

            if($usersModel->save()){

                if(Yii::app()->user->role == 'user'){
                    $this->redirect(array('users/edit/userid/' . $usersModel->usserid));
                }else{
                    $this->redirect(array('index'));
                }

            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('details',array(
            'model'=>$usersModel,
        ));
    }

    public function actionDelete()
    {
        $userid = $_POST['userid'];

        //echo $_POST['userid'];
        $usersModel = Users::model()->findByPk((int)$userid);
        //$usersModel = new Users();
        $usersModel->isactive=0;

        if ($usersModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }

        $result = $usersModel->save();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }

        echo '{"result":"success", "message":""}';
        Yii::app()->end();

    }

    public function actionIndex()
    {
        $listUsers          = Users::model()->findAll('isactive=1');
        $this->render('list',array('listUsers'=>$listUsers));
    }

    public function actionList()
    {
        $this->render('list');
    }

    public function actionListjson() {

        // /////////////////////////////////////////////////////////////////////
        // Create a Db Criteria to filter and customise the resulting results
        // /////////////////////////////////////////////////////////////////////
        $searchCriteria = new CDbCriteria;

        // Paging criteria
        // Set defaults
        $limitStart 	           = isset($_POST['start'])?$_POST['start']:0;
        $limitItems 	           = isset($_POST['length'])?$_POST['length']:Yii::app()->params['PAGESIZEREC'];

        $searchCriteria->limit 		 = $limitItems;
        $searchCriteria->offset 	 = $limitStart;

        $listUsers          = Users::model()->findAll($searchCriteria);


        $countRows 		            = Users::model()->count($searchCriteria);;
        $countTotalRecords 		    = Users::model()->count();

        /*
         * Output
         */
        $resultsAdvertTable = array(
            "iTotalRecords"         => $countRows,
            "iTotalDisplayRecords"  => $countTotalRecords,
            "aaData"                => array()
        );

        foreach($listUsers as $item){

            $fullname = $item->attributes['firstname'] . ' ' . $item->attributes['lastname'];

            $rowResult = array(
                $fullname,
                $item->attributes['reeanumber'],
                $item->attributes['irdnumber'],
                $item->attributes['companyname'],
                ''
            );

            $resultsAdvertTable['aaData'][] = $rowResult;

        }

        echo json_encode($resultsAdvertTable);
        //echo CJSON::encode($resultsAdvertTable);
        Yii::app()->end();

}

    public function actionUseraccountslist() {
        $listUseraccounts         = Useraccounts::model()->findAll();
        $this->render('useraccountlist',array('listUseraccounts'=>$listUseraccounts));
    }

    public function actionEdituseraccounts($useraccountsid){
        //echo 'ss';die();

        $userAccount = Useraccounts::model()->findByPk((int) $useraccountsid);
        if($userAccount===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Useraccounts']))
        {
            $userAccount->attributes           = $_POST['Useraccounts'];

            if($userAccount->save()){
                $this->redirect(array('useraccountslist'));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }
        }

        // Show the details screen
        $this->render('useraccountdetails',array(
            'model'=>$userAccount,
        ));
    }

    public function actionBonusdeductionlist() {
        $listBonusdeduction       = Bonusdeductions::model()->findAll();
        $this->render('bonusdeductionlist',array('listBonusdeduction'=>$listBonusdeduction));
    }

    public function actionCreatebonusdeduction(){
        $bonusdeductionModel  = new Bonusdeductions();

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($advertisementModel);

        if(isset($_POST['Bonusdeductions']))
        {
            //print_r( $_POST['Bonusdeductions']);die();
            $bonusdeductionModel = Bonusdeductions::model()->find('userid=' . $_POST['Bonusdeductions']['userid']);
            if(!empty($bonusdeductionModel)){
                $bonusdeductionModel->amount = $bonusdeductionModel->amount + $_POST['Bonusdeductions']['amount'];
                $bonusdeductionModel->description = $_POST['Bonusdeductions']['description'];
                $bonusdeductionModel->gst = $_POST['Bonusdeductions']['gst'];
                if($bonusdeductionModel->save()){

                    $bonusline = new Bonusdeductionsline();
                    $bonusline->amount = $_POST['Bonusdeductions']['amount'];
                    $bonusline->bonusdeductionsid = $bonusdeductionModel->bonusdeductionid;
                    $bonusline->description = $_POST['Bonusdeductions']['description'];
                    $bonusline->gst = $_POST['Bonusdeductions']['gst'];

                    $start_date = $_POST['txtDate'];
                    $start_date = strtotime($start_date);
                    $start_date = date("Y/m/d", $start_date);
                    $bonusline->createdtime = $start_date;

                    $bonusline->save();
                    $this->redirect(array('bonusdeductionlist'));
                }else{
                    Yii::app()->user->setFlash('error', "Error creating a user record.'");
                }
            }else{

                //print_r($_POST['Bonusdeductions']);die();
                $bonusdeductionModel1 = new Bonusdeductions();
                $bonusdeductionModel1->amount = $_POST['Bonusdeductions']['amount'];
                $bonusdeductionModel1->description = $_POST['Bonusdeductions']['descri[tion'];
                $bonusdeductionModel1->userid = $_POST['Bonusdeductions']['userid'];
                $bonusdeductionModel1->gst = $_POST['Bonusdeductions']['gst'];
                if($bonusdeductionModel1->save()){

                    $bonusline = new Bonusdeductionsline();
                    $bonusline->amount = $_POST['Bonusdeductions']['amount'];
                    $bonusline->bonusdeductionsid = $bonusdeductionModel1->bonusdeductionid;
                    $bonusline->description = $_POST['Bonusdeductions']['description'];
                    $bonusline->gst = $_POST['Bonusdeductions']['gst'];

                    $start_date = $_POST['txtDate'];
                    $start_date = strtotime($start_date);
                    $start_date = date("Y/m/d", $start_date);
                    $bonusline->createdtime = $start_date;

                    if(!$bonusline->save()){
                        print_r($bonusline->getErrors());die();
                    }else{
                        $this->redirect(array('bonusdeductionlist'));
                    }

                }else{
                    Yii::app()->user->setFlash('error', "Error creating a user record.'");
                }
            }

        }
        $userid = '';
        if(isset($_GET['uid'])){
            $userid = $_GET['uid'];

        }
        $this->render('bonusdeductiondetails',array(
            'model'=>$bonusdeductionModel,'userid'=>$userid
        ));

    }

    public function actionViewbonusdeduction($bonusdeductionid){
        $bonusdeductionModel = Bonusdeductions::model()->findByPk((int) $bonusdeductionid);

        $this->render('bonusdeductionview',array(
            'model'=>$bonusdeductionModel,
        ));

    }

    public function actionEditbonusdeduction($bonusdeductionid){

        $bonusdeductionModel = Bonusdeductions::model()->findByPk((int) $bonusdeductionid);
        if($bonusdeductionModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Bonusdeductions']))
        {

            $bonusdeductionModel->attributes             = $_POST['Bonusdeductions'];

            if($bonusdeductionModel->save()){
                $this->redirect(array('bonusdeductionlist'));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('bonusdeductiondetails',array(
            'model'=>$bonusdeductionModel,
        ));
    }

    public function actionDeletebonusdeduction(){

        $bonusdeductionid = $_POST['bonusdeductionid'];
        $bonusdeductionModel = Bonusdeductions::model()->findByPk((int)$bonusdeductionid);

        if ($bonusdeductionModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }


        $result = $bonusdeductionModel->delete();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "message":""}';
        Yii::app()->end();
    }

    public function actionManagerslist() {
        $listManagers         = Managers::model()->findAll();
        $this->render('managerslist',array('listManagers'=>$listManagers));
    }

    public function actionCreatemanager(){
        $managerModel  = new Managers();

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($advertisementModel);

        if(isset($_POST['Managers']))
        {

            $managerModel->attributes             = $_POST['Managers'];

            if($managerModel->save()){

                $this->redirect(array('managerslist'));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('managersdetails',array(
            'model'=>$managerModel,
        ));
    }

    public function actionEditmanager($managerid){

        $managersModel = Managers::model()->findByPk((int) $managerid);
        if($managersModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Managers']))
        {

            $managersModel->attributes             = $_POST['Managers'];

            if($managersModel->save()){
                $this->redirect(array('managerslist'));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('managersdetails',array(
            'model'=>$managersModel,
        ));
    }

    public function actionDeletemanager(){

        $managerid = $_POST['managerid'];
        $managersModel = Managers::model()->findByPk((int)$managerid);

        if ($managersModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }


        $result = $managersModel->delete
            ();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "messages":""}';
        Yii::app()->end();
    }

    public function actionMarketingfeelist() {
        $listMarketing         = MarketingFee::model()->findAll();
        $this->render('marketingfeelist',array('listMarketing'=>$listMarketing));
    }

    public function actionGetpaymentdetails() {
        $id = $_POST['paymentid'];
        $payment = MarketingFeePayments::model()->findByPk($id);
        $dp = date('d/m/Y',strtotime($payment->date_paid));
        $param = array('id'	=>	$id,
            'marketingfeeid'	=>	$payment->marketing_fee_id,
            'description'	=>	$payment->description,
            'supplier'	=>	$payment->supplier,
            'amount'	=> $payment->amount,
            'datepaid'	=> $dp,
        );

        $finaljson = json_encode($param);
        echo $finaljson;
    }

    public function actionCreatemarketingfee(){
        $marketingFee  = new MarketingFee();

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($advertisementModel);

        if(isset($_POST['MarketingFee']))
        {

            $marketingFee->attributes             = $_POST['MarketingFee'];
            $marketingFee->current_amount = $marketingFee->initial_amount;
            $start_date = $_POST['MarketingFee']['fund_received'];
            $start_date = strtotime($start_date);
            $start_date = date("Y/m/d", $start_date);
            $marketingFee->fund_received = $start_date;
            $marketingFee->datetime_created = date("Y/m/d");

            if($marketingFee->save()){

                $hdPersonNumber = $_POST['hdPersonNumber'];
                $affected = MarketingFeeSalesperson::model()->deleteAll('marketing_fee_id='.$marketingFee->id);

                for($i=1;$i<=$hdPersonNumber;$i++) {
                    if(!empty($_POST['selPerson'. $i])) {
                        $marketingPerson = new MarketingFeeSalesperson();
                        $marketingPerson->marketing_fee_id = $marketingFee->id;
                        $marketingPerson->userid = $_POST['selPerson'. $i];
                        $marketingPerson->save();
                    }
                }

                $this->redirect(array('editmarketingfee','marketingid'=>$marketingFee->id));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        $marketingFee->initial_amount = 0;
        $marketingFee->current_amount = 0;
        // Show the details screen
        $this->render('marketingfeedetails',array(
            'model'=>$marketingFee,
        ));
    }

    public function actionDeletemarketingfee(){

        $marketingid = $_POST['marketingid'];
        $marketingModel = MarketingFee::model()->findByPk((int)$marketingid);

        if ($marketingModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }


        $result = $marketingModel->delete();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "message":""}';
        Yii::app()->end();
    }

    public function actionDeletepayment(){

        $paymentid = $_POST['paymentid'];
        $payment = MarketingFeePayments::model()->findByPk((int)$paymentid);

        if ($payment == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }


        $result = $payment->delete();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "message":""}';
        Yii::app()->end();
    }

    public function actionEditmarketingfee($marketingid){

        $marketingModel = MarketingFee::model()->findByPk((int) $marketingid);
        if($marketingModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['MarketingFee']))
        {



        }

        // Show the details screen
        $this->render('marketingfeeview',array(
            'model'=>$marketingModel,
        ));
    }

    public function actionSavemarketingsales(){
        $hdSalescount = $_POST['hdsalescount'];


        for($i=1;$i<=$hdSalescount;$i++) {
            if(!empty($_POST['txtSalesAmt'. $i])) {
                $marketingsales = MarketingFeeSalesperson::model()->findByPk($_POST['hdsalesid' . $i]);

                $marketingsales->amount = $_POST['txtSalesAmt'. $i];
                $marketingsales->gst = $_POST['txtGST'. $i];
                $marketingsales->issaved = '1';

                $marketingsales->saves();
            }
        }


        $this->redirect(array('users/Editmarketingfee','marketingid'=>$_POST['hdmarketingid']));

    }

    public function actionSavemarketingfeepayment(){
        $marketingid= $_POST['hdmarketingfeeid'];

        if($_POST['hdisedit']=='0')     {
            $marketingModel = MarketingFee::model()->findByPk((int) $marketingid);
            $marketingModel->current_amount = $marketingModel->current_amount - $_POST['txtAmount'];
            $marketingModel->save();

            $payment = new MarketingFeePayments();
            $payment->marketing_fee_id = $marketingid;
            $payment->description = $_POST['txtDescription'];
            $payment->supplier = $_POST['txtSupplier'];
            $payment->amount = $_POST['txtAmount'];

            $start_date = $_POST['txtDate'];
            $start_date = strtotime($start_date);
            $start_date = date("Y/m/d", $start_date);
            $payment->date_paid = $start_date;

            if($payment->save()){

                foreach($marketingModel->marketingFeeSalespeople as $person){
                    $person->issaved  = '0';
                    $person->save();
                }
                $this->redirect(array('users/Editmarketingfee','marketingid'=>$marketingid));


            }
        }else{

            $paymentid = $_POST['hdpaymentid'];
            $payment = MarketingFeePayments::model()->findByPk($paymentid);

            $marketingModel = MarketingFee::model()->findByPk((int) $marketingid);
            $marketingModel->current_amount = $marketingModel->current_amount + $payment->amount;
            $marketingModel->current_amount = $marketingModel->current_amount - $_POST['txtAmount'];
            $marketingModel->save();

            $payment->marketing_fee_id = $marketingid;
            $payment->description = $_POST['txtDescription'];
            $payment->supplier = $_POST['txtSupplier'];
            $payment->amount = $_POST['txtAmount'];

            $start_date = $_POST['txtDate'];
            $start_date = strtotime($start_date);
            $start_date = date("Y/m/d", $start_date);
            $payment->date_paid = $start_date;

            if($payment->save()){

                foreach($marketingModel->marketingFeeSalespeople as $person){
                    $person->issaved  = '0';
                    $person->save();
                }
                $this->redirect(array('users/Editmarketingfee','marketingid'=>$marketingid));
            }
        }



    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
} 