<?php
/**
 * Created by PhpStorm.
 * User: JV
 * Date: 7/18/14
 * Time: 1:01 AM
 */

class OrderController extends Controller
{
    public $layout='bootstraplayout';

    public function actionList()
    {
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
        $this->render('list');
    }

    public function actionIndex()
    {
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
        $this->render('list');
    }

    public function actionCreate(){
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $signModel = new Signorder();

        if(isset($_POST['Signorder']))
        {
           //print_r($_POST['Signorder']);die();
           $signModel->attributes  = $_POST['Signorder'];

           $signModel->street_address =$_POST['Signorder']['street_address'];
           $signModel->installation_instruction =$_POST['Signorder']['installation_instruction'];
           $signModel->photo =$_POST['Signorder']['photo'];
           $signModel->createdby_id = Yii::app()->user->id;
           $signModel->create_date =  date("Y/m/d");

           $start_date = $_POST['Signorder']['installation_date'];
           $start_date = strtotime($start_date);
           $start_date = date("Y/m/d", $start_date);
           $signModel->installation_date = $start_date;

           if($signModel->save()){

               $hdPersonNumber = $_POST['hdPersonNumber'];
               $affected = Signorderpersons::model()->deleteAll('signorderid='.$signModel->id);

               for($i=1;$i<=$hdPersonNumber;$i++) {
                   if(isset ($_POST['txtPerson'. $i])) {
                       $signPerson = new Signorderpersons();
                       $signPerson->signorderid = $signModel->id;
                       $signPerson->person_name = $_POST['txtPerson'. $i];
                       $signPerson->save();
                   }
               }
                $this->redirect(array('list'));
            }else{
               print_r($signModel->getErrors());die();
                Yii::app()->user->setFlash('error', "Error creating a sign order.'");
            }
        }else{
            $signModel->installation_date = date("Y/m/d");
            $signModel->size = 'medium';
            $signModel->type='lease';
        }

        $this->render('create',array('model'=>$signModel));
    }

    public function actionEdit($id)
    {
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
        $signModel = Signorder::model()->findByPk((int) $id);
        if($signModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Signorder']))
        {

            $signModel->attributes  = $_POST['Signorder'];

            $signModel->street_address =$_POST['Signorder']['street_address'];
            $signModel->installation_instruction =$_POST['Signorder']['installation_instruction'];
            $signModel->photo =$_POST['Signorder']['photo'];
            $signModel->modifiedby_id = Yii::app()->user->id;
            $signModel->modified_date =  date("Y/m/d");

            $start_date = $_POST['Signorder']['installation_date'];
            $start_date = strtotime($start_date);
            $start_date = date("Y/m/d", $start_date);
            $signModel->installation_date = $start_date;

            if($signModel->save()){
                $this->redirect(array('list'));
            }else{
                print_r($signModel->getErrors());die();
                Yii::app()->user->setFlash('error', "Error creating a sign order.'");
            }

        }

        // Show the details screen
        $this->render('create',array('model'=>$signModel));
    }

    public function actionDelete()
    {
        $id = $_POST['id'];


        $signModel = Signorder::model()->findByPk((int)$id);
        $result = $signModel->delete();

        if ($signModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid Sign Order"}';
            Yii::app()->end();
        }

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to mark record for deletion"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }

        echo '{"result":"success", "message":""}';
        Yii::app()->end();

    }

    public function actionApprove()
    {
        $id = $_POST['id'];

        $signModel = Signorder::model()->findByPk((int)$id);
        $signModel->approvedby_id=Yii::app()->user->id;
        $signModel->approved_date = date("Y/m/d");
        if ($signModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid Sign Order"}';
            Yii::app()->end();
        }

        //$usersModel->isactive='0';
        $result = $signModel->save();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to approve"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }

        echo '{"result":"success", "messsage":""}';
        Yii::app()->end();
    }

    public function actionChangestatus()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];

        $signModel = Signorder::model()->findByPk((int)$id);
        $signModel->status=$status;
        if ($signModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid Sign Order"}';
            Yii::app()->end();
        }

        //$usersModel->isactive='0';


        $result = $signModel->save();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to approve"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "message":""}';
        Yii::app()->end();


    }

    public function actionImageUpload(){
        $error = "";
        $msg = "";
        $uploaded='';
        $toupload='';
        $fileElementName = 'picToUpload';

        if(!empty($_FILES[$fileElementName]['error'])) {
            switch($_FILES[$fileElementName]['error']) {

                case '1':
                    $error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;
                case '2':
                    $error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;
                case '3':
                    $error = 'The uploaded file was only partially uploaded';
                    break;
                case '4':
                    $error = 'No file was uploaded.';
                    break;

                case '6':
                    $error = 'Missing a temporary folder';
                    break;
                case '7':
                    $error = 'Failed to write file to disk';
                    break;
                case '8':
                    $error = 'File upload stopped by extension';
                    break;
                case '999':
                default:
                    $error = 'No error code avaiable';
            }
        }elseif(empty($_FILES['picToUpload']['tmp_name']) || $_FILES['picToUpload']['tmp_name'] == 'none') {
            $error = 'No file was uploaded..';
        }else {

            $uploaddir = 'uploads/signorderuploads/';

            if (is_uploaded_file($_FILES['picToUpload']['tmp_name'])) {
                $fileAgreement = basename($_FILES['picToUpload']['name']);
                $toupload = time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                $fileAgreement =$uploaddir .  time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['picToUpload']['tmp_name'], $fileAgreement);
            }

            /*

            $fname = $_FILES['picToUpload']['name'];
            $info = pathinfo($fname);
            $toupload = time()  . '.' . $info['extension'];

            $touploadpath = Yii::app()->basePath . '\\..\\uploads\\signorderuploads\\' . $toupload;


            move_uploaded_file($_FILES['picToUpload']['tmp_name'],$touploadpath);
            */


        }
        echo "{";
        echo				"error: '" . $error . "',\n";
        echo				"msg: '" . $toupload  . "'\n";
        echo "}";
    }

}

?>