<?php
/**
 * Created by PhpStorm.
 * User: JV
 * Date: 7/4/14
 * Time: 10:24 PM
 */

class TransactionController extends Controller
{
    public $layout='bootstraplayout';

    public function actionTransactionList(){

        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
        $userid=Yii::app()->user->id;
        $listTR = new Transactionreport();

        if(Yii::app()->user->role=='accounts' || Yii::app()->user->role == 'superuser'){
            $listTR = Transactionreport::model()->findAll('markdelete<>1');
        }else{
            $listTR  = Transactionreport::model()->findAll('(listeruserpersonid=' .$userid . ' OR selleruserpersonid='.$userid . ' OR createdbyid='.$userid.' ) AND markdelete<>1');
        }

        $this->render('transactionreportlist',array('listTR'=>$listTR));
}

    public function actionManagercalculation($managerid){
        $this->layout='bootstraplayout2';
        $trmanagers = Transactionmanagers::model()->findByPk($managerid);

        $this->render('managerscalculation',array('bonusamt'=>'0','bonusdesc'=>'No Bonus','trmanager'=>$trmanagers,'tr'=>$trmanagers->transaction->transactionreport,'userid'=>$trmanagers->manager->userid,'transaction'=>$trmanagers->transaction));
    }

    public function actionUpdatemanagercalculation(){
        $this->layout='bootstraplayout2';
        $managerid = $_POST['hdmanagerid'];
        $trmanagers = Transactionmanagers::model()->findByPk($managerid);

        $trmanagers->companyinvoice = $_POST['txtInvoice'];

        $start_date = $_POST['txtInvoiceDate'];

        $start_date = strtotime($start_date);
        //echo $start_date;die();
        $start_date = date("Y/m/d", $start_date);
        $trmanagers->invoicedate = $start_date;

        $start_date = $_POST['txtPaymentReceived'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $trmanagers->paymentrecieveddate = $start_date;

        $start_date = $_POST['txtPaymentSalesperson'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $trmanagers->paymenttosalesperson = $start_date;

        $trmanagers->isclosed = $_POST['hdisclosed'];

        if($trmanagers->save()){
            $this->redirect(array('transaction/managercalculation','managerid'=>$managerid));
        }
    }

    public function actionPrintManagerCalculation($managerid){
        $trmanager = Transactionmanagers::model()->findByPk($managerid);


        $pdf = new Pdf();
        $pdf->setSize('a4', 'portrait');
        //$this->render('spreadsheetnew',array('bonusamt'=>$bonusamt,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid));
        $pdf->renderPartial('printmanager',array('trmanager'=>$trmanager,'transaction'=>$trmanager->transaction,'tr'=>$trmanager->transaction->transactionreport,'userid'=>$trmanager->manager->userid));
        $pdf->stream($trmanager->transaction->ptr. '-' . $trmanager->manager->user->lastname . '.pdf');
    }

    public function actionSpreadsheet($transactionreportid){
        $this->layout='bootstraplayout2';
        $tr = Transactionreport::model()->findByPk($transactionreportid);

        $userid = '';
        if(isset($_GET['userid'])){
            $userid = $_GET['userid'];
        }else{
            $trlist = Trlistingpersons::model()->find('transactionreportid=' .$transactionreportid);
            if(count($trlist)>0){
                $userid = $trlist->userid;
            }else{
                $trsell = Trsellingpersons::model()->find('transactionreportid=' .$transactionreportid);
                $userid = $trsell->userid;
            }

        }

        $bonus = Bonusdeductions::model()->find('userid=' . $userid . ' ORDER BY userid DESC ');
        $bonusamt=0;
        $bonusgst =  0;
        $bonusdesc='No Bonus / Deductions';

        //$user = Users::model()->findByPk(Yii::app()->user->id);

        if(!empty($bonus)){
            $bonusamt=$bonus->amount;
            $bonusdesc=$bonus->description;
            $bonusgst = $bonus->gst;
        }

        $transaction = Transaction::model()->find('transactionreportid=' . $transactionreportid . ' AND userid='.$userid);


        if(empty($transaction)){
            $this->render('spreadsheetnew',array('bonusamt'=>$bonusamt,'bonusgst'=>$bonusgst,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid));
        }else{
            $this->render('updatecalculation',array('bonusamt'=>$bonusamt,'bonusgst'=>$bonusgst,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid,'transaction'=>$transaction));
        }
    }

    public function actionGetBonus(){
        $userid = $_POST['userid'];

        $bonus = Bonusdeductions::model()->find('userid=' . $userid . ' ORDER BY userid DESC ');
        $user = Users::model()->findByPk($userid);
        $arr= array();
        if(empty($bonus)){
            $arr = array(
                'amount'=>'0',
                'description'=>'No Bonus / deductions',
                'share'=>$user->usershare,
                'withholdingtax'=>$user->withholdingtax
            );
        }else{
            $arr = array(
                'amount'=>$bonus->amount,
                'description'=>$bonus->description,
                'share'=>$user->usershare,
                'withholdingtax'=>$user->withholdingtax
            );
        }

        echo CJSON::encode($arr);
    }

    public function actionAddtransactionreport(){

        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $this->render('addtransactionreportnew');
    }

    public function actionEditTransaction($transactionreportid){

        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $tr = Transactionreport::model()->findByPk($transactionreportid);
        $this->render('edittransactionreportnew',array('model'=>$tr));
    }

    public function actionReviewTransaction($transactionreportid){
        $tr = Transactionreport::model()->findByPk($transactionreportid);
        $this->render('reviewtransactionreport',array('model'=>$tr));
    }

    public function actionConfirmtransaction(){
       // print_r($_POST);die();

        $tr = new Transactionreport();
        $isconfirm =  0;

        $tr->transactiontype = $_POST['selTranstype'];
        $tr->address = $_POST['txtAddressProperty'];
        $tr->saleleaseprice = $_POST['txtSalesPrice'];
        /*
        $tr->listertype = $_POST['hdlistercolleague'];
        if($_POST['hdlistercolleague'] == 'colleague') {
        if($_POST['hdlistercolleague'] == 'colleague') {
            $tr->listeruserpersonid = $_POST['selListerPerson'];

            if($_POST['hdsellercolleague'] != 'colleague'){
                $isconfirm='1';
            }
        }else{
            $tr->listerotherperson = $_POST['txtListerOtherComp'];
            $tr->listerotherpersonname = $_POST['txtListerOtherSalesPerson'];
        }
        */
        $tr->listershare = $_POST['txtListerShare'];
        /*
        $tr->sellertype = $_POST['hdsellercolleague'];
        if($_POST['hdsellercolleague'] == 'colleague') {
            $tr->selleruserpersonid = $_POST['selSellerPerson'];

            if($_POST['hdlistercolleague'] != 'colleague'){
                $isconfirm='1';
            }
        }else{
            $tr->sellerotherperson = $_POST['txtSellerOtherComp'];
            $tr->sellerotherpersonname = $_POST['txtSellerOtherSalesPerson'];
        }
        */

        $tr->sellershare = $_POST['txtSellerShare'];
        $tr->ispowerteam = $_POST['hdpowerteam'];
        $tr->ispowerteamseller = $_POST['hdpowerteamseller'];
        $tr->listingreferralperc = $_POST['txtListingReferralPerc'];
        $tr->listingsplitamt = $_POST['txtListingSplitAmt'];
        $tr->listingreferralamt = $_POST['txtListingReferralAmt'];
        $tr->listinggrossbroughtin = $_POST['txtListingGrossBroughtIn'];
        $tr->sellingreferralperc = $_POST['txtSellingReferralPerc'];
        $tr->sellingsplitamt = $_POST['txtSellingSplitAmt'];
        $tr->sellingreferralamt = $_POST['txtSellingReferralAmt'];
        $tr->sellinggrossbroughtin = $_POST['txtSellingGrossBroughtIn'];
        //$tr->conjuctionaldefaultsplit = $_POST[''];
        $tr->conjuctionalothercompany = $_POST['txtConjunctionalComp'];
        $tr->conjuctionalothersplit = $_POST['txtConjunctionalSplit'];
        $tr->commissiondepositamtincGST = $_POST['txtDepositIncGST'];
        $tr->commissionamtincGST = $_POST['txtCommissionIncGST'];
        $tr->commissionamtexcGST = $_POST['txtCommissionExcGST'];
        $tr->commissiondepositamtexcGST = $_POST['txtDepositExcGST'];
        $tr->ptdrnumber = $_POST['txtPtr'];

        $start_date = $_POST['txtDate'];

        $start_date = strtotime($start_date);

        $start_date = date("Y/m/d", $start_date);
        //echo $start_date;die();
        $tr->createdtime = $start_date;

        $start_date = $_POST['txtDateAgreement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->agreementdate = $start_date;

        $start_date = $_POST['txtDateUnconditional'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->unconditionaldate = $start_date;

        $start_date = $_POST['txtDatePossession'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->posessiondate = $start_date;

        $start_date = $_POST['txtDateSettlement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->settlementdate = $start_date;

        $tr->createdbyid =  $_POST['hduserid'];

        $tr->status = '0';

        if($tr->save()){
            $hdlistingnumber = $_POST['hdlistingnumber'];
            $affected = Trlistingpersons::model()->deleteAll('transactionreportid='.$tr->transactionreportid);

            for($i=1;$i<=$hdlistingnumber;$i++) {
                if(isset ($_POST['hdlistertype'. $i])) {
                    if($_POST['hdlistertype'. $i] == 'colleague'){
                        if(!empty($_POST['selListerPerson'. $i])){
                            $trlisting = new Trlistingpersons();
                            $trlisting->type= $_POST['hdlistertype'. $i];
                            $trlisting->transactionreportid = $tr->transactionreportid;
                            $trlisting->userid = $_POST['selListerPerson'. $i];
                            $trlisting->share = $_POST['txtListerShare'. $i];
                            $trlisting->save();
                        }
                    }else{
                        $trlisting = new Trlistingpersons();
                        $trlisting->type= $_POST['hdlistertype'. $i];
                        $trlisting->transactionreportid = $tr->transactionreportid;
                        $trlisting->share = $_POST['txtListerShare'. $i];
                        $trlisting->otherperson = $_POST['txtListerOtherSalesPerson'. $i];
                        $trlisting->save();
                    }
                }
            }

            $hdsellingnumber = $_POST['hdsellingnumber'];
            $affected = Trsellingpersons::model()->deleteAll('transactionreportid='.$tr->transactionreportid);

            for($i=1;$i<=$hdsellingnumber;$i++) {
                if(isset ($_POST['hdsellertype'. $i])) {
                    if($_POST['hdsellertype'. $i] == 'colleague'){
                        if(!empty($_POST['selSellerPerson'. $i])){
                            $trselling = new Trsellingpersons();
                            $trselling->type= $_POST['hdsellertype'. $i];
                            $trselling->transactionreportid = $tr->transactionreportid;
                            $trselling->userid = $_POST['selSellerPerson'. $i];
                            $trselling->share = $_POST['txtSellerShare'. $i];
                            $trselling->save();
                        }
                    }else{
                        $trselling = new Trsellingpersons();
                        $trselling->type= $_POST['hdsellertype'. $i];
                        $trselling->transactionreportid = $tr->transactionreportid;
                        $trselling->otherperson = $_POST['txSellerOtherSalesPerson'. $i];
                        $trselling->share = $_POST['txtSellerShare'. $i];
                        $trselling->save();
                    }
                }
            }

            //save vendor
            $vendor = new Trvendorlandlordassignor();
            $vendor->transactionreportid = $tr->transactionreportid;
            $vendor->type = $_POST['selVendorType'];
            $vendor->legalentity = $_POST['txtVendorLegalName'];
            $vendor->contactname = $_POST['txtVendorContactName'];
            $vendor->contactnumber = $_POST['txtVendorContactNo'];
            $vendor->email = $_POST['txtVendorEmail'];
            $vendor->faxnumber = $_POST['txtVendorFax'];
            $vendor->streetaddress = $_POST['txtVendorStreet'];
            $vendor->suburb = $_POST['txtVendorSuburb'];
            $vendor->city = $_POST['txtVendorCity'];
            //$vendor->countryid = $_POST[''];
            $vendor->solicitorsfirm = $_POST['txtVendorLawfirm'];
            $vendor->individualactingemail = $_POST['txtVendorLawyerEmail'];
            $vendor->individualactingfaxnumber = $_POST['txtVendorLawyerFax'];
            $vendor->individualactingfirstname = $_POST['txtVendorLawyerFname'];
            $vendor->individualactinglastname = $_POST['txtVendorLawyerLname'];
            $vendor->individualactingphonenumber = $_POST['txtVendorLawyerPhone'];
            if(!$vendor->save()){
                print_r($vendor->getErrors());
            }

            $purchase = new Trpurchasertenantasignee();
            $purchase->transactionreportid = $tr->transactionreportid;
            $purchase->type = $_POST['selPurchaserType'];
            $purchase->legalentity = $_POST['txtPurchaserLegalName'];
            $purchase->contactname = $_POST['txtPurchaserContactName'];
            $purchase->contactnumber = $_POST['txtPurchaserContactNo'];
            $purchase->email = $_POST['txtPurchaserEmail'];
            $purchase->faxnumber = $_POST['txtPurchaserFax'];
            $purchase->streetaddress = $_POST['txtPurchaserStreet'];
            $purchase->suburb = $_POST['txtPurchaserSuburb'];
            $purchase->city = $_POST['txtPurchaserCity'];
            //$purchase->countryid = $_POST[''];
            $purchase->solicitorsfirm = $_POST['txtPurchaserLawfirm'];
            $purchase->individualactingemail = $_POST['txtPurchaserLawyerEmail'];
            $purchase->individualactingfaxnumber = $_POST['txtPurchaserLawyerFax'];
            $purchase->individualactingfirstname = $_POST['txtPurchaserLawyerFname'];
            $purchase->individualactinglastname = $_POST['txtPurchaserLawyerLname'];
            $purchase->individualactingphonenumber = $_POST['txtPurchaserLawyerPhone'];
            if(!$purchase->save()){
                print_r($purchase->getErrors());
            }
            $notes = new Trnotes();
            $notes->transactionreportid = $tr->transactionreportid;
            $notes->ispropertyinspected = $_POST['ispropertyinspected'];
            $notes->ispropertyinspectedtext = $_POST['txtIspropertyinspected'];
            $notes->statementboundaries = $_POST['txtStatemenBoundaries'];
            $notes->statementconstruction = $_POST['txtStatementConstruction'];
            $notes->ispotentialproblems = $_POST['ispotentialproblems'];
            $notes->ispotentialproblemtext = $_POST['txtIspotentialproblems'];
            $notes->ispurchaserwarranty = $_POST['ispurchasewarranty'];
            $notes->ispurchaserwarrantytext = $_POST['txtIsPurchaserWarranty'];
            $notes->isvendorwarranty = $_POST['isvendorwarranty'];
            $notes->isvendorwarrantytext = $_POST['txtIsVendorWarranty'];
            $notes->isGSTdiscussion = $_POST['isgstdiscussion'];
            $notes->isGSTdiscussiontext = $_POST['txtIsGSTDiscussion'];
            $notes->isadvicesought = $_POST['isadvicesought'];
            $notes->isadvicesoughttext = $_POST['txtoptionIsAdviceSought'];
            $notes->ispurchaserboughtproperty = $_POST['ispurchaserbroughtproperty'];
            $notes->isviewedandappraised = $_POST['isviewedandappraised'];
            $notes->isdifferenceopinion = $_POST['isdifferenceopinon'];
            $notes->isdifferenceopiniontext = $_POST['txtIsDifferenceInOpinion'];
            $notes->iscommissiondiscussed = $_POST['iscommissiondiscussed'];
            $notes->iscommissiondiscusstext = $_POST['txtIsCommissionDiscussed'];
            $notes->isvendorincludechattel = $_POST['isvendorincludechattel'];
            $notes->isvendorincludechatteltext = $_POST['txtIsVendorIncludeChattel'];
            $notes->isreferaccountant = $_POST['isreferaccountant'];
            $notes->isreferaccountanttext = $_POST['txtIsReferAccountant'];
            $notes->vendordescriberesalevalue = $_POST['txtVendorDescribeSaleValue'];
            $notes->purchaserdiscussedresalevalue = $_POST['txtPurchaserDescribeSaleValue'];
            $notes->othercomments = $_POST['txtOtherComments'];
            //$notes->isagree = $_POST[''];
            if(!$notes->save()){
                print_r($notes->getErrors());
            }

            $confirm = new Trconfirmation();
            $confirm->isagree ='1';
            $toemail='';
            $name = '';
            $confirm->transactionreportid =$tr->transactionreportid;

            /*
            if($_POST['selListerPerson'] == $_POST['hduserid']){
                if($_POST['hdlistercolleague'] == 'colleague') {
                    $confirm->listingpersonagreed = '1';
                    $confirm->lisingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->listingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->listingpersonsigniture = $_SERVER['REMOTE_ADDR'];

                    if($_POST['hdsellercolleague'] == 'colleague') {
                        $user = Users::model()->findByPk( $_POST['selSellerPerson']);
                        $toemail = $user->personalemail;
                        $name = $user->fullname;
                    }
                }

            }

            if($_POST['selSellerPerson'] == $_POST['hduserid']){
                if($_POST['hdsellercolleague'] == 'colleague') {
                    $confirm->sellingpersonagreed = '1';
                    $confirm->sellingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->sellingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->sellingpersonsigniture = $_SERVER['REMOTE_ADDR'];

                    if($_POST['hdlistercolleague'] == 'colleague') {
                        $user = Users::model()->findByPk($_POST['selListerPerson']);
                        $toemail = $user->personalemail;
                        $name = $user->fullname;
                    }
                }
            }
            */


            $uploaddir = 'uploads/';
            if (is_uploaded_file($_FILES['uploadAgreement']['tmp_name'])) {
                $fileAgreement = basename($_FILES['uploadAgreement']['name']);
                $confirm->fileagreementlocalname=$fileAgreement;
                $confirm->fileagreement ='agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                $fileAgreement =$uploaddir . 'agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['uploadAgreement']['tmp_name'], $fileAgreement);
            }

            if (is_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'])) {
                $fileTransaction = basename($_FILES['uploadTransactionReport']['name']);
                $confirm->filetransactionlocalname=$fileTransaction;
                $confirm->fileTransaction = 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                $fileTransaction =$uploaddir . 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'], $fileTransaction);
            }

            if (is_uploaded_file($_FILES['uploadOther1']['tmp_name'])) {
                $fileOther1 = basename($_FILES['uploadOther1']['name']);
                $confirm->fileotherdoc1localname=$fileOther1;
                $confirm->fileotherdoc1 ='o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                $fileOther1 =$uploaddir. 'o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['uploadOther1']['tmp_name'], $fileOther1);
            }

            if (is_uploaded_file($_FILES['uploadOther2']['tmp_name'])) {
                $fileOther2 = basename($_FILES['uploadOther2']['name']);
                $confirm->fileotherdoc2localname=$fileOther2;
                $confirm->fileotherdoc2 ='o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                $fileOther2 =$uploaddir. 'o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['uploadOther2']['tmp_name'], $fileOther2);
            }


            if(!$confirm->save()){
                print_r($confirm->getErrors());
            }

            //$this->updateTRStatus($tr->transactionreportid);

            $userCreatedby = Users::model()->findByPk($_POST['hduserid']);

            $download_link = CHtml::link('Click Here', $this->createAbsoluteUrl('/transaction/edittransaction',array('transactionreportid'=>$tr->transactionreportid)));

            $message = "<p>Hi </p>";
            $message .= "<p>The transaction report for ".$_POST['txtAddressProperty']." created by ".$userCreatedby->fullname.", has been confirmed.</p>";
            $message .="<p>To view your transaction ".$download_link."</p>";
            $message .= "<p>Best regards,<br />P</p>";

            $mail = new JPhpMailer();
            $mail->IsSMTP();
            $mail->Port= 587;
            $mail->SMTPSecure = "tls";
            $mail->Host = 'mail.agentsites.co.nz';
            $mail->SMTPAuth = true;
            $mail->Username = 'system@agentsites.co.nz';
            $mail->Password = 'jameslaw';
            $mail->SetFrom('powerteam@jameslaw.co.nz', 'Powerteam');
            $mail->Subject = "Transaction Report has been made";
            $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

            $mail->MsgHTML($message);
            $mail->AddAddress('accounts@jameslaw.co.nz','Accounts');
            $mail->AddCC('janjunsay@gmail.com','Admin');

            $mail->SMTPDebug =1;
            if(!$mail->send()){
                echo "Mailer Error: " . $mail->ErrorInfo;die();
        }


            //if(($_POST['hdlistercolleague'] == 'colleague')||())
            /*

            if($isconfirm=='1'){

                $this->updateTRStatus($tr->transactionreportid);

                $userCreatedby = Users::model()->findByPk($_POST['hduserid']);

                $message = "<p>Hi </p>";
                $message .= "<p>The transaction report for ".$_POST['txtAddressProperty']." created by ".$userCreatedby->fullname.", has been confirmed by ".$name. ".</p>";
                //$message .="<p>".$download_link."</p>";
                $message .= "<p>Best regards,<br />P</p>";

                $mail = new JPhpMailer();
                $mail->IsSMTP();
                $mail->Port= 587;
                $mail->SMTPSecure = "tls";
                $mail->Host = 'mail.agentsites.co.nz';
                $mail->SMTPAuth = true;
                $mail->Username = 'system@agentsites.co.nz';
                $mail->Password = 'jameslaw';
                $mail->SetFrom('powerteam@jameslaw.co.nz', 'Powerteam');
                $mail->Subject = "Transaction Report has been made";
                $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

                $mail->MsgHTML($message);
                $mail->AddAddress('janjunsay@gmail.com','Admin');

                $mail->SMTPDebug =1;
                if(!$mail->send()){
                    echo "Mailer Error: " . $mail->ErrorInfo;die();
                }
            }else{
                //echo $name . '-' . $toemail;die();
                $download_link = CHtml::link('Click Here to view', $this->createAbsoluteUrl('/transaction/reviewtransaction/transactionreportid/'.$tr->transactionreportid));

                $message = "<p>Hi,".$name." </p>";
                $message .= "<p>A transaction report has been created in your name.</p>";
                $message .="<p>".$download_link."</p>";
                $message .= "<p>Best regards,<br />James Law</p>";

                $mail = new JPhpMailer();
                $mail->IsSMTP();
                $mail->Port= 587;
                $mail->SMTPSecure = "tls";
                $mail->Host = 'mail.agentsites.co.nz';
                $mail->SMTPAuth = true;
                $mail->Username = 'system@agentsites.co.nz';
                $mail->Password = 'jameslaw';
                $mail->SetFrom('powerteam@jameslaw.co.nz', 'Powerteam');
                $mail->Subject = "Transaction Report has been made";
                $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

                $mail->MsgHTML($message);

                if($_POST['selListerPerson'] != $_POST['hduserid'] && $_POST['selSellerPerson'] != $_POST['hduserid']){
                    $userList = Users::model()->findByPk( $_POST['selListerPerson']);
                    $userSell = Users::model()->findByPk( $_POST['selSellerPerson']);

                    $mail->AddAddress($userList->personalemail,$userList->fullname);
                }else{
                    $mail->AddAddress($toemail,$name);
                }

                $mail->SMTPDebug =1;
                if(!$mail->send()){
                    echo "Mailer Error: " . $mail->ErrorInfo;die();
                }
            } */

            //save logs
            $trlog = new Trlogs();
            $trlog->transactionreportid = $tr->transactionreportid;
            $trlog->createdbyid = $_POST['hduserid'];
            $trlog->message = 'Transaction report was created.';
            if(!$trlog->save()){
                print_r($trlog->getErrors());
            }
            $this->redirect(array('transactionlist'));

        }else{
            print_r($tr->getErrors());
        }


    }



    public function actionUpdatetransaction(){

        //print_r($_POST);die();

        $tr = Transactionreport::model()->findByPk($_POST['hdtransactionreportid']);

        $tr->transactiontype = $_POST['selTranstype'];
        $tr->address = $_POST['txtAddressProperty'];
        $tr->saleleaseprice = $_POST['txtSalesPrice'];
        /*
        $tr->listertype = $_POST['hdlistercolleague'];
        if($_POST['hdlistercolleague'] == 'colleague') {
            $tr->listeruserpersonid = $_POST['selListerPerson'];
        }else{
            $tr->listerotherperson = $_POST['txtListerOtherComp'];
            $tr->listerotherpersonname = $_POST['txtListerOtherSalesPerson'];
        }
        */
        $tr->listershare = $_POST['txtListerShare'];
        /*
        $tr->sellertype = $_POST['hdsellercolleague'];
        if($_POST['hdsellercolleague'] == 'colleague') {
            $tr->selleruserpersonid = $_POST['selSellerPerson'];
        }else{
            $tr->sellerotherperson = $_POST['txtSellerOtherComp'];
            $tr->sellerotherpersonname = $_POST['txtSellerOtherSalesPerson'];

        }
        */
        $tr->sellershare = $_POST['txtSellerShare'];
        $tr->ispowerteam = $_POST['hdpowerteam'];
        $tr->ispowerteamseller = $_POST['hdpowerteamseller'];
        $tr->listingreferralperc = $_POST['txtListingReferralPerc'];
        $tr->listingsplitamt = $_POST['txtListingSplitAmt'];
        $tr->listingreferralamt = $_POST['txtListingReferralAmt'];
        $tr->listinggrossbroughtin = $_POST['txtListingGrossBroughtIn'];
        $tr->sellingreferralperc = $_POST['txtSellingReferralPerc'];
        $tr->sellingsplitamt = $_POST['txtSellingSplitAmt'];
        $tr->sellingreferralamt = $_POST['txtSellingReferralAmt'];
        $tr->sellinggrossbroughtin = $_POST['txtSellingGrossBroughtIn'];
        //$tr->conjuctionaldefaultsplit = $_POST[''];
        $tr->conjuctionalothercompany = $_POST['txtConjunctionalComp'];
        $tr->conjuctionalothersplit = $_POST['txtConjunctionalSplit'];
        $tr->commissiondepositamtincGST = $_POST['txtDepositIncGST'];
        $tr->commissionamtincGST = $_POST['txtCommissionIncGST'];
        $tr->commissionamtexcGST = $_POST['txtCommissionExcGST'];
        $tr->commissiondepositamtexcGST = $_POST['txtDepositExcGST'];
        $tr->ptdrnumber = $_POST['txtPtr'];

        $start_date = $_POST['txtDate'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        //$tr->createdtime = $start_date;

        $start_date = $_POST['txtDateAgreement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->agreementdate = $start_date;

        $start_date = $_POST['txtDateUnconditional'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->unconditionaldate = $start_date;

        $start_date = $_POST['txtDatePossession'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->posessiondate = $start_date;

        $start_date = $_POST['txtDateSettlement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->settlementdate = $start_date;

        //$tr->createdbyid =  $_POST['hduserid'];
        $tr->status = $_POST['hdisconfirm'];

        if($tr->save()){


            $hdlistingnumber = $_POST['hdlistingnumber'];
            $affected = Trlistingpersons::model()->deleteAll('transactionreportid='.$tr->transactionreportid);

            for($i=1;$i<=$hdlistingnumber;$i++) {
                if(isset ($_POST['hdlistertype'. $i])) {
                    if($_POST['hdlistertype'. $i] == 'colleague'){
                        if(!empty($_POST['selListerPerson'. $i])){
                            $trlisting = new Trlistingpersons();
                            $trlisting->type= $_POST['hdlistertype'. $i];
                            $trlisting->transactionreportid = $tr->transactionreportid;
                            $trlisting->userid = $_POST['selListerPerson'. $i];
                            $trlisting->share = $_POST['txtListerShare'. $i];

                            $trlisting->save();
                        }
                    }else{
                        $trlisting = new Trlistingpersons();
                        $trlisting->type= $_POST['hdlistertype'. $i];
                        $trlisting->transactionreportid = $tr->transactionreportid;
                        $trlisting->share = $_POST['txtListerShare'. $i];
                        $trlisting->otherperson = $_POST['txtListerOtherSalesPerson'. $i];

                        $trlisting->save();
                    }
                }
            }

            $hdsellingnumber = $_POST['hdsellingnumber'];
            $affected = Trsellingpersons::model()->deleteAll('transactionreportid='.$tr->transactionreportid);

            for($i=1;$i<=$hdsellingnumber;$i++) {
                if(isset ($_POST['hdsellertype'. $i])) {
                    if($_POST['hdsellertype'. $i] == 'colleague'){
                        if(!empty($_POST['selSellerPerson'. $i])){
                            $trselling = new Trsellingpersons();
                            $trselling->type= $_POST['hdsellertype'. $i];
                            $trselling->transactionreportid = $tr->transactionreportid;
                            $trselling->userid = $_POST['selSellerPerson'. $i];
                            $trselling->share = $_POST['txtSellerShare'. $i];
                            $trselling->save();
                        }
                    }else{
                        $trselling = new Trsellingpersons();
                        $trselling->type= $_POST['hdsellertype'. $i];
                        $trselling->transactionreportid = $tr->transactionreportid;
                        $trselling->otherperson = $_POST['txSellerOtherSalesPerson'. $i];
                        $trselling->share = $_POST['txtSellerShare'. $i];
                        $trselling->save();
                    }
                }
            }

            //save vendor
            $vendor = Trvendorlandlordassignor::model()->find('transactionreportid=' . $tr->transactionreportid);
            $vendor->transactionreportid = $tr->transactionreportid;
            $vendor->type = $_POST['selVendorType'];
            $vendor->legalentity = $_POST['txtVendorLegalName'];
            $vendor->contactname = $_POST['txtVendorContactName'];
            $vendor->contactnumber = $_POST['txtVendorContactNo'];
            $vendor->email = $_POST['txtVendorEmail'];
            $vendor->faxnumber = $_POST['txtVendorFax'];
            $vendor->streetaddress = $_POST['txtVendorStreet'];
            $vendor->suburb = $_POST['txtVendorSuburb'];
            $vendor->city = $_POST['txtVendorCity'];
            //$vendor->countryid = $_POST[''];
            $vendor->solicitorsfirm = $_POST['txtVendorLawfirm'];
            $vendor->individualactingemail = $_POST['txtVendorLawyerEmail'];
            $vendor->individualactingfaxnumber = $_POST['txtVendorLawyerFax'];
            $vendor->individualactingfirstname = $_POST['txtVendorLawyerFname'];
            $vendor->individualactinglastname = $_POST['txtVendorLawyerLname'];
            $vendor->individualactingphonenumber = $_POST['txtVendorLawyerPhone'];
            if(!$vendor->save()){
                print_r($vendor->getErrors());
            }

            //$purchase = new Trpurchasertenantasignee();
            $purchase = Trpurchasertenantasignee::model()->find('transactionreportid=' . $tr->transactionreportid);
            $purchase->transactionreportid = $tr->transactionreportid;
            $purchase->type = $_POST['selPurchaserType'];
            $purchase->legalentity = $_POST['txtPurchaserLegalName'];
            $purchase->contactname = $_POST['txtPurchaserContactName'];
            $purchase->contactnumber = $_POST['txtPurchaserContactNo'];
            $purchase->email = $_POST['txtPurchaserEmail'];
            $purchase->faxnumber = $_POST['txtPurchaserFax'];
            $purchase->streetaddress = $_POST['txtPurchaserStreet'];
            $purchase->suburb = $_POST['txtPurchaserSuburb'];
            $purchase->city = $_POST['txtPurchaserCity'];
            //$purchase->countryid = $_POST[''];
            $purchase->solicitorsfirm = $_POST['txtPurchaserLawfirm'];
            $purchase->individualactingemail = $_POST['txtPurchaserLawyerEmail'];
            $purchase->individualactingfaxnumber = $_POST['txtPurchaserLawyerFax'];
            $purchase->individualactingfirstname = $_POST['txtPurchaserLawyerFname'];
            $purchase->individualactinglastname = $_POST['txtPurchaserLawyerLname'];
            $purchase->individualactingphonenumber = $_POST['txtPurchaserLawyerPhone'];
            if(!$purchase->save()){
                print_r($purchase->getErrors());
            }

            //$notes = new Trnotes();
            $notes = Trnotes::model()->find('transactionreportid=' . $tr->transactionreportid);
            $notes->transactionreportid = $tr->transactionreportid;
            $notes->ispropertyinspected = $_POST['ispropertyinspected'];
            $notes->ispropertyinspectedtext = $_POST['txtIspropertyinspected'];
            $notes->statementboundaries = $_POST['txtStatemenBoundaries'];
            $notes->statementconstruction = $_POST['txtStatementConstruction'];
            $notes->ispotentialproblems = $_POST['ispotentialproblems'];
            $notes->ispotentialproblemtext = $_POST['txtIspotentialproblems'];
            $notes->ispurchaserwarranty = $_POST['ispurchasewarranty'];
            $notes->ispurchaserwarrantytext = $_POST['txtIsPurchaserWarranty'];
            $notes->isvendorwarranty = $_POST['isvendorwarranty'];
            $notes->isvendorwarrantytext = $_POST['txtIsVendorWarranty'];
            $notes->isGSTdiscussion = $_POST['isgstdiscussion'];
            $notes->isGSTdiscussiontext = $_POST['txtIsGSTDiscussion'];
            $notes->isadvicesought = $_POST['isadvicesought'];
            $notes->isadvicesoughttext = $_POST['txtoptionIsAdviceSought'];
            $notes->ispurchaserboughtproperty = $_POST['ispurchaserbroughtproperty'];
            $notes->isviewedandappraised = $_POST['isviewedandappraised'];
            $notes->isdifferenceopinion = $_POST['isdifferenceopinon'];
            $notes->isdifferenceopiniontext = $_POST['txtIsDifferenceInOpinion'];
            $notes->iscommissiondiscussed = $_POST['iscommissiondiscussed'];
            $notes->iscommissiondiscusstext = $_POST['txtIsCommissionDiscussed'];
            $notes->isvendorincludechattel = $_POST['isvendorincludechattel'];
            $notes->isvendorincludechatteltext = $_POST['txtIsVendorIncludeChattel'];
            $notes->isreferaccountant = $_POST['isreferaccountant'];
            $notes->isreferaccountanttext = $_POST['txtIsReferAccountant'];
            $notes->vendordescriberesalevalue = $_POST['txtVendorDescribeSaleValue'];
            $notes->purchaserdiscussedresalevalue = $_POST['txtPurchaserDescribeSaleValue'];
            $notes->othercomments = $_POST['txtOtherComments'];

            if(!$notes->save()){
                print_r($notes->getErrors());die();
            }

            //$confirm = new Trconfirmation();
            $confirm = Trconfirmation::model()->find('transactionreportid=' . $tr->transactionreportid);
            $confirm->isagree ='1';
            $confirm->transactionreportid =$tr->transactionreportid;
            /*
            if($_POST['selListerPerson'] == $_POST['hduserid']){
                if($_POST['hdlistercolleague'] == 'colleague') {
                    $confirm->listingpersonagreed = '1';
                    $confirm->lisingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->listingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->listingpersonsigniture = $_SERVER['REMOTE_ADDR'];
                }

            }

            if($_POST['selSellerPerson'] == $_POST['hduserid']){
                if($_POST['hdsellercolleague'] == 'colleague') {
                    $confirm->sellingpersonagreed = '1';
                    $confirm->sellingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->sellingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->sellingpersonsigniture = $_SERVER['REMOTE_ADDR'];
                }
            }
            */
            $uploaddir = 'uploads/';

            if(isset($_FILES['uploadAgreement'])){
                if (is_uploaded_file($_FILES['uploadAgreement']['tmp_name'])) {
                    $fileAgreement = basename($_FILES['uploadAgreement']['name']);
                    $confirm->fileagreementlocalname=$fileAgreement;
                    $confirm->fileagreement ='agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                    $fileAgreement =$uploaddir . 'agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadAgreement']['tmp_name'], $fileAgreement);
                }
            }

            if(isset($_FILES['uploadTransactionReport'])){
                if (is_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'])) {
                    $fileTransaction = basename($_FILES['uploadTransactionReport']['name']);
                    $confirm->filetransactionlocalname=$fileTransaction;
                    $confirm->fileTransaction = 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                    $fileTransaction =$uploaddir . 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'], $fileTransaction);
                }
            }

            if(isset($_FILES['uploadOther1'])){
                if (is_uploaded_file($_FILES['uploadOther1']['tmp_name'])) {
                    $fileOther1 = basename($_FILES['uploadOther1']['name']);
                    $confirm->fileotherdoc1localname=$fileOther1;
                    $confirm->fileotherdoc1 ='o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                    $fileOther1 =$uploaddir. 'o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadOther1']['tmp_name'], $fileOther1);
                }
            }

            if(isset($_FILES['uploadOther2'])){
                if (is_uploaded_file($_FILES['uploadOther2']['tmp_name'])) {
                    $fileOther2 = basename($_FILES['uploadOther2']['name']);
                    $confirm->fileotherdoc2localname=$fileOther2;
                    $confirm->fileotherdoc2 ='o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                    $fileOther2 =$uploaddir. 'o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadOther2']['tmp_name'], $fileOther2);
                }
            }

            if(!$confirm->save()){
                print_r($confirm->getErrors());
            }

            //save logs
            $trlog = new Trlogs();
            $trlog->transactionreportid = $tr->transactionreportid;
            $trlog->createdbyid = $_POST['hduserid'];
            if($tr->status=='1') {
                $trlog->message = 'Transaction report was confirmed.';
            }else{
                $trlog->message = 'Transaction report was updated.';
            }

            if(!$trlog->save()){
                print_r($trlog->getErrors());
            }

            $this->redirect(array('transactionlist'));

        }else{
            print_r($tr->getErrors());
        }
    }

    public function actionUpdateconfirm(){

        $tr = Transactionreport::model()->findByPk($_POST['hdtransactionreportid']);

        $tr->transactiontype = $_POST['selTranstype'];
        $tr->address = $_POST['txtAddressProperty'];
        $tr->saleleaseprice = $_POST['txtSalesPrice'];
        $tr->listertype = $_POST['hdlistercolleague'];

        if($_POST['hdlistercolleague'] == 'colleague') {
            $tr->listeruserpersonid = $_POST['selListerPerson'];
        }else{
            $tr->listerotherperson = $_POST['txtListerOtherComp'];
            $tr->listerotherpersonname = $_POST['txtListerOtherSalesPerson'];
        }
        $tr->listershare = $_POST['txtListerShare'];

        $tr->sellertype = $_POST['hdsellercolleague'];

        if($_POST['hdsellercolleague'] == 'colleague') {
            $tr->selleruserpersonid = $_POST['selSellerPerson'];
        }else{
            $tr->sellerotherperson = $_POST['txtSellerOtherComp'];
            $tr->sellerotherpersonname = $_POST['txtSellerOtherSalesPerson'];
        }

        $tr->sellershare = $_POST['txtSellerShare'];
        $tr->ispowerteam = $_POST['hdpowerteam'];
        $tr->ispowerteamseller = $_POST['hdpowerteamseller'];
        $tr->listingreferralperc = $_POST['txtListingReferralPerc'];
        $tr->listingsplitamt = $_POST['txtListingSplitAmt'];
        $tr->listingreferralamt = $_POST['txtListingReferralAmt'];
        $tr->listinggrossbroughtin = $_POST['txtListingGrossBroughtIn'];
        $tr->sellingreferralperc = $_POST['txtSellingReferralPerc'];
        $tr->sellingsplitamt = $_POST['txtSellingSplitAmt'];
        $tr->sellingreferralamt = $_POST['txtSellingReferralAmt'];
        $tr->sellinggrossbroughtin = $_POST['txtSellingGrossBroughtIn'];
        //$tr->conjuctionaldefaultsplit = $_POST[''];
        $tr->conjuctionalothercompany = $_POST['txtConjunctionalComp'];
        $tr->conjuctionalothersplit = $_POST['txtConjunctionalSplit'];
        $tr->commissiondepositamtincGST = $_POST['txtDepositIncGST'];
        $tr->commissionamtincGST = $_POST['txtCommissionIncGST'];
        $tr->commissionamtexcGST = $_POST['txtCommissionExcGST'];
        $tr->commissiondepositamtexcGST = $_POST['txtDepositExcGST'];
        $tr->ptdrnumber = $_POST['txtPtr'];

        $start_date = $_POST['txtDate'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        //$tr->createdtime = $start_date;

        $start_date = $_POST['txtDateAgreement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->agreementdate = $start_date;

        $start_date = $_POST['txtDateUnconditional'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->unconditionaldate = $start_date;

        $start_date = $_POST['txtDatePossession'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->posessiondate = $start_date;

        $start_date = $_POST['txtDateSettlement'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $tr->settlementdate = $start_date;

        //$tr->createdbyid =  $_POST['hduserid'];

        $tr->status = $_POST['txtDateSettlement'];

        if($tr->save()){

            //save vendor
            $vendor = Trvendorlandlordassignor::model()->find('transactionreportid=' . $tr->transactionreportid);
            $vendor->transactionreportid = $tr->transactionreportid;
            $vendor->type = $_POST['selVendorType'];
            $vendor->legalentity = $_POST['txtVendorLegalName'];
            $vendor->contactname = $_POST['txtVendorContactName'];
            $vendor->contactnumber = $_POST['txtVendorContactNo'];
            $vendor->email = $_POST['txtVendorEmail'];
            $vendor->faxnumber = $_POST['txtVendorFax'];
            $vendor->streetaddress = $_POST['txtVendorStreet'];
            $vendor->suburb = $_POST['txtVendorSuburb'];
            $vendor->ciy = $_POST['txtVendorCity'];
            //$vendor->countryid = $_POST[''];
            $vendor->solicitorsfirm = $_POST['txtVendorLawfirm'];
            $vendor->individualactingemail = $_POST['txtVendorLawyerEmail'];
            $vendor->individualactingfaxnumber = $_POST['txtVendorLawyerFax'];
            $vendor->individualactingfirstname = $_POST['txtVendorLawyerFname'];
            $vendor->individualactinglastname = $_POST['txtVendorLawyerLname'];
            $vendor->individualactingphonenumber = $_POST['txtVendorLawyerPhone'];
            if(!$vendor->save()){
                print_r($vendor->getErrors());
            }

            //$purchase = new Trpurchasertenantasignee();
            $purchase = Trpurchasertenantasignee::model()->find('transactionreportid=' . $tr->transactionreportid);
            $purchase->transactionreportid = $tr->transactionreportid;
            $purchase->type = $_POST['selPurchaserType'];
            $purchase->legalentity = $_POST['txtPurchaserLegalName'];
            $purchase->contactname = $_POST['txtPurchaserContactName'];
            $purchase->contactnumber = $_POST['txtPurchaserContactNo'];
            $purchase->email = $_POST['txtPurchaserEmail'];
            $purchase->faxnumber = $_POST['txtPurchaserFax'];
            $purchase->streetaddress = $_POST['txtPurchaserStreet'];
            $purchase->suburb = $_POST['txtPurchaserSuburb'];
            $purchase->city = $_POST['txtPurchaserCity'];
            //$purchase->countryid = $_POST[''];
            $purchase->solicitorsfirm = $_POST['txtPurchaserLawfirm'];
            $purchase->individualactingemail = $_POST['txtPurchaserLawyerEmail'];
            $purchase->individualactingfaxnumber = $_POST['txtPurchaserLawyerFax'];
            $purchase->individualactingfirstname = $_POST['txtPurchaserLawyerFname'];
            $purchase->individualactingphonenumber = $_POST['txtPurchaserLawyerPhone'];
            if(!$purchase->save()){
                print_r($purchase->getErrors());
            }

            //$notes = new Trnotes();
            $notes = Trnotes::model()->find('transactionreportid=' . $tr->transactionreportid);
            $notes->transactionreportid = $tr->transactionreportid;
            $notes->ispropertyinspected = $_POST['ispropertyinspected'];
            $notes->ispropertyinspectedtext = $_POST['txtIspropertyinspected'];
            $notes->statementboundaries = $_POST['txtStatemenBoundaries'];
            $notes->statementconstruction = $_POST['txtStatementConstruction'];
            $notes->ispotentialproblems = $_POST['ispotentialproblems'];
            $notes->ispotentialproblemtext = $_POST['txtIspotentialproblems'];
            $notes->ispurchaserwarranty = $_POST['ispurchasewarranty'];
            $notes->ispurchaserwarrantytext = $_POST['txtIsPurchaserWarranty'];
            $notes->isvendorwarranty = $_POST['isvendorwarranty'];
            $notes->isvendorwarrantytext = $_POST['txtIsVendorWarranty'];
            $notes->isGSTdiscussion = $_POST['isgstdiscussion'];
            $notes->isGSTdiscussiontext = $_POST['txtIsGSTDiscussion'];
            $notes->isadvicesought = $_POST['isadvicesought'];
            $notes->isadvicesoughttext = $_POST['txtoptionIsAdviceSought'];
            $notes->ispurchaserboughtproperty = $_POST['ispurchaserbroughtproperty'];
            $notes->isviewedandappraised = $_POST['isviewedandappraised'];
            $notes->isdifferenceopinion = $_POST['isdifferenceopinon'];
            $notes->isdifferenceopiniontext = $_POST['txtIsDifferenceInOpinion'];
            $notes->iscommissiondiscussed = $_POST['iscommissiondiscussed'];
            $notes->iscommissiondiscusstext = $_POST['txtIsCommissionDiscussed'];
            $notes->isvendorincludechattel = $_POST['isvendorincludechattel'];
            $notes->isvendorincludechatteltext = $_POST['txtIsVendorIncludeChattel'];
            $notes->isreferaccountant = $_POST['isreferaccountant'];
            $notes->isreferaccountanttext = $_POST['txtIsReferAccountant'];
            $notes->vendordescriberesalevalue = $_POST['txtVendorDescribeSaleValue'];
            $notes->purchaserdiscussedresalevalue = $_POST['txtPurchaserDescribeSaleValue'];
            $notes->othercomments = $_POST['txtOtherComments'];

            if(!$notes->save()){
                print_r($notes->getErrors());die();
            }

            //$confirm = new Trconfirmation();
            $confirm = Trconfirmation::model()->find('transactionreportid=' . $tr->transactionreportid);
            $confirm->isagree ='1';
            $islisteragree = '0';
            $isselleragree = '0';

            $confirm->transactionreportid =$tr->transactionreportid;
            if($_POST['selListerPerson'] == $_POST['hduserid']){
                if($_POST['hdlistercolleague'] == 'colleague') {
                    $confirm->listingpersonagreed = '1';
                    $confirm->lisingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->listingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->listingpersonsigniture = $_SERVER['REMOTE_ADDR'];
                    $islisteragree = '1';
                }

            }

            if($_POST['selSellerPerson'] == $_POST['hduserid']){
                if($_POST['hdsellercolleague'] == 'colleague') {
                    $confirm->sellingpersonagreed = '1';
                    $confirm->sellingpersonipaddress = $_SERVER['REMOTE_ADDR'];
                    $confirm->sellingpersonagreeddate = new CDbExpression('NOW()');
                    $confirm->sellingpersonsigniture = $_SERVER['REMOTE_ADDR'];
                    $isselleragree = '0';
                }
            }

            $uploaddir = 'uploads/';

            if(isset($_FILES['uploadAgreement'])){
                if (is_uploaded_file($_FILES['uploadAgreement']['tmp_name'])) {
                    $fileAgreement = basename($_FILES['uploadAgreement']['name']);
                    $confirm->fileagreementlocalname=$fileAgreement;
                    $confirm->fileagreement ='agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                    $fileAgreement =$uploaddir . 'agr' . time() .'.'.  pathinfo($fileAgreement, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadAgreement']['tmp_name'], $fileAgreement);
                }
            }

            if(isset($_FILES['uploadTransactionReport'])){
                if (is_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'])) {
                    $fileTransaction = basename($_FILES['uploadTransactionReport']['name']);
                    $confirm->filetransactionlocalname=$fileTransaction;
                    $confirm->fileTransaction = 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                    $fileTransaction =$uploaddir . 'tra' . time() .'.'.  pathinfo($fileTransaction, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadTransactionReport']['tmp_name'], $fileTransaction);
                }
            }

            if(isset($_FILES['uploadOther1'])){
                if (is_uploaded_file($_FILES['uploadOther1']['tmp_name'])) {
                    $fileOther1 = basename($_FILES['uploadOther1']['name']);
                    $confirm->fileotherdoc1localname=$fileOther1;
                    $confirm->fileotherdoc1 ='o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                    $fileOther1 =$uploaddir. 'o1' . time() .'.'.  pathinfo($fileOther1, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadOther1']['tmp_name'], $fileOther1);
                }
            }

            if(isset($_FILES['uploadOther2'])){
                if (is_uploaded_file($_FILES['uploadOther2']['tmp_name'])) {
                    $fileOther2 = basename($_FILES['uploadOther2']['name']);
                    $confirm->fileotherdoc2localname=$fileOther2;
                    $confirm->fileotherdoc2 ='o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                    $fileOther2 =$uploaddir. 'o2' . time() .'.'.  pathinfo($fileOther2, PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['uploadOther2']['tmp_name'], $fileOther2);
                }
            }

            if(!$confirm->save()){
                print_r($confirm->getErrors());
            }else{
                if($confirm->listingpersonagreed = '1' && $confirm->sellingpersonagreed = '1'){
                    $this->updateTRStatus($tr->transactionreportid);

                    $userCreatedby = Users::model()->findByPk($_POST['hduserid']);
                    $userLister = Users::model()->findByPk($_POST['selListerPerson']);
                    $userSeller = Users::model()->findByPk($_POST['selSellerPerson']);



                    $message = "<p>Hi </p>";
                    $message .= "<p>The transaction report for ".$_POST['txtAddressProperty']." created by ".$userCreatedby->fullname.", has been confirmed by ".$userLister->fullname. " and  ".$userSeller->fullname.".</p>";
                    //$message .="<p>".$download_link."</p>";
                    $message .= "<p>Best regards,<br />Powerteam</p>";

                    $mail = new JPhpMailer();
                    $mail->IsSMTP();
                    $mail->Port= 587;
                    $mail->SMTPSecure = "tls";
                    $mail->Host = 'mail.agentsites.co.nz';
                    $mail->SMTPAuth = true;
                    $mail->Username = 'system@agentsites.co.nz';
                    $mail->Password = 'jameslaw';
                    $mail->SetFrom('powerteam@jameslaw.co.nz', 'Powerteam');
                    $mail->Subject = "Transaction Report has been made";
                    $mail->AltBody = 'To view message, please use an HTML compatible email viewer!';

                    $mail->MsgHTML($message);
                    $mail->AddAddress('janjunsay@gmail.com','Admin');

                    $mail->SMTPDebug =1;
                    if(!$mail->send()){
                        echo "Mailer Error: " . $mail->ErrorInfo;die();
                    }
                }
            }
            $this->redirect(array('transactionlist'));

        }else{
            print_r($tr->getErrors());
        }
    }

    public function updateTRStatus($id){
        $tr = Transactionreport::model()->findByPk($id);
        $tr->status = '1';
        $tr->save();
    }


    public function actionDownloadlink($file){

        //$name	= $_GET['file'];
        $upload_path = 'uploads/';

        if( file_exists( $upload_path.$file ) ){
            Yii::app()->getRequest()->sendFile( $file , file_get_contents( $upload_path.$file ) );
        }
        else{
            $this->render('download404');
        }
    }

    public function actionSavecalculation(){
        $transaction = new Transaction();

        $start_date = $_POST['txtDate'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->transactiondate = $start_date;

        $transaction->userid = $_POST['selPerson'];
        $transaction->ptr = $_POST['txtPTR'];
        $transaction->property = $_POST['txtProperty'];
        $transaction->transactionreportid = $_POST['hdtransactionreportid'];
        $transaction->totalcommissionrecievedincGST = $_POST['txtCTRMainInc'];
        $transaction->totalcommissionrecievedexcGST = $_POST['txtCTRMainExc'];
        $transaction->listingpartshareindividual = $_POST['txtListerPercentage'];
        $transaction->sellingpartshareindividual = $_POST['txtSellerPercentage'];
        $transaction->listingpartreferal = $_POST['txtListingPartReferral'];
        $transaction->listingreferalpayableto = $_POST['txtListingPayable'];
        $transaction->sellingpartreferal = $_POST['txtSellingPartReferral'];
        $transaction->sellingreferalpayableto = $_POST['txtSellingPayable'];
        $transaction->lessdeductionsexcGST = $_POST['txtLessOtherReferral'];
        $transaction->bonusdeductions = $_POST['txtBonusDeduction'];
        $transaction->bonusdeductiondesc = $_POST['txtBonusDeductionDesc'];
        $transaction->companyinvoice = $_POST['txtInvoice'];

        $start_date = $_POST['txtInvoiceDate'];

        $start_date = strtotime($start_date);
        //echo $start_date;die();
        $start_date = date("Y/m/d", $start_date);
        $transaction->invoicedate = $start_date;

        $start_date = $_POST['txtPaymentReceived'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->paymentrecieveddate = $start_date;

        $start_date = $_POST['txtPaymentSalesperson'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->paymenttosalesperson = $start_date;

        $transaction->gst = $_POST['txtGST'];
        //echo $_POST['txtGST'];die();
        if($transaction->save()){

            $trdetails = new Transactiondetails();
            $trdetails->transactionid = $transaction->transactionid;
            $trdetails->netcommisionshare = $_POST['txtNetCommToShare'];
            $trdetails->listingsplitamt = $_POST['txtListerSplitAmt'];
            $trdetails->lesslistingreferalamt = $_POST['txtLessListingRefAmt'];
            $trdetails->listinggrossbroughtin = $_POST['txtListingGrossBrought'];
            $trdetails->sellersplitamt = $_POST['txtSellingSplitAmt'];
            $trdetails->lesssellingreferealamt = $_POST['txtLessSellingRefAmt'];
            $trdetails->sellinggrossbroughtin = $_POST['txtSellingGrossBrought'];
            $trdetails->listernetbroughtfees = $_POST['txtListerNetBoughtFees'];
            $trdetails->sellernetbroughtfees = $_POST['txtSellerNetBoughtFees'];
            $trdetails->listerfeeshareamt = $_POST['txtListerFeeShareAmt'];
            $trdetails->sellerfeeshareamt = $_POST['txtSellerFeeShareAmt'];
            $trdetails->totalfeesbroughtin = $_POST['txtIndTotalFeesBought'];
            $trdetails->individualshare = $_POST['txtIndividualSharePercentage'];
            $trdetails->withholdingtax = $_POST['txtIndWithHoldingTax'];
            $trdetails->individualshareamt = $_POST['txtIndividualShareAmount'];
            $trdetails->companyshare = $_POST['txtCompanySharePercentage'];
            $trdetails->checkindividualcompany = $_POST['txtCheckIndividualComp'];
            if(!$trdetails->save()){
                print_r($trdetails->getErrors());die();
            }

            $trsalesperson = new Transactionsalesperson();
            $trsalesperson->transactionid = $transaction->transactionid;
            $trsalesperson->bonusdeduction = $_POST['txtSalesBonusDeduction'];
            $trsalesperson->bonusdeductiongst = $_POST['txtSalesBonusDeductionGST'];
            $trsalesperson->netsharebeforetax = $_POST['txtSalesNetShare'];
            $trsalesperson->withholdingtax = $_POST['txtSalesWithHoldingTax'];
            $trsalesperson->amtinvoicecompany = $_POST['txtSalesAmtToInvoice'];
            $trsalesperson->plusgst = $_POST['txtSalesPlusGST'];
            $trsalesperson->grandtotal = $_POST['txtSalesGrandTotal'];
            if(!$trsalesperson->save()){
                print_r($trsalesperson->getErrors());die();
            }

            $trcommission = new Transactioncompanycommission();
            $trcommission->transactionid = $transaction->transactionid;
            $trcommission->totalamtincGST = $_POST['txtCommTotalAmtGST'];
            $trcommission->totalGSTcomponent = $_POST['txtCommTotalGSTComponent'];
            $trcommission->totalpayoutshare = $_POST['txtCommTotalPayoutShare'];
            $trcommission->totalpayoutshare = $_POST['txtCommTotalPayoutShare'];
            $trcommission->companyshare = $_POST['txtCommCompShare'];
            $trcommission->totalpaytosalesperson = $_POST['txtCommTotalPayableToPerson'];
            $trcommission->totalpayetransfer = $_POST['txtCommTotalPAYETransfer'];
            $trcommission->payablepaye = $_POST['txtCommPayablePAYE'];
            $trcommission->GSTtransfertootheraccnt = $_POST['txtCommGSTTransfer'];
            if(!$trcommission->save()){
                print_r($trcommission->getErrors());die();
            }

            $hdmanagerscount = $_POST['hdmanagerscount'];
            $affected = Transactionmanagers::model()->deleteAll('transactionid='.$transaction->transactionid);

            for($i=1;$i<=$hdmanagerscount;$i++) {
                $trmanager = new Transactionmanagers();
                $trmanager->transactionid = $transaction->transactionid;
                $trmanager->managerid = $_POST['hdmanagerid'. $i];
                $trmanager->companyshareamt = $_POST['hdshareamt'. $i];
                $trmanager->managershare = $_POST['hdmanagershare'. $i];
                $trmanager->transactionreportid = $_POST['hdtransactionreportid'];
                $trmanager->save();
            }
            //$this->Printcalculation($transaction->transactionid);

            //save logs
            $trlog = new Trlogs();
            $trlog->transactionreportid = $_POST['hdtransactionreportid'];
            $trlog->createdbyid = Yii::app()->user->id;
            $trlog->message = 'Commission calculation for '. $transaction->user->fullname . ' was saved.';
            if(!$trlog->save()){
                print_r($trlog->getErrors());
            }

            $this->redirect(array('transaction/spreadsheet','transactionreportid'=>$_POST['hdtransactionreportid'], 'userid'=>$_POST['selPerson']));
        }else{
            print_r($transaction->getErrors());die();
        }
    }

    public function actionUpdatecalculation(){
        $transaction = Transaction::model()->findByPk($_POST['hdtransactionid']);
       // $transaction = new Transaction();
        $start_date = $_POST['txtDate'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->transactiondate = $start_date;

        $transaction->userid = $_POST['selPerson'];
        $transaction->ptr = $_POST['txtPTR'];
        $transaction->property = $_POST['txtProperty'];
        $transaction->transactionreportid = $_POST['hdtransactionreportid'];
        $transaction->totalcommissionrecievedincGST = $_POST['txtCTRMainInc'];
        $transaction->totalcommissionrecievedexcGST = $_POST['txtCTRMainExc'];
        $transaction->listingpartshareindividual = $_POST['txtListerPercentage'];
        $transaction->sellingpartshareindividual = $_POST['txtSellerPercentage'];
        $transaction->listingpartreferal = $_POST['txtListingPartReferral'];
        $transaction->listingreferalpayableto = $_POST['txtListingPayable'];
        $transaction->sellingpartreferal = $_POST['txtSellingPartReferral'];
        $transaction->sellingreferalpayableto = $_POST['txtSellingPayable'];
        $transaction->lessdeductionsexcGST = $_POST['txtLessOtherReferral'];
        $transaction->bonusdeductions = $_POST['txtBonusDeduction'];
        $transaction->bonusdeductiondesc = $_POST['txtBonusDeductionDesc'];
        $transaction->companyinvoice = $_POST['txtInvoice'];

        $start_date = $_POST['txtInvoiceDate'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->invoicedate = $start_date;

        $start_date = $_POST['txtPaymentReceived'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->paymentrecieveddate = $start_date;

        $start_date = $_POST['txtPaymentSalesperson'];
        $start_date = strtotime($start_date);
        $start_date = date("Y/m/d", $start_date);
        $transaction->paymenttosalesperson = $start_date;

        $transaction->gst = $_POST['txtGST'];
        $transaction->isclosed = $_POST['hdisclosed'];

        if($transaction->save()){

            $trdetails = Transactiondetails::model()->find('transactionid=' . $transaction->transactionid);
            //$trdetails= new Transactiondetails();
            $trdetails->transactionid = $transaction->transactionid;
            $trdetails->netcommisionshare = $_POST['txtNetCommToShare'];
            $trdetails->listingsplitamt = $_POST['txtListerSplitAmt'];
            $trdetails->lesslistingreferalamt = $_POST['txtLessListingRefAmt'];
            $trdetails->listinggrossbroughtin = $_POST['txtListingGrossBrought'];
            $trdetails->sellersplitamt = $_POST['txtSellingSplitAmt'];
            $trdetails->lesssellingreferealamt = $_POST['txtLessSellingRefAmt'];
            $trdetails->sellinggrossbroughtin = $_POST['txtSellingGrossBrought'];
            $trdetails->listernetbroughtfees = $_POST['txtListerNetBoughtFees'];
            $trdetails->sellernetbroughtfees = $_POST['txtSellerNetBoughtFees'];
            $trdetails->listerfeeshareamt = $_POST['txtListerFeeShareAmt'];
            $trdetails->sellerfeeshareamt = $_POST['txtSellerFeeShareAmt'];
            $trdetails->totalfeesbroughtin = $_POST['txtIndTotalFeesBought'];
            $trdetails->individualshare = $_POST['txtIndividualSharePercentage'];
            $trdetails->withholdingtax = $_POST['txtIndWithHoldingTax'];
            $trdetails->individualshareamt = $_POST['txtIndividualShareAmount'];
            $trdetails->companyshare = $_POST['txtCompanySharePercentage'];
            $trdetails->checkindividualcompany = $_POST['txtCheckIndividualComp'];
            if(!$trdetails->save()){
                print_r($trdetails->getErrors());die();
            }

            $trsalesperson = Transactionsalesperson::model()->find('transactionid=' . $transaction->transactionid);
            $trsalesperson->transactionid = $transaction->transactionid;
            $trsalesperson->bonusdeduction = $_POST['txtSalesBonusDeduction'];
            $trsalesperson->bonusdeductiongst = $_POST['txtSalesBonusDeductionGST'];
            $trsalesperson->netsharebeforetax = $_POST['txtSalesNetShare'];
            $trsalesperson->withholdingtax = $_POST['txtSalesWithHoldingTax'];
            $trsalesperson->amtinvoicecompany = $_POST['txtSalesAmtToInvoice'];
            $trsalesperson->plusgst = $_POST['txtSalesPlusGST'];
            $trsalesperson->grandtotal = $_POST['txtSalesGrandTotal'];
            if(!$trsalesperson->save()){
                print_r($trsalesperson->getErrors());die();
            }

            $trcommission = Transactioncompanycommission::model()->find('transactionid=' . $transaction->transactionid);
            $trcommission->transactionid = $transaction->transactionid;
            $trcommission->totalamtincGST = $_POST['txtCommTotalAmtGST'];
            $trcommission->totalGSTcomponent = $_POST['txtCommTotalGSTComponent'];
            $trcommission->totalpayoutshare = $_POST['txtCommTotalPayoutShare'];
            $trcommission->totalpayoutshare = $_POST['txtCommTotalPayoutShare'];
            $trcommission->companyshare = $_POST['txtCommCompShare'];
            $trcommission->totalpaytosalesperson = $_POST['txtCommTotalPayableToPerson'];
            $trcommission->totalpayetransfer = $_POST['txtCommTotalPAYETransfer'];
            $trcommission->payablepaye = $_POST['txtCommPayablePAYE'];
            $trcommission->GSTtransfertootheraccnt = $_POST['txtCommGSTTransfer'];
            if(!$trcommission->save()){
                print_r($trcommission->getErrors());die();
            }

            $hdmanagerscount = $_POST['hdmanagerscount'];
            $affected = Transactionmanagers::model()->deleteAll('transactionid='.$transaction->transactionid);

            for($i=1;$i<=$hdmanagerscount;$i++) {
                $trmanager = new Transactionmanagers();
                $trmanager->transactionid = $transaction->transactionid;
                $trmanager->managerid = $_POST['hdmanagerid'. $i];
                $trmanager->companyshareamt = $_POST['hdshareamt'. $i];
                $trmanager->managershare = $_POST['hdmanagershare'. $i];
                $trmanager->transactionreportid = $_POST['hdtransactionreportid'];
                $trmanager->save();
            }

            if($_POST['hdisclosed']=='1'){
                if(intval($_POST['txtBonusDeduction']) != 0){
                    $bonusdeduc = Bonusdeductions::model()->find('userid=' . $_POST['selPerson']);
                    if(empty($bonusdeduc)){
                        $b = new Bonusdeductions();
                        $b->amount = $_POST['txtBonusDeduction'];
                        $b->userid = $_POST['selPerson'];
                        if($b->save()){
                            $bonusline = new Bonusdeductionsline();
                            $bonusline->amount =-1 * $_POST['txtBonusDeduction'];
                            $bonusline->bonusdeductionsid = $b->bonusdeductionid;
                            $bonusline->description = $transaction->bonusdeductiondesc;
                            $bonusline->refnumber  = $transaction->transactionid;
                            $start_date = $_POST['txtPaymentSalesperson'];
                            $start_date = strtotime($start_date);
                            $start_date = date("Y/m/d", $start_date);
                            $bonusline->createdtime = $start_date;

                            $bonusline->save();
                        }
                    }else{
                        $amt = $bonusdeduc->amount - $_POST['txtBonusDeduction'];

                        $bonusdeduc->amount = $amt;
                        if($bonusdeduc->save()){
                            $bonusline = new Bonusdeductionsline();
                            $bonusline->amount = -1 * $_POST['txtBonusDeduction'];
                            $bonusline->bonusdeductionsid = $bonusdeduc->bonusdeductionid;
                            $bonusline->refnumber  = $transaction->transactionid;
                            $bonusline->description = $transaction->bonusdeductiondesc;
                            $start_date = $_POST['txtPaymentSalesperson'];
                            $start_date = strtotime($start_date);
                            $start_date = date("Y/m/d", $start_date);
                            $bonusline->createdtime = $start_date;

                            $bonusline->save();
                        }
                    }
                }
            }


            //save logs
            $trlog = new Trlogs();
            $trlog->transactionreportid = $_POST['hdtransactionreportid'];
            $trlog->createdbyid = Yii::app()->user->id;
            $trlog->message = 'Commission calculation for '. $transaction->user->fullname . ' was updated.';
            if(!$trlog->save()){
                print_r($trlog->getErrors());
            }

            //$this->Printcalculation($transaction->transactionid);

            $this->redirect(array('transaction/spreadsheet','transactionreportid'=>$_POST['hdtransactionreportid'], 'userid'=>$_POST['selPerson']));
        }else{
            print_r($transaction->getErrors());die();
        }
    }

    public function actionPhpinfo(){
        phpinfo();
    }

    public function actionPrinttest(){
        ini_set('max_execution_time', 300);
        $realp = realpath(dirname(__FILE__). '/../../css/');

        $pdf = new Pdf();
        $pdf->setSize('a4', 'portrait');
        //$this->render('spreadsheetnew',array('bonusamt'=>$bonusamt,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid));
        $pdf->renderPartial('printtest',array('s'=>'s'));
        $pdf->streamwithpath('printtest.pdf',$realp);
    }

    public function actionPrintTransactionreport($id){
        ini_set('max_execution_time', 300);
        $transactionreport = Transactionreport::model()->findByPk($id);

        $pdf = new Pdf();
        $pdf->setSize('a4', 'portrait');
        //$this->render('spreadsheetnew',array('bonusamt'=>$bonusamt,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid));
        $pdf->renderPartial('printtest',array('model'=>$transactionreport));
        $pdf->stream($transactionreport->transactionreportid. '.pdf');
    }

    public function actionPrintcalculation($id){

        $transaction = Transaction::model()->findByPk($id);

        $pdf = new Pdf();
        $pdf->setSize('a4', 'portrait');
        //$this->render('spreadsheetnew',array('bonusamt'=>$bonusamt,'bonusdesc'=>$bonusdesc,'tr'=>$tr,'userid'=>$userid));
        $pdf->renderPartial('printcalculation',array('bonusamt'=>$transaction->bonusdeductions,'bonusdesc'=>'','transaction'=>$transaction,'tr'=>$transaction->transactionreport,'userid'=>$transaction->userid));
        $pdf->stream($transaction->ptr. '-' . $transaction->user->lastname . '.pdf');
    }

    public function actionDeleteadmin(){
        if(isset ($_POST)) {
            foreach ($_POST['files'] as $key => $val) {
                $file = Transactionreport::model()->findByPk($val);
                $file->markdelete = '1';
                $file->save();
            }
        }
    }

    public function actionConfirmadmin(){
        if(isset ($_POST)) {
            foreach ($_POST['files'] as $key => $val) {
                $file = Transactionreport::model()->findByPk($val);
                $file->status = '1';
                $file->save();
            }
        }
    }

    public function actionMarkdelete(){
        $id = $_GET['trid'];

        $tr= Transactionreport::model()->findByPk($id);
        $tr->markdelete= '1';
        if($tr->save()){
            $this->redirect(array('transactionlist'));
        }
    }

    public function actionMarkFallen(){
        $id = $_GET['trid'];

        $tr= Transactionreport::model()->findByPk($id);
        $tr->status= '2';
        if($tr->save()){
            $this->redirect(array('transactionlist'));
        }
    }

    public function actionGetCalculations(){
        $trid = $_POST['trid'];
        $arr= array();
        $arrlist = array();

        $trlist = Trlistingpersons::model()->findAll('transactionreportid=' . $trid);
        foreach($trlist as $list){
            $name='';
            if($list->type=='colleague'){
                $name=$list->user->fullname;
            }else{
                $name=$list->otherperson;
            }
            $status = 'Calculate';

            $trans = Transaction::model()->find('userid=' . $list->userid . ' AND transactionreportid=' . $trid);
            if(!empty($trans)){
                if($trans->isclosed == '1') $status='Closed';
            }else{
                $status = 'Calculate';
            }
            array_push($arr,$name);
            array_push($arrlist,array('fullname'=>$name,'status'=>$status));
        }

        $trsell = Trsellingpersons::model()->findAll('transactionreportid=' . $trid);
        foreach($trsell as $listsell){
            if($listsell->type=='colleague'){
                $name=$listsell->user->fullname;
            }else{
                $name=$listsell->otherperson;
            }
            $status = 'Calculate';
            $trans = Transaction::model()->find('userid=' . $listsell->userid . ' AND transactionreportid=' . $trid);
            if(!empty($trans)){
                if($trans->isclosed == '1') $status='Closed';
            }else{
                $status = 'Calculate';
            }

            if (!in_array($name, $arr)) {
                array_push($arrlist,array('fullname'=>$name,'status'=>$status));
            }
        }

        echo CJSON::encode($arrlist);
    }
}

?>