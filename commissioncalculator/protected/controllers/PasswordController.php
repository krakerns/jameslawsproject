<?php

class PasswordController extends Controller
{

    public $layout='bootstraplayout';

	public function actionCreate()
	{
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $passwordModel  = new Passwordmngr();

        // Uncomment the following line if AJAX validation is needed
        //$this->performAjaxValidation($advertisementModel);

        if(isset($_POST['Passwordmngr']))
        {

            $passwordModel->attributes             = $_POST['Passwordmngr'];
            $passwordModel->user_id = Yii::app()->user->id;
            if($passwordModel->save()){

               $this->redirect(array('list'));
            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('details',array(
            'model'=>$passwordModel,
        ));


        //$this->render('details');
	}

	public function actionEdit($pid)
	{
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }

        $passwordModel = Passwordmngr::model()->findByPk((int) $pid);
        if($passwordModel===null)
        {
            throw new CHttpException(404,'The requested page does not exist.');
        }

        if(isset($_POST['Passwordmngr']))
        {

            $passwordModel->attributes             = $_POST['Passwordmngr'];

            if($passwordModel->save()){

                $this->redirect(array('list'));

            }else{
                Yii::app()->user->setFlash('error', "Error creating a user record.'");
            }

        }

        // Show the details screen
        $this->render('details',array(
            'model'=>$passwordModel,
        ));
	}

    public function actionDelete(){
        $passwordid = $_POST['passwordid'];
        $passwordModel = Passwordmngr::model()->findByPk((int)$passwordid);

        if ($passwordModel == null)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Invalid User"}';
            Yii::app()->end();
        }


        $result = $passwordModel->delete();

        if ($result == false)
        {
            header("Content-type: application/json");
            echo '{"result":"fail", "message":"Failed to delete"}';
            Yii::app()->end();
        }
        else
        {
            //$this->redirect(array('index'));
        }


        echo '{"result":"success", "message":""}';
        Yii::app()->end();
    }

	public function actionIndex()
	{
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
		$this->render('index');
	}

	public function actionList()
	{
        if(Yii::app()->user->role == 'user'){
            $this->layout='bootstraplayoutuser';
        }
        $listpassword = Passwordmngr::model()->findAll('user_id='.Yii::app()->user->id);
		$this->render('list',array('listpassword'=>$listpassword));
	}


}