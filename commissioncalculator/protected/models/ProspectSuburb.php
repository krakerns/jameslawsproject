<?php

/**
 * This is the model class for table "{{prospect_suburb}}".
 *
 * The followings are the available columns in table '{{prospect_suburb}}':
 * @property integer $id
 * @property integer $prospect_id
 * @property integer $suburb_id
 * @property string $created_time
 * @property integer $createdby_id
 */
class ProspectSuburb extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{prospect_suburb}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prospect_id, suburb_id, createdby_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, prospect_id, suburb_id, created_time, createdby_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'createdby' => array(self::BELONGS_TO, 'Users', 'createdby_id'),
			'prospect' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'suburb' => array(self::BELONGS_TO, 'Suburb', 'suburb_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'prospect_id' => 'Prospect',
			'suburb_id' => 'Suburb',
			'created_time' => 'Created Time',
			'createdby_id' => 'Createdby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('prospect_id',$this->prospect_id);

		$criteria->compare('suburb_id',$this->suburb_id);

		$criteria->compare('created_time',$this->created_time,true);

		$criteria->compare('createdby_id',$this->createdby_id);

		return new CActiveDataProvider('ProspectSuburb', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return ProspectSuburb the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}