<?php

/**
 * This is the model class for table "{{transactionreport}}".
 *
 * The followings are the available columns in table '{{transactionreport}}':
 * @property integer $transactionreportid
 * @property integer $transactiontype
 * @property string $address
 * @property double $saleleaseprice
 * @property string $agreementdate
 * @property string $unconditionaldate
 * @property string $posessiondate
 * @property string $settlementdate
 * @property string $listertype
 * @property integer $listeruserpersonid
 * @property string $listerotherperson
 * @property double $listershare
 * @property string $sellertype
 * @property integer $selleruserpersonid
 * @property string $sellerotherperson
 * @property double $sellershare
 * @property integer $ispowerteam
 * @property double $listingsplitamt
 * @property double $listingreferralamt
 * @property double $listinggrossbroughtin
 * @property double $sellingsplitamt
 * @property double $sellingreferralamt
 * @property double $sellinggrossbroughtin
 * @property string $conjuctionaldefaultsplit
 * @property string $conjuctionalothersplit
 * @property string $conjuctionalothercompany
 * @property double $commissionamtexcGST
 * @property double $commissionamtincGST
 * @property double $commissiondepositamtincGST
 * @property double $commissiondepositamtexcGST
 * @property string $ptdrnumber
 * @property string $createdtime
 * @property string $modifiedtime
 * @property integer $createdbyid
 * @property integer $modifiedbyid
 * @property integer $status
 * @property double $listingreferralperc
 * @property double $sellingreferralperc
 * @property integer $ispowerteamseller
 * @property string $listerotherpersonname
 * @property string $sellerotherpersonname
 * @property integer $markdelete
 * @property integer $isdraft
 *
 * The followings are the available model relations:
 * @property Transaction[] $transactions
 * @property Transactionmanagers[] $transactionmanagers
 * @property Users $listeruserperson
 * @property Users $selleruserperson
 * @property Users $createdby
 * @property Users $modifiedby
 * @property Trconfirmation[] $trconfirmations
 * @property Trlistingpersons[] $trlistingpersons
 * @property Trlogs[] $trlogs
 * @property Trnotes[] $trnotes
 * @property Trpurchasertenantasignee[] $trpurchasertenantasignees
 * @property Trsellingpersons[] $trsellingpersons
 * @property Trvendorlandlordassignor[] $trvendorlandlordassignors
 */
class Transactionreport extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{transactionreport}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(

            array('transactiontype, listeruserpersonid, selleruserpersonid, ispowerteam, createdbyid, modifiedbyid, status, ispowerteamseller, markdelete', 'numerical', 'integerOnly'=>true),
            array('saleleaseprice, listershare, sellershare, listingsplitamt, listingreferralamt, listinggrossbroughtin, sellingsplitamt, sellingreferralamt, sellinggrossbroughtin, commissionamtexcGST, commissionamtincGST, commissiondepositamtincGST, commissiondepositamtexcGST, listingreferralperc, sellingreferralperc', 'numerical'),
            array('listertype, sellertype', 'length', 'max'=>9),
            array('listerotherperson, sellerotherperson, conjuctionalothercompany, listerotherpersonname, sellerotherpersonname', 'length', 'max'=>100),
            array('conjuctionaldefaultsplit, conjuctionalothersplit', 'length', 'max'=>20),
            array('ptdrnumber', 'length', 'max'=>36),
            array('address, agreementdate, unconditionaldate, posessiondate, settlementdate, createdtime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('transactionreportid, transactiontype, address, saleleaseprice, agreementdate, unconditionaldate, posessiondate, settlementdate, listertype, listeruserpersonid, listerotherperson, listershare, sellertype, selleruserpersonid, sellerotherperson, sellershare, ispowerteam, listingsplitamt, listingreferralamt, listinggrossbroughtin, sellingsplitamt, sellingreferralamt, sellinggrossbroughtin, conjuctionaldefaultsplit, conjuctionalothersplit, conjuctionalothercompany, commissionamtexcGST, commissionamtincGST, commissiondepositamtincGST, commissiondepositamtexcGST, ptdrnumber, createdtime, modifiedtime, createdbyid, modifiedbyid, status, listingreferralperc, sellingreferralperc, ispowerteamseller, listerotherpersonname, sellerotherpersonname, markdelete,isdraft', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'transactions' => array(self::HAS_MANY, 'Transaction', 'transactionreportid'),
            'transactionmanagers' => array(self::HAS_MANY, 'Transactionmanagers', 'transactionreportid'),
            'listeruserperson' => array(self::BELONGS_TO, 'Users', 'listeruserpersonid'),
            'selleruserperson' => array(self::BELONGS_TO, 'Users', 'selleruserpersonid'),
            'createdby' => array(self::BELONGS_TO, 'Users', 'createdbyid'),
            'modifiedby' => array(self::BELONGS_TO, 'Users', 'modifiedbyid'),
            'trconfirmations' => array(self::HAS_MANY, 'Trconfirmation', 'transactionreportid'),
            'trlistingpersons' => array(self::HAS_MANY, 'Trlistingpersons', 'transactionreportid'),
            'trlogs' => array(self::HAS_MANY, 'Trlogs', 'transactionreportid'),
            'trnotes' => array(self::HAS_MANY, 'Trnotes', 'transactionreportid'),
            'trpurchasertenantasignees' => array(self::HAS_MANY, 'Trpurchasertenantasignee', 'transactionreportid'),
            'trsellingpersons' => array(self::HAS_MANY, 'Trsellingpersons', 'transactionreportid'),
            'trvendorlandlordassignors' => array(self::HAS_MANY, 'Trvendorlandlordassignor', 'transactionreportid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'transactionreportid' => 'Transactionreportid',
            'transactiontype' => 'Transactiontype',
            'address' => 'Address',
            'saleleaseprice' => 'Saleleaseprice',
            'agreementdate' => 'Agreementdate',
            'unconditionaldate' => 'Unconditionaldate',
            'posessiondate' => 'Posessiondate',
            'settlementdate' => 'Settlementdate',
            'listertype' => 'Listertype',
            'listeruserpersonid' => 'Listeruserpersonid',
            'listerotherperson' => 'Listerotherperson',
            'listershare' => 'Listershare',
            'sellertype' => 'Sellertype',
            'selleruserpersonid' => 'Selleruserpersonid',
            'sellerotherperson' => 'Sellerotherperson',
            'sellershare' => 'Sellershare',
            'ispowerteam' => 'Ispowerteam',
            'listingsplitamt' => 'Listingsplitamt',
            'listingreferralamt' => 'Listingreferralamt',
            'listinggrossbroughtin' => 'Listinggrossbroughtin',
            'sellingsplitamt' => 'Sellingsplitamt',
            'sellingreferralamt' => 'Sellingreferralamt',
            'sellinggrossbroughtin' => 'Sellinggrossbroughtin',
            'conjuctionaldefaultsplit' => 'Conjuctionaldefaultsplit',
            'conjuctionalothersplit' => 'Conjuctionalothersplit',
            'conjuctionalothercompany' => 'Conjuctionalothercompany',
            'commissionamtexcGST' => 'Commissionamtexc Gst',
            'commissionamtincGST' => 'Commissionamtinc Gst',
            'commissiondepositamtincGST' => 'Commissiondepositamtinc Gst',
            'commissiondepositamtexcGST' => 'Commissiondepositamtexc Gst',
            'ptdrnumber' => 'Ptdrnumber',
            'createdtime' => 'Createdtime',
            'modifiedtime' => 'Modifiedtime',
            'createdbyid' => 'Createdbyid',
            'modifiedbyid' => 'Modifiedbyid',
            'status' => 'Status',
            'listingreferralperc' => 'Listingreferralperc',
            'sellingreferralperc' => 'Sellingreferralperc',
            'ispowerteamseller' => 'Ispowerteamseller',
            'listerotherpersonname' => 'Listerotherpersonname',
            'sellerotherpersonname' => 'Sellerotherpersonname',
            'markdelete' => 'Markdelete',
            'isdraft' => 'Isdraft',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('transactionreportid',$this->transactionreportid);
        $criteria->compare('transactiontype',$this->transactiontype);
        $criteria->compare('address',$this->address,true);
        $criteria->compare('saleleaseprice',$this->saleleaseprice);
        $criteria->compare('agreementdate',$this->agreementdate,true);
        $criteria->compare('unconditionaldate',$this->unconditionaldate,true);
        $criteria->compare('posessiondate',$this->posessiondate,true);
        $criteria->compare('settlementdate',$this->settlementdate,true);
        $criteria->compare('listertype',$this->listertype,true);
        $criteria->compare('listeruserpersonid',$this->listeruserpersonid);
        $criteria->compare('listerotherperson',$this->listerotherperson,true);
        $criteria->compare('listershare',$this->listershare);
        $criteria->compare('sellertype',$this->sellertype,true);
        $criteria->compare('selleruserpersonid',$this->selleruserpersonid);
        $criteria->compare('sellerotherperson',$this->sellerotherperson,true);
        $criteria->compare('sellershare',$this->sellershare);
        $criteria->compare('ispowerteam',$this->ispowerteam);
        $criteria->compare('listingsplitamt',$this->listingsplitamt);
        $criteria->compare('listingreferralamt',$this->listingreferralamt);
        $criteria->compare('listinggrossbroughtin',$this->listinggrossbroughtin);
        $criteria->compare('sellingsplitamt',$this->sellingsplitamt);
        $criteria->compare('sellingreferralamt',$this->sellingreferralamt);
        $criteria->compare('sellinggrossbroughtin',$this->sellinggrossbroughtin);
        $criteria->compare('conjuctionaldefaultsplit',$this->conjuctionaldefaultsplit,true);
        $criteria->compare('conjuctionalothersplit',$this->conjuctionalothersplit,true);
        $criteria->compare('conjuctionalothercompany',$this->conjuctionalothercompany,true);
        $criteria->compare('commissionamtexcGST',$this->commissionamtexcGST);
        $criteria->compare('commissionamtincGST',$this->commissionamtincGST);
        $criteria->compare('commissiondepositamtincGST',$this->commissiondepositamtincGST);
        $criteria->compare('commissiondepositamtexcGST',$this->commissiondepositamtexcGST);
        $criteria->compare('ptdrnumber',$this->ptdrnumber,true);
        $criteria->compare('createdtime',$this->createdtime,true);
        $criteria->compare('modifiedtime',$this->modifiedtime,true);
        $criteria->compare('createdbyid',$this->createdbyid);
        $criteria->compare('modifiedbyid',$this->modifiedbyid);
        $criteria->compare('status',$this->status);
        $criteria->compare('listingreferralperc',$this->listingreferralperc);
        $criteria->compare('sellingreferralperc',$this->sellingreferralperc);
        $criteria->compare('ispowerteamseller',$this->ispowerteamseller);
        $criteria->compare('listerotherpersonname',$this->listerotherpersonname,true);
        $criteria->compare('sellerotherpersonname',$this->sellerotherpersonname,true);
        $criteria->compare('markdelete',$this->markdelete);
        $criteria->compare('isdraft',$this->isdraft);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Transactionreport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function listTransactiontype()
    {

        return array('1'    => 'Residential Sale',
            '2'    => 'Commercial Sale',
            '3'       => 'Commercial Lease',
            '4'=>'Commercial Assignment of Lease',
            '5'=>'Business Sale');
    }

    public function getTransactionTypeName(){

        $ret = '';

        switch($this->transactiontype){
            case '1':
                $ret = 'Residential Sale';
                break;
            case '2':
                $ret = 'Commercial Sale';
                break;
            case '3':
                $ret = 'Commercial Lease';
                break;
            case '4':
                $ret = 'Commercial Assignment Of Lease';
                break;
            case '5':
                $ret = 'Business Sale';
                break;
        }

        return $ret;
    }
}
