<?php

/**
 * This is the model class for table "{{marketing_fee_payments}}".
 *
 * The followings are the available columns in table '{{marketing_fee_payments}}':
 * @property integer $id
 * @property integer $marketing_fee_id
 * @property double $amount
 * @property string $description
 * @property string $date_paid
 * @property string $supplier
 *
 * The followings are the available model relations:
 * @property MarketingFee $marketingFee
 */
class MarketingFeePayments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{marketing_fee_payments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('marketing_fee_id', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('supplier', 'length', 'max'=>100),
			array('description, date_paid', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, marketing_fee_id, amount, description, date_paid, supplier', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'marketingFee' => array(self::BELONGS_TO, 'MarketingFee', 'marketing_fee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'marketing_fee_id' => 'Marketing Fee',
			'amount' => 'Amount',
			'description' => 'Description',
			'date_paid' => 'Date Paid',
			'supplier' => 'Supplier',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('marketing_fee_id',$this->marketing_fee_id);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('date_paid',$this->date_paid,true);
		$criteria->compare('supplier',$this->supplier,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarketingFeePayments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
