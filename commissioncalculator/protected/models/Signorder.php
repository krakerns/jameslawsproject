<?php

/**
 * This is the model class for table "{{signorder}}".
 *
 * The followings are the available columns in table '{{signorder}}':
 * @property integer $id
 * @property string $size
 * @property integer $on_sign
 * @property string $street_address
 * @property string $suburb
 * @property string $status
 * @property string $line1
 * @property string $line2
 * @property string $line3
 * @property string $line4
 * @property string $installation_date
 * @property string $installation_instruction
 * @property string $photo
 * @property string $sent_order
 * @property string $sent_date
 * @property integer $createdby_id
 * @property string $create_date
 * @property integer $modifiedby_id
 * @property string $modified_date
 * @property integer $approvedby_id
 * @property string $approved_date
 * @property integer $is_valid
 * @property integer $salesperson_id
 * @property string $type
 *
 * The followings are the available model relations:
 * @property Users $createdby
 * @property Users $modifiedby
 * @property Users $approvedby
 * @property Users $salesperson
 * @property Signorderpersons[] $signorderpersons
 */
class Signorder extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{signorder}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('on_sign, createdby_id, modifiedby_id, approvedby_id, is_valid, salesperson_id', 'numerical', 'integerOnly'=>true),
            array('size', 'length', 'max'=>6),
            array('suburb', 'length', 'max'=>100),
            array('status', 'length', 'max'=>9),
            array('line1, line2, line3, line4', 'length', 'max'=>200),
            array('sent_order', 'length', 'max'=>5),
            array('type', 'length', 'max'=>7),
            array('create_date, modified_date, approved_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, size, on_sign, street_address, suburb, status, line1, line2, line3, line4, installation_date, installation_instruction, photo, sent_order, sent_date, createdby_id, create_date, modifiedby_id, modified_date, approvedby_id, approved_date, is_valid, salesperson_id, type', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'createdby' => array(self::BELONGS_TO, 'Users', 'createdby_id'),
            'modifiedby' => array(self::BELONGS_TO, 'Users', 'modifiedby_id'),
            'approvedby' => array(self::BELONGS_TO, 'Users', 'approvedby_id'),
            'salesperson' => array(self::BELONGS_TO, 'Users', 'salesperson_id'),
            'signorderpersons' => array(self::HAS_MANY, 'Signorderpersons', 'signorderid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'size' => 'Size',
            'on_sign' => 'On Sign',
            'street_address' => 'Street Address',
            'suburb' => 'Suburb',
            'status' => 'Status',
            'line1' => 'Line1',
            'line2' => 'Line2',
            'line3' => 'Line3',
            'line4' => 'Line4',
            'installation_date' => 'Installation Date',
            'installation_instruction' => 'Installation Instruction',
            'photo' => 'Photo',
            'sent_order' => 'Sent Order',
            'sent_date' => 'Sent Date',
            'createdby_id' => 'Createdby',
            'create_date' => 'Create Date',
            'modifiedby_id' => 'Modifiedby',
            'modified_date' => 'Modified Date',
            'approvedby_id' => 'Approvedby',
            'approved_date' => 'Approved Date',
            'is_valid' => 'Is Valid',
            'salesperson_id' => 'Salesperson',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('size',$this->size,true);
        $criteria->compare('on_sign',$this->on_sign);
        $criteria->compare('street_address',$this->street_address,true);
        $criteria->compare('suburb',$this->suburb,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('line1',$this->line1,true);
        $criteria->compare('line2',$this->line2,true);
        $criteria->compare('line3',$this->line3,true);
        $criteria->compare('line4',$this->line4,true);
        $criteria->compare('installation_date',$this->installation_date,true);
        $criteria->compare('installation_instruction',$this->installation_instruction,true);
        $criteria->compare('photo',$this->photo,true);
        $criteria->compare('sent_order',$this->sent_order,true);
        $criteria->compare('sent_date',$this->sent_date,true);
        $criteria->compare('createdby_id',$this->createdby_id);
        $criteria->compare('create_date',$this->create_date,true);
        $criteria->compare('modifiedby_id',$this->modifiedby_id);
        $criteria->compare('modified_date',$this->modified_date,true);
        $criteria->compare('approvedby_id',$this->approvedby_id);
        $criteria->compare('approved_date',$this->approved_date,true);
        $criteria->compare('is_valid',$this->is_valid);
        $criteria->compare('salesperson_id',$this->salesperson_id);
        $criteria->compare('type',$this->type,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Signorder the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function listSizes()
    {

        return array('small'    => 'Small',
            'medium'    => 'Medium',
            'large'       => 'Large');
    }

    public function listOnSign()
    {

        return array('1'    => 'Yes',
            '0'    => 'No');
    }

    public function listType()
    {

        return array('sale'    => 'Sale',
            'lease'    => 'Lease',
            'auction'       => 'Auction',
            'tender'       => 'Tender',
        );
    }

    public function listStatus()
    {

        return array('ordered'    => 'Ordered',
            'installed'    => 'Installed',
            'removed'       => 'Removed'
        );
    }
}
