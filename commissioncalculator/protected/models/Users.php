<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $reeanumber
 * @property string $irdnumber
 * @property string $companyname
 * @property string $companyemail
 * @property string $personalemail
 * @property string $mobilephonenumber
 * @property double $usershare
 * @property double $payepercentage
 * @property double $withholdingtax
 * @property string $createdtime
 * @property string $modifiedtime
 * @property integer $createdby
 * @property integer $modifiedby
 * @property integer $isactive
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('firstname, lastname', 'required'),
			array('createdby, modifiedby', 'numerical', 'integerOnly'=>true),
			array('usershare, payepercentage, withholdingtax', 'numerical'),
			array('firstname, lastname, reeanumber, irdnumber', 'length', 'max'=>36),
			array('companyname, companyemail, personalemail', 'length', 'max'=>100),
			array('mobilephonenumber', 'length', 'max'=>50),
			array('createdtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('userid, firstname, lastname, reeanumber, irdnumber, companyname, companyemail, personalemail, mobilephonenumber, usershare, payepercentage, withholdingtax, createdtime, modifiedtime, createdby, modifiedby,isactive', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bonusdeductions' => array(self::HAS_MANY, 'Bonusdeductions', 'userid'),
			'managers' => array(self::HAS_MANY, 'Managers', 'usertomanage'),
			'transactions' => array(self::HAS_MANY, 'Transaction', 'modifiedby'),
			'transactionreports' => array(self::HAS_MANY, 'Transactionreport', 'modifiedbyid'),
			'useraccounts' => array(self::HAS_MANY, 'Useraccounts', 'userid'),
			'createdby0' => array(self::BELONGS_TO, 'Users', 'createdby'),
			'users' => array(self::HAS_MANY, 'Users', 'modifiedby'),
			'modifiedby0' => array(self::BELONGS_TO, 'Users', 'modifiedby'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'reeanumber' => 'Reeanumber',
			'irdnumber' => 'Irdnumber',
			'companyname' => 'Companyname',
			'companyemail' => 'Companyemail',
			'personalemail' => 'Personalemail',
			'mobilephonenumber' => 'Mobilephonenumber',
			'usershare' => 'Usershare',
			'payepercentage' => 'Payepercentage',
			'withholdingtax' => 'Withholdingtax',
			'createdtime' => 'Createdtime',
			'modifiedtime' => 'Modifiedtime',
			'createdby' => 'Createdby',
			'modifiedby' => 'Modifiedby',
            'isactive' => 'Isactive',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);

		$criteria->compare('firstname',$this->firstname,true);

		$criteria->compare('lastname',$this->lastname,true);

		$criteria->compare('reeanumber',$this->reeanumber,true);

		$criteria->compare('irdnumber',$this->irdnumber,true);

		$criteria->compare('companyname',$this->companyname,true);

		$criteria->compare('companyemail',$this->companyemail,true);

		$criteria->compare('personalemail',$this->personalemail,true);

		$criteria->compare('mobilephonenumber',$this->mobilephonenumber,true);

		$criteria->compare('usershare',$this->usershare);

		$criteria->compare('payepercentage',$this->payepercentage);

		$criteria->compare('withholdingtax',$this->withholdingtax);

		$criteria->compare('createdtime',$this->createdtime,true);

		$criteria->compare('modifiedtime',$this->modifiedtime,true);

		$criteria->compare('createdby',$this->createdby);

		$criteria->compare('modifiedby',$this->modifiedby);

        $criteria->compare('isactive',$this->isactive);

		return new CActiveDataProvider('Users', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}



    public function beforeSave() {

        // /////////////////////////////////////////////////////////////////
        // Set the create time and user for new records
        // /////////////////////////////////////////////////////////////////
        if ($this->isNewRecord) {
            $this->createdtime = new CDbExpression('NOW()');
            //$this->created_by   = Yii::app()->user->id;
        }

        // /////////////////////////////////////////////////////////////////
        // The modified log details is set for record creation and update
        // /////////////////////////////////////////////////////////////////
        $this->modifiedtime = new CDbExpression('NOW()');
        //$this->modified_by   = Yii::app()->user->id;


        return parent::beforeSave();
    }

    public function getFullName(){
        return $this->firstname . ' ' . $this->lastname;
    }
}