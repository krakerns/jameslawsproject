<?php

/**
 * This is the model class for table "{{bonusdeductionsline}}".
 *
 * The followings are the available columns in table '{{bonusdeductionsline}}':
 * @property integer $id
 * @property integer $bonusdeductionsid
 * @property double $amount
 * @property string $createdtime
 * @property string $description
 * @property integer $refnumber
 *
 * The followings are the available model relations:
 * @property Bonusdeductions $bonusdeductions
 */
class Bonusdeductionsline extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bonusdeductionsline}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bonusdeductionsid, refnumber', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('description', 'length', 'max'=>225),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, bonusdeductionsid, amount, createdtime, description, refnumber', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bonusdeductions' => array(self::BELONGS_TO, 'Bonusdeductions', 'bonusdeductionsid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'bonusdeductionsid' => 'Bonusdeductionsid',
			'amount' => 'Amount',
			'createdtime' => 'Createdtime',
			'description' => 'Description',
			'refnumber' => 'Refnumber',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('bonusdeductionsid',$this->bonusdeductionsid);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('refnumber',$this->refnumber);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bonusdeductionsline the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
