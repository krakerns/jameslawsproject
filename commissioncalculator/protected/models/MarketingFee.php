<?php

/**
 * This is the model class for table "{{marketing_fee}}".
 *
 * The followings are the available columns in table '{{marketing_fee}}':
 * @property integer $id
 * @property string $invoice
 * @property string $property
 * @property integer $user_id
 * @property string $fund_received
 * @property string $description
 * @property double $initial_amount
 * @property integer $current_amount
 * @property string $datetime_created
 * @property integer $createdby_id
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Users $createdby
 * @property MarketingFeePayments[] $marketingFeePayments
 * @property MarketingFeeSalesperson[] $marketingFeeSalespeople
 */
class MarketingFee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{marketing_fee}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, current_amount, createdby_id', 'numerical', 'integerOnly'=>true),
			array('initial_amount', 'numerical'),
			array('invoice', 'length', 'max'=>100),
			array('property, fund_received, description, datetime_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, invoice, property, user_id, fund_received, description, initial_amount, current_amount, datetime_created, createdby_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
			'createdby' => array(self::BELONGS_TO, 'Users', 'createdby_id'),
			'marketingFeePayments' => array(self::HAS_MANY, 'MarketingFeePayments', 'marketing_fee_id'),
			'marketingFeeSalespeople' => array(self::HAS_MANY, 'MarketingFeeSalesperson', 'marketing_fee_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invoice' => 'Invoice',
			'property' => 'Property',
			'user_id' => 'User',
			'fund_received' => 'Fund Received',
			'description' => 'Description',
			'initial_amount' => 'Initial Amount',
			'current_amount' => 'Current Amount',
			'datetime_created' => 'Datetime Created',
			'createdby_id' => 'Createdby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('invoice',$this->invoice,true);
		$criteria->compare('property',$this->property,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('fund_received',$this->fund_received,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('initial_amount',$this->initial_amount);
		$criteria->compare('current_amount',$this->current_amount);
		$criteria->compare('datetime_created',$this->datetime_created,true);
		$criteria->compare('createdby_id',$this->createdby_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarketingFee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
