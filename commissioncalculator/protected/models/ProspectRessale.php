<?php

/**
 * This is the model class for table "{{prospect_ressale}}".
 *
 * The followings are the available columns in table '{{prospect_ressale}}':
 * @property integer $id
 * @property integer $prospect_id
 * @property string $property_type
 * @property string $purpose
 * @property integer $bedroom
 * @property integer $bathroom
 * @property integer $price
 * @property string $enquiry_on
 * @property string $free_time_to
 * @property string $free_time_from
 * @property string $created_time
 * @property integer $createdby_id
 * @property string $modified_time
 * @property integer $modifiedby_id
 */
class ProspectRessale extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{prospect_ressale}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prospect_id, bedroom, bathroom, price, createdby_id, modifiedby_id', 'numerical', 'integerOnly'=>true),
			array('property_type', 'length', 'max'=>9),
			array('purpose', 'length', 'max'=>10),
			array('enquiry_on, free_time_to, free_time_from, modified_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, prospect_id, property_type, purpose, bedroom, bathroom, price, enquiry_on, free_time_to, free_time_from, created_time, createdby_id, modified_time, modifiedby_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'modifiedby' => array(self::BELONGS_TO, 'Users', 'modifiedby_id'),
			'prospect' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'createdby' => array(self::BELONGS_TO, 'Users', 'createdby_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'prospect_id' => 'Prospect',
			'property_type' => 'Property Type',
			'purpose' => 'Purpose',
			'bedroom' => 'Bedroom',
			'bathroom' => 'Bathroom',
			'price' => 'Price',
			'enquiry_on' => 'Enquiry On',
			'free_time_to' => 'Free Time To',
			'free_time_from' => 'Free Time From',
			'created_time' => 'Created Time',
			'createdby_id' => 'Createdby',
			'modified_time' => 'Modified Time',
			'modifiedby_id' => 'Modifiedby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('prospect_id',$this->prospect_id);

		$criteria->compare('property_type',$this->property_type,true);

		$criteria->compare('purpose',$this->purpose,true);

		$criteria->compare('bedroom',$this->bedroom);

		$criteria->compare('bathroom',$this->bathroom);

		$criteria->compare('price',$this->price);

		$criteria->compare('enquiry_on',$this->enquiry_on,true);

		$criteria->compare('free_time_to',$this->free_time_to,true);

		$criteria->compare('free_time_from',$this->free_time_from,true);

		$criteria->compare('created_time',$this->created_time,true);

		$criteria->compare('createdby_id',$this->createdby_id);

		$criteria->compare('modified_time',$this->modified_time,true);

		$criteria->compare('modifiedby_id',$this->modifiedby_id);

		return new CActiveDataProvider('ProspectRessale', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return ProspectRessale the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function listPropertytype()
    {
        return array('house'    => 'House',
            'apartment'    => 'Apartment',
            'unit'    => 'Unit',
            'townhouse'       => 'Town House');
    }

    public function getPropertyTypeName(){
        $ret = '';
        switch($this->property_type){
            case 'house':
                $ret = 'House';
                break;
            case 'apartment':
                $ret = 'Apartment';
                break;
            case 'unit':
                $ret = 'Unit';
                break;
            case 'townhouse':
                $ret = 'Town House';
                break;

        }

        return $ret;
    }
}