<?php

/**
 * This is the model class for table "{{trconfirmationdocuments}}".
 *
 * The followings are the available columns in table '{{trconfirmationdocuments}}':
 * @property integer $trconfirmationdocumentsid
 * @property integer $trconfirmationid
 * @property string $filename
 * @property string $localname
 */
class Trconfirmationdocuments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{trconfirmationdocuments}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trconfirmationid', 'numerical', 'integerOnly'=>true),
			array('filename', 'length', 'max'=>300),
			array('localname', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('trconfirmationdocumentsid, trconfirmationid, filename, localname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'trconfirmation' => array(self::BELONGS_TO, 'Trconfirmation', 'trconfirmationid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trconfirmationdocumentsid' => 'Trconfirmationdocumentsid',
			'trconfirmationid' => 'Trconfirmationid',
			'filename' => 'Filename',
			'localname' => 'Localname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trconfirmationdocumentsid',$this->trconfirmationdocumentsid);

		$criteria->compare('trconfirmationid',$this->trconfirmationid);

		$criteria->compare('filename',$this->filename,true);

		$criteria->compare('localname',$this->localname,true);

		return new CActiveDataProvider('Trconfirmationdocuments', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Trconfirmationdocuments the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}