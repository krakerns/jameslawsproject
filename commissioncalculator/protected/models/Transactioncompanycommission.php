<?php

/**
 * This is the model class for table "{{transactioncompanycommission}}".
 *
 * The followings are the available columns in table '{{transactioncompanycommission}}':
 * @property integer $transactioncompanycommissionid
 * @property integer $transactionid
 * @property double $totalamtincGST
 * @property double $totalGSTcomponent
 * @property double $totalpayoutshare
 * @property double $companyshare
 * @property double $totalpaytosalesperson
 * @property double $totalpayetransfer
 * @property double $payablepaye
 * @property double $GSTtransfertootheraccnt
 */
class Transactioncompanycommission extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transactioncompanycommission}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionid', 'numerical', 'integerOnly'=>true),
			array('totalamtincGST, totalGSTcomponent, totalpayoutshare, companyshare, totalpaytosalesperson, totalpayetransfer, payablepaye, GSTtransfertootheraccnt', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('transactioncompanycommissionid, transactionid, totalamtincGST, totalGSTcomponent, totalpayoutshare, companyshare, totalpaytosalesperson, totalpayetransfer, payablepaye, GSTtransfertootheraccnt', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaction' => array(self::BELONGS_TO, 'Transaction', 'transactionid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transactioncompanycommissionid' => 'Transactioncompanycommissionid',
			'transactionid' => 'Transactionid',
			'totalamtincGST' => 'Totalamtinc Gst',
			'totalGSTcomponent' => 'Total Gstcomponent',
			'totalpayoutshare' => 'Totalpayoutshare',
			'companyshare' => 'Companyshare',
			'totalpaytosalesperson' => 'Totalpaytosalesperson',
			'totalpayetransfer' => 'Totalpayetransfer',
			'payablepaye' => 'Payablepaye',
			'GSTtransfertootheraccnt' => 'Gsttransfertootheraccnt',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transactioncompanycommissionid',$this->transactioncompanycommissionid);

		$criteria->compare('transactionid',$this->transactionid);

		$criteria->compare('totalamtincGST',$this->totalamtincGST);

		$criteria->compare('totalGSTcomponent',$this->totalGSTcomponent);

		$criteria->compare('totalpayoutshare',$this->totalpayoutshare);

		$criteria->compare('companyshare',$this->companyshare);

		$criteria->compare('totalpaytosalesperson',$this->totalpaytosalesperson);

		$criteria->compare('totalpayetransfer',$this->totalpayetransfer);

		$criteria->compare('payablepaye',$this->payablepaye);

		$criteria->compare('GSTtransfertootheraccnt',$this->GSTtransfertootheraccnt);

		return new CActiveDataProvider('Transactioncompanycommission', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Transactioncompanycommission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}