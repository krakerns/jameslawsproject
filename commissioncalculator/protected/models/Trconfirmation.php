<?php

/**
 * This is the model class for table "{{trconfirmation}}".
 *
 * The followings are the available columns in table '{{trconfirmation}}':
 * @property integer $trconfirmationid
 * @property integer $transactionreportid
 * @property integer $listingpersonagreed
 * @property string $listingpersonagreeddate
 * @property string $listingpersonsigniture
 * @property string $lisingpersonipaddress
 * @property integer $sellingpersonagreed
 * @property string $sellingpersonagreeddate
 * @property string $sellingpersonsigniture
 * @property string $sellingpersonipaddress
 * @property integer $manageragreed
 * @property string $manageragreeddate
 * @property string $managersigniture
 * @property string $manageripaddress
 * @property integer $isagree
 * @property string $fileagreement
 * @property string $fileagreementlocalname
 * @property string $fileTransaction
 * @property string $filetransactionlocalname
 * @property string $fileotherdoc1
 * @property string $fileotherdoc1localname
 * @property string $fileotherdoc2
 * @property string $fileotherdoc2localname
 *
 * The followings are the available model relations:
 * @property Transactionreport $transactionreport
 * @property Trconfirmationdocuments[] $trconfirmationdocuments
 */
class Trconfirmation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{trconfirmation}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionreportid, listingpersonagreed, sellingpersonagreed, manageragreed, isagree', 'numerical', 'integerOnly'=>true),
			array('listingpersonsigniture, lisingpersonipaddress, sellingpersonsigniture, sellingpersonipaddress, managersigniture, manageripaddress', 'length', 'max'=>100),
			array('fileagreement, fileagreementlocalname, fileTransaction, filetransactionlocalname, fileotherdoc1, fileotherdoc1localname, fileotherdoc2, fileotherdoc2localname', 'length', 'max'=>225),
			array('listingpersonagreeddate, sellingpersonagreeddate, manageragreeddate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('trconfirmationid, transactionreportid, listingpersonagreed, listingpersonagreeddate, listingpersonsigniture, lisingpersonipaddress, sellingpersonagreed, sellingpersonagreeddate, sellingpersonsigniture, sellingpersonipaddress, manageragreed, manageragreeddate, managersigniture, manageripaddress, isagree, fileagreement, fileagreementlocalname, fileTransaction, filetransactionlocalname, fileotherdoc1, fileotherdoc1localname, fileotherdoc2, fileotherdoc2localname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
			'trconfirmationdocuments' => array(self::HAS_MANY, 'Trconfirmationdocuments', 'trconfirmationid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trconfirmationid' => 'Trconfirmationid',
			'transactionreportid' => 'Transactionreportid',
			'listingpersonagreed' => 'Listingpersonagreed',
			'listingpersonagreeddate' => 'Listingpersonagreeddate',
			'listingpersonsigniture' => 'Listingpersonsigniture',
			'lisingpersonipaddress' => 'Lisingpersonipaddress',
			'sellingpersonagreed' => 'Sellingpersonagreed',
			'sellingpersonagreeddate' => 'Sellingpersonagreeddate',
			'sellingpersonsigniture' => 'Sellingpersonsigniture',
			'sellingpersonipaddress' => 'Sellingpersonipaddress',
			'manageragreed' => 'Manageragreed',
			'manageragreeddate' => 'Manageragreeddate',
			'managersigniture' => 'Managersigniture',
			'manageripaddress' => 'Manageripaddress',
			'isagree' => 'Isagree',
			'fileagreement' => 'Fileagreement',
			'fileagreementlocalname' => 'Fileagreementlocalname',
			'fileTransaction' => 'File Transaction',
			'filetransactionlocalname' => 'Filetransactionlocalname',
			'fileotherdoc1' => 'Fileotherdoc1',
			'fileotherdoc1localname' => 'Fileotherdoc1localname',
			'fileotherdoc2' => 'Fileotherdoc2',
			'fileotherdoc2localname' => 'Fileotherdoc2localname',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trconfirmationid',$this->trconfirmationid);
		$criteria->compare('transactionreportid',$this->transactionreportid);
		$criteria->compare('listingpersonagreed',$this->listingpersonagreed);
		$criteria->compare('listingpersonagreeddate',$this->listingpersonagreeddate,true);
		$criteria->compare('listingpersonsigniture',$this->listingpersonsigniture,true);
		$criteria->compare('lisingpersonipaddress',$this->lisingpersonipaddress,true);
		$criteria->compare('sellingpersonagreed',$this->sellingpersonagreed);
		$criteria->compare('sellingpersonagreeddate',$this->sellingpersonagreeddate,true);
		$criteria->compare('sellingpersonsigniture',$this->sellingpersonsigniture,true);
		$criteria->compare('sellingpersonipaddress',$this->sellingpersonipaddress,true);
		$criteria->compare('manageragreed',$this->manageragreed);
		$criteria->compare('manageragreeddate',$this->manageragreeddate,true);
		$criteria->compare('managersigniture',$this->managersigniture,true);
		$criteria->compare('manageripaddress',$this->manageripaddress,true);
		$criteria->compare('isagree',$this->isagree);
		$criteria->compare('fileagreement',$this->fileagreement,true);
		$criteria->compare('fileagreementlocalname',$this->fileagreementlocalname,true);
		$criteria->compare('fileTransaction',$this->fileTransaction,true);
		$criteria->compare('filetransactionlocalname',$this->filetransactionlocalname,true);
		$criteria->compare('fileotherdoc1',$this->fileotherdoc1,true);
		$criteria->compare('fileotherdoc1localname',$this->fileotherdoc1localname,true);
		$criteria->compare('fileotherdoc2',$this->fileotherdoc2,true);
		$criteria->compare('fileotherdoc2localname',$this->fileotherdoc2localname,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trconfirmation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
