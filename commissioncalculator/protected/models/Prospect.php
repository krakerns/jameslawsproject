<?php

/**
 * This is the model class for table "{{prospect}}".
 *
 * The followings are the available columns in table '{{prospect}}':
 * @property integer $id
 * @property integer $client_id
 * @property integer $city_id
 * @property string $prospect_type_enum
 * @property string $created_time
 * @property integer $createby_id
 * @property string $modified_time
 * @property integer $modifiedby_id
 */
class Prospect extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{prospect}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, city_id, createby_id, modifiedby_id', 'numerical', 'integerOnly'=>true),
			array('prospect_type_enum', 'length', 'max'=>15),
			array('modified_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, client_id, city_id, prospect_type_enum, created_time, createby_id, modified_time, modifiedby_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comlease_notes' => array(self::HAS_MANY, 'ComleaseNotes', 'prospect_id'),
			'modifiedby' => array(self::BELONGS_TO, 'Users', 'modifiedby_id'),
			'client' => array(self::BELONGS_TO, 'ProspectClient', 'client_id'),
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'createby' => array(self::BELONGS_TO, 'Users', 'createby_id'),
			'prospect_bussales' => array(self::HAS_MANY, 'ProspectBussale', 'prospect_id'),
			'prospect_comleases' => array(self::HAS_MANY, 'ProspectComlease', 'prospect_id'),
			'prospect_comsales' => array(self::HAS_MANY, 'ProspectComsale', 'prospect_id'),
			'prospect_ressales' => array(self::HAS_MANY, 'ProspectRessale', 'prospect_id'),
			'prospect_suburbs' => array(self::HAS_MANY, 'ProspectSuburb', 'prospect_id'),
			'prospect_users' => array(self::HAS_MANY, 'ProspectUser', 'prospect_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'client_id' => 'Client',
			'city_id' => 'City',
			'prospect_type_enum' => 'Prospect Type Enum',
			'created_time' => 'Created Time',
			'createby_id' => 'Createby',
			'modified_time' => 'Modified Time',
			'modifiedby_id' => 'Modifiedby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('client_id',$this->client_id);

		$criteria->compare('city_id',$this->city_id);

		$criteria->compare('prospect_type_enum',$this->prospect_type_enum,true);

		$criteria->compare('created_time',$this->created_time,true);

		$criteria->compare('createby_id',$this->createby_id);

		$criteria->compare('modified_time',$this->modified_time,true);

		$criteria->compare('modifiedby_id',$this->modifiedby_id);

		return new CActiveDataProvider('Prospect', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Prospect the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function listProspecttype()
    {
        return array('commerciallease'    => 'Commercial Lease',
            'residentialsale'    => 'Residential Sale',
            'commercialsale'       => 'Commercial Sale',
            'businesssale'       => 'Business Sale');
    }
}