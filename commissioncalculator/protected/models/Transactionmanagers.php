<?php

/**
 * This is the model class for table "{{transactionmanagers}}".
 *
 * The followings are the available columns in table '{{transactionmanagers}}':
 * @property integer $transactionmanagersid
 * @property integer $transactionid
 * @property integer $managerid
 * @property double $companyshareamt
 * @property double $managershare
 * @property integer $transactionreportid
 * @property string $companyinvoice
 * @property string $invoicedate
 * @property string $paymentrecieveddate
 * @property string $paymenttosalesperson
 * @property integer $isclosed
 *
 * The followings are the available model relations:
 * @property Transaction $transaction
 * @property Managers $manager
 * @property Transactionreport $transactionreport
 */
class Transactionmanagers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transactionmanagers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionid, managerid, transactionreportid, isclosed', 'numerical', 'integerOnly'=>true),
			array('companyshareamt, managershare', 'numerical'),
			array('companyinvoice', 'length', 'max'=>100),
			array('invoicedate, paymentrecieveddate, paymenttosalesperson', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transactionmanagersid, transactionid, managerid, companyshareamt, managershare, transactionreportid, companyinvoice, invoicedate, paymentrecieveddate, paymenttosalesperson, isclosed', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaction' => array(self::BELONGS_TO, 'Transaction', 'transactionid'),
			'manager' => array(self::BELONGS_TO, 'Managers', 'managerid'),
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transactionmanagersid' => 'Transactionmanagersid',
			'transactionid' => 'Transactionid',
			'managerid' => 'Managerid',
			'companyshareamt' => 'Companyshareamt',
			'managershare' => 'Managershare',
			'transactionreportid' => 'Transactionreportid',
			'companyinvoice' => 'Companyinvoice',
			'invoicedate' => 'Invoicedate',
			'paymentrecieveddate' => 'Paymentrecieveddate',
			'paymenttosalesperson' => 'Paymenttosalesperson',
			'isclosed' => 'Isclosed',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transactionmanagersid',$this->transactionmanagersid);
		$criteria->compare('transactionid',$this->transactionid);
		$criteria->compare('managerid',$this->managerid);
		$criteria->compare('companyshareamt',$this->companyshareamt);
		$criteria->compare('managershare',$this->managershare);
		$criteria->compare('transactionreportid',$this->transactionreportid);
		$criteria->compare('companyinvoice',$this->companyinvoice,true);
		$criteria->compare('invoicedate',$this->invoicedate,true);
		$criteria->compare('paymentrecieveddate',$this->paymentrecieveddate,true);
		$criteria->compare('paymenttosalesperson',$this->paymenttosalesperson,true);
		$criteria->compare('isclosed',$this->isclosed);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactionmanagers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
