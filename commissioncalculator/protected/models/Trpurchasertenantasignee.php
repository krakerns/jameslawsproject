<?php

/**
 * This is the model class for table "{{trpurchasertenantasignee}}".
 *
 * The followings are the available columns in table '{{trpurchasertenantasignee}}':
 * @property integer $trpurchasertenantasigneeid
 * @property integer $transactionreportid
 * @property integer $type
 * @property string $legalentity
 * @property string $contactname
 * @property string $contactnumber
 * @property string $email
 * @property string $faxnumber
 * @property string $streetaddress
 * @property string $suburb
 * @property string $city
 * @property integer $countryid
 * @property string $solicitorsfirm
 * @property string $individualactingfirstname
 * @property string $individualactinglastname
 * @property string $individualactingemail
 * @property string $individualactingphonenumber
 * @property string $individualactingfaxnumber
 */
class Trpurchasertenantasignee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{trpurchasertenantasignee}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionreportid, type, countryid', 'numerical', 'integerOnly'=>true),
			array('contactname, email, suburb, solicitorsfirm, individualactinglastname, individualactingemail', 'length', 'max'=>100),
			array('contactnumber, faxnumber, individualactingphonenumber, individualactingfaxnumber', 'length', 'max'=>20),
			array('streetaddress', 'length', 'max'=>255),
			array('city, individualactingfirstname', 'length', 'max'=>36),
			array('legalentity', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('trpurchasertenantasigneeid, transactionreportid, type, legalentity, contactname, contactnumber, email, faxnumber, streetaddress, suburb, city, countryid, solicitorsfirm, individualactingfirstname, individualactinglastname, individualactingemail, individualactingphonenumber, individualactingfaxnumber', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
			'country' => array(self::BELONGS_TO, 'Country', 'countryid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trpurchasertenantasigneeid' => 'Trpurchasertenantasigneeid',
			'transactionreportid' => 'Transactionreportid',
			'type' => 'Type',
			'legalentity' => 'Legalentity',
			'contactname' => 'Contactname',
			'contactnumber' => 'Contactnumber',
			'email' => 'Email',
			'faxnumber' => 'Faxnumber',
			'streetaddress' => 'Streetaddress',
			'suburb' => 'Suburb',
			'city' => 'City',
			'countryid' => 'Countryid',
			'solicitorsfirm' => 'Solicitorsfirm',
			'individualactingfirstname' => 'Individualactingfirstname',
			'individualactinglastname' => 'Individualactinglastname',
			'individualactingemail' => 'Individualactingemail',
			'individualactingphonenumber' => 'Individualactingphonenumber',
			'individualactingfaxnumber' => 'Individualactingfaxnumber',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trpurchasertenantasigneeid',$this->trpurchasertenantasigneeid);

		$criteria->compare('transactionreportid',$this->transactionreportid);

		$criteria->compare('type',$this->type);

		$criteria->compare('legalentity',$this->legalentity,true);

		$criteria->compare('contactname',$this->contactname,true);

		$criteria->compare('contactnumber',$this->contactnumber,true);

		$criteria->compare('email',$this->email,true);

		$criteria->compare('faxnumber',$this->faxnumber,true);

		$criteria->compare('streetaddress',$this->streetaddress,true);

		$criteria->compare('suburb',$this->suburb,true);

		$criteria->compare('city',$this->city,true);

		$criteria->compare('countryid',$this->countryid);

		$criteria->compare('solicitorsfirm',$this->solicitorsfirm,true);

		$criteria->compare('individualactingfirstname',$this->individualactingfirstname,true);

		$criteria->compare('individualactinglastname',$this->individualactinglastname,true);

		$criteria->compare('individualactingemail',$this->individualactingemail,true);

		$criteria->compare('individualactingphonenumber',$this->individualactingphonenumber,true);

		$criteria->compare('individualactingfaxnumber',$this->individualactingfaxnumber,true);

		return new CActiveDataProvider('Trpurchasertenantasignee', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Trpurchasertenantasignee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function listType()
    {

        return array('1'    => 'Purchaser',
            '2'    => 'Tenant',
            '3'       => 'Assignee');
    }
}