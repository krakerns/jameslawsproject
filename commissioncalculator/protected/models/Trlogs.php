<?php

/**
 * This is the model class for table "{{trlogs}}".
 *
 * The followings are the available columns in table '{{trlogs}}':
 * @property integer $id
 * @property integer $transactionreportid
 * @property string $message
 * @property string $createdtime
 * @property integer $createdbyid
 *
 * The followings are the available model relations:
 * @property Transactionreport $transactionreport
 * @property Users $createdby
 */
class Trlogs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{trlogs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionreportid, createdbyid', 'numerical', 'integerOnly'=>true),
			array('message', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, transactionreportid, message, createdtime, createdbyid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
			'createdby' => array(self::BELONGS_TO, 'Users', 'createdbyid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transactionreportid' => 'Transactionreportid',
			'message' => 'Message',
			'createdtime' => 'Createdtime',
			'createdbyid' => 'Createdbyid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('transactionreportid',$this->transactionreportid);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('createdbyid',$this->createdbyid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trlogs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
