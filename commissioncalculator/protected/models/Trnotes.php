<?php

/**
 * This is the model class for table "{{trnotes}}".
 *
 * The followings are the available columns in table '{{trnotes}}':
 * @property integer $trnotesid
 * @property integer $transactionreportid
 * @property string $notes
 * @property integer $ispropertyinspected
 * @property string $ispropertyinspectedtext
 * @property string $statementboundaries
 * @property string $statementconstruction
 * @property integer $ispotentialproblems
 * @property string $ispotentialproblemtext
 * @property integer $ispurchaserwarranty
 * @property integer $isvendorwarranty
 * @property integer $isGSTdiscussion
 * @property string $isGSTdiscussiontext
 * @property integer $isadvicesought
 * @property string $isadvicesoughttext
 * @property integer $ispurchaserboughtproperty
 * @property integer $isviewedandappraised
 * @property integer $isdifferenceopinion
 * @property string $isdifferenceopiniontext
 * @property integer $iscommissiondiscussed
 * @property string $iscommissiondiscusstext
 * @property integer $isvendorincludechattel
 * @property string $isvendorincludechatteltext
 * @property integer $isreferaccountant
 * @property string $isreferaccountanttext
 * @property string $vendordescriberesalevalue
 * @property string $purchaserdiscussedresalevalue
 * @property string $othercomments
 * @property integer $isagree
 * @property string $ispurchaserwarrantytext
 * @property string $isvendorwarrantytext
 *
 * The followings are the available model relations:
 * @property Transactionreport $transactionreport
 */
class Trnotes extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{trnotes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionreportid, ispropertyinspected, ispotentialproblems, ispurchaserwarranty, isvendorwarranty, isGSTdiscussion, isadvicesought, ispurchaserboughtproperty, isviewedandappraised, isdifferenceopinion, iscommissiondiscussed, isvendorincludechattel, isreferaccountant, isagree', 'numerical', 'integerOnly'=>true),
			array('ispropertyinspectedtext', 'length', 'max'=>100),
			array('ispotentialproblemtext, isadvicesoughttext, isdifferenceopiniontext, vendordescriberesalevalue, purchaserdiscussedresalevalue', 'length', 'max'=>255),
			array('isGSTdiscussiontext, iscommissiondiscusstext, isvendorincludechatteltext, isreferaccountanttext, ispurchaserwarrantytext, isvendorwarrantytext', 'length', 'max'=>225),
			array('notes, statementboundaries, statementconstruction, othercomments', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('trnotesid, transactionreportid, notes, ispropertyinspected, ispropertyinspectedtext, statementboundaries, statementconstruction, ispotentialproblems, ispotentialproblemtext, ispurchaserwarranty, isvendorwarranty, isGSTdiscussion, isGSTdiscussiontext, isadvicesought, isadvicesoughttext, ispurchaserboughtproperty, isviewedandappraised, isdifferenceopinion, isdifferenceopiniontext, iscommissiondiscussed, iscommissiondiscusstext, isvendorincludechattel, isvendorincludechatteltext, isreferaccountant, isreferaccountanttext, vendordescriberesalevalue, purchaserdiscussedresalevalue, othercomments, isagree, ispurchaserwarrantytext, isvendorwarrantytext', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trnotesid' => 'Trnotesid',
			'transactionreportid' => 'Transactionreportid',
			'notes' => 'Notes',
			'ispropertyinspected' => 'Ispropertyinspected',
			'ispropertyinspectedtext' => 'Ispropertyinspectedtext',
			'statementboundaries' => 'Statementboundaries',
			'statementconstruction' => 'Statementconstruction',
			'ispotentialproblems' => 'Ispotentialproblems',
			'ispotentialproblemtext' => 'Ispotentialproblemtext',
			'ispurchaserwarranty' => 'Ispurchaserwarranty',
			'isvendorwarranty' => 'Isvendorwarranty',
			'isGSTdiscussion' => 'Is Gstdiscussion',
			'isGSTdiscussiontext' => 'Is Gstdiscussiontext',
			'isadvicesought' => 'Isadvicesought',
			'isadvicesoughttext' => 'Isadvicesoughttext',
			'ispurchaserboughtproperty' => 'Ispurchaserboughtproperty',
			'isviewedandappraised' => 'Isviewedandappraised',
			'isdifferenceopinion' => 'Isdifferenceopinion',
			'isdifferenceopiniontext' => 'Isdifferenceopiniontext',
			'iscommissiondiscussed' => 'Iscommissiondiscussed',
			'iscommissiondiscusstext' => 'Iscommissiondiscusstext',
			'isvendorincludechattel' => 'Isvendorincludechattel',
			'isvendorincludechatteltext' => 'Isvendorincludechatteltext',
			'isreferaccountant' => 'Isreferaccountant',
			'isreferaccountanttext' => 'Isreferaccountanttext',
			'vendordescriberesalevalue' => 'Vendordescriberesalevalue',
			'purchaserdiscussedresalevalue' => 'Purchaserdiscussedresalevalue',
			'othercomments' => 'Othercomments',
			'isagree' => 'Isagree',
			'ispurchaserwarrantytext' => 'Ispurchaserwarrantytext',
			'isvendorwarrantytext' => 'Isvendorwarrantytext',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trnotesid',$this->trnotesid);
		$criteria->compare('transactionreportid',$this->transactionreportid);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('ispropertyinspected',$this->ispropertyinspected);
		$criteria->compare('ispropertyinspectedtext',$this->ispropertyinspectedtext,true);
		$criteria->compare('statementboundaries',$this->statementboundaries,true);
		$criteria->compare('statementconstruction',$this->statementconstruction,true);
		$criteria->compare('ispotentialproblems',$this->ispotentialproblems);
		$criteria->compare('ispotentialproblemtext',$this->ispotentialproblemtext,true);
		$criteria->compare('ispurchaserwarranty',$this->ispurchaserwarranty);
		$criteria->compare('isvendorwarranty',$this->isvendorwarranty);
		$criteria->compare('isGSTdiscussion',$this->isGSTdiscussion);
		$criteria->compare('isGSTdiscussiontext',$this->isGSTdiscussiontext,true);
		$criteria->compare('isadvicesought',$this->isadvicesought);
		$criteria->compare('isadvicesoughttext',$this->isadvicesoughttext,true);
		$criteria->compare('ispurchaserboughtproperty',$this->ispurchaserboughtproperty);
		$criteria->compare('isviewedandappraised',$this->isviewedandappraised);
		$criteria->compare('isdifferenceopinion',$this->isdifferenceopinion);
		$criteria->compare('isdifferenceopiniontext',$this->isdifferenceopiniontext,true);
		$criteria->compare('iscommissiondiscussed',$this->iscommissiondiscussed);
		$criteria->compare('iscommissiondiscusstext',$this->iscommissiondiscusstext,true);
		$criteria->compare('isvendorincludechattel',$this->isvendorincludechattel);
		$criteria->compare('isvendorincludechatteltext',$this->isvendorincludechatteltext,true);
		$criteria->compare('isreferaccountant',$this->isreferaccountant);
		$criteria->compare('isreferaccountanttext',$this->isreferaccountanttext,true);
		$criteria->compare('vendordescriberesalevalue',$this->vendordescriberesalevalue,true);
		$criteria->compare('purchaserdiscussedresalevalue',$this->purchaserdiscussedresalevalue,true);
		$criteria->compare('othercomments',$this->othercomments,true);
		$criteria->compare('isagree',$this->isagree);
		$criteria->compare('ispurchaserwarrantytext',$this->ispurchaserwarrantytext,true);
		$criteria->compare('isvendorwarrantytext',$this->isvendorwarrantytext,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Trnotes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
