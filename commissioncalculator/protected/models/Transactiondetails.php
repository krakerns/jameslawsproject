<?php

/**
 * This is the model class for table "{{transactiondetails}}".
 *
 * The followings are the available columns in table '{{transactiondetails}}':
 * @property integer $transactiondetailsid
 * @property integer $transactionid
 * @property double $netcommisionshare
 * @property double $listingsplitamt
 * @property double $lesslistingreferalamt
 * @property double $listinggrossbroughtin
 * @property double $sellersplitamt
 * @property double $lesssellingreferealamt
 * @property double $sellinggrossbroughtin
 * @property double $listernetbroughtfees
 * @property double $listerfeeshareamt
 * @property double $sellerfeeshareamt
 * @property double $totalfeesbroughtin
 * @property double $individualshare
 * @property double $withholdingtax
 * @property double $individualshareamt
 * @property double $companyshare
 * @property double $checkindividualcompany
 * @property double $sellernetbroughtfees
 *
 * The followings are the available model relations:
 * @property Transaction $transaction
 */
class Transactiondetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transactiondetails}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionid', 'numerical', 'integerOnly'=>true),
			array('netcommisionshare, listingsplitamt, lesslistingreferalamt, listinggrossbroughtin, sellersplitamt, lesssellingreferealamt, sellinggrossbroughtin, listernetbroughtfees, listerfeeshareamt, sellerfeeshareamt, totalfeesbroughtin, individualshare, withholdingtax, individualshareamt, companyshare, checkindividualcompany, sellernetbroughtfees', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transactiondetailsid, transactionid, netcommisionshare, listingsplitamt, lesslistingreferalamt, listinggrossbroughtin, sellersplitamt, lesssellingreferealamt, sellinggrossbroughtin, listernetbroughtfees, listerfeeshareamt, sellerfeeshareamt, totalfeesbroughtin, individualshare, withholdingtax, individualshareamt, companyshare, checkindividualcompany, sellernetbroughtfees', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaction' => array(self::BELONGS_TO, 'Transaction', 'transactionid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transactiondetailsid' => 'Transactiondetailsid',
			'transactionid' => 'Transactionid',
			'netcommisionshare' => 'Netcommisionshare',
			'listingsplitamt' => 'Listingsplitamt',
			'lesslistingreferalamt' => 'Lesslistingreferalamt',
			'listinggrossbroughtin' => 'Listinggrossbroughtin',
			'sellersplitamt' => 'Sellersplitamt',
			'lesssellingreferealamt' => 'Lesssellingreferealamt',
			'sellinggrossbroughtin' => 'Sellinggrossbroughtin',
			'listernetbroughtfees' => 'Listernetbroughtfees',
			'listerfeeshareamt' => 'Listerfeeshareamt',
			'sellerfeeshareamt' => 'Sellerfeeshareamt',
			'totalfeesbroughtin' => 'Totalfeesbroughtin',
			'individualshare' => 'Individualshare',
			'withholdingtax' => 'Withholdingtax',
			'individualshareamt' => 'Individualshareamt',
			'companyshare' => 'Companyshare',
			'checkindividualcompany' => 'Checkindividualcompany',
			'sellernetbroughtfees' => 'Sellernetbroughtfees',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transactiondetailsid',$this->transactiondetailsid);
		$criteria->compare('transactionid',$this->transactionid);
		$criteria->compare('netcommisionshare',$this->netcommisionshare);
		$criteria->compare('listingsplitamt',$this->listingsplitamt);
		$criteria->compare('lesslistingreferalamt',$this->lesslistingreferalamt);
		$criteria->compare('listinggrossbroughtin',$this->listinggrossbroughtin);
		$criteria->compare('sellersplitamt',$this->sellersplitamt);
		$criteria->compare('lesssellingreferealamt',$this->lesssellingreferealamt);
		$criteria->compare('sellinggrossbroughtin',$this->sellinggrossbroughtin);
		$criteria->compare('listernetbroughtfees',$this->listernetbroughtfees);
		$criteria->compare('listerfeeshareamt',$this->listerfeeshareamt);
		$criteria->compare('sellerfeeshareamt',$this->sellerfeeshareamt);
		$criteria->compare('totalfeesbroughtin',$this->totalfeesbroughtin);
		$criteria->compare('individualshare',$this->individualshare);
		$criteria->compare('withholdingtax',$this->withholdingtax);
		$criteria->compare('individualshareamt',$this->individualshareamt);
		$criteria->compare('companyshare',$this->companyshare);
		$criteria->compare('checkindividualcompany',$this->checkindividualcompany);
		$criteria->compare('sellernetbroughtfees',$this->sellernetbroughtfees);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactiondetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
