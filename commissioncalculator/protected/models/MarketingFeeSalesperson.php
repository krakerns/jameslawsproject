<?php

/**
 * This is the model class for table "{{marketing_fee_salesperson}}".
 *
 * The followings are the available columns in table '{{marketing_fee_salesperson}}':
 * @property integer $id
 * @property integer $marketing_fee_id
 * @property integer $userid
 * @property double $amount
 * @property double $gst
 * @property integer $issaved
 *
 * The followings are the available model relations:
 * @property MarketingFee $marketingFee
 * @property Users $user
 */
class MarketingFeeSalesperson extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{marketing_fee_salesperson}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('marketing_fee_id, userid, issaved', 'numerical', 'integerOnly'=>true),
			array('amount, gst', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, marketing_fee_id, userid, amount, gst, issaved', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'marketingFee' => array(self::BELONGS_TO, 'MarketingFee', 'marketing_fee_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'marketing_fee_id' => 'Marketing Fee',
			'userid' => 'Userid',
			'amount' => 'Amount',
			'gst' => 'Gst',
			'issaved' => 'Issaved',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('marketing_fee_id',$this->marketing_fee_id);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('gst',$this->gst);
		$criteria->compare('issaved',$this->issaved);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarketingFeeSalesperson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
