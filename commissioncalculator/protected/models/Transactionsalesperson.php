<?php

/**
 * This is the model class for table "{{transactionsalesperson}}".
 *
 * The followings are the available columns in table '{{transactionsalesperson}}':
 * @property integer $transactionsalespersonid
 * @property integer $transactionid
 * @property double $bonusdeduction
 * @property double $netsharebeforetax
 * @property double $withholdingtax
 * @property double $amtinvoicecompany
 * @property double $plusgst
 * @property double $grandtotal
 * @property double $bonusdeductiongst
 *
 * The followings are the available model relations:
 * @property Transaction $transaction
 */
class Transactionsalesperson extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transactionsalesperson}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactionid', 'numerical', 'integerOnly'=>true),
			array('bonusdeduction, netsharebeforetax, withholdingtax, amtinvoicecompany, plusgst, grandtotal, bonusdeductiongst', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transactionsalespersonid, transactionid, bonusdeduction, netsharebeforetax, withholdingtax, amtinvoicecompany, plusgst, grandtotal, bonusdeductiongst', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transaction' => array(self::BELONGS_TO, 'Transaction', 'transactionid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transactionsalespersonid' => 'Transactionsalespersonid',
			'transactionid' => 'Transactionid',
			'bonusdeduction' => 'Bonusdeduction',
			'netsharebeforetax' => 'Netsharebeforetax',
			'withholdingtax' => 'Withholdingtax',
			'amtinvoicecompany' => 'Amtinvoicecompany',
			'plusgst' => 'Plusgst',
			'grandtotal' => 'Grandtotal',
			'bonusdeductiongst' => 'Bonusdeductiongst',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transactionsalespersonid',$this->transactionsalespersonid);
		$criteria->compare('transactionid',$this->transactionid);
		$criteria->compare('bonusdeduction',$this->bonusdeduction);
		$criteria->compare('netsharebeforetax',$this->netsharebeforetax);
		$criteria->compare('withholdingtax',$this->withholdingtax);
		$criteria->compare('amtinvoicecompany',$this->amtinvoicecompany);
		$criteria->compare('plusgst',$this->plusgst);
		$criteria->compare('grandtotal',$this->grandtotal);
		$criteria->compare('bonusdeductiongst',$this->bonusdeductiongst);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transactionsalesperson the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
