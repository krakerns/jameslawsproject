<?php

/**
 * This is the model class for table "{{transaction}}".
 *
 * The followings are the available columns in table '{{transaction}}':
 * @property integer $transactionid
 * @property string $transactiondate
 * @property integer $userid
 * @property string $property
 * @property double $propertylistingsplit
 * @property double $propertysellingsplit
 * @property string $ptr
 * @property double $totalcommissionrecievedincGST
 * @property double $totalcommissionrecievedexcGST
 * @property double $listingpartshareindividual
 * @property double $sellingpartshareindividual
 * @property double $listingpartreferal
 * @property string $listingreferalpayableto
 * @property double $sellingpartreferal
 * @property string $sellingreferalpayableto
 * @property double $lessdeductionsexcGST
 * @property double $bonusdeductions
 * @property string $createdtime
 * @property string $modifiedtime
 * @property integer $createdby
 * @property integer $modifiedby
 * @property integer $transactionreportid
 * @property integer $isclosed
 * @property string $companyinvoice
 * @property string $invoicedate
 * @property string $paymentrecieveddate
 * @property string $paymenttosalesperson
 * @property string $bonusdeductiondesc
 * @property double $gst
 *
 * The followings are the available model relations:
 * @property Transactionreport $transactionreport
 * @property Users $user
 * @property Users $createdby0
 * @property Users $modifiedby0
 * @property Transactioncompanycommission[] $transactioncompanycommissions
 * @property Transactiondetails[] $transactiondetails
 * @property Transactionmanagers[] $transactionmanagers
 * @property Transactionsalesperson[] $transactionsalespeople
 */
class Transaction extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{transaction}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('transactiondate, userid', 'required'),
			array('userid, createdby, modifiedby, transactionreportid, isclosed', 'numerical', 'integerOnly'=>true),
			array('propertylistingsplit, propertysellingsplit, totalcommissionrecievedincGST, totalcommissionrecievedexcGST, listingpartshareindividual, sellingpartshareindividual, listingpartreferal, sellingpartreferal, lessdeductionsexcGST, bonusdeductions, gst', 'numerical'),
			array('ptr', 'length', 'max'=>36),
			array('listingreferalpayableto, sellingreferalpayableto, companyinvoice', 'length', 'max'=>100),
			array('bonusdeductiondesc', 'length', 'max'=>225),
			array('property, createdtime, invoicedate, paymentrecieveddate, paymenttosalesperson', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('transactionid, transactiondate, userid, property, propertylistingsplit, propertysellingsplit, ptr, totalcommissionrecievedincGST, totalcommissionrecievedexcGST, listingpartshareindividual, sellingpartshareindividual, listingpartreferal, listingreferalpayableto, sellingpartreferal, sellingreferalpayableto, lessdeductionsexcGST, bonusdeductions, createdtime, modifiedtime, createdby, modifiedby, transactionreportid, isclosed, companyinvoice, invoicedate, paymentrecieveddate, paymenttosalesperson, bonusdeductiondesc, gst', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'transactionreport' => array(self::BELONGS_TO, 'Transactionreport', 'transactionreportid'),
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'createdby0' => array(self::BELONGS_TO, 'Users', 'createdby'),
			'modifiedby0' => array(self::BELONGS_TO, 'Users', 'modifiedby'),
			'transactioncompanycommissions' => array(self::HAS_MANY, 'Transactioncompanycommission', 'transactionid'),
			'transactiondetails' => array(self::HAS_MANY, 'Transactiondetails', 'transactionid'),
			'transactionmanagers' => array(self::HAS_MANY, 'Transactionmanagers', 'transactionid'),
			'transactionsalespeople' => array(self::HAS_MANY, 'Transactionsalesperson', 'transactionid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'transactionid' => 'Transactionid',
			'transactiondate' => 'Transactiondate',
			'userid' => 'Userid',
			'property' => 'Property',
			'propertylistingsplit' => 'Propertylistingsplit',
			'propertysellingsplit' => 'Propertysellingsplit',
			'ptr' => 'Ptr',
			'totalcommissionrecievedincGST' => 'Totalcommissionrecievedinc Gst',
			'totalcommissionrecievedexcGST' => 'Totalcommissionrecievedexc Gst',
			'listingpartshareindividual' => 'Listingpartshareindividual',
			'sellingpartshareindividual' => 'Sellingpartshareindividual',
			'listingpartreferal' => 'Listingpartreferal',
			'listingreferalpayableto' => 'Listingreferalpayableto',
			'sellingpartreferal' => 'Sellingpartreferal',
			'sellingreferalpayableto' => 'Sellingreferalpayableto',
			'lessdeductionsexcGST' => 'Lessdeductionsexc Gst',
			'bonusdeductions' => 'Bonusdeductions',
			'createdtime' => 'Createdtime',
			'modifiedtime' => 'Modifiedtime',
			'createdby' => 'Createdby',
			'modifiedby' => 'Modifiedby',
			'transactionreportid' => 'Transactionreportid',
			'isclosed' => 'Isclosed',
			'companyinvoice' => 'Companyinvoice',
			'invoicedate' => 'Invoicedate',
			'paymentrecieveddate' => 'Paymentrecieveddate',
			'paymenttosalesperson' => 'Paymenttosalesperson',
			'bonusdeductiondesc' => 'Bonusdeductiondesc',
			'gst' => 'Gst',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('transactionid',$this->transactionid);
		$criteria->compare('transactiondate',$this->transactiondate,true);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('property',$this->property,true);
		$criteria->compare('propertylistingsplit',$this->propertylistingsplit);
		$criteria->compare('propertysellingsplit',$this->propertysellingsplit);
		$criteria->compare('ptr',$this->ptr,true);
		$criteria->compare('totalcommissionrecievedincGST',$this->totalcommissionrecievedincGST);
		$criteria->compare('totalcommissionrecievedexcGST',$this->totalcommissionrecievedexcGST);
		$criteria->compare('listingpartshareindividual',$this->listingpartshareindividual);
		$criteria->compare('sellingpartshareindividual',$this->sellingpartshareindividual);
		$criteria->compare('listingpartreferal',$this->listingpartreferal);
		$criteria->compare('listingreferalpayableto',$this->listingreferalpayableto,true);
		$criteria->compare('sellingpartreferal',$this->sellingpartreferal);
		$criteria->compare('sellingreferalpayableto',$this->sellingreferalpayableto,true);
		$criteria->compare('lessdeductionsexcGST',$this->lessdeductionsexcGST);
		$criteria->compare('bonusdeductions',$this->bonusdeductions);
		$criteria->compare('createdtime',$this->createdtime,true);
		$criteria->compare('modifiedtime',$this->modifiedtime,true);
		$criteria->compare('createdby',$this->createdby);
		$criteria->compare('modifiedby',$this->modifiedby);
		$criteria->compare('transactionreportid',$this->transactionreportid);
		$criteria->compare('isclosed',$this->isclosed);
		$criteria->compare('companyinvoice',$this->companyinvoice,true);
		$criteria->compare('invoicedate',$this->invoicedate,true);
		$criteria->compare('paymentrecieveddate',$this->paymentrecieveddate,true);
		$criteria->compare('paymenttosalesperson',$this->paymenttosalesperson,true);
		$criteria->compare('bonusdeductiondesc',$this->bonusdeductiondesc,true);
		$criteria->compare('gst',$this->gst);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
