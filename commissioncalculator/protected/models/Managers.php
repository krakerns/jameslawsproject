<?php

/**
 * This is the model class for table "{{managers}}".
 *
 * The followings are the available columns in table '{{managers}}':
 * @property integer $managerid
 * @property integer $userid
 * @property integer $usertomanage
 * @property double $shareonuser
 */
class Managers extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{managers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'required'),
			array('userid, usertomanage', 'numerical', 'integerOnly'=>true),
			array('shareonuser', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('managerid, userid, usertomanage, shareonuser', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'usertomanage0' => array(self::BELONGS_TO, 'Users', 'usertomanage'),
			'transactionmanagers' => array(self::HAS_MANY, 'Transactionmanagers', 'managerid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'managerid' => 'Managerid',
			'userid' => 'Userid',
			'usertomanage' => 'Usertomanage',
			'shareonuser' => 'Shareonuser',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('managerid',$this->managerid);

		$criteria->compare('userid',$this->userid);

		$criteria->compare('usertomanage',$this->usertomanage);

		$criteria->compare('shareonuser',$this->shareonuser);

		return new CActiveDataProvider('Managers', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Managers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}