<?php

/**
 * This is the model class for table "{{useraccounts}}".
 *
 * The followings are the available columns in table '{{useraccounts}}':
 * @property integer $useraccountsid
 * @property integer $userid
 * @property string $accounttype
 * @property string $username
 * @property string $password
 */
class Useraccounts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{useraccounts}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'numerical', 'integerOnly'=>true),
			array('accounttype', 'length', 'max'=>9),
			array('username, password', 'length', 'max'=>36),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('useraccountsid, userid, accounttype, username, password', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'useraccountsid' => 'Useraccountsid',
			'userid' => 'Userid',
			'accounttype' => 'Accounttype',
			'username' => 'Username',
			'password' => 'Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('useraccountsid',$this->useraccountsid);

		$criteria->compare('userid',$this->userid);

		$criteria->compare('accounttype',$this->accounttype,true);

		$criteria->compare('username',$this->username,true);

		$criteria->compare('password',$this->password,true);

		return new CActiveDataProvider('Useraccounts', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Useraccounts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function listType()
    {

        return array('superuser'    => 'Super User',
            'accounts'    => 'Accounts',
            'user'       => 'User');
    }

    public function validatePassword($password)
    {
        return $password===$this->password;
    }
}