<?php

/**
 * This is the model class for table "{{prospect_bussale}}".
 *
 * The followings are the available columns in table '{{prospect_bussale}}':
 * @property integer $id
 * @property integer $prospect_id
 * @property string $business_type
 * @property integer $price_from
 * @property integer $price_to
 * @property string $enquiry_on
 * @property string $free_time_to
 * @property string $free_time_from
 * @property string $created_time
 * @property integer $createdby_id
 * @property string $modified_time
 * @property integer $modifiedby_id
 */
class ProspectBussale extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{prospect_bussale}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prospect_id, price_from, price_to, createdby_id, modifiedby_id', 'numerical', 'integerOnly'=>true),
			array('business_type', 'length', 'max'=>225),
			array('enquiry_on, free_time_to, free_time_from, modified_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, prospect_id, business_type, price_from, price_to, enquiry_on, free_time_to, free_time_from, created_time, createdby_id, modified_time, modifiedby_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'modifiedby' => array(self::BELONGS_TO, 'Users', 'modifiedby_id'),
			'prospect' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
			'createdby' => array(self::BELONGS_TO, 'Users', 'createdby_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'prospect_id' => 'Prospect',
			'business_type' => 'Business Type',
			'price_from' => 'Price From',
			'price_to' => 'Price To',
			'enquiry_on' => 'Enquiry On',
			'free_time_to' => 'Free Time To',
			'free_time_from' => 'Free Time From',
			'created_time' => 'Created Time',
			'createdby_id' => 'Createdby',
			'modified_time' => 'Modified Time',
			'modifiedby_id' => 'Modifiedby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('prospect_id',$this->prospect_id);

		$criteria->compare('business_type',$this->business_type,true);

		$criteria->compare('price_from',$this->price_from);

		$criteria->compare('price_to',$this->price_to);

		$criteria->compare('enquiry_on',$this->enquiry_on,true);

		$criteria->compare('free_time_to',$this->free_time_to,true);

		$criteria->compare('free_time_from',$this->free_time_from,true);

		$criteria->compare('created_time',$this->created_time,true);

		$criteria->compare('createdby_id',$this->createdby_id);

		$criteria->compare('modified_time',$this->modified_time,true);

		$criteria->compare('modifiedby_id',$this->modifiedby_id);

		return new CActiveDataProvider('ProspectBussale', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return ProspectBussale the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}