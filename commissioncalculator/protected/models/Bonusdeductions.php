<?php

/**
 * This is the model class for table "{{bonusdeductions}}".
 *
 * The followings are the available columns in table '{{bonusdeductions}}':
 * @property integer $bonusdeductionid
 * @property integer $userid
 * @property double $amount
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Users $user
 * @property Bonusdeductionsline[] $bonusdeductionslines
 */
class Bonusdeductions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{bonusdeductions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('description', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bonusdeductionid, userid, amount, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'Users', 'userid'),
			'bonusdeductionslines' => array(self::HAS_MANY, 'Bonusdeductionsline', 'bonusdeductionsid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bonusdeductionid' => 'Bonusdeductionid',
			'userid' => 'Userid',
			'amount' => 'Amount',
			'description' => 'Description',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bonusdeductionid',$this->bonusdeductionid);
		$criteria->compare('userid',$this->userid);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bonusdeductions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
