<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Jameslaw.com',
	'defaultController' => 'site',
    'layout' => 'bootstraplayout',

	// preloading 'log' component
    'preload'=>array('log', 'session'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.extensions.phpmailer.*',
        'application.extensions.pdf.*',
        'application.extensions.file.*',

	),

	'behaviors' => array(
		'runEnd' => array(
		'class'  => 'application.components.WebApplicationEndBehavior',
	 	),
	),


	'modules'=>array(
		// uncomment the following to enable the Gii tool
		// /*
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'sows',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		// */
	),

	// application components
	'components'=>array(
//  		'user'=>array(
//  			// enable cookie-based authentication
//  			'allowAutoLogin'=>true,
//  			'loginUrl'=>'site/login',
//  			'class' => "UserIdentity",
//  		),
	'user'=>array(
	    // enable cookie-based authentication
	    'allowAutoLogin'=>false,
	    'loginUrl' => array('/site/login'),
	     'class'=>'WebUser',
	),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
//        'showScriptName' => false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

			),
		),

		// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=jameslaw_db',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
			'enableParamLogging' => true,
		),

        /*
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=jameslaw_commission',
            'emulatePrepare' => true,
            'username' => 'jameslaw_commusr',
            'password' => 'letmein1111',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
            'enableParamLogging' => true,
        ),
        */

		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'info, vardump',
                    'logFile'=>'application_info',
                    'maxLogFiles'=>10
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace',
                    'logFile'=>'application_info',
                    'maxLogFiles'=>10,
                    'categories'=>'system.db.CDbCommand'
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace, error, warning',
                ),
//                 array(
//                     'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
//                     'ipFilters'=>array('127.0.0.1','192.168.1.1'),
//                 ),
                /* array(
                 'class'=>'CFileLogRoute',
                    'levels'=>'trace, info, error, warning, vardump', //info, error, warning, vardump',
                ),
        array(
            'class'=>'CEmailLogRoute',
            'levels'=>'error, warning',
            'emails'=>'me@me.com',
        ),*/
            ),
        ),

// /////////////////////////////////////////////////////////////////////////////




	),

	// application-level parameters that can be accessed
	// ...using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
	'params' => require(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'parameters.php' ),

);
