/**
 * Created by JV on 7/5/14.
 */




$(document).ready(function(){

    $("input[name*='optionsListertype']").on("change",function(){

        if($(this).val() == 'option1'){

            $('#dvlistercolleagueopt').show();
            $('#dvlisterotheropt').hide();
            $('#hdlistercolleague').val('colleague');
        }else{
            $('#dvlisterotheropt').show();
            $('#dvlistercolleagueopt').hide();
            $('#hdlistercolleague').val('other');
        }

    });

    $("input[name*='optionSellertype']").on("change",function(){

        if($(this).val() == 'option1'){

            $('#dvsellercolleagueopt').show();
            $('#dvsellerotheropt').hide();
            $('#hdsellercolleague').val('colleague');
        }else{
            $('#dvsellerotheropt').show();
            $('#dvsellercolleagueopt').hide();
            $('#hdsellercolleague').val('other');
        }

    });

    $("input[name*='optionsPowerteam']").on("change",function(){
        if($(this).val() == 'yes'){
            $('#hdpowerteam').val('1');
            $('#txtListingReferralPerc').val('20.00');

            calculateListingsplit();
            calculateSellingsplit();
        }else{
            $('#hdpowerteam').val('0');
        }
    });

    $("input[name*='p1']").on("change",function(){
        if($(this).val() == 'yes'){
            $('#hdpowerteamseller').val('1');
            $('#txtSellingReferralPerc').val('20.00');

            calculateListingsplit();
            calculateSellingsplit();
        }else{
            $('#hdpowerteamseller').val('0');
        }

    });

    $("input[name*='optionIspropertyinspected']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#ispropertyinspected').val('1');
            $('#dvIspropertyinspected').hide();
        }else{

            $('#ispropertyinspected').val('0');
            $('#dvIspropertyinspected').show();
        }
    });

    $("input[name*='optionIspotentialproblems']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#ispotentialproblems').val('1');
            $('#dvIspotentialproblems').show();
        }else{

            $('#ispotentialproblems').val('0');
            $('#dvIspotentialproblems').hide();
        }
    });

    $("input[name*='optionIsPurchaserWarranty']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#ispurchasewarranty').val('1');
            $('#dvIsPurchaserWarranty').show();
        }else{

            $('#ispurchasewarranty').val('0');
            $('#dvIsPurchaserWarranty').hide();
        }
    });

    $("input[name*='optionIsVendorWarranty']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isvendorwarranty').val('1');
            $('#dvIsVendorWarranty').show();
        }else{

            $('#isvendorwarranty').val('0');
            $('#dvIsVendorWarranty').hide();
        }
    });

    $("input[name*='optionIsGSTDiscussion']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isgstdiscussion').val('1');
            $('#dvIsGSTDiscussion').show();
        }else{

            $('#isgstdiscussion').val('0');
            $('#dvIsGSTDiscussion').hide();
        }
    });



    $("input[name*='optionIsAdviceSought']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isadvicesought').val('1');
            $('#dvIsAdviceSought').show();
        }else{

            $('#isadvicesought').val('0');
            $('#dvIsAdviceSought').hide();
        }
    });

    $("input[name*='optionIsPurchaserBroughtProperty']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#ispurchaserbroughtproperty').val('1');

        }else{

            $('#ispurchaserbroughtproperty').val('0');

        }
    });

    $("input[name*='optionIsViewedAndAppraised']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isviewedandappraised').val('1');

        }else{

            $('#isviewedandappraised').val('0');

        }
    });

    $("input[name*='optionIsDifferenceInOpinion']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isdifferenceopinon').val('1');
            $('#dvIsDifferenceInOpinion').show();
        }else{

            $('#isdifferenceopinon').val('0');
            $('#dvIsDifferenceInOpinion').hide();
        }
    });

    $("input[name*='optionIsCommissionDiscussed']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#iscommissiondiscussed').val('1');
            $('#dvIsCommissionDiscussed').show();
        }else{

            $('#iscommissiondiscussed').val('0');
            $('#dvIsCommissionDiscussed').hide();
        }
    });

    $("input[name*='optionIsVendorIncludeChattel']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isvendorincludechattel').val('1');
            $('#dvIsVendorIncludeChattel').show();
        }else{

            $('#isvendorincludechattel').val('0');
            $('#dvIsVendorIncludeChattel').hide();
        }
    });

    $("input[name*='optionIsReferAccountant']").on("change",function(){

        if($(this).val() == 'yes'){

            $('#isreferaccountant').val('1');
            $('#dvIsReferAccountant').show();
        }else{

            $('#isreferaccountant').val('0');
            $('#dvIsReferAccountant').hide();
        }
    });

    $('#btnConfirm').on('click',function(){
        if($('#selListerPerson1').val() == '' && $('#selSellerPerson1').val()==''){
            alert('You should select either a Listing Person or a Selling Person');
            return;
        }

        var valid = $('#fmrValidate').parsley('validate');
        if(valid) {
            fillsummary();
            $('#showConfirm').click();
        }
    });

    $('#btnSaveDraft').on('click',function(){
        $('#hdisdraft').val('1');
        $('#fmrValidate').submit();
    });

    $('#btnFinalConfirm').on('click',function(){
        if(!checkIsAgree()){
            alert('You must type "AGREE" to proceed.');
        }else{
            if(confirm("Details on this transaction cannot be changed after pressing the CONFIRM button.Are you sure to confirm?")){
                $(this).prop('disabled', true);
                $('#fmrValidate').submit();
            }
        }
    });

    $('#btnUpdate').on('click',function(){
        var valid = $('#fmrValidate').parsley('validate');
        if(valid) {
            $('#fmrValidate').submit();
        }
    });

    $('#btnUpdateconfirm').on('click',function(){

        var answer = confirm("Do you want to confirm transaction ?");

        if(answer){
            var valid = $('#fmrValidate').parsley('validate');
            if(valid) {
                $('#hdisconfirm').val('1');
                $('#fmrValidate').submit();
            }
        }

    });

    $("#txtCommissionExcGST").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) * 1.15

        $('#txtCommissionIncGST').val(m.toFixed(2));
        calculateListingsplit();
        calculateSellingsplit();
    });

    $("#txtCommissionIncGST").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) / 1.15

        $('#txtCommissionExcGST').val(m.toFixed(2));
        calculateListingsplit();
        calculateSellingsplit();
    });

    $("#txtDepositExcGST").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) * 1.15

        $('#txtDepositIncGST').val(m.toFixed(2));

    });

    $("#txtDepositIncGST").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) / 1.15

        $('#txtDepositExcGST').val(m.toFixed(2));

    });

    $('#txtListerShare').on('focusout',function(){
        var val2 = 100 - parseFloat($(this).val());

        $('#txtSellerShare').val(val2);
        calculateListingsplit();
        calculateSellingsplit();

    });

    $('#txtSellerShare').on('focusout',function(){

        var val2 = 100 - parseFloat($(this).val());
        $('#txtListerShare').val(val2);

        calculateListingsplit();
        calculateSellingsplit();
    });

    $('#txtListingReferralPerc').on('focusout',function(){

        calculateListingsplit();
        calculateSellingsplit();
    });

    $('#txtSellingReferralPerc').on('focusout',function(){

        calculateListingsplit();
        calculateSellingsplit();
    });

    $('#btnaddanotherlister').on('click',function(){
        transaction.addlister();
    }).trigger('change');

    $('#btnaddanotherseller').on('click',function(){
        transaction.addseller();
    }).trigger('change');



});

function calculateListingsplit(){

    var total = (parseFloat($('#txtListerShare').val())/100) * parseFloat($('#txtCommissionExcGST').val());
    $('#txtListingSplitAmt').val(total.toFixed(2));

    total = (parseFloat($('#txtListingReferralPerc').val())/100) * total;
    $('#txtListingReferralAmt').val(total.toFixed(2));

    total = parseFloat($('#txtListingSplitAmt').val()) -total;
    $('#txtListingGrossBroughtIn').val(total.toFixed(2));

}

function calculateSellingsplit(){

    var total = (parseFloat($('#txtSellerShare').val())/100) * parseFloat($('#txtCommissionExcGST').val());
    $('#txtSellingSplitAmt').val(total.toFixed(2));

    total = (parseFloat($('#txtSellingReferralPerc').val())/100) * total;
    $('#txtSellingReferralAmt').val(total.toFixed(2));

    total = parseFloat($('#txtListingSplitAmt').val()) -total;
    $('#txtSellingGrossBroughtIn').val(total.toFixed(2));

}

function fillsummary(){

    $('#sumtrtype').html($('#selTranstype option:selected').text());
    $('#sumtradress').html($('#txtAddressProperty').val());
    $('#sumtrlistshare').html($('#txtListerShare').val()+' %');
    $('#sumtrlistperson').html($('#selListerPerson option:selected').text());
    $('#sumtrptr').html($('#txtPtr').val());
    $('#sumtrsaleprice').html($('#txtSalesPrice').val());
    $('#sumtrsalesperson').html($('#selSellerPerson option:selected').text());
    $('#sumtrsellershare').html($('#txtSellerShare').val());
    $('#sumtraggrdate').html($('#txtDateAgreement').val());
}

function checkIsAgree(){

    if($('#txtConfirmAgree').val() == "AGREE"){
        return true;
    }

    return false;
}

function optlisterfunction(lsternumber,type){
    if(type==0){
        $('#dvlistercolleagueopt'+lsternumber).hide();
        $('#dvlisterotheropt'+lsternumber).show();
        $('#hdlistertype'+lsternumber).val('other');
    }else{
        $('#dvlistercolleagueopt'+lsternumber).show();
        $('#dvlisterotheropt'+lsternumber).hide();
        $('#hdlistertype'+lsternumber).val('colleague');
    }


}

function optsellerfunction(sellernumber,type){
    if(type==0){
        $('#dvsellercolleagueopt'+sellernumber).hide();
        $('#dvsellerotheropt'+sellernumber).show();
        $('#hdsellertype'+sellernumber).val('other');
    }else{
        $('#dvsellercolleagueopt'+sellernumber).show();
        $('#dvsellerotheropt'+sellernumber).hide();
        $('#hdsellertype'+sellernumber).val('colleague');
    }
}


