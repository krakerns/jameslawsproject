/**
 * Created by JV on 7/8/14.
 */

$(document).ready(function(){
    $('#buttonCalculate').click(function(){

        calculate();

    });

    $('#txtGST').focusout(function(){
        calculate();
    });

    $('#selPerson').change(function(){
        jQuery.ajax({
            type: "POST",
            url: '<?php echo CController::createURL("transaction/GetBonus"); ?>',
            dataType: "json",
            data:'userid='+$(this).val(),
            success: function(data){
                $('#txtSalesBonusDeduction').val(data.amount);
                $('#spanBonusDeduction').html(data.description);

                $('#txtBonusDeduction').val(data.amount);
                $('#spanBonusDeduction1').html(data.description);

                $('#txtIndividualSharePercentage').val(data.share);
                $('#txtIndWithHoldingTax').val(data.withholdingtax);
            }
        });
    });

    $("#txtCTRMainInc").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) / 1.15

        $('#txtCTRMainExc').val(m.toFixed(2));

    });

    $("#txtCTRMainExc").focusout(function(){
        var m = 0;

        m = parseFloat($(this).val()) * 1.15

        $('#txtCTRMainInc').val(m.toFixed(2));

    });

    $('#btnClose').on('click',function(){

        var answer = confirm("Do you want to close transaction ?");

        if(answer){
            $('#hdisclosed').val('1');
            $('#frmTransaction').submit();
        }
    });
    /*
    $('#txtListerPercentage').on('focusout',function(){
        var val2 = 100 - parseFloat($(this).val());

        $('#txtSellerPercentage').val(val2);
    });

    $('#txtSellerPercentage').on('focusout',function(){

        var val2 = 100 - parseFloat($(this).val());
        $('#txtListerPercentage').val(val2);
    });
    */

    //$('.dollartext').before('<span>$</span>')


});



function calculateDianne(){
    var m=0;

    m= parseFloat($('#txtCTRMainExc').val()) - parseFloat($('#txtLessOtherReferral').val());
    $('#txtNetCommToShare').val(m.toFixed(2));

    m= parseFloat($('#txtNetCommToShare').val()) * .5;
    $('#txtListerSplitAmt').val(m.toFixed(2));

    m= parseFloat($('#txtListerSplitAmt').val()) * (parseFloat($('#txtListingPartReferral').val())/100);
    $('#txtLessListingRefAmt').val(m.toFixed(2));

    m= parseFloat($('#txtListerSplitAmt').val()) - parseFloat($('#txtLessListingRefAmt').val());
    $('#txtListingGrossBrought').val(m.toFixed(2));

    m= parseFloat($('#txtNetCommToShare').val()) * .5;
    $('#txtSellingSplitAmt').val(m.toFixed(2));

    m= parseFloat($('#txtSellingSplitAmt').val()) * (parseFloat($('#txtSellingPartReferral').val())/100);
    $('#txtLessSellingRefAmt').val(m.toFixed(2));

    m= parseFloat($('#txtSellingSplitAmt').val()) - parseFloat($('#txtLessSellingRefAmt').val());
    $('#txtSellingGrossBrought').val(m.toFixed(2));

    m= parseFloat($('#txtNetCommToShare').val()) * .5;
    $('#txtListerNetBoughtFees').val(m.toFixed(2));

    m= parseFloat($('#txtListingGrossBrought').val()) * (parseFloat($('#txtListerPercentage').val())/100);
    $('#txtListerFeeShareAmt').val(m.toFixed(2));

    m= parseFloat($('#txtSellingGrossBrought').val()) * (parseFloat($('#txtSellerPercentage').val())/100);
    $('#txtSellerFeeShareAmt').val(m.toFixed(2));

    m= parseFloat($('#txtListerFeeShareAmt').val()) + parseFloat($('#txtSellerFeeShareAmt').val());
    $('#txtIndTotalFeesBought').val(m.toFixed(2));

    var tr='';
    $("#tBodyDetails").html('');

    /*
     if($('#selPerson').val()=="1"){
     $('#txtIndividualSharePercentage').val('60');
     $('#txtIndWithHoldingTax').val('20');

     }else{

     $('#txtIndividualSharePercentage').val('50');
     $('#txtIndWithHoldingTax').val('20');

     }
     */

    m= parseFloat($('#txtIndTotalFeesBought').val()) * (parseFloat($('#txtIndividualSharePercentage').val())/100);
    $('#txtIndividualShareAmount').val(m.toFixed(2));

    m= parseFloat($('#txtIndTotalFeesBought').val()) - parseFloat($('#txtIndividualShareAmount').val());
    $('#txtCompanySharePercentage').val(m.toFixed(2));

    m= parseFloat($('#txtIndividualShareAmount').val()) + parseFloat($('#txtCompanySharePercentage').val());
    $('#txtCheckIndividualComp').val(m.toFixed(2));
    $('#panelDetails').show();

    $('#txtSalesBonusDeduction').val('-' + $('#txtBonusDeduction').val());

    m= parseFloat($('#txtIndividualShareAmount').val()) - parseFloat($('#txtBonusDeduction').val());
    $('#txtSalesNetShare').val(m.toFixed(2));

    m= parseFloat($('#txtIndividualShareAmount').val()) * (parseFloat($('#txtIndWithHoldingTax').val())/100);
    $('#txtSalesWithHoldingTax').val(m.toFixed(2));

    m= parseFloat($('#txtSalesNetShare').val()) - parseFloat($('#txtSalesWithHoldingTax').val());
    $('#txtSalesAmtToInvoice').val(m.toFixed(2));

    m= parseFloat($('#txtSalesNetShare').val()) * .15;
    $('#txtSalesPlusGST').val(m.toFixed(2));

    m= parseFloat($('#txtSalesPlusGST').val()) + parseFloat($('#txtSalesAmtToInvoice').val());
    $('#txtSalesGrandTotal').val(m.toFixed(2));
    $('#panelSalesPerson').show();

    /*
     var tr='';
     $("#tBodyDetails").html('');

     if($('#selPerson').val()=="1"){
     m= parseFloat($('#txtCompanySharePercentage').val()) * (50/100);
     tr='<tr><td>Rosh Daji</td><td>50%</td><td>'+$('#txtCompanySharePercentage').val()+'</td><td>'+ m.toFixed(2)+'</td></tr>';
     $('#managerPercentage').val('50');
     }else{
     m= parseFloat($('#txtCompanySharePercentage').val()) * (20/100);
     tr='<tr><td>Michelle Kennedy</td><td>20%</td><td>'+$('#txtCompanySharePercentage').val()+'</td><td>'+ m.toFixed(2)+'</td></tr>';
     $('#managerPercentage').val('20');
     }

     $('#tBodyDetails').append(tr);

     $('#panelManagers').show();
     */

    m= parseFloat($('#txtCheckIndividualComp').val()) * 1.15;
    $('#txtCommTotalAmtGST').val(m.toFixed(2));

    m= parseFloat($('#txtCommTotalAmtGST').val()) * (3/23);
    $('#txtCommTotalGSTComponent').val(m.toFixed(2));

    m= ((parseFloat($('#txtIndividualSharePercentage').val())/100) + (1-(parseFloat($('#txtIndividualSharePercentage').val())/100)) * (parseFloat($('#managerPercentage').val())/100)) * 100;
    $('#txtCommTotalPayoutShare').val(m);

    m= 100 - parseFloat($('#txtCommTotalPayoutShare').val());
    $('#txtCommCompShare').val(m);

    $('#txtCommTotalPayableToPerson').val($('#txtSalesGrandTotal').val());
    $('#txtCommTotalPAYETransfer').val($('#txtSalesWithHoldingTax').val());

    m= parseFloat($('#txtCommTotalPayableToPerson').val()) + parseFloat($('#txtCommTotalPAYETransfer').val());
    $('#txtCommPayablePAYE').val(m.toFixed(2));

    m= parseFloat($('#txtCommTotalGSTComponent').val()) - parseFloat($('#txtSalesPlusGST').val());
    $('#txtCommGSTTransfer').val(m.toFixed(2));

    $('#panelCommission').show();
}
