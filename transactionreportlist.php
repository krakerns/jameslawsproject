<?php
$spreadsheet_url = Yii::app()->createUrl('transaction/spreadsheet');
$edit_url = Yii::app()->createUrl('transaction/edittransaction');
$delete_url = Yii::app()->createUrl('users/delete');

//echo Yii::app()->user->role;die();
?>


<script type="text/javascript">

    var tbl_users;

    $(document).ready(function(){

        tbl_users=$('#tbl_users').dataTable({
            "bProcessing": true,
            "sDom": "<'row'<'col-sm-6'l><'col-sm-6'f>r>t<'row'<'col-sm-6'i><'col-sm-6'p>>",
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 5, "desc" ]]
        });

        $('#btnAction').on('click',function(){
            if($('#selectAction').val()=="1"){
                confirmTransaction();
            }else {
                deleteTransaction();
            }
        });

    });

    function confirmTransaction(){
        var answer = confirm("Do you want to confirm selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingissues:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/confirmadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteTransaction(){
        var answer = confirm("Do you want to delete selected transactions ?");

        if (answer){
            var file = [];
            $.each($(".lingissues:checked"), function(){
                file.push($(this).val());
            });
            $.post("<?php echo CController::createURL("transaction/deleteadmin"); ?>",
                {
                    files : file
                },
                function(data){
                    window.location = "<?php echo CController::createURL('transaction/transactionlist'); ?>";
                });
        }
    }

    function deleteItem(userid){
        var result = window.confirm("Do you really want to Delete the user record?");
        if (result == false) {
            e.preventDefault();
            return false;
        }
        else {

            $.ajax({
                type: 'POST',
                data: { "userid": userid },
                dataType: 'json',
                url: '<?php echo CController::createURL("users/delete"); ?>',
                success: function (data) {

                    if (data.result == 'success') {
                        // Refresh the table
                        // var oTable = $('#tModuleListing').dataTable();
                        //table.draw();
                        window.location = '<?php echo CController::createURL("users/index"); ?>';
                    }
                    else {
                        alert('Failed to Delete item - ' + data.message);
                    }

                },
                error: function () {
                    alert('Failed to Delete item - unexpected error');
                    // todo: add proper error message
                }
            });
        }
    }
</script>

<section class="panel">
    <header class="panel-heading">
        <h4>Transaction Reports List &nbsp;&nbsp;<a id="add" name="add" href="<?php echo Yii::app()->createUrl('transaction/addtransactionreport'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a></h4>
    </header>
    <div class="table-responsive">
        <table id="tbl_users" class="table table-striped m-b-none">
            <thead>
            <tr>
                <th width="3%"><input style="width:50px !important;" type="checkbox"></th>
                <th width="10%">TR ID</th>
                <th width="15%">Listing Persons</th>
                <th width="15%">Selling Persons</th>
                <th width="27%">Property</th>
                <th width="10%">Date</th>
                <th width="10%">Status</th>
                <th width="5%">&nbsp;</th>
                <th width="5%">&nbsp;</th>
            </tr>
            </thead>
            <?php
            if(count($listTR>0)){
                echo '<tbody>';
                foreach($listTR as $tr){

                    $status = ($tr->status=='1')?'Confirmed':'Pending';
                    echo '<tr>';
                    echo '<td><input class="lingissues" type="checkbox" value="'.$tr->transactionreportid.'" style="width:50px !important;" name="post[]"></td>';
                    echo '<td>TR-'.leading_zeros($tr->transactionreportid,8).'</td>';
                    $name ='';
                    if(count($tr->trlistingpersons)>0){
                        foreach($tr->trlistingpersons as $person){
                            if($person->type=='colleague'){
                                $name .= $person->user->fullname .',';
                            }else{
                                $name.= $person->otherperson;
                            }
                        }
                    }
                    echo '<td>'.$name.'</td>';
                    $name ='';
                    if(count($tr->trsellingpersons)>0){
                        $name ='';
                        foreach($tr->trsellingpersons as $person){
                            if($person->type=='colleague'){
                                $name .= $person->user->fullname .',';
                            }else{
                                $name.= $person->otherperson;
                            }
                        }
                    }

                    echo '<td>'.$name.'</td>';
                    echo '<td>'.$tr->address.'</td>';
                    echo '<td>'. date('m/d/Y',strtotime($tr->createdtime)) .'</td>';
                    echo '<td>'. $status .'</td>';

                    if((Yii::app()->user->role == 'accounts' || Yii::app()->user->role == 'superuser')){
                        echo '<td><a class="editrow btn btn-success btn-xs" href="'.$spreadsheet_url.'/transactionreportid/'.$tr->transactionreportid.'">Calculate</a> </td>';
                    }else{
                        echo '<td></td>';
                    }

                    echo '<td align="center"> <a class="editrow btn btn-info btn-xs" href="'.$edit_url.'/transactionreportid/'.$tr->transactionreportid.'">View</a> &nbsp;&nbsp;</td>';
                    echo '</tr>';
                }
                echo '</tbody>';
            }
            ?>


        </table>
    </div>
    <footer class="panel-footer">
        <div class="row">
            <div class="col-sm-4 hidden-xs">
                <select id="selectAction" class="input-sm form-control input-s inline">
                    <option value="2">Delete selected</option>
                </select>
                <button id="btnAction" class="btn btn-sm btn-white">Apply</button>
            </div>
        </div>
    </footer>
</section>